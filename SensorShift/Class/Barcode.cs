﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace ActroLib
{
    public class Barcode
    {
        public List<SerialPort> mSR700 = new List<SerialPort>();
        public void Init()
        {
            mSR700.Add(new SerialPort("COM5"));
            mSR700.Add(new SerialPort("COM6"));
            mSR700.Add(new SerialPort("COM7"));
            mSR700.Add(new SerialPort("COM8"));
            for (int i = 0; i < mSR700.Count; i++)
            {
                mSR700[i].BaudRate = 115200;         
                mSR700[i].DataBits = 8;             
                mSR700[i].Parity = Parity.Even;   
                mSR700[i].StopBits = StopBits.One;   
            }
        }
        public string GetBarCode(int ch)
        {
            byte[] recvBytes = new byte[128];
            int recvSize;
            byte STX = 0x02;
            //byte ETX = 0x03;
            //byte CR = 0x0d;

            if (mSR700[ch].IsOpen == false)
            {
                mSR700[ch].Open();
                mSR700[ch].ReadTimeout = 100;
                if (mSR700[ch].IsOpen == false)
                    return "Fail";
            }

            for (; ; )
            {
                try
                {
                    recvSize = ReadDataSub(ch, recvBytes);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(mSR700[ch].PortName + "\r\n" + ex.Message);    // disappeared
                    return "Fail";
                }
                if (recvSize == 0)
                {
                    MessageBox.Show(mSR700[ch].PortName + " has no data.");
                    return "Fail";
                }
                if (recvBytes[0] == STX)
                {
                    return "Fail";
                }
                else
                {
                    recvBytes[recvSize] = 0;
                    return Encoding.GetEncoding("Shift_JIS").GetString(recvBytes);
                }
            }
        }
        public bool RequestBarcode(int ch)
        {
            string lon = "\x02LON\x03";   // <STX>LON<ETX>
            byte[] sendBytes = Encoding.ASCII.GetBytes(lon);

            try
            {
                if (mSR700[ch].IsOpen)
                    mSR700[ch].Close();

                mSR700[ch].Open();
                mSR700[ch].ReadTimeout = 100;
            }
            catch //(Exception ex)
            {
                //MessageBox.Show(mSR700[ch].PortName + "\r\n" + ex.Message);  // non-existent or disappeared
                return false;
            }

            if (mSR700[ch].IsOpen)
            {
                try
                {
                    mSR700[ch].Write(sendBytes, 0, sendBytes.Length);
                }
                catch //(IOException ex)
                {
                    //MessageBox.Show(mSR700[ch].PortName + "\r\n" + ex.Message);    // disappeared
                    return false;
                }
            }
            else
            {
                //MessageBox.Show(mSR700[ch].PortName + " is disconnected.");
                return false;
            }
            return true;
        }
        private int ReadDataSub(int ch, byte[] recvBytes)
        {
            int recvSize = 0;
            bool isCommandRes = false;
            byte d;
            byte STX = 0x02;
            byte ETX = 0x03;
            byte CR = 0x0d;

            //
            // Distinguish between command response and read data.
            //
            try
            {
                d = (byte)mSR700[ch].ReadByte();
                recvBytes[recvSize++] = d;
                if (d == STX)
                {
                    isCommandRes = true;    // Distinguish between command response and read data.
                }
            }
            catch (TimeoutException)
            {
                return 0;   //  No data received.
            }

            //
            // Receive data until the terminator character.
            //
            for (; ; )
            {
                try
                {
                    d = (byte)mSR700[ch].ReadByte();
                    recvBytes[recvSize++] = d;

                    if (isCommandRes && (d == ETX))
                    {
                        break;  // Command response is received completely.
                    }
                    else if (d == CR)
                    {
                        break;  // Read data is received completely.
                    }
                }
                catch (TimeoutException ex)
                {
                    MessageBox.Show(ex.Message);
                    return 0;
                }
            }
            return recvSize;
        }
    }
}
