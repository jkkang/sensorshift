﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ActroLib
{
    public class ChartList
    {
        public StrokeChart[] Stroke = new StrokeChart[2] { new StrokeChart(), new StrokeChart() };
        public ThetaChart[] Theta = new ThetaChart[2] { new ThetaChart(), new ThetaChart() };
        public RotationChart[] Rotation = new RotationChart[2] { new RotationChart(), new RotationChart() };
        public TrajectoryChart[] Trajectory = new TrajectoryChart[2] { new TrajectoryChart(), new TrajectoryChart() };
        public StepChart[] Step = new StepChart[2] { new StepChart(), new StepChart() };
    }
    public class BaseChart
    {
        public Process Process { get { return PROCESS.Process; } }
        public Chart C = new Chart();
        public string Title = "";
        public bool IsFalg = false;
        public bool IsFront = false;
        public int StartPoint = 1;
        public Rectangle OldPt;
        public int RangeMin;
        public int RangeMax;

        public virtual void Init(Chart chart)
        {
            C = chart;
            C.ChartAreas[0].Position.X = 0;
            C.ChartAreas[0].Position.Y = 0;
            C.ChartAreas[0].Position.Height = 99;
            C.ChartAreas[0].Position.Width = 100;
            C.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Calibri", 9, FontStyle.Bold);
            C.ChartAreas[0].AxisY.LabelStyle.Font = new Font("Calibri", 9, FontStyle.Bold);
            C.ChartAreas[0].AxisX.ScaleView.Position = 0;
            C.ChartAreas[0].AxisY.ScaleView.Position = 0;
            C.ChartAreas[0].AxisX.MinorGrid.LineColor = Color.WhiteSmoke;
            C.ChartAreas[0].AxisY.MinorGrid.LineColor = Color.WhiteSmoke;
            C.ChartAreas[0].AxisX.LineColor = Color.DarkGray;
            C.ChartAreas[0].AxisY.LineColor = Color.DarkGray;
            C.BackColor = System.Drawing.SystemColors.ControlLightLight;
            C.Titles.Clear();
            C.Series.Clear();
        }
        public virtual void ChartSet(int yMax = 0, int xMax = 0, bool isClear = false) { }
        public void Clear()
        {
            ChartSet(0, 0, true);
            for (int i = 0; i < C.Series.Count; i++)
            {
                C.Series[i].Points.Clear();
            }
            C.Series[0].Points.AddXY(0, 0);
        }
        public void AddSeries(string label, SeriesChartType type, Color color, bool isVisible, bool Ysecondary = false)
        {
            C.Series.Add(label); 
            C.Series[C.Series.Count - 1].ChartType = type;
            C.Series[C.Series.Count - 1].Color = color;
            C.Series[C.Series.Count - 1].IsVisibleInLegend = isVisible;
            if(Ysecondary) C.Series[C.Series.Count - 1].YAxisType = AxisType.Secondary;
        }
        public void SafeAddChart(int type, int count, double[] x, double[] y, double[] y2 = null, double[] y3 = null, double[] y4 = null)
        {
            if (C.InvokeRequired)
            {
                C.Invoke(new MethodInvoker(delegate ()
                {
                    AddChart(type, count, x, y, y2, y3, y4);
                }));
            }
            else AddChart(type, count, x, y, y2, y3, y4);
        }
        public virtual void AddChart(int type, int count, double[] x, double[] y, double[] y2 = null, double[] y3 = null, double[] y4 = null) { }
        public void ToggleZoom(int left)
        {
            if (!IsFront)
            {
                IsFront = true;
                OldPt.Width = C.Width;
                OldPt.Height = C.Height;
                OldPt.X = C.Left;
                OldPt.Y = C.Top;

                C.Width = 953;
                C.Height = 563;
                C.Left = 3 + (left / 2) * 956;

                C.Top = 88;
                Title = C.Titles[0].Text;
                C.Titles[0].Text = Title;
                C.Titles[0].Font = new Font("Malgun Gothic", 14, FontStyle.Bold); ;
                C.ChartAreas[0].AxisY.MinorGrid.Enabled = true;
                C.ChartAreas[0].AxisY.LabelStyle.Format = "0";
                C.Legends[0].Position = new ElementPosition(0, 0, 30, 30);
                C.BringToFront();
            }
            else
            {
                IsFront = false;
                C.Width = OldPt.Width;
                C.Height = OldPt.Height;
                C.Left = OldPt.X;
                C.Top = OldPt.Y;
                C.Legends[0].Position = new ElementPosition(0, 0, 40, 20);
                C.SendToBack();
                C.Titles[0].Text = Title;
                C.Titles[0].Font = new Font("Malgun Gothic", 9, FontStyle.Bold); ;
                C.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
                C.ChartAreas[0].AxisY.LabelStyle.Format = "0";
            }
        }
    }
    public class StrokeChart : BaseChart
    {
        public override void Init(Chart chart)
        {
            base.Init(chart);

            C.Titles.Add("Code vs Stroke (Y1)\r\nCode vs Current/Hall (Y2)");
            C.Titles[0].Font = new Font("Calibri", 9, FontStyle.Bold);

            AddSeries("X Stroke", SeriesChartType.FastLine, Color.Red, true);
            AddSeries("Y Stroke", SeriesChartType.FastLine, Color.Blue, true);
            AddSeries("X Current", SeriesChartType.FastLine, Color.LightPink, true, true);
            AddSeries("Y Current", SeriesChartType.FastLine, Color.Turquoise, true, true);
            AddSeries("X Hall", SeriesChartType.FastLine, Color.Bisque, true, true);
            AddSeries("Y Hall", SeriesChartType.FastLine, Color.LightSkyBlue, true, true);
            AddSeries("X Stroke Bwd", SeriesChartType.FastLine, Color.OrangeRed, true);
            AddSeries("Y Stroke Bwd", SeriesChartType.FastLine, Color.DarkViolet, true);
            AddSeries("X Current Bwd", SeriesChartType.FastLine, Color.LightGray, true, true);
            AddSeries("Y Current Bwd", SeriesChartType.FastLine, Color.Thistle, true, true);
            AddSeries("X Hall Bwd", SeriesChartType.FastLine, Color.Bisque, true, true);
            AddSeries("Y Hall Bwd", SeriesChartType.FastLine, Color.LightSkyBlue, true, true);
        }
        public override void AddChart(int type, int count, double[] x, double[] y, double[] y2 = null, double[] y3 = null, double[] y4 = null)
        {
            base.AddChart(type, count, x, y, y2, y3);
            while (IsFalg)
                Thread.Sleep(10);
            IsFalg = true;

            string Axis;
            if (type == (int)AO_TYPE._CLX) Axis = "X ";
            else Axis = "Y ";

            foreach(var c in C.Series)
                if(c.Name.Contains(Axis))
                    c.Points.Clear();

            for (int i = StartPoint; i < count / 2; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    C.Series[Axis + "Stroke"].Points.AddXY(x[i], y[i]);
                    C.Series[Axis + "Current"].Points.AddXY(x[i], y2[i]);
                    C.Series[Axis + "Hall"].Points.AddXY(x[i], y3[i] / 100);
                }
            }

            for (int i = count / 2; i < count - 2; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    C.Series[Axis + "Stroke Bwd"].Points.AddXY(x[i], y[i]);
                    C.Series[Axis + "Current Bwd"].Points.AddXY(x[i], y2[i]);
                    C.Series[Axis + "Hall Bwd"].Points.AddXY(x[i], y3[i] / 100);
                }
            }

            double max = y[0];
            for (int i = 1; i < y.Length; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    max = Math.Max(max, y[i]);
                }
            }
            if (double.IsNaN(max) || double.IsInfinity(max)) max = 0;
            ChartSet((int)max);

            IsFalg = false;
        }
        public override void ChartSet(int yMax = 0, int xMax = 0, bool isClear = false)
        {
            base.ChartSet();
            if (isClear)
                C.ChartAreas[0].AxisY.Maximum = yMax = 600;
            if(!isClear && yMax <= 0)
                yMax = 600;

            C.ChartAreas[0].AxisX.Minimum = 4000;
            C.ChartAreas[0].AxisX.Maximum = 28000;
            C.ChartAreas[0].AxisX.Interval = 2400;

            //AutoFit ==============
            int ret = (yMax / 100);
            yMax = ret * 100 + 100;
            //=======================

            C.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisX.MinorGrid.Interval = 2400;

            C.ChartAreas[0].AxisY.Minimum = -200;
            if(C.ChartAreas[0].AxisY.Maximum < yMax)
                C.ChartAreas[0].AxisY.Maximum = yMax;

            C.ChartAreas[0].AxisY.Interval = (yMax + 200)/10;

            C.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisY.MinorGrid.Interval = (yMax + 200) / 10;

            C.ChartAreas[0].AxisY2.Minimum = 0;
            C.ChartAreas[0].AxisY2.Maximum = 400;
            C.ChartAreas[0].AxisY2.Interval = 40;

            C.ChartAreas[0].AxisY2.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY2.MinorGrid.Enabled = false;

            C.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
            C.ChartAreas[0].AxisY2.LabelStyle.ForeColor = Color.DarkGreen;
            C.ChartAreas[0].AxisY2.LabelStyle.Font = new Font("Calibri", 9, FontStyle.Bold);
        }
    }
    public class ThetaChart : BaseChart
    {
        public override void Init(Chart chart)
        {
            base.Init(chart);

            C.Titles.Add("Theta Plot");
            C.Titles[0].Font = new Font("Calibri", 9, FontStyle.Bold);

            AddSeries("X Drv", SeriesChartType.FastLine, Color.Red, true, false);
            AddSeries("Y Drv", SeriesChartType.FastLine, Color.Blue, true, false);
        }
        public override void AddChart(int type, int count, double[] x, double[] y, double[] y2 = null, double[] y3 = null, double[] y4 = null)
        {
            base.AddChart(type, count, x, y, y2, y3);
            while (IsFalg)
                Thread.Sleep(10);
            IsFalg = true;

            string Axis;
            if (type == (int)AO_TYPE._CLX) Axis = "X ";
            else Axis = "Y ";

            foreach (var c in C.Series)
                if (c.Name.Contains(Axis))
                    c.Points.Clear();

            for (int i = StartPoint; i < count / 2; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    C.Series[Axis + "Drv"].Points.AddXY(x[i], y[i]);
                }
            }

            double max = y[0];
            for (int i = 1; i < y.Length; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    max = Math.Max(max, Math.Abs(y[i]));
                }
            }
            if (double.IsNaN(max) || double.IsInfinity(max)) max = 0;
            ChartSet((int)max);

            IsFalg = false;
        }
        public override void ChartSet(int yMax = 0, int xMax = 0, bool isClear = false)
        {
            base.ChartSet();
            C.ChartAreas[0].AxisX.Minimum = 4000;
            C.ChartAreas[0].AxisX.Maximum = 28000;
            C.ChartAreas[0].AxisX.Interval = 2400;

            //AutoFit ==============
            yMax += 1;
            //=======================

            C.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisX.MinorGrid.Interval = 2400;

            int yMin = -yMax;
            C.ChartAreas[0].AxisY.Minimum = yMin;
            C.ChartAreas[0].AxisY.Maximum = yMax;
            C.ChartAreas[0].AxisY.Interval = 1;

            C.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisY.MinorGrid.Interval = 1;
        }
    }
    public class RotationChart : BaseChart
    {     
        public override void Init(Chart chart)
        {
            base.Init(chart);

            C.Titles.Add("Code vs Theta (Y1)\r\nCode vs Current/Hall (Y2)");
            C.Titles[0].Font = new Font("Calibri", 9, FontStyle.Bold);

            AddSeries("R Theta", SeriesChartType.FastLine, Color.Red, true);
            AddSeries("R Current", SeriesChartType.FastLine, Color.LightPink, true, true);
            AddSeries("X1 Hall", SeriesChartType.FastLine, Color.Bisque, true, true);
            AddSeries("X2 Hall", SeriesChartType.FastLine, Color.LightSkyBlue, true, true);
            AddSeries("R Theta Bwd", SeriesChartType.FastLine, Color.OrangeRed, true);
            AddSeries("R Current Bwd", SeriesChartType.FastLine, Color.DarkViolet, true, true);
            AddSeries("X1 Hall Bwd", SeriesChartType.FastLine, Color.Bisque, true, true);
            AddSeries("X2 Hall Bwd", SeriesChartType.FastLine, Color.LightSkyBlue, true, true);
        }
        public override void AddChart(int type, int count, double[] x, double[] y, double[] y2 = null, double[] y3 = null, double[] y4 = null)
        {
            base.AddChart(type, count, x, y, y2, y3);
            while (IsFalg)
                Thread.Sleep(10);
            IsFalg = true;

            foreach (var c in C.Series)
                    c.Points.Clear();

            for (int i = StartPoint; i < count / 2; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    C.Series["R Theta"].Points.AddXY(x[i], y[i]);
                    C.Series["R Current"].Points.AddXY(x[i], y2[i]);
                    C.Series["X1 Hall"].Points.AddXY(x[i], y3[i] / 100);
                    C.Series["X2 Hall"].Points.AddXY(x[i], y4[i] / 100);
                }
            }

            for (int i = count / 2; i < count - 2; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    C.Series["R Theta Bwd"].Points.AddXY(x[i], y[i]);
                    C.Series["R Current Bwd"].Points.AddXY(x[i], y2[i]);
                    C.Series["X1 Hall Bwd"].Points.AddXY(x[i], y3[i] / 100);
                    C.Series["X2 Hall Bwd"].Points.AddXY(x[i], y4[i] / 100);
                }
            }

            double max = y[0];
            for (int i = 1; i < y.Length; i++)
            {
                if (x[i] >= RangeMin && x[i] <= RangeMax)
                {
                    max = Math.Max(max, Math.Abs(y[i]));
                }
            }


            if (double.IsNaN(max) || double.IsInfinity(max)) max = 0;
            ChartSet((int)max);
            
            IsFalg = false;
        }
        public override void ChartSet(int yMax = 0, int xMax = 0, bool isClear = false)
        {
            base.ChartSet();
            C.ChartAreas[0].AxisX.Minimum = Process.Rcp.iRDrvCodeMin;
            C.ChartAreas[0].AxisX.Maximum = Process.Rcp.iRDrvCodeMax;
            C.ChartAreas[0].AxisX.Interval = (Process.Rcp.iRDrvCodeMax - Process.Rcp.iRDrvCodeMin) / 10;

            C.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisX.MinorGrid.Interval = (Process.Rcp.iRDrvCodeMax - Process.Rcp.iRDrvCodeMin) / 10;

            //AutoFit ==============
            yMax += 1;
            //=======================
            int yMin = -yMax;
            C.ChartAreas[0].AxisY.Minimum = yMin;
            C.ChartAreas[0].AxisY.Maximum = yMax;
            C.ChartAreas[0].AxisY.Interval = 1;

            C.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisY.MinorGrid.Interval = 1;

            C.ChartAreas[0].AxisY2.Minimum = 0;
            C.ChartAreas[0].AxisY2.Maximum = 400;
            C.ChartAreas[0].AxisY2.Interval = 40;

            C.ChartAreas[0].AxisY2.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY2.MinorGrid.Enabled = false;

            C.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
            C.ChartAreas[0].AxisY2.LabelStyle.ForeColor = Color.DarkGreen;
            C.ChartAreas[0].AxisY2.LabelStyle.Font = new Font("Calibri", 9, FontStyle.Bold);
        }
    }
    public class TrajectoryChart : BaseChart
    {
        public override void Init(Chart chart)
        {
            base.Init(chart);

            C.Titles.Add("XY Trajectory");
            C.Titles[0].Font = new Font("Calibri", 9, FontStyle.Bold);

            AddSeries("X Drv", SeriesChartType.FastLine, Color.Red, true);
            AddSeries("Y Drv", SeriesChartType.FastLine, Color.Blue, true);
            AddSeries("R Drv", SeriesChartType.FastLine, Color.Green, true);
        }
        public override void AddChart(int type, int count, double[] x, double[] y, double[] y2 = null, double[] y3 = null, double[] y4 = null)
        {
            base.AddChart(type, count, x, y, y2, y3);
            while (IsFalg)
                Thread.Sleep(10);
            IsFalg = true;

            string Axis;
            if (type == (int)AO_TYPE._CLX) Axis = "X ";
            else if (type == 1) Axis = "Y ";
            else Axis = "R ";

            foreach (var c in C.Series)
                if (c.Name.Contains(Axis))
                    c.Points.Clear();

            for (int i = StartPoint; i < count - 2; i++)
            {
                if (y2[i] >= RangeMin && y2[i] <= RangeMax)
                {
                    if (Axis == "X " || Axis == "R ") C.Series[Axis + "Drv"].Points.AddXY(x[i] - 300, y[i]);
                    if (Axis == "Y ") C.Series[Axis + "Drv"].Points.AddXY(x[i], y[i] - 300);
                }
            }

            ChartSet();

            IsFalg = false;
        }
        public override void ChartSet(int yMax = 0, int xMax = 0, bool isClear = false)
        {
            base.ChartSet();
            if (isClear)
                C.ChartAreas[0].AxisY.Maximum = yMax = 600;
            if (!isClear && yMax <= 0)
                yMax = 600;

            //AutoFit ==============
            int ret = (yMax / 100);
            yMax = ret * 100 + 100;
            //=======================

            C.ChartAreas[0].AxisX.Minimum = -300;
            C.ChartAreas[0].AxisX.Maximum = 300;
            C.ChartAreas[0].AxisX.Interval = 100;
            C.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisX.MinorGrid.Interval = 100;

            C.ChartAreas[0].AxisY.Minimum = -300;
            C.ChartAreas[0].AxisY.Maximum = 300;
            C.ChartAreas[0].AxisY.Interval = 100;

            C.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisY.MinorGrid.Interval = 100;

            C.ChartAreas[0].AxisY2.Minimum = 0;
            C.ChartAreas[0].AxisY2.Maximum = 400;
            C.ChartAreas[0].AxisY2.Interval = 40;

            C.ChartAreas[0].AxisY2.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY2.MinorGrid.Enabled = false;

            C.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
            C.ChartAreas[0].AxisY2.LabelStyle.ForeColor = Color.DarkGreen;
            C.ChartAreas[0].AxisY2.LabelStyle.Font = new Font("Calibri", 9, FontStyle.Bold);
        }
    }
    public class StepChart : BaseChart
    {
        public override void Init(Chart chart)
        {
            base.Init(chart);

            C.Titles.Add("Step Respons");
            C.Titles[0].Font = new Font("Calibri", 9, FontStyle.Bold);

            AddSeries("X Step", SeriesChartType.FastLine, Color.Red, true);
            AddSeries("Y Step", SeriesChartType.FastLine, Color.Blue, true);
            AddSeries("R Step", SeriesChartType.FastLine, Color.Green, true);
            AddSeries("X Hall", SeriesChartType.FastLine, Color.Bisque, true, true);
            AddSeries("Y Hall", SeriesChartType.FastLine, Color.LightSkyBlue, true, true);
        }
        public override void AddChart(int type, int count, double[] x, double[] y, double[] y2 = null, double[] y3 = null, double[] y4 = null)
        {
            base.AddChart(type, count, x, y, y2, y3);
            while (IsFalg)
                Thread.Sleep(10);
            IsFalg = true;

            string Axis;
            if (type == (int)AO_TYPE._CLXStep) Axis = "X ";
            else if (type == (int)AO_TYPE._CLYStep) Axis = "Y ";
            else Axis = "R ";

            foreach (var c in C.Series)
                if (c.Name.Contains(Axis))
                    c.Points.Clear();

            for (int i = StartPoint; i < count - 2; i++)
            {
                if (Axis == "X ")
                {
                    C.Series[Axis + "Step"].Points.AddXY(x[i], y[i]);
                    C.Series[Axis + "Hall"].Points.AddXY(x[i], y2[i] / 100);
                }
                if (Axis == "Y ")
                {
                    C.Series[Axis + "Step"].Points.AddXY(x[i], y[i]);
                    C.Series[Axis + "Hall"].Points.AddXY(x[i], y2[i] / 100);
                }
            }

            double max = y[0];
            for (int i = 1; i < y.Length; i++)
                max = Math.Max(max, Math.Abs(y[i]));
            if (double.IsNaN(max) || double.IsInfinity(max)) max = 0;
            ChartSet((int)max);

            IsFalg = false;
        }
        public override void ChartSet(int yMax = 0, int xMax = 0, bool isClear = false)
        {
            base.ChartSet();
            if (isClear)
                C.ChartAreas[0].AxisY.Maximum = yMax = 50;
            if (!isClear && yMax <= 0)
                yMax = 50;

            //AutoFit ==============
            int ret = (yMax / 50);
            yMax = ret * 50 + 50;
            //=======================

            C.ChartAreas[0].AxisX.Minimum = 100;
            C.ChartAreas[0].AxisX.Maximum = 200;
            C.ChartAreas[0].AxisX.Interval = 10;
            C.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisX.MinorGrid.Interval = 10;

            C.ChartAreas[0].AxisY.Minimum = 0;
            C.ChartAreas[0].AxisY.Maximum = yMax;
            C.ChartAreas[0].AxisY.Interval = 5;

            C.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY.MinorGrid.Enabled = true;
            C.ChartAreas[0].AxisY.MinorGrid.Interval = 5;

            C.ChartAreas[0].AxisY2.Minimum = 140;
            C.ChartAreas[0].AxisY2.Maximum = 170;
            C.ChartAreas[0].AxisY2.Interval = 3;

            C.ChartAreas[0].AxisY2.MajorGrid.Enabled = false;
            C.ChartAreas[0].AxisY2.MinorGrid.Enabled = false;

            C.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
            C.ChartAreas[0].AxisY2.LabelStyle.ForeColor = Color.DarkGreen;
            C.ChartAreas[0].AxisY2.LabelStyle.Font = new Font("Calibri", 9, FontStyle.Bold);
        }
    }
}
