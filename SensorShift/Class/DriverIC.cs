﻿using Dln;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActroLib
{
    public class DriverIC
    {
        public LogEvent Log { get { return PROCESS.LogEvent; } }
        public Option Option { get { return PROCESS.Option; } }
        public Model Model { get { return PROCESS.Model; } }
        public Device[] DLNdevice = new Device[4];
        public Dln.I2cMaster.Port[] DLNi2c = new Dln.I2cMaster.Port[4];
        public Dln.Gpio.Module[] DLNgpio = new Dln.Gpio.Module[4];

        public int m_PortCount = 0;
        public bool IsLEDOn = false;
        public bool IsSocketLoad = false;
        public bool IsCoverLoad = false;
        public bool IsAllLoad = false;
        public bool m_bUnsafe = false;
        public int m_nUnsafe = 0;
        public bool m_bOccupied = false;
        private bool isSafeCheck = false;
        public bool IsSafeCheck
        {
            get { return isSafeCheck; }
            set
            {
                if (value != isSafeCheck)
                {
                    isSafeCheck = value; if (isSafeCheck) IsSafeOn = false;
                }
            }
        }

        private bool isSafeOn = false;
        public bool IsSafeOn
        {
            get { return isSafeOn; }
            set { if (value != isSafeOn) { isSafeOn = value; SafetyOn?.Invoke(null, EventArgs.Empty); } }
        }
        public bool IsVirtual = false;

        public event EventHandler SwitchOn = null;
        public event EventHandler SafetyOn = null;

        public bool[] m_CurSensexists = new bool[4];
        public int m_DAC_Addr = 0x4C;
        public int m_CS_Addr = 0x40;
        public int m_DrvIC_Addr = 0x61;
        public string[] m_errMsg = new string[4] { "", "", "", "" };
        public byte[] m_0x0004 = new byte[4];

        public class PinItem
        {
            public int Num { get; set; }
            public int Dir { get; set; }
            public bool IsPullDown { get; set; }
        }
        public List<PinItem> PinList = new List<PinItem>();

        public PinItem ID1;
        public PinItem ID2;
        public PinItem Switch;
        public PinItem Safety;
        public PinItem OISReset1;
        public PinItem OISReset2;
        public PinItem SolCover1;
        public PinItem SolCover2;
        public PinItem SolSocket1;
        public PinItem SolSocket2;

        public int nI2Cclock = 400;
        public int PortCnt { get; private set; }
        public DriverIC(bool isVirtual)
        {
            IsVirtual = isVirtual;
            if (IsVirtual) return;

            //Pin Set Adress ===============================================================
            PinList.Add(ID1 = new PinItem() { Num = 4, Dir = 0, IsPullDown = true });
            PinList.Add(ID2 = new PinItem() { Num = 6, Dir = 0, IsPullDown = true });
            PinList.Add(Switch = new PinItem() { Num = 21, Dir = 0, IsPullDown = true });
            PinList.Add(Safety = new PinItem() { Num = 0, Dir = 0, IsPullDown = true });
            PinList.Add(OISReset1 = new PinItem() { Num = 14, Dir = 1, IsPullDown = true });
            PinList.Add(OISReset2 = new PinItem() { Num = 15, Dir = 1, IsPullDown = true });
            PinList.Add(SolCover1 = new PinItem() { Num = 8, Dir = 1, IsPullDown = false });
            PinList.Add(SolCover2 = new PinItem() { Num = 9, Dir = 1, IsPullDown = false });
            PinList.Add(SolSocket1 = new PinItem() { Num = 10, Dir = 1, IsPullDown = false });
            PinList.Add(SolSocket2 = new PinItem() { Num = 11, Dir = 1, IsPullDown = false });

            //Devicce Set ===================================================================
            Library.Connect("localhost", Connection.DefaultPort);

            PortCnt = (int)(Device.Count());

            if (PortCnt == 0)
            {
                MessageBox.Show("--- No DLN-series adapters!! ---", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            for (int i = 0; i < PortCnt; i++)
            {
                try
                {
                    DLNdevice[i] = Device.Open(i);

                    if (DLNdevice[i].I2cMaster.Ports[0].Restrictions.MaxReplyCount != Restriction.NotSupported)
                        DLNdevice[i].I2cMaster.Ports[0].MaxReplyCount = 10;

                    if (DLNdevice[i].I2cMaster.Ports[0].Restrictions.Frequency == Restriction.MustBeDisabled)
                        DLNdevice[i].I2cMaster.Ports[0].Enabled = false;

                    DLNdevice[i].I2cMaster.Ports[0].Frequency = nI2Cclock * 1000;
                    DLNdevice[i].I2cMaster.Ports[0].Enabled = true;

                    for (int j = 0; j < PinList.Count; j++)
                    {
                        DLNdevice[i].Gpio.Pins[PinList[j].Num].Enabled = true;
                        DLNdevice[i].Gpio.Pins[PinList[j].Num].Direction = PinList[j].Dir;
                        DLNdevice[i].Gpio.Pins[PinList[j].Num].PulldownEnabled = PinList[j].IsPullDown;
                    }

                    //Port Swich ====================================================================
                    if (DLNdevice[i].Gpio.Pins[ID1.Num].Value == 1)
                        PortSwitchSet(i, 1);
                    else
                        PortSwitchSet(i, 0);
                    if (PortCnt > 2)
                    {
                        if (DLNdevice[i].Gpio.Pins[ID2.Num].Value == 1)
                            PortSwitchSet(i, 3);
                        else
                            PortSwitchSet(i, 2);
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("--- Device doesn't respond!! ---", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            //Power Reset On 
            DLNgpio[0].Pins[OISReset1.Num].OutputValue = 1;
            DLNgpio[0].Pins[OISReset2.Num].OutputValue = 1;

            DLNgpio[1].Pins[OISReset1.Num].OutputValue = 1;
            DLNgpio[1].Pins[OISReset2.Num].OutputValue = 1;


            //Pin status Check ===================================================
            if (DLNgpio[0].Pins[SolSocket1.Num].OutputValue == 0 && DLNgpio[0].Pins[SolSocket2.Num].OutputValue == 1)
            {
                IsSocketLoad = true;
            }
            if (DLNgpio[0].Pins[SolCover1.Num].OutputValue == 0 && DLNgpio[0].Pins[SolCover1.Num].OutputValue == 1)
            {
                IsCoverLoad = true;
            }
            if (IsSocketLoad && IsCoverLoad) IsAllLoad = true;
        }
        public void PortSwitchSet(int scr, int det)
        {
            DLNi2c[det] = DLNdevice[scr].I2cMaster.Ports[0];
            DLNgpio[det] = DLNdevice[scr].Gpio;
            //===============================================================================

            if (det == 0)
            {
                DLNgpio[det].Pins[Switch.Num].ConditionMetThreadSafe += SWEventHandler;
                DLNgpio[det].Pins[Switch.Num].SetEventConfiguration(Dln.Gpio.EventType.LevelHigh, 50);
            }
            if (det == 1)
            {
                DLNgpio[det].Pins[Safety.Num].ConditionMetThreadSafe += SafeEventHandler;
                DLNgpio[det].Pins[8].OutputValue = 0;
                DLNgpio[det].Pins[Safety.Num].SetEventConfiguration(Dln.Gpio.EventType.LevelHigh, 50);
            }
        }
        private void SWEventHandler(object sender, Dln.Gpio.ConditionMetEventArgs e)
        {
            SwitchOn?.Invoke(null, EventArgs.Empty);
        }
        private void SafeEventHandler(object sender, Dln.Gpio.ConditionMetEventArgs e)
        {
            if (!Option.m_bSafeSensor) return;
            if (Model.SafeMode == "0")
            {
                if (e.Value == 1 && IsSafeOn)
                {
                    DLNgpio[1].Pins[Safety.Num].SetEventConfiguration(Dln.Gpio.EventType.LevelLow, 50);
                    DLNgpio[1].Pins[8].OutputValue = 0;
                    if (IsSafeCheck) IsSafeOn = false;

                }
                else if (e.Value == 0 && !IsSafeOn)
                {
                    DLNgpio[1].Pins[Safety.Num].SetEventConfiguration(Dln.Gpio.EventType.LevelHigh, 50);
                    DLNgpio[1].Pins[8].OutputValue = 1;
                    IsSafeOn = true;
                }
            }
            else
            {
                if (e.Value == 1 && !IsSafeOn)
                {
                    DLNgpio[1].Pins[Safety.Num].SetEventConfiguration(Dln.Gpio.EventType.LevelLow, 50);
                    DLNgpio[1].Pins[8].OutputValue = 1;
                    IsSafeOn = true;
                }
                else if (e.Value == 0 && IsSafeOn)
                {
                    DLNgpio[1].Pins[Safety.Num].SetEventConfiguration(Dln.Gpio.EventType.LevelHigh, 50);
                    DLNgpio[1].Pins[8].OutputValue = 0;
                    if (IsSafeCheck) IsSafeOn = false;
                }
                else if(e.Value == 0 && !IsSafeOn)
                {
                    DLNgpio[1].Pins[8].OutputValue = 0;
                }
            }
        }
        public void DriverICPowerOn(int port)
        {
            DLNgpio[0].Pins[OISReset1.Num].OutputValue = 1;
            DLNgpio[0].Pins[OISReset2.Num].OutputValue = 1;

            DLNgpio[1].Pins[OISReset1.Num].OutputValue = 1;
            DLNgpio[1].Pins[OISReset2.Num].OutputValue = 1;

        }
        public void DriverICPowerOff(int port)
        {
            DLNgpio[0].Pins[OISReset1.Num].OutputValue = 0;
            DLNgpio[0].Pins[OISReset2.Num].OutputValue = 0;

            DLNgpio[1].Pins[OISReset1.Num].OutputValue = 0;
            DLNgpio[1].Pins[OISReset2.Num].OutputValue = 0;

        }
        public void PowerReset(int port)
        {
            DriverICPowerOff(0);
            DriverICPowerOff(1);
            Thread.Sleep(200);
            DriverICPowerOn(0);
            DriverICPowerOn(1);
        }
        public void SetI2CClock(int nI2Cclock)
        {
            for (int i = 0; i < PortCnt; i++)
                DLNi2c[i].Enabled = false;

            Thread.Sleep(10);

            for (int i = 0; i < PortCnt; i++)
            {
                DLNi2c[i].Frequency = nI2Cclock * 1000;
                DLNi2c[i].Enabled = true;
            }
        }
        public void SetFailLED(int ch, bool IsFail)
        {
            try
            {
                int lch = (ch / 2) * 2 + 1; //  0,1 --> 1   ;; 2, 3 --> 3
                                            //  ch0, ch1 -> ch1 ;   ch2, ch3 -> ch3
                                            //int dio = 26 + (ch % 2);
                DLNgpio[lch].Pins[26 + (ch % 2)].OutputValue = (IsFail ? 1 : 0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void DriverICPower(int port, int ChannelCnt, bool IsOn = true)
        {
            if (IsVirtual)
            {
                Log.AddLog(0, "Virtual DriverICPower");
                Log.AddLog(1, "Virtual DriverICPower");
                return;
            }
            if (IsOn)
            {
                if (ChannelCnt == 1)
                    DLNgpio[port * 2].Pins[31].Direction = 1;
                else
                {
                    DLNgpio[port * 2 + 1].Pins[9].Direction = 1;
                    DLNgpio[port * 2 + 1].Pins[9].OutputValue = 1;
                }
            }
            else
            {
                if (ChannelCnt == 1)
                    DLNgpio[port * 2].Pins[31].Direction = 0;
                else
                {
                    DLNgpio[port * 2 + 1].Pins[9].OutputValue = 0;
                    DLNgpio[port * 2 + 1].Pins[9].Direction = 0;
                }

            }
        }
        public void OISDriverICReset(int port, bool IsOn = true)
        {
            if (IsVirtual)
            {
                Log.AddLog(0, "Virtual OISDriverICReset");
                Log.AddLog(1, "Virtual OISDriverICReset");
                return;
            }
            if (IsOn)
            {
                DLNgpio[port * 2].Pins[14].OutputValue = 1;

            }
            else
            {
                DLNgpio[port * 2].Pins[14].OutputValue = 0;
            }
        }
        public void ToggleSocket()
        {
            if (IsSocketLoad = !IsSocketLoad) LoadSocket(0);
            else UnloadSocket(0);
        }
        public void ToggleCover()
        {
            if (IsCoverLoad = !IsCoverLoad) LoadCover(0);
            else UnloadCover(0);
        }
        public void ToggleAll(bool isLoad)
        {
            if (IsSocketLoad && IsCoverLoad && !isLoad)
            {
                UnloadCover(0, true); IsCoverLoad = false;
                UnloadSocket(0, true); IsSocketLoad = false;
                return;
            }
            else if (IsSocketLoad && !IsCoverLoad && !isLoad)
            {
                UnloadSocket(0, true); IsSocketLoad = false;
                return;
            }
            else if (!IsSocketLoad && IsCoverLoad && !isLoad)
            {
                UnloadCover(0, true); IsCoverLoad = false;
                return;
            }
            else if (IsSocketLoad && !IsCoverLoad && isLoad)
            {
                LoadCover(0, true); IsCoverLoad = true;
                return;
            }
            else if (!IsSocketLoad && IsCoverLoad && isLoad)
            {
                LoadSocket(0, true); IsSocketLoad = true;
                return;
            }
            else if (!IsSocketLoad && !IsCoverLoad && isLoad)
            {
                if (!IsSocketLoad)
                { LoadSocket(0, true); IsSocketLoad = true; }
                if (!IsCoverLoad)
                { LoadCover(0, true); IsCoverLoad = true; }
                return;
            }

        }
        public void UnloadSocket(int port, bool isDeay = false)
        {
            int ch = port * 2;
            if (DLNgpio[ch] == null) return;

            if (IsCoverLoad) { MessageBox.Show("Cover is Loaded!!", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
            if (IsSafeOn)
            {
                DLNgpio[ch].Pins[SolSocket1.Num].OutputValue = 0;
                DLNgpio[ch].Pins[SolSocket2.Num].OutputValue = 1;
                return;
            }

            DLNgpio[ch].Pins[SolSocket1.Num].OutputValue = 1;
            DLNgpio[ch].Pins[SolSocket2.Num].OutputValue = 0;
            if (isDeay) Thread.Sleep(1500);
        }
        public void LoadSocket(int port, bool isDeay = false)
        {
            int ch = port * 2;
            if (DLNgpio[ch] == null) return;
            if (IsCoverLoad) { MessageBox.Show("Cover is Loaded!!", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
            DLNgpio[ch].Pins[SolSocket1.Num].OutputValue = 0;
            DLNgpio[ch].Pins[SolSocket2.Num].OutputValue = 1;
            if (isDeay) Thread.Sleep(1500);
        }
        public void UnloadCover(int port, bool isDeay = false)
        {
            int ch = port * 2;
            if (DLNgpio[ch] == null) return;
            DLNgpio[ch].Pins[SolCover1.Num].OutputValue = 1;
            DLNgpio[ch].Pins[SolCover2.Num].OutputValue = 0;

            if (isDeay) Thread.Sleep(1000);
        }
        public void LoadCover(int port, bool isDeay = false)
        {
            int ch = port * 2;
            if (DLNgpio[ch] == null) return;
            DLNgpio[ch].Pins[SolCover1.Num].OutputValue = 0;
            DLNgpio[ch].Pins[SolCover2.Num].OutputValue = 1;
            if (isDeay) Thread.Sleep(1500);
        }
        //public bool CheckCurSensExists(int ch)
        //{
        //    try
        //    {
        //        int[] list = DLNi2c[ch].ScanDevices();

        //        m_CurSensexists[ch] = false;

        //        if (list.Length > 0)
        //        {
        //            foreach (int addr in list)
        //            {
        //                if (addr == 0x40) m_CurSensexists[ch] = true;
        //            }
        //        }
        //        return m_CurSensexists[ch];
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("CheckCurSensExists() " + "," + ch.ToString() + "," + ex.Message);
        //        return false;
        //    }
        //}
        public bool SetLEDpower(int ch, int value)
        {
            byte bufferH;
            byte[] bufferL = new byte[1];
            int lDACaddr = m_DAC_Addr + (ch % 2);
            bufferH = (byte)(value / 256);
            bufferL[0] = (byte)(value % 256);

            int lch = 0;
            if (ch > 1) lch = 1;
            while (m_bOccupied)
            {
                Thread.Sleep(1);
            }
            m_bOccupied = true;
            try
            {
                if (IsVirtual)
                {
                    m_bOccupied = false;
                    return true;
                }

                DLNi2c[lch].Write(lDACaddr, 1, bufferH, bufferL);
                m_bOccupied = false;
                return true;
            }
            catch //(Exception ex)
            {
                m_bOccupied = false;
                return false;
            }

        }
        public double GetCurrent(int ch)
        {
            double res = 0;
            int RegAddr = 0x01;
            byte[] buffer2 = new byte[2];
            try
            {
                if (IsVirtual)
                {
                    m_bOccupied = false;
                    return 0;
                }
                DLNi2c[ch].Read(m_CS_Addr, 1, RegAddr, buffer2);
                res = (buffer2[0] * 256 + buffer2[1]) / 10.0;
            }
            catch //(Exception ex)
            {
                // MessageBox.Show("Fail to Set LED Power " + ex.Message);
            }
            return res;
        }

        //New ======================================================================
        public int SlaveAddr { get; set; }
    }
}
