﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace ActroLib
{
    public class FZCoefficient
    {
        public double[,] mData = null;
        public double[,] mDataX = null;
        public double[,] mDataY = null;
        public int mInitNumX = 1;
        public int mInitNumY = 1;
        public int mInitNumDataSet = 1;
        public int mDataColLength = 0;
        public int mDerivedOrder = 0;
        public int mNumV = 0;
        public int mNthVset = 0;
        public int mMaxDimensionOfEquation = 0;
        public int[] mAttentionY = new int[4] { -1, -1, -1, -1 };
        public double mMinSensitivity = 0.00001;
        public double mErrThreshold = 0.001;
        public double[] mErrHistory = new double[100];
        public int[] uOn = null;
        public int[] uID = null;
        public double[,] u = null;

        public bool m_bNonStopProcessDone = false;

        public int mNumVnow = 0;
        public int[,] mExtNumVnow = new int[4, 2];
        public int[] uIDreadable = null;
        public int[,] m_uIndex = null;
        public int[,] m_LastUIndex = new int[4, 100];
        public int[,] m_LastVIndex = new int[4, 100];

        public string[] m_uConfig = null;
        public int[] m_u4C = new int[4];
        public int m_u4Cindex = 0;
        public double[,] matrixU = null;
        public double[] matrixY = null;
        //double[,] matrixInvU = null;
        public double[] matrixC = null;
        public double[,] matrixC_U = new double[4, 100];
        public double[,] matrixC_V = new double[4, 100];
        //double[] m_rank = null;
        public double m_OfsX = 0;
        public double m_OfsY = 0;
        public string mProcessMsg = "";
        public string[] mResultMsg = new string[4] { "", "", "", "" };
        public double[] mMarkDistNear = new double[4] { 9.3, 9.3, 9.3, 9.3 };
        public double[] mMarkDistFar = new double[4] { 11.6, 11.6, 11.6, 11.6 };
        public double[] mMarkDistDecenter = new double[4] { 0, 0, 0, 0 };

        public string m_strMsg = "";

        public struct Point2d
        {
            public double X;
            public double Y;
        }
        public struct Poly2nd
        {
            //  y = a(x-b)^2 + c
            public double a;
            public double b;
            public double c;
        }

        //private delegate void SafeCallDelegate(string text);
        public FZCoefficient()
        {
            ResetLog();
        }
        public int GetRealParameters()
        {
            string sFilePath = "C:\\FZ4PTest\\DoNotTouch\\MarkGaps_SZ3900A.txt";

            if (!File.Exists(sFilePath))
            {
                StreamWriter sw = new StreamWriter(sFilePath);
                sw.WriteLine("9.3\t11.6\t0.0");
                sw.WriteLine("9.3\t11.6\t0.0");
                sw.Close();
                return 2;
            }


            StreamReader sr = new StreamReader(sFilePath);
            string allLines = sr.ReadToEnd();
            string[] eachLine = allLines.Split("\n".ToCharArray());

            if (eachLine.Length > 3)
            {
                for (int i = 0; i < 4; i++)
                {
                    string[] figures = eachLine[i].Split('\t');
                    mMarkDistNear[i] = Convert.ToDouble(figures[0]);
                    mMarkDistFar[i] = Convert.ToDouble(figures[1]);
                    mMarkDistDecenter[i] = Convert.ToDouble(figures[2]);
                }
                return 4;
            }
            else if (eachLine.Length > 1)
            {
                int iChannel = 1;
                if (eachLine[1].Length > 4)
                    iChannel = 2;

                for (int i = 0; i < iChannel; i++)
                {
                    string[] figures = eachLine[i].Split('\t');
                    mMarkDistNear[i] = Convert.ToDouble(figures[0]);
                    mMarkDistFar[i] = Convert.ToDouble(figures[1]);
                    mMarkDistDecenter[i] = Convert.ToDouble(figures[2]);
                }
                return iChannel;
            }
            else if (eachLine.Length > 0 && eachLine[0].Length > 4)
            {
                string[] figures = eachLine[0].Split('\t');
                mMarkDistNear[0] = Convert.ToDouble(figures[0]);
                mMarkDistFar[0] = Convert.ToDouble(figures[1]);
                mMarkDistDecenter[0] = Convert.ToDouble(figures[2]);
                return 1;
            }

            return 0;
        }

        public bool LoadDataFile()
        {
            string sFilePath = "C:\\FZ4PTest\\DoNotTouch\\";
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = "txt";
            openFile.InitialDirectory = sFilePath;

            openFile.Filter = "txt(*.txt)|*.txt";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                string sFileName = openFile.FileName;
                //Thread threadReadDataFile = new Thread(() => ReadDataFile(sFileName));
                //threadReadDataFile.Start();
                ReadDataFile(sFileName);
            }
            return true;
        }

        public bool ReadDataFile(string filename)
        {
            StreamReader sr = new StreamReader(filename);
            string bulkData = sr.ReadToEnd();
            sr.Close();
            string[] fullLines = bulkData.Split("\r".ToCharArray());

            int allLines = fullLines.Length;
            string[] aLine;
            int i = 0;
            int colLength = 0;
            if (fullLines[0].Length > 1)
            {
                aLine = fullLines[0].Split('\t');
                colLength = aLine.Length;
            }
            else
                return false;
            mInitNumDataSet = allLines;
            mDataColLength = colLength;

            mData = new double[colLength, mInitNumDataSet];

            for (i = 0; i < allLines; i++)
            {
                if (fullLines[i].Length > 1)
                {
                    aLine = fullLines[i].Split('\t');
                    if (aLine.Length < colLength) break;
                    for (int j = 0; j < colLength; j++)
                        mData[j, i] = Convert.ToDouble(aLine[j]);
                }
                else
                    break;
            }
            mInitNumDataSet = i;
            //tbDataFile.Text = filename;
            //mProcessMsg += ">Number of Data : " + mInitNumDataSet.ToString() + "\r\n";
            //mProcessMsg += ">Number of Column : " + mDataColLength.ToString() + "\r\n";
            return true;
        }


        public bool ApplyCondition(bool IsAuto = false)
        {
            mInitNumX = 4;
            mInitNumY = 3;
            mDerivedOrder = 4;
            mMaxDimensionOfEquation = 18;
            mMinSensitivity = 0.002;
            mErrThreshold = 0.0001;
            //mMinSensitivity = 0.001;
            //mErrThreshold = 0.00005;

            mDataX = new double[mInitNumX, mInitNumDataSet];
            mDataY = new double[mInitNumY, mInitNumDataSet];

            mNumV = mInitNumX;
            if (mDerivedOrder > 1)
            {
                mNumV += (mInitNumX + 1) * mInitNumX / 2;
            }
            if (mDerivedOrder > 2)
            {
                mNumV += (mInitNumX + 2) * (mInitNumX + 1) * mInitNumX / 6;
            }
            if (mDerivedOrder > 3)
            {
                mNumV += (mInitNumX + 3) * (mInitNumX + 2) * (mInitNumX + 1) * mInitNumX / 24;
            }
            if (mDerivedOrder > 4)
            {
                mNumV += (mInitNumX + 4) * (mInitNumX + 3) * (mInitNumX + 2) * (mInitNumX + 1) * mInitNumX / 120;
            }

            m_uConfig = new string[mNumV];
            u = new double[mNumV, mInitNumDataSet];
            uOn = new int[mNumV];
            uID = new int[mNumV];
            uIDreadable = new int[mNumV];
            m_uIndex = new int[100, mNumV];

            if (mDataColLength < (mInitNumX + mInitNumY))
            {
                mProcessMsg += "Data Column Length is less than num X + num Y.\r\n";
                return true;
            }
            for (int i = 0; i < mInitNumDataSet; i++)
            {
                for (int j = 0; j < mInitNumX; j++)
                {
                    mDataX[j, i] = mData[j, i];
                }
                for (int j = 0; j < mInitNumY; j++)
                {
                    mDataY[j, i] = mData[mInitNumX + j, i];
                }
            }
            return true;
        }
        public bool CalcStep1(int selectedIndex = -1)
        {
            int last_u = 0;
            int i = 0;
            int iSelected = 0;

            if (selectedIndex < 0)
            {
                mAttentionY[iSelected++] = 0;
                mAttentionY[iSelected++] = 1;
            }
            for (i = 0; i < 4; i++)
            {
                if (i >= iSelected)
                    mAttentionY[i] = -1;

                if (mAttentionY[i] > -1)
                {
                    mProcessMsg += ">> mAttentionY[" + i.ToString() + "]=" + mAttentionY[i].ToString() + "\r\n";
                }
            }


            for (i = 0; i < mInitNumX; i++)
            {
                uOn[i] = 1;
                uID[i] = i;
                uIDreadable[i] = i + 1;
                for (int j = 0; j < mInitNumDataSet; j++)
                {
                    u[i, j] = mDataX[i, j];
                }
            }
            last_u = i;
            int lcur_uID = 0;
            bool IsStop = false;
            if (mDerivedOrder > 1)
            {
                for (int a = 0; a < mInitNumX; a++)
                {
                    for (int b = 0; b < mInitNumX; b++)
                    {
                        lcur_uID = (int)(Math.Pow(10, a + 1) + Math.Pow(10, b + 1));
                        IsStop = false;
                        for (int z = 0; z < last_u; z++)
                        {
                            if (uID[z] == lcur_uID)
                            {
                                IsStop = true;
                                break;
                            }
                        }
                        if (IsStop)
                            continue;
                        uOn[last_u] = 1;
                        uID[last_u] = lcur_uID;
                        uIDreadable[last_u] = (a + 1) + (b + 1) * 10;
                        for (int j = 0; j < mInitNumDataSet; j++)
                        {
                            u[last_u, j] = mDataX[a, j] * mDataX[b, j];
                        }
                        last_u++;
                    }
                }
            }
            if (mDerivedOrder > 2)
            {
                for (int c = 0; c < mInitNumX; c++)
                {
                    for (int b = 0; b < mInitNumX; b++)
                    {
                        for (int a = 0; a < mInitNumX; a++)
                        {
                            lcur_uID = (int)(Math.Pow(10, a + 1) + Math.Pow(10, b + 1) + Math.Pow(10, c + 1));
                            IsStop = false;
                            for (int z = 0; z < last_u; z++)
                            {
                                if (uID[z] == lcur_uID)
                                {
                                    IsStop = true;
                                    break;
                                }
                            }
                            if (IsStop)
                                continue;
                            uOn[last_u] = 1;
                            uID[last_u] = lcur_uID;
                            uIDreadable[last_u] = (a + 1) + (b + 1) * 10 + (c + 1) * 100;
                            for (int j = 0; j < mInitNumDataSet; j++)
                            {
                                u[last_u, j] = mDataX[a, j] * mDataX[b, j] * mDataX[c, j];
                            }
                            last_u++;
                        }
                    }
                }
            }
            if (mDerivedOrder > 3)
            {
                for (int d = 0; d < mInitNumX; d++)
                {
                    for (int c = 0; c < mInitNumX; c++)
                    {
                        for (int b = 0; b < mInitNumX; b++)
                        {
                            for (int a = 0; a < mInitNumX; a++)
                            {
                                lcur_uID = (int)(Math.Pow(10, a + 1) + Math.Pow(10, b + 1) + Math.Pow(10, c + 1) + Math.Pow(10, d + 1));
                                IsStop = false;
                                for (int z = 0; z < last_u; z++)
                                {
                                    if (uID[z] == lcur_uID)
                                    {
                                        IsStop = true;
                                        break;
                                    }
                                }
                                if (IsStop)
                                    continue;
                                uOn[last_u] = 1;
                                uID[last_u] = lcur_uID;
                                uIDreadable[last_u] = (a + 1) + (b + 1) * 10 + (c + 1) * 100 + (d + 1) * 1000;
                                for (int j = 0; j < mInitNumDataSet; j++)
                                {
                                    u[last_u, j] = mDataX[a, j] * mDataX[b, j] * mDataX[c, j] * mDataX[d, j];
                                }
                                last_u++;
                            }
                        }
                    }
                }
            }
            if (mDerivedOrder > 4)
            {
                for (int e = 0; e < mInitNumX; e++)
                {
                    for (int d = 0; d < mInitNumX; d++)
                    {
                        for (int c = 0; c < mInitNumX; c++)
                        {
                            for (int b = 0; b < mInitNumX; b++)
                            {
                                for (int a = 0; a < mInitNumX; a++)
                                {
                                    lcur_uID = (int)(Math.Pow(10, a + 1) + Math.Pow(10, b + 1) + Math.Pow(10, c + 1) + Math.Pow(10, d + 1) + Math.Pow(10, e + 1));
                                    IsStop = false;
                                    for (int z = 0; z < last_u; z++)
                                    {
                                        if (uID[z] == lcur_uID)
                                        {
                                            IsStop = true;
                                            break;
                                        }
                                    }
                                    if (IsStop)
                                        continue;
                                    uOn[last_u] = 1;
                                    uID[last_u] = lcur_uID;
                                    uIDreadable[last_u] = (a + 1) + (b + 1) * 10 + (c + 1) * 100 + (d + 1) * 1000 + (e + 1) * 10000;
                                    for (int j = 0; j < mInitNumDataSet; j++)
                                    {
                                        u[last_u, j] = mDataX[a, j] * mDataX[b, j] * mDataX[c, j] * mDataX[d, j] * mDataX[e, j];
                                    }
                                    last_u++;
                                }
                            }
                        }
                    }
                }
            }
            // Check Derived Variables
            //string strID = "\r\n";
            //StreamWriter wr = new StreamWriter("DerivedVars.csv");
            //for (i = 0; i < last_u; i++)
            //{
            //    strID += i.ToString() + " ; " + uIDreadable[i].ToString() + "\t" + uID[i].ToString() + "\r\n";
            //    for (int j = 0; j < mInitNumDataSet; j++)
            //        wr.WriteLine(uID[i].ToString() + "," + u[i, j]);
            //}
            //wr.Close();
            return true;
        }

        public bool CalcStep2(ref int p, ref int nestedCount, int ySelectedIndex = 0)
        {
            //  FInd First 2 indepnedent variables
            SaveLog(">>CalcStep2()");
            nestedCount++;

            int attentionY = mAttentionY[ySelectedIndex];
            bool res = false;

            if (attentionY < 0) attentionY = 0;
            SaveLog("\r\n nestedCount = " + nestedCount.ToString() + "\r\n AttentionY = " + attentionY.ToString() + "\r\n");
            matrixU = new double[2, 2];
            matrixY = new double[2];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int z = 0; z < mInitNumDataSet; z++)
                        matrixU[i, j] += u[m_uIndex[p, i], z] * u[m_uIndex[p, j], z];
                }
            }
            int rankU = RankOfU(matrixU, 2);
            if (rankU == 2)
            {
                SaveLog("Rank = 2 at " + p.ToString() + " at " + nestedCount.ToString() + "th\r\n");
                for (int i = 0; i < 2; i++)
                {
                    for (int z = 0; z < mInitNumDataSet; z++)
                        matrixY[i] += u[m_uIndex[p, i], z] * mDataY[attentionY, i];
                }
                return true;
            }
            else if (rankU == 0)
            {
                m_uIndex[p, 0] = m_uIndex[p, 1] + 1;
                m_uIndex[p, 1] = m_uIndex[p, 1] + 2;
                if (m_uIndex[p, 1] >= mNumV)
                    return false;
                p++;
                res = CalcStep2(ref p, ref nestedCount, ySelectedIndex);
                SaveLog(" Rank = 0 at " + p.ToString() + " at " + nestedCount.ToString() + "th\r\n");
                if (res)
                    return true;
                else
                    return false;
            }
            // In case of rank 1
            SaveLog(" Rank = 1 at " + p.ToString() + " at " + nestedCount.ToString() + "th\r\n");
            while (true)
            {
                m_u4C[m_u4Cindex++] = m_uIndex[p, 0];
                m_u4C[m_u4Cindex++] = m_uIndex[p, 1];
                if (m_u4Cindex == 4)
                {
                    //  Calc 4 cases to find First 2 independent Vars.
                    matrixU = new double[2, 2];
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            for (int z = 0; z < mInitNumDataSet; z++)
                                matrixU[i, j] += u[m_u4C[0], z] * u[m_u4C[2], z];
                        }
                    }
                    rankU = RankOfU(matrixU, 2);
                    if (rankU == 2)
                        return true;
                    matrixU = new double[2, 2];
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            for (int z = 0; z < mInitNumDataSet; z++)
                                matrixU[i, j] += u[m_u4C[1], z] * u[m_u4C[3], z];
                        }
                    }
                    rankU = RankOfU(matrixU, 2);
                    if (rankU == 2)
                        return true;
                    matrixU = new double[2, 2];
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            for (int z = 0; z < mInitNumDataSet; z++)
                                matrixU[i, j] += u[m_u4C[1], z] * u[m_u4C[2], z];
                        }
                    }
                    rankU = RankOfU(matrixU, 2);
                    if (rankU == 2)
                        return true;
                    matrixU = new double[2, 2];
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            for (int z = 0; z < mInitNumDataSet; z++)
                                matrixU[i, j] += u[m_u4C[0], z] * u[m_u4C[3], z];
                        }
                    }
                    rankU = RankOfU(matrixU, 2);
                    if (rankU == 2)
                        return true;
                }
                else
                {
                    m_uIndex[p, 0] = m_uIndex[p, 1] + 1;
                    m_uIndex[p, 1] = m_uIndex[p, 1] + 2;
                    if (m_uIndex[p, 1] >= mNumV)
                        return false;
                    p++;
                    res = CalcStep2(ref p, ref nestedCount, ySelectedIndex);
                    if (res)
                        return true;
                }
                if (m_uIndex[p, 1] >= mNumV)
                    return false;
            }
            //return false;
        }

        public bool CalcStep3()
        {
            //  Add One more Vars to the Equation
            SaveLog(">>CalcStep3()");
            int p = mNthVset;
            int rankU = 0;
            int lNumVnow = mNumVnow + 1;
            string strlog = "";

            if (lNumVnow >= mNumV)
            {
                SaveLog("Already Number of Variables are full!");
                return false;
            }

            //if (lNumVnow > mMaxDimensionOfEquation)
            //{
            //    SaveLog("Already Number of Variables are full!");
            //    return false;
            //}
            if (lNumVnow > 1)
                m_uIndex[mNthVset, lNumVnow - 1] = m_uIndex[mNthVset, lNumVnow - 2];

            while (true)
            {
                SaveLog("mNthVset=" + mNthVset.ToString() + " lNumVnow=" + lNumVnow.ToString() + "\r\n");
                //WriteTextSafe(strlog);
                while (true)
                {
                    m_uIndex[mNthVset, lNumVnow - 1]++;
                    //SaveLog("m_uIndex[mNthVset, lNumVnow - 1]=" + m_uIndex[mNthVset, lNumVnow - 1].ToString());
                    if (m_uIndex[mNthVset, lNumVnow - 1] >= mNumV)
                    {
                        mNumVnow = lNumVnow - 1;
                        for (int i = 0; i < mNumVnow; i++)
                            strlog += m_uIndex[mNthVset, i] + "\t";
                        strlog += " \r\n";
                        SaveLog(strlog + "No more uIndex");
                        mProcessMsg = "No more uIndex";
                        return false;
                    }
                    if (uOn[m_uIndex[mNthVset, lNumVnow - 1]] > 0)
                        break;
                }
                matrixU = new double[lNumVnow, lNumVnow];
                for (int i = 0; i < lNumVnow; i++)
                {
                    for (int j = 0; j < lNumVnow; j++)
                    {
                        for (int z = 0; z < mInitNumDataSet; z++)
                            matrixU[i, j] += u[m_uIndex[p, i], z] * u[m_uIndex[p, j], z];
                    }
                }
                //SaveLog("Call RankOfU()");
                //WriteTextSafe(strlog );
                rankU = RankOfU(matrixU, lNumVnow);
                //SaveLog("RankOfU() finish");
                //strlog = "";
                //for (int i = 0; i < lNumVnow; i++)
                //    strlog += m_uIndex[mNthVset, i] + "\t";
                //strlog += " : rank is " + rankU.ToString() + " \r\n";
                //SaveLog(strlog);
                //WriteTextSafe(strlog );
                if (rankU == lNumVnow)
                    break;
            }
            SaveLog("New Var Added : " + m_uIndex[mNthVset, lNumVnow - 1].ToString());
            //MessageBox.Show("A_222");
            //strlog = "FInal lNumVnow=" + lNumVnow.ToString() + "\r\n"; ;
            mProcessMsg = "New Rank is " + rankU.ToString() + "\r\n";

            if (rankU < lNumVnow)
                return false;

            mNumVnow = lNumVnow;
            return true;
        }
        public bool CalcStep3(double[] lSensitivity)
        {
            SaveLog(">>CalcStep3(lSensitivity)");
            //  Add One more Vars to the Equation
            int p = mNthVset;
            int rankU = 0;
            int lNumVnow = mNumVnow;
            //string strlog = "";
            int iDel = 0;
            double lminSens = 999999;

            for (int i = 0; i < mNumVnow; i++)
            {
                if (lminSens > lSensitivity[i])
                {
                    iDel = i;
                    lminSens = lSensitivity[i];
                }
            }
            for (int i = iDel; i < mNumVnow - 1; i++)
                m_uIndex[p, i] = m_uIndex[p, i + 1];

            m_uIndex[p, mNumVnow - 1] = m_uIndex[p, mNumVnow - 2];

            while (true)
            {
                m_uIndex[p, mNumVnow - 1]++;
                if (m_uIndex[p, mNumVnow - 1] >= mNumV)
                    return false;

                matrixU = new double[lNumVnow, lNumVnow];
                for (int i = 0; i < lNumVnow; i++)
                {
                    for (int j = 0; j < lNumVnow; j++)
                    {
                        for (int z = 0; z < mInitNumDataSet; z++)
                            matrixU[i, j] += u[m_uIndex[p, i], z] * u[m_uIndex[p, j], z];
                    }
                }
                rankU = RankOfU(matrixU, lNumVnow);

                if (rankU == lNumVnow)
                    break;
            }
            return true;
        }

        public bool CalcStep4(int ySelectedIndex = 0)
        {
            //SaveLog(">>CalcStep4()");
            int p = mNthVset;
            int attentionY = mAttentionY[ySelectedIndex];
            if (attentionY < 0) attentionY = 0;

            matrixY = new double[mNumVnow];
            matrixU = new double[mNumVnow, mNumVnow];

            //string strlog = "";
            //strlog += "MatrixY[] : \r\n";
            //StreamWriter wwr = new StreamWriter("matrixY.csv");
            for (int z = 0; z < mInitNumDataSet; z++)
            {
                for (int i = 0; i < mNumVnow; i++)
                {
                    matrixY[i] += u[m_uIndex[p, i], z] * mDataY[attentionY, z];
                    //wwr.Write(u[m_uIndex[p, i], z].ToString("E7") + "," + mDataY[attentionY, z].ToString("E7") + ",");
                }
                //wwr.Write("\r\n");
            }
            //wwr.Close();

            //MessageBox.Show("CCC");
            //for (int i = 0; i < mNumVnow; i++)
            //    strlog += matrixY[i].ToString("E7") + "\t";
            //strlog += "\r\nMatrixU[" + mNumVnow.ToString() + "] : \r\n";
            for (int i = 0; i < mNumVnow; i++)
            {
                for (int j = 0; j < mNumVnow; j++)
                {
                    for (int z = 0; z < mInitNumDataSet; z++)
                        matrixU[i, j] += u[m_uIndex[p, i], z] * u[m_uIndex[p, j], z];
                    //strlog += matrixU[i, j].ToString("E7") + "\t";
                }
                //strlog += "\r\n";
            }
            //MessageBox.Show("DDD");
            if (RankOfU(matrixU, mNumVnow) < mNumVnow)
            {
                //SaveLog("Lack of Rank");
                return false;
            }

            //MessageBox.Show("EEE");
            InverseU(ref matrixU, mNumVnow);
            //strlog += "\r\nInv matrixU[" + mNumVnow.ToString() + "] : \r\n";
            //for (int i = 0; i < mNumVnow; i++)
            //{
            //    for (int j = 0; j < mNumVnow; j++)
            //    {
            //        strlog += matrixU[i, j].ToString("E7") + "\t";
            //    }
            //    strlog += "\r\n";
            //}
            matrixC = new double[mNumVnow];
            MatrixCross(ref matrixU, ref matrixY, ref matrixC, mNumVnow);

            //strlog = "> Equation Coefs\r\n";
            //for (int i = 0; i < mNumVnow; i++)
            //{
            //    strlog += "\t" + m_uIndex[p, i].ToString() + "\t" + uIDreadable[m_uIndex[p, i]].ToString() + "\t" + matrixC[i].ToString("E4") + "\r\n";
            //}
            //SaveLog(strlog);
            return true;
        }
        public int RankOfU(double[,] mbym, int dim)
        {
            int rank = 0;
            double[,] mat = new double[dim, dim];

            for (int i = 0; i < dim; i++)
                for (int j = 0; j < dim; j++)
                    mat[i, j] = mbym[i, j]; // coefficients of our matrix

            for (int k = 0; k < dim; k++)
            {
                for (int i = k + 1; i < dim; i++)
                {
                    // Check if divider is equal to zero
                    if (mat[k, k] == 0)
                    {
                        // It is - pivot row
                        mat = RowPivot(mat, k);
                    }

                    double c = mat[i, k] / mat[k, k];

                    for (int k1 = 0; k1 < dim; k1++)
                    {
                        mat[i, k1] = mat[i, k1] - mat[k, k1] * c;
                    }
                }

                // Check if created row's elements sum is non-zero
                double sum = 0;

                for (int i = 0; i < dim; i++)
                {
                    sum += mat[k, i];
                }
                if (sum != 0) { rank++; } // Increase rank if sum of new row is non-zero.
            }
            //SaveLog(">>RankOfU() : " + rank.ToString());
            return rank;
        }
        private double[,] RowPivot(Double[,] matrix, int k)
        {
            // k - starting row to search for non-zero element
            for (int i = k + 1; i < matrix.GetLength(0); i++)
            {
                if (matrix[i, i] != 0)
                {
                    double[] x = new double[matrix.GetLength(1)];

                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        x[j] = matrix[k, j];
                        matrix[k, j] = matrix[i, j];
                        matrix[i, j] = x[j];
                    }
                    break;
                }
            }
            return matrix;
        }

        public void InverseU(ref double[,] invM, int dim)
        {
            //  Calculate Inverse Matrix of matrixU and save result to invM
            //double[,] copySrc = new double[dim, dim];

            //for (int i = 0; i < dim; ++i) // copy the values
            //    for (int j = 0; j < dim; ++j)
            //        copySrc[i, j] = invM[i, j];

            //double[,] result = MatrixInverse(copySrc, dim);
            //for (int i = 0; i < dim; ++i) // copy the values
            //    for (int j = 0; j < dim; ++j)
            //        invM[i, j] = result[i, j];

            int info;
            alglib.matinvreport rep;
            alglib.rmatrixinverse(ref invM, out info, out rep);
        }
        public void MatrixCross(ref double[,] dimbydim, ref double[] dimby1, ref double[] res, int dim)
        {
            //  Calculate [ dim x dim ] x [ dim ]
            for (int i = 0; i < dim; i++)
            {
                res[i] = 0;
                for (int j = 0; j < dim; j++)
                {
                    res[i] += dimbydim[i, j] * dimby1[j];
                }
            }
        }
        public void MatrixCross(ref double[,] dimbydimL, ref double[,] dimbydimR, ref double[,] dimbydimRes, int dim)
        {
            //  Calculate [ dim x dim ] x [ dim ]
            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    dimbydimRes[i, j] = 0;
                    for (int k = 0; k < dim; k++)
                        dimbydimRes[i, j] += dimbydimL[i, k] * dimbydimR[k, j];
                }
            }
        }
        public double MatrixInnerProduct(ref double[] dimL, ref double[] dimR, int dim)
        {
            //  Calculate [ dim x dim ] x [ dim ]
            double res = 0;
            for (int i = 0; i < dim; i++)
            {
                res += dimL[i] * dimR[i];
            }
            return res;
        }
        public void GetPP(ref double[] pp, int numRow, int numData)
        {
            double min = 0;
            double max = 0;

            int p = mNthVset;

            for (int i = 0; i < numRow; i++)
            {
                min = 999999999;
                max = -999999999;
                for (int j = 0; j < numData; j++)
                {
                    if (u[m_uIndex[p, i], j] < min)
                        min = u[m_uIndex[p, i], j];
                    if (u[m_uIndex[p, i], j] > max)
                        max = u[m_uIndex[p, i], j];
                }
                pp[i] = max - min;
            }
        }

        public void RunNonstopProcess()
        {
            m_bNonStopProcessDone = false;
            ResetLog();
            int nPort = GetRealParameters();
            for (int i = 0; i < nPort; i++)
                NonStopProcess(i);
            m_bNonStopProcessDone = true;
        }
        public bool NonStopProcess(int iPort = 0, bool IsOptimize = true)
        {
            //CalcStep1();
            SaveLog(">DistParams[" + iPort + "] : " + mMarkDistNear[iPort].ToString("F3") + "\t" + mMarkDistFar[iPort].ToString("F3") + "\t" + mMarkDistDecenter[iPort].ToString("F3"));
            GenerateSourceData(mMarkDistNear[iPort], mMarkDistFar[iPort], mMarkDistDecenter[iPort]);
            int ySelectedIndex = 0;
            int p = 0;
            int nestedCount = 0;
            int itrCount = 0;
            bool res = true;

            if (IsOptimize)
                mProcessMsg = "";

            ApplyCondition(true);

            while (true)
            {
                p = 0;
                nestedCount = 0;
                m_uIndex[p, 0] = 0;
                m_uIndex[p, 1] = 1;
                for (int qq = 2; qq < mNumV; qq++)
                {
                    m_uIndex[p, qq] = 0;
                    m_uIndex[p, qq] = 0;
                }
                mNumVnow = 2;
                itrCount = 0;
                mNthVset = p;
                CalcStep1();
                if (mAttentionY[ySelectedIndex] > -1)
                {
                    mProcessMsg += ">>For Y " + ySelectedIndex + "\r\n";
                    SaveLog(mProcessMsg);
                    CalcStep2(ref p, ref nestedCount, ySelectedIndex);
                    CalcStep3();
                    CalcStep4(ySelectedIndex);

                    res = true;

                    while (true)
                    {
                        mErrHistory[itrCount] = CalcRssError(ySelectedIndex);
                        mProcessMsg = "RSS Error \t" + mErrHistory[itrCount].ToString("E5") + " \titrCount = \t" + itrCount.ToString() + " \t mNumVnow \t" + mNumVnow + "\r\n";
                        SaveLog(mProcessMsg);

                        if (IsOptimize && (mErrHistory[itrCount] < mErrThreshold))
                        {
                            SaveLog("Finish Good");
                            break;
                        }

                        if (++itrCount > 99)
                        {
                            SaveLog("Finish 0");
                            break;
                        }

                        if (mNumVnow < mMaxDimensionOfEquation)
                        {
                            if (itrCount < 2)
                                SaveLog("Call CalcStep3");
                            if (m_uIndex[mNthVset, mNumVnow] >= mNumV - 1)
                            {
                                mProcessMsg = "No more uIndex\r\n";
                                break;
                            }

                            if (CalcStep3())    //  CalcStep3() : rank 가 맞는 변수를 추가하는 프로세스
                            {
                                //CalcStep4();
                                CalcStep4(ySelectedIndex);  //  CalcStep4() : 주어진 변수로부터 LMS 계수를 구하는 프로세스
                                mErrHistory[itrCount] = CalcRssError(ySelectedIndex);
                                SaveLog("Old Err = " + mErrHistory[itrCount - 1].ToString("E5") + "\tNew Err = " + mErrHistory[itrCount].ToString("E5"));

                                if (mNumVnow > 2)
                                {
                                    if (mErrHistory[itrCount] > mErrHistory[itrCount - 1])
                                    {
                                        // Error 가 너무 커진 경우 마지막으로 추가된 변수를 제거해야 한다.
                                        if (itrCount < 2)
                                            SaveLog("Remove Lastly Added Vars");
                                        RemLastAddedVars();
                                        //CalcStep3();
                                        CalcStep4(ySelectedIndex);
                                        continue;
                                    }
                                    else
                                    {
                                        if (itrCount < 2)
                                            SaveLog("Call RemUnsensMinSensVars A");
                                        RemUnsensMinSensVars(ySelectedIndex);
                                    }
                                }
                            }
                        }
                        if (IsOptimize && (mNumVnow > 8) && mErrHistory[itrCount] > mErrHistory[itrCount - 1])
                        {
                            if (itrCount < 2)
                                SaveLog("Call RemUnsensMinSensVars B");
                            res = RemUnsensMinSensVars(ySelectedIndex);
                        }
                        else if (!IsOptimize && (mNumVnow == mMaxDimensionOfEquation))
                        {
                            SaveLog("Finish 1");
                            break;
                        }

                        if (mProcessMsg == "No more uIndex")
                        {
                            SaveLog("Finish 2");
                            break;
                        }
                        if (mNumVnow > mMaxDimensionOfEquation)
                        {
                            SaveLog("Finish 3");
                            break;
                        }
                    }
                    if (mProcessMsg != "No more uIndex")
                        RemUnsensMinSensVars(ySelectedIndex);
                    //else
                    //    p--;

                    //mProcessMsg += ">Found Solution\r\n";

                    string strlog = p.ToString() + "> Final Equation Coefs : Port " + iPort.ToString() + " Yindex " + ySelectedIndex.ToString() + " " + mNumVnow + " \r\n";
                    double lLastErr = CalcRssError(ySelectedIndex);
                    mProcessMsg += "RSS Error \t" + lLastErr.ToString("E5") + " \titrCount = \t" + itrCount.ToString() + " \t mNumVnow \t" + mNumVnow + "\r\n";
                    for (int i = 0; i < mNumVnow; i++)
                    {
                        strlog += m_uIndex[p, i].ToString() + "\t" + uIDreadable[m_uIndex[p, i]].ToString() + "\t" + matrixC[i].ToString("E4") + "\t::";
                        strlog += (uIDreadable[m_uIndex[p, i]] / 1000).ToString()
                                  + ":" + ((uIDreadable[m_uIndex[p, i]] / 100) % 10).ToString()
                                  + ":" + ((uIDreadable[m_uIndex[p, i]] / 10) % 10).ToString()
                                  + ":" + (uIDreadable[m_uIndex[p, i]] % 10).ToString();
                        strlog += "\r\n";
                    }
                    mProcessMsg += strlog;
                    mResultMsg[iPort * 2 + ySelectedIndex] = mProcessMsg;
                    SaveLog(mProcessMsg);
                    mProcessMsg = "";
                    if (ySelectedIndex == 0)
                    {
                        for (int i = 0; i < mNumVnow; i++)
                        {
                            m_LastUIndex[iPort, i] = m_uIndex[p, i];
                            matrixC_U[iPort, i] = matrixC[i];
                        }
                        mExtNumVnow[iPort, 0] = mNumVnow;

                    }
                    else if (ySelectedIndex == 1)
                    {
                        for (int i = 0; i < mNumVnow; i++)
                        {
                            m_LastVIndex[iPort, i] = m_uIndex[p, i];
                            matrixC_V[iPort, i] = matrixC[i];
                        }
                        mExtNumVnow[iPort, 1] = mNumVnow;
                    }
                    ySelectedIndex++;
                }
                else
                    break;
            }

            return true;
        }
        //public void CopyCoefficients(ref double[,] mu, ref double[,] mv, ref int[,] ui, ref int[,] vi, ref int[,] Nv, ref int[] uid, bool Is34To12 = false)
        //{
        //    for (int i = 0; i < uid.Length; i++)
        //        uIDreadable[i] = uid[i];

        //    if (!Is34To12)
        //    {
        //        for (int iPort = 0; iPort < 4; iPort++)
        //        {
        //            for (int i = 0; i < Nv[iPort, 0]; i++)
        //            {
        //                m_LastUIndex[iPort, i] = ui[iPort, i];
        //                matrixC_U[iPort, i] = mu[iPort, i];
        //            }
        //            mExtNumVnow[iPort, 0] = Nv[iPort, 0];
        //            for (int i = 0; i < Nv[iPort, 1]; i++)
        //            {
        //                m_LastVIndex[iPort, i] = vi[iPort, i];
        //                matrixC_V[iPort, i] = mv[iPort, i];
        //            }
        //            mExtNumVnow[iPort, 1] = Nv[iPort, 1];
        //        }
        //    }
        //    else
        //    {

        //        for (int iPort = 2; iPort < 4; iPort++)
        //        {
        //            for (int i = 0; i < Nv[iPort, 0]; i++)
        //            {
        //                m_LastUIndex[iPort - 2, i] = ui[iPort, i];
        //                matrixC_U[iPort - 2, i] = mu[iPort, i];
        //            }
        //            mExtNumVnow[iPort - 2, 0] = Nv[iPort, 0];
        //            for (int i = 0; i < Nv[iPort, 1]; i++)
        //            {
        //                m_LastVIndex[iPort - 2, i] = vi[iPort, i];
        //                matrixC_V[iPort - 2, i] = mv[iPort, i];
        //            }
        //            mExtNumVnow[iPort - 2, 1] = Nv[iPort, 1];
        //        }
        //    }
        //}
        public double CalcRssError(int ySelectedIndex = 0)
        {
            double res = 0;
            double err = 0;
            double curValue = 0;
            int p = mNthVset;
            double ideal = 0;
            int attentionY = mAttentionY[ySelectedIndex];
            if (attentionY < 0) attentionY = 0;

            //StreamWriter wr;
            //string rssFile = "RssErr_" + attentionY.ToString() + ".csv";
            //wr = new StreamWriter(rssFile);
            for (int z = 0; z < mInitNumDataSet; z++)
            {
                curValue = 0;
                for (int j = 0; j < mNumVnow; j++)
                {
                    curValue += matrixC[j] * u[m_uIndex[p, j], z];
                }
                err = curValue - mDataY[attentionY, z];
                //wr.WriteLine(curValue.ToString("E6") + "," + mDataY[attentionY, z].ToString("E6") + "," + err.ToString("E6"));
                res += (err * err);
                ideal += mDataY[attentionY, z] * mDataY[attentionY, z];
            }
            //wr.Close();

            res = Math.Sqrt(res / mInitNumDataSet) / Math.Sqrt(ideal / mInitNumDataSet);

            //SaveLog("RssError = " + res.ToString("F5"));
            return res;
        }

        public void CalcVarSensitivity(ref double[] lsens, bool IsShowLog = true)
        {
            //SaveLog(">>CalcVarSensitivity()");
            double[] pp = new double[mNumVnow];
            GetPP(ref pp, mNumVnow, mInitNumDataSet);
            int p = mNthVset;

            for (int i = 0; i < mNumVnow; i++)
            {
                lsens[i] = matrixC[i] * pp[i];
            }
            if (IsShowLog)
            {
                string strlog = "\r\n>>CalcVarSensitivity()y\r\n";
                for (int i = 0; i < mNumVnow; i++)
                {
                    strlog += m_uIndex[p, i].ToString() + "\t" + uIDreadable[m_uIndex[p, i]].ToString() + "\t" + lsens[i].ToString("E5") + "\r\n";
                }
                SaveLog(strlog);
            }
        }

        public void RemUnsensVars(int ySelectedIndex = 0)
        {
            SaveLog(">>RemUnsensVars ");

            int p = mNthVset;
            double[] lsens = new double[mNumVnow];

            //CalcVarSensitivity(ref lsens, false);
            int lNumVnow = mNumVnow;
            bool[] l_uOn = new bool[mNumVnow];
            int[] l_uIndex = new int[mNumVnow];
            for (int i = 0; i < lNumVnow; i++)
            {
                if (Math.Abs(matrixC[i]) < 1.0e-22)
                    l_uOn[i] = false;
                else
                    l_uOn[i] = true;
            }
            int k = 0;
            for (int i = 0; i < lNumVnow; i++)
            {
                if (l_uOn[i])
                    l_uIndex[k++] = m_uIndex[p, i];
            }
            for (int i = 0; i < k; i++)
            {
                m_uIndex[p, i] = l_uIndex[i];
            }


            mNumVnow = k;
            matrixY = new double[mNumVnow];
            matrixC = new double[mNumVnow];
            matrixU = new double[mNumVnow, mNumVnow];

            for (int i = 0; i < mNumVnow; i++)
            {
                for (int j = 0; j < mNumVnow; j++)
                {
                    for (int z = 0; z < mInitNumDataSet; z++)
                        matrixU[i, j] += u[m_uIndex[p, i], z] * u[m_uIndex[p, j], z];
                }
            }
            CalcStep4(ySelectedIndex);
        }

        public bool RemUnsensMinSensVars(int ySelectedIndex = 0)
        {
            SaveLog(">>RemUnsensMinSensVars()");
            int p = mNthVset;
            double[] lsens = new double[mNumVnow];

            CalcVarSensitivity(ref lsens, false);   // CalcVarSensitivity() : 각 변수별 감도를 구한다.

            //  감도가 일정값 이하인 변수를 제거한다.
            //  감도가 가장 낮은 변수를 제거한다.

            int lNumVnow = mNumVnow;
            bool[] l_uOn = new bool[mNumVnow];
            int[] l_uIndex = new int[mNumVnow];
            double lminSens = 999999;
            //double lminSens2 = 999999;
            int lminSensIndex = -1;
            int[] lminSensIndex2 = new int[100];
            int count2nd = 0;
            for (int i = 0; i < lNumVnow; i++)
            {
                if (uOn[m_uIndex[p, i]] > 0)
                    l_uOn[i] = true;
                if (i < 4)
                {
                    if (matrixC[i] < 1.0e-10)
                    {
                        lminSens = Math.Abs(lsens[i]);
                        lminSensIndex = i;
                    }
                }
                else if (i < 14)
                {
                    if (matrixC[i] < 1.0e-14)
                    {
                        lminSens = Math.Abs(lsens[i]);
                        lminSensIndex = i;
                    }
                }
                else if (i < 34)
                {
                    if (matrixC[i] < 1.0e-17)
                    {
                        lminSens = Math.Abs(lsens[i]);
                        lminSensIndex = i;
                    }
                }
                //if (m_uIndex[p, i] == 3) continue;
                if ((Math.Abs(lsens[i]) < lminSens) && (lNumVnow > 5))
                {
                    lminSens = Math.Abs(lsens[i]);
                    lminSensIndex = i;
                }
                if (Math.Abs(lsens[i]) < mMinSensitivity)
                {
                    lminSensIndex2[count2nd] = i;
                    count2nd++;
                }
            }
            if (count2nd == 0 && lminSensIndex < 0)
            {
                return false;
            }
            //SaveLog(" B removed : " + lminSensIndex.ToString());

            string lstr = "count2nd= " + count2nd.ToString() + "\r\n";
            if (lminSensIndex >= 0 && lminSens < (mMinSensitivity * 5) && count2nd == 0)
            {
                lstr += "Removed lminSensIndex= " + lminSensIndex.ToString() + "\r\n";
                l_uOn[lminSensIndex] = false;
                uOn[m_uIndex[p, lminSensIndex]] = -1;
            }
            for (int i = 0; i < count2nd; i++)
            {
                SaveLog("Removed Index: " + i.ToString());
                l_uOn[lminSensIndex2[i]] = false;
                uOn[m_uIndex[p, lminSensIndex2[i]]] = -1;
            }
            int k = 0;
            for (int i = 0; i < lNumVnow; i++)
            {
                //SaveLog(" l_uOn[" + i.ToString() +"]=" + l_uOn[i].ToString());
                if (l_uOn[i])
                    l_uIndex[k++] = m_uIndex[p, i];
            }
            mNumVnow = k;
            lstr += "reduced mNumVnow= " + mNumVnow.ToString() + "\r\n";
            for (int i = 0; i < k; i++)
            {
                m_uIndex[p, i] = l_uIndex[i];
                lstr += m_uIndex[p, i].ToString() + "-";
            }
            SaveLog(lstr);
            matrixY = new double[mNumVnow];
            //matrixC = new double[mNumVnow];
            matrixU = new double[mNumVnow, mNumVnow];
            bool res = true;
            res = CalcStep3();    //  변수 1개 추가
            if (!res)
                return res;
            res = CalcStep4(ySelectedIndex);
            if (!res)
                return res;

            return true;
        }
        public void RemLastAddedVars()
        {
            uOn[m_uIndex[mNthVset, mNumVnow - 1]] = -1;

            mNumVnow--;
            int p = mNthVset;

            matrixY = new double[mNumVnow];
            matrixU = new double[mNumVnow, mNumVnow];

            for (int i = 0; i < mNumVnow; i++)
            {
                for (int j = 0; j < mNumVnow; j++)
                {
                    for (int z = 0; z < mInitNumDataSet; z++)
                        matrixU[i, j] += u[m_uIndex[p, i], z] * u[m_uIndex[p, j], z];
                }
            }
        }

        public void ResetLog()
        {
            if (File.Exists("GMVLR_log.txt"))
                File.Delete("GMVLR_log.txt");
        }
        public void SaveLog(string lstr)
        {
            StreamWriter wr = File.AppendText("GMVLR_log.txt");
            wr.WriteLine(lstr);
            wr.Close();
        }
        static double[,] MatrixInverse(double[,] matrix, int dim)
        {
            int n = dim;
            double[,] result = new double[dim, dim];

            for (int i = 0; i < dim; ++i) // copy the values
                for (int j = 0; j < dim; ++j)
                    result[i, j] = matrix[i, j];

            int[] perm;
            int toggle;
            double[,] lum = MatrixDecompose(matrix, out perm, out toggle, dim);
            if (lum == null)
                throw new Exception("Unable to compute inverse");

            double[] b = new double[n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == perm[j])
                        b[j] = 1.0;
                    else
                        b[j] = 0.0;
                }
                double[] x = HelperSolve(lum, b, dim); // 

                for (int j = 0; j < n; j++)
                    result[j, i] = x[j];
            }
            return result;
        }

        static double[,] MatrixDecompose(double[,] matrix, out int[] perm, out int toggle, int dim)
        {
            // Doolittle LUP decomposition with partial pivoting.
            // rerturns: result is L (with 1s on diagonal) and U;
            // perm holds row permutations; toggle is +1 or -1 (even or odd)
            int rows = dim;
            int cols = dim; // assume square
            int n = dim; // convenience

            double[,] result = new double[dim, dim];

            for (int i = 0; i < dim; ++i) // copy the values
                for (int j = 0; j < dim; ++j)
                    result[i, j] = matrix[i, j];

            perm = new int[n]; // set up row permutation result
            for (int i = 0; i < n; i++) { perm[i] = i; }

            toggle = 1; // toggle tracks row swaps.
                        // +1 -greater-than even, -1 -greater-than odd. used by MatrixDeterminant

            for (int j = 0; j < n - 1; j++) // each column
            {
                double colMax = Math.Abs(result[j, j]); // find largest val in col
                int pRow = j;
                // reader Matt V needed this:
                for (int i = j + 1; i < n; ++i)
                {
                    if (Math.Abs(result[i, j]) > colMax)
                    {
                        colMax = Math.Abs(result[i, j]);
                        pRow = i;
                    }
                }
                // Not sure if this approach is needed always, or not.

                if (pRow != j) // if largest value not on pivot, swap rows
                {
                    double[] rowTmp = new double[n];
                    for (int k = 0; k < n; k++)
                        rowTmp[k] = result[pRow, k];

                    for (int k = 0; k < n; k++)
                    {
                        result[pRow, k] = result[j, k];
                        result[j, k] = rowTmp[k];
                    }

                    int tmp = perm[pRow]; // and swap perm info
                    perm[pRow] = perm[j];
                    perm[j] = tmp;

                    toggle = -toggle; // adjust the row-swap toggle
                }

                // --------------------------------------------------
                // This part added later (not in original)
                // and replaces the 'return null' below.
                // if there is a 0 on the diagonal, find a good row
                // from i = j+1 down that doesn't have
                // a 0 in column j, and swap that good row with row j
                // --------------------------------------------------

                if (result[j, j] == 0.0)
                {
                    // find a good row to swap
                    int goodRow = -1;
                    for (int row = j + 1; row < n; ++row)
                    {
                        if (result[row, j] != 0.0)
                            goodRow = row;
                    }

                    if (goodRow == -1)
                        throw new Exception("Cannot use Doolittle's method");

                    // swap rows so 0.0 no longer on diagonal
                    //double[] rowPtr = result[goodRow];
                    //result[goodRow] = result[j];
                    //result[j] = rowPtr;

                    double[] rowTmp = new double[n];
                    for (int k = 0; k < n; k++)
                        rowTmp[k] = result[goodRow, k];

                    for (int k = 0; k < n; k++)
                    {
                        result[goodRow, k] = result[j, k];
                        result[j, k] = rowTmp[k];
                    }


                    int tmp = perm[goodRow]; // and swap perm info
                    perm[goodRow] = perm[j];
                    perm[j] = tmp;

                    toggle = -toggle; // adjust the row-swap toggle
                }
                // --------------------------------------------------
                // if diagonal after swap is zero . .
                //if (Math.Abs(result[j][j]) less-than 1.0E-20) 
                //  return null; // consider a throw

                for (int i = j + 1; i < n; ++i)
                {
                    result[i, j] /= result[j, j];
                    for (int k = j + 1; k < n; ++k)
                    {
                        result[i, k] -= result[i, j] * result[j, k];
                    }
                }
            } // main j column loop

            return result;
        } // MatrixDecompose

        static double[] HelperSolve(double[,] luMatrix, double[] b, int dim)
        {
            // before calling this helper, permute b using the perm array
            // from MatrixDecompose that generated luMatrix
            int n = dim;
            double[] x = new double[n];
            b.CopyTo(x, 0);

            for (int i = 1; i < n; i++)
            {
                double sum = x[i];
                for (int j = 0; j < i; ++j)
                    sum -= luMatrix[i, j] * x[j];
                x[i] = sum;
            }
            x[n - 1] /= luMatrix[n - 1, n - 1];
            for (int i = n - 2; i >= 0; i--)
            {
                double sum = x[i];
                for (int j = i + 1; j < n; ++j)
                    sum -= luMatrix[i, j] * x[j];
                x[i] = sum / luMatrix[i, i];
            }

            return x;
        }

        //public void GenerateCircleImage(double Initial_Theta, double Initial_Psi, double L1cx, double L1cy, double L2cx, double L2cy, double lnear, double lfar, double ldecenter)
        //{
        //    string srcFile = "";

        //    double L1_L2_InitDecenter = ldecenter;

        //    double[] L1 = new double[3] { 3.54, 0, 9.0 }; //    Near
        //    double[] L2 = new double[3] { 4.68, 0, 12.0 }; //    Far 

        //    //double Initial_Theta = 135.0; //  135 는 135deg 로써 중심상태
        //    //double Initial_Psi = 0; //  0 는 0deg 로써 중심상태

        //    double[] Xtheta = new double[50000];
        //    double[] Xpsi = new double[50000];
        //    double[] Xheight = new double[50000];

        //    L1[2] = lnear;
        //    L2[2] = lfar;

        //    if (srcFile == "")
        //    {
        //        srcFile = "C:\\FZ4PTest\\DoNotTouch\\CircleSrc_" + (L1cx * 1000).ToString("F0") + "_" + (L1cy * 1000).ToString("F0") + ".txt";
        //    }

        //    // center of prism surface

        //    int k = 0;
        //    double h, M_PI;
        //    double[] N0 = new double[3] { 0, 0, 0 };
        //    double[] N0res = new double[3] { 0, 0, 0 };
        //    double[] N0org = new double[3] { 0, 0, 0 };
        //    double[,] Rr = new double[3, 3];
        //    double[,] R = new double[3, 3];
        //    double[,] u = new double[4, 50000];
        //    double[,] u_tmp = new double[4, 50000];
        //    double[] uErr1 = new double[50000];
        //    double[] uErr2 = new double[50000];

        //    double[] Rc = new double[3] { 0, 0, 0 };
        //    //  for SZ3720/3721/3720E   { -3.2, 0, 0};
        //    //  for SZ2780/Sz3750       { -3.3, 0, 0};
        //    //  for SZ4460/4460E        { -2.4, 0, 0};
        //    //  for SZ2790              { 0, 0, 0};

        //    double[] Xi = new double[3] { 1, 0, 0 };
        //    double[] Xo = new double[3];
        //    double[] Nr = new double[3];
        //    double[] N0_L1 = new double[3];
        //    double[] N0_L2 = new double[3];
        //    double[] Pr1 = new double[3];
        //    double[] Pr2 = new double[3];
        //    double[] X1 = new double[2];
        //    double[] X2 = new double[2];
        //    double[] X1org = new double[2];
        //    double[] X2org = new double[2];
        //    double ct = 0;
        //    double st = 0;
        //    double cp = 0;
        //    double sp = 0;
        //    double[,] Ry = new double[3, 3];
        //    double[,] Rz = new double[3, 3];
        //    double ii;

        //    double[] surfNi0 = new double[3] { 1, 0, 0 };
        //    double[] surfNr0 = new double[3] { 1, 0, 1 };   //  {0.707107, 0, 0.707107};
        //    double[] surfNo0 = new double[3] { 0, 0, 1 };

        //    double[] surfPi0 = new double[3] { 2.54, 0, 0 };    //  mm
        //    double[] surfPo0 = new double[3] { 0, 0, 2.54 };    //  mm
        //    double[] beamV = new double[3] { 1, 0, 0 };
        //    double rIndex = 1.0;
        //    //double[] markP0 = new double[3] { 0, 0, 8.5 };  //  Near Mark   L1
        //    //double[] markP1 = new double[3] { 0, 0, 11.0 }; //  Far Mark    L2

        //    double[] resMarkP = new double[3];
        //    double[] resMmirror1 = new double[3];
        //    double[] resMmirror2 = new double[3];

        //    double[] imgP_L1 = new double[3] { 10, 0.0, 0.0 };
        //    double[] imgP_L2 = new double[3] { 10, 0.0, 0.0 };
        //    double[] comp_imgP_L1 = new double[3];
        //    double[] comp_imgP_L2 = new double[3];
        //    //

        //    M_PI = Math.Asin(1) * 2;
        //    for (int hi = 0; hi < 1; hi++)
        //    {
        //        h = hi * 0.06;
        //        N0[0] = h / 1.41421356; N0[1] = 0; N0[2] = h / 1.41421356;
        //        for (double xi = 0; xi <= 360; xi += 10)
        //        {
        //            L1[0] = L1cx + 0.75 * Math.Cos(xi / 180.0 * 3.14159265);
        //            L1[1] = L1cy + 0.75 * Math.Sin(xi / 180.0 * 3.14159265);
        //            L2[0] = L2cx + 0.1 * Math.Cos(xi / 180.0 * 3.14159265);
        //            L2[1] = L2cy + 0.1 * Math.Sin(xi / 180.0 * 3.14159265);
        //            double Xshift = 0;
        //            cp = 0;
        //            for (double yi = 0; yi <= 0; yi += 1)
        //            {
        //                double Yshift = yi;

        //                double theta = Initial_Theta / 180.0 * M_PI;
        //                double psi = Initial_Psi / 180.0 * M_PI;

        //                RotationR(theta, psi, ref R);

        //                MatrixCross(ref R, ref Xi, ref Nr, 3);//Nr = R * Xi;

        //                // reflection beam direction vector XoXi
        //                // Xo = 2 * (Xi'*Nr)*Nr - Xi; // ( Xo + Xi ) / 2 = (Xi' * Nr)*Nr
        //                double tmpXiNr = MatrixInnerProduct(ref Xi, ref Nr, 3);
        //                for (int w = 0; w < 3; w++)
        //                    Xo[w] = 2 * tmpXiNr * Nr[w] - Xi[w];

        //                ///////////////////////////////////////////////////////////////////
        //                ///////////////////////////////////////////////////////////////////
        //                //  Rotate N0 by R with Offset Rc
        //                // Surface Equation : N * ( P - P0 ) = 0 ; P0 = (0,0,0)
        //                theta = theta - 135.0 / 180 * M_PI; //  여기 135 는 절대 불변 회전축 Offset 에해 회전하면서 반사점의 이동을 계산

        //                RotationR(theta, psi, ref Rr);

        //                //N0 = Rr * (N0org - Rc)' + Rc';
        //                for (int w = 0; w < 3; w++)
        //                    N0res[w] = N0org[w] - Rc[w];

        //                MatrixCross(ref Rr, ref N0res, ref N0, 3);
        //                for (int w = 0; w < 3; w++)
        //                    N0[w] = N0[w] + Rc[w];

        //                ///////////////////////////////////////////////////////////////////
        //                ///////////////////////////////////////////////////////////////////

        //                // Cross Point
        //                //t1 = ((N0 - L1) * Nr) / (Xo' * Nr );
        //                for (int w = 0; w < 3; w++)
        //                    N0_L1[w] = N0[w] - L1[w];

        //                double tmpN0L1Nr = MatrixInnerProduct(ref N0_L1, ref Nr, 3);
        //                double tmpXoNr = MatrixInnerProduct(ref Xo, ref Nr, 3);
        //                double t1 = tmpN0L1Nr / tmpXoNr;


        //                for (int w = 0; w < 3; w++)
        //                    Pr1[w] = t1 * Xo[w] + L1[w];


        //                for (int w = 0; w < 3; w++)
        //                    N0_L2[w] = N0[w] - L2[w];


        //                double tmpN0L2Nr = MatrixInnerProduct(ref N0_L2, ref Nr, 3);
        //                double t2 = tmpN0L2Nr / tmpXoNr;

        //                for (int w = 0; w < 3; w++)
        //                    Pr2[w] = t2 * Xo[w] + L2[w];

        //                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                ////  여기서부터는 상기좌표로부터 다시 굴절율을 고려한 프리즘을 통과하여 마크평면과 교차하는 점의 좌표를 계산하는 과정

        //                ////  `굴절율이 적용됬을 때 L1 이 센서면으로 투영되는 점  좌표 계산
        //                imgDPtoMarkDP(rIndex, Pr1, L1, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L1, ref comp_imgP_L1);
        //                //  `굴절율이 적용됬을 때 Pr2 를 투영시키는 마크평면상의 점 좌표 계산
        //                imgDPtoMarkDP(rIndex, Pr2, L2, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L2, ref comp_imgP_L2);

        //                for (int w = 0; w < 3; w++)
        //                {
        //                    Pr1[w] = comp_imgP_L1[w];
        //                    Pr2[w] = comp_imgP_L2[w];
        //                }

        //                // 여기까지가 프리즘 굴절율을 1.717 로 한 상태에서 카메라에서 얻어지는 좌표(um)
        //                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //                //  기준위치에서의 좌표값들
        //                X1[0] = 90.909 * Pr1[1];
        //                X1[1] = 90.909 * Pr1[2];  // Y -> imgX , Z -> imgY
        //                X2[0] = 90.909 * Pr2[1];
        //                X2[1] = 90.909 * Pr2[2];  // Y -> imgX , Z -> imgY
        //                X1org[0] = X1[0]; // 40도 일 때의 근마크의 영상좌표
        //                X1org[1] = X1[1]; // 40도 일 때의 근마크의 영상좌표
        //                X2org[0] = X2[0]; // 40도 일 때의 원마크의 영상좌표
        //                X2org[1] = X2[1]; // 40도 일 때의 원마크의 영상좌표


        //                //ii=0;
        //                theta = Initial_Theta / 180.0 * M_PI;
        //                psi = Initial_Psi / 180.0 * M_PI;

        //                Xtheta[k] = 0;
        //                Xpsi[k] = 0;
        //                Xheight[k] = h;

        //                RotationR(theta, psi, ref R);

        //                // Source beam direction Vector
        //                //Xi = [1 0 0]';

        //                // Reflection Surface direction vector Nr
        //                // Surface Equation : N * ( P - P0 ) = 0 ; P0 = (0,0,0)
        //                //Nr = R * Xi;
        //                MatrixCross(ref R, ref Xi, ref Nr, 3);

        //                // reflection beam direction vector XoXi
        //                // Xo = 2 * (Xi'*Nr)*Nr - Xi; // ( Xo + Xi ) / 2 = (Xi' * Nr)*Nr
        //                tmpXiNr = MatrixInnerProduct(ref Xi, ref Nr, 3);
        //                for (int w = 0; w < 3; w++)
        //                    Xo[w] = 2 * tmpXiNr * Nr[w] - Xi[w];

        //                ///////////////////////////////////////////////////////////////////
        //                ///////////////////////////////////////////////////////////////////
        //                //  Rotate N0 by R with Offset Rc
        //                theta = theta - 135.0 / 180 * M_PI; //  여기 135 는 절대 불변 회전축 Offset 에해 회전하면서 반사점의 이동을 계산

        //                RotationR(theta, psi, ref Rr);

        //                //N0 = Rr * (N0org - Rc)' + Rc';
        //                for (int w = 0; w < 3; w++)
        //                    N0res[w] = N0org[w] - Rc[w];

        //                MatrixCross(ref Rr, ref N0res, ref N0, 3);
        //                for (int w = 0; w < 3; w++)
        //                    N0[w] = N0[w] + Rc[w];

        //                ///////////////////////////////////////////////////////////////////
        //                ///////////////////////////////////////////////////////////////////


        //                // Cross Point
        //                //t1 = ((N0 - L1) * Nr) / (Xo' * Nr );
        //                for (int w = 0; w < 3; w++)
        //                    N0_L1[w] = N0[w] - L1[w];
        //                tmpN0L1Nr = MatrixInnerProduct(ref N0_L1, ref Nr, 3);
        //                tmpXoNr = MatrixInnerProduct(ref Xo, ref Nr, 3);
        //                t1 = tmpN0L1Nr / tmpXoNr;


        //                //Pr1 = t1 * Xo' + L1;
        //                for (int w = 0; w < 3; w++)
        //                    Pr1[w] = t1 * Xo[w] + L1[w];

        //                //t2 = ((N0 - L2) * Nr) / (Xo' * Nr );
        //                for (int w = 0; w < 3; w++)
        //                    N0_L2[w] = N0[w] - L2[w];
        //                tmpN0L2Nr = MatrixInnerProduct(ref N0_L2, ref Nr, 3);
        //                t2 = tmpN0L2Nr / tmpXoNr;

        //                //Pr2 = t2 * Xo' + L2;
        //                for (int w = 0; w < 3; w++)
        //                    Pr2[w] = t2 * Xo[w] + L2[w];

        //                // 여기까지가 프리즘 굴절율을 1.0 으로 한 상태에서 카메라에서 얻어지는 좌표(mm)
        //                //  Pr2 : L2 로부터 굴절율 1.0 프리즘을 통과하여 sensor 면으로 투사된 점의 WCS 좌표
        //                //  Pr1 : L1 로부터 굴절율 1.0 프리즘을 통과하여 sensor 면으로 투사된 점의 WCS 좌표

        //                //  굴절율 0 일 때 Pr1 이 L1 에 매칭되는지 검증
        //                //ImgPtoMarkP(1.0, Pr1, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L1, ref resMmirror1);
        //                ////  굴절율 0 일 때 Pr2 이 L2 에 매칭되는지 검증
        //                //ImgPtoMarkP(1.0, Pr2, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L2, ref resMmirror2);

        //                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                ////  여기서부터는 상기좌표로부터 다시 굴절율을 고려한 프리즘을 통과하여 마크평면과 교차하는 점의 좌표를 계산하는 과정

        //                ////  `굴절율이 적용됬을 때 L1 이 센서면으로 투영되는 점  좌표 계산
        //                uErr1[k] = imgDPtoMarkDP(rIndex, Pr1, L1, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L1, ref comp_imgP_L1);
        //                //  `굴절율이 적용됬을 때 Pr2 를 투영시키는 마크평면상의 점 좌표 계산
        //                uErr2[k] = imgDPtoMarkDP(rIndex, Pr2, L2, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L2, ref comp_imgP_L2);

        //                for (int w = 0; w < 3; w++)
        //                {
        //                    Pr1[w] = comp_imgP_L1[w];
        //                    Pr2[w] = comp_imgP_L2[w];
        //                }

        //                // 여기까지가 프리즘 굴절율을 1.717 로 한 상태에서 카메라에서 얻어지는 좌표(um)
        //                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //                X1[0] = 90.909 * Pr1[1];
        //                X1[1] = 90.909 * Pr1[2];  // Y -> imgX , Z -> imgY
        //                X2[0] = 90.909 * Pr2[1];
        //                X2[1] = 90.909 * Pr2[2];  // Y -> imgX , Z -> imgY

        //                u_tmp[0, k] = X1[0];
        //                u_tmp[1, k] = X1[1];
        //                u_tmp[2, k] = X2[0];
        //                u_tmp[3, k] = X2[1];

        //                X1[0] = X1[0] - X1org[0];
        //                X1[1] = X1[1] - X1org[1];
        //                X2[0] = X2[0] - X2org[0];
        //                X2[1] = X2[1] - X2org[1];

        //                u[0, k] = X2[0] - X1[0];    //  X 방향 원마크이동량 - 근마크 이동량
        //                u[1, k] = X2[1] - X1[1];    //  Y 방향 원마크이동량 - 근마크 이동량
        //                u[2, k] = (X2org[0] - X1org[0]);// 40도 일 때의 원마크X좌표 빼기 근마크X좌표       
        //                u[3, k] = (X2org[1] - X1org[1]);// 40도 일 때의 원마크Y좌표 빼기 근마크Y좌표 에서 표준거리를 뺀 값
        //                k = k + 1;
        //            }
        //        }
        //    }
        //    StreamWriter wr = new StreamWriter(srcFile);
        //    //wr.WriteLine("dX\tdy\tdX0\tdY0\ttheta\tpsi\tH\tNear X\tNear Y\tFar X\tFar Y");
        //    for (int line = 0; line < k; line++)
        //        wr.WriteLine(u[0, line].ToString("F9") + "\t" + u[1, line].ToString("F9") + "\t" + u[2, line].ToString("F9") + "\t" + u[3, line].ToString("F9") + "\t" + Xtheta[line].ToString("F9") + "\t" + Xpsi[line].ToString("F9") + "\t" + Xheight[line].ToString("F9") + "\t" + u_tmp[0, line].ToString("F9") + "\t" + u_tmp[1, line].ToString("F9") + "\t" + u_tmp[2, line].ToString("F9") + "\t" + u_tmp[3, line].ToString("F9"));// + "\t" + uErr1[line].ToString("E5") + "\t" + uErr2[line].ToString("E5"));

        //    wr.Close();

        //    ReadDataFile(srcFile);
        //}

        private void GenerateSourceData(double lnear = 9, double lfar = 12, double ldecenter = 0)
        {
            string srcFile = "";

            double L1_L2_InitDecenter = ldecenter;

            //  X drv
            //// Near Mark for 18.5deg ~ 29.5deg
            //double[] L1 = new double[3] { -1.21, 2.82, 9.0 }; //    Near
            //double[] L2 = new double[3] { -1.66, 3.84, 12.0 }; //    Far 
            //double Initial_Theta = 135; //  135 는 135deg 로써 중심상태
            //double Initial_Psi = 18.5; //  0 는 0deg 로써 중심상태

            // Near Mark for 7.0deg ~ 19.0deg
            //double[] L1 = new double[3] { -0.35, 1.51, 9.0 }; //    Near
            //double[] L2 = new double[3] { -0.48, 2.07, 12.0 }; //    Far 
            //double Initial_Theta = 135; //  135 는 135deg 로써 중심상태
            //double Initial_Psi = 7.0; //  0 는 0deg 로써 중심상태

            // Near Mark for 18.5deg ~ 29.5deg , Y 7.0 ~ 11.0
            //double[] L1 = new double[3] { 0.51, 3.60, 9.0 }; //    Near
            //double[] L2 = new double[3] { 0.61, 4.90, 12.0 }; //    Far 
            //double Initial_Theta = 142.0; //  135 는 135deg 로써 중심상태
            //double Initial_Psi = 18.5; //  0 는 0deg 로써 중심상태


            //  Y drv
            // Near Mark for 9.3deg ~ 15.3deg
            //double[] L1 = new double[3] { 3.54, 0, 9.0 }; //    Near
            //double[] L2 = new double[3] { 4.68, 0, 12.0 }; //    Far 
            //double Initial_Theta = 144.3; //  135 는 135deg 로써 중심상태
            //double Initial_Psi = 0; //  0 는 0deg 로써 중심상태

            //// Near Mark for X 3.0deg ~ 10.0deg
            //double[] L1 = new double[3] { 1.79, 0, 9.0 }; //    Near
            //double[] L2 = new double[3] { 2.36, 0, 12.0 }; //    Far 
            //double Initial_Theta = 138.0; //  135 는 135deg 로써 중심상태
            //double Initial_Psi = 0; //  0 는 0deg 로써 중심상태

            ////Near Mark for X = -3.5deg ~3.5deg Y = -7deg ~ 7deg
            double[] L1 = new double[3] { 0, 0, 9.3 }; // 
            double[] L2 = new double[3] { 0, 0, 12.2 }; // 
            double Initial_Theta = 135; //  135 는 135deg 로써 중심상태
            double Initial_Psi = 0; //  0 는 0deg 로써 중심상태

            // Near Mark for X -3.0deg ~ -10.0deg
            //double[] L1 = new double[3] { -1.79, 0, 9.0 }; //    Near
            //double[] L2 = new double[3] { -2.36, 0, 12.0 }; //    Far 
            //double Initial_Theta = 144.0; //  135 는 135deg 로써 중심상태
            //double Initial_Psi = 0; //  0 는 0deg 로써 중심상태

            double[] Xtheta = new double[50000];
            double[] Xpsi = new double[50000];
            double[] Xheight = new double[50000];

            L1[2] = lnear;
            L2[2] = lfar;

            if (srcFile == "")
            {
                srcFile = "C:\\FZ4PTest\\DoNotTouch\\DefaultSrc_" + (lnear * 1000).ToString("F0") + "_" + (lfar * 1000).ToString("F0") + "_" + (L1_L2_InitDecenter * 1000).ToString("F0") + ".txt";
            }

            // center of prism surface

            int k = 0;
            double h, M_PI;
            double[] N0 = new double[3] { 0, 0, 0 };
            double[] N0res = new double[3] { 0, 0, 0 };
            double[] N0org = new double[3] { 0, 0, 0 };
            double[,] Rr = new double[3, 3];
            double[,] R = new double[3, 3];
            double[,] u = new double[4, 250000];
            double[,] u_tmp = new double[4, 250000];
            double[] uErr1 = new double[250000];
            double[] uErr2 = new double[250000];

            double[] Rc = new double[3] { 0, 0, 0 };
            //  for SZ3720/3721/3720E   { -3.2, 0, 0};
            //  for SZ2780/Sz3750       { -3.3, 0, 0};
            //  for SZ4460/4460E        { -2.4, 0, 0};
            //  for SZ2790              { 0, 0, 0};

            double[] Xi = new double[3] { 1, 0, 0 };
            double[] Xo = new double[3];
            double[] Nr = new double[3];
            double[] N0_L1 = new double[3];
            double[] N0_L2 = new double[3];
            double[] Pr1 = new double[3];
            double[] Pr2 = new double[3];
            double[] X1 = new double[2];
            double[] X2 = new double[2];
            double[] X1org = new double[2];
            double[] X2org = new double[2];
            double Xshift = 0;
            double Yshift = 0;
            double theta = 0;
            double psi = 0;
            //double ct = 0;
            //double st = 0;
            //double cp = 0;
            //double sp = 0;
            double[,] Ry = new double[3, 3];
            double[,] Rz = new double[3, 3];
            double ii;

            double[] surfNi0 = new double[3] { 1, 0, 0 };
            double[] surfNr0 = new double[3] { 1, 0, 1 };   //  {0.707107, 0, 0.707107};
            double[] surfNo0 = new double[3] { 0, 0, 1 };

            double[] surfPi0 = new double[3] { 2.54, 0, 0 };    //  mm
            double[] surfPo0 = new double[3] { 0, 0, 2.54 };    //  mm
            double[] beamV = new double[3] { 1, 0, 0 };
            double rIndex = 1.0;
            //double[] markP0 = new double[3] { 0, 0, 8.5 };  //  Near Mark   L1
            //double[] markP1 = new double[3] { 0, 0, 11.0 }; //  Far Mark    L2

            double[] resMarkP = new double[3];
            double[] resMmirror1 = new double[3];
            double[] resMmirror2 = new double[3];

            double[] imgP_L1 = new double[3] { 10, 0.0, 0.0 };
            double[] imgP_L2 = new double[3] { 10, 0.0, 0.0 };
            double[] comp_imgP_L1 = new double[3];
            double[] comp_imgP_L2 = new double[3];
            //

            M_PI = Math.Asin(1) * 2;
            for (int hi = 0; hi < 1; hi++)
            {
                h = hi * 0.06;
                N0[0] = h / 1.41421356; N0[1] = 0; N0[2] = h / 1.41421356;
                // X1org , X2org
                //for (double xi = 0; xi <= 0; xi += 0.1)
                //{
                //    Xshift = xi;
                //    cp = 0;
                //    for (double yi = 0; yi <= 0; yi += 0.1)
                //    {
                for (double xi = -0.2; xi <= 0.2; xi += 0.08)
                {
                    Xshift = xi;
                    //cp = 0;
                    for (double yi = -0.2; yi <= 0.2; yi += 0.08)
                    {
                        Yshift = yi;

                        L1[0] = Xshift;
                        L1[1] = -L1_L2_InitDecenter + Yshift;
                        L2[1] = L1_L2_InitDecenter;

                        theta = Initial_Theta / 180.0 * M_PI; //  기준 위치
                        psi = Initial_Psi / 180.0 * M_PI;

                        RotationR(theta, psi, ref R);

                        MatrixCross(ref R, ref Xi, ref Nr, 3);//Nr = R * Xi;

                        // reflection beam direction vector XoXi
                        // Xo = 2 * (Xi'*Nr)*Nr - Xi; // ( Xo + Xi ) / 2 = (Xi' * Nr)*Nr
                        double tmpXiNr = MatrixInnerProduct(ref Xi, ref Nr, 3);
                        for (int w = 0; w < 3; w++)
                            Xo[w] = 2 * tmpXiNr * Nr[w] - Xi[w];

                        ///////////////////////////////////////////////////////////////////
                        ///////////////////////////////////////////////////////////////////
                        //  Rotate N0 by R with Offset Rc
                        // Surface Equation : N * ( P - P0 ) = 0 ; P0 = (0,0,0)
                        theta = theta - 135.0 / 180 * M_PI; //  여기 135 는 절대 불변 회전축 Offset 에해 회전하면서 반사점의 이동을 계산

                        RotationR(theta, psi, ref Rr);

                        //N0 = Rr * (N0org - Rc)' + Rc';
                        for (int w = 0; w < 3; w++)
                            N0res[w] = N0org[w] - Rc[w];

                        MatrixCross(ref Rr, ref N0res, ref N0, 3);
                        for (int w = 0; w < 3; w++)
                            N0[w] = N0[w] + Rc[w];

                        ///////////////////////////////////////////////////////////////////
                        ///////////////////////////////////////////////////////////////////

                        // Cross Point
                        //t1 = ((N0 - L1) * Nr) / (Xo' * Nr );
                        for (int w = 0; w < 3; w++)
                            N0_L1[w] = N0[w] - L1[w];

                        double tmpN0L1Nr = MatrixInnerProduct(ref N0_L1, ref Nr, 3);
                        double tmpXoNr = MatrixInnerProduct(ref Xo, ref Nr, 3);
                        double t1 = tmpN0L1Nr / tmpXoNr;


                        for (int w = 0; w < 3; w++)
                            Pr1[w] = t1 * Xo[w] + L1[w];


                        for (int w = 0; w < 3; w++)
                            N0_L2[w] = N0[w] - L2[w];


                        double tmpN0L2Nr = MatrixInnerProduct(ref N0_L2, ref Nr, 3);
                        double t2 = tmpN0L2Nr / tmpXoNr;

                        for (int w = 0; w < 3; w++)
                            Pr2[w] = t2 * Xo[w] + L2[w];

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        ////  여기서부터는 상기좌표로부터 다시 굴절율을 고려한 프리즘을 통과하여 마크평면과 교차하는 점의 좌표를 계산하는 과정

                        ////  `굴절율이 적용됬을 때 L1 이 센서면으로 투영되는 점  좌표 계산
                        imgDPtoMarkDP(rIndex, Pr1, L1, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L1, ref comp_imgP_L1);
                        //  `굴절율이 적용됬을 때 Pr2 를 투영시키는 마크평면상의 점 좌표 계산
                        imgDPtoMarkDP(rIndex, Pr2, L2, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L2, ref comp_imgP_L2);

                        for (int w = 0; w < 3; w++)
                        {
                            Pr1[w] = comp_imgP_L1[w];
                            Pr2[w] = comp_imgP_L2[w];
                        }

                        // 여기까지가 프리즘 굴절율을 1.717 로 한 상태에서 카메라에서 얻어지는 좌표(um)
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        //  기준위치에서의 좌표값들
                        X1[0] = 90.909 * Pr1[1];
                        X1[1] = -90.909 * Pr1[2];  // Y -> imgX , Z -> -imgY
                        X2[0] = 90.909 * Pr2[1];
                        X2[1] = -90.909 * Pr2[2];  // Y -> imgX , Z -> -imgY
                        X1org[0] = X1[0]; // 40도 일 때의 근마크의 영상좌표
                        X1org[1] = X1[1]; // 40도 일 때의 근마크의 영상좌표
                        X2org[0] = X2[0]; // 40도 일 때의 원마크의 영상좌표
                        X2org[1] = X2[1]; // 40도 일 때의 원마크의 영상좌표


                        for (double i = -30; i <= 30; i += 6)    //  Initial_Theta ~ Initial_Theta + 6.0deg
                        {
                            for (double j = -60; j <= 60; j += 12)
                            {
                                //for (double j = -25; j <= 25; j += 10)
                                //{
                                //    for (double i = -25; i <= 25; i += 10)
                                //    {
                                ii = i;
                                //ii=0;
                                theta = (Initial_Theta + ii / 10) / 180.0 * M_PI;
                                psi = (Initial_Psi + j / 10) / 180.0 * M_PI;

                                Xtheta[k] = ii / 10;
                                Xpsi[k] = j / 10;
                                Xheight[k] = h;

                                RotationR(theta, psi, ref R);

                                // Source beam direction Vector
                                //Xi = [1 0 0]';

                                // Reflection Surface direction vector Nr
                                // Surface Equation : N * ( P - P0 ) = 0 ; P0 = (0,0,0)
                                //Nr = R * Xi;
                                MatrixCross(ref R, ref Xi, ref Nr, 3);

                                // reflection beam direction vector XoXi
                                // Xo = 2 * (Xi'*Nr)*Nr - Xi; // ( Xo + Xi ) / 2 = (Xi' * Nr)*Nr
                                tmpXiNr = MatrixInnerProduct(ref Xi, ref Nr, 3);
                                for (int w = 0; w < 3; w++)
                                    Xo[w] = 2 * tmpXiNr * Nr[w] - Xi[w];

                                ///////////////////////////////////////////////////////////////////
                                ///////////////////////////////////////////////////////////////////
                                //  Rotate N0 by R with Offset Rc
                                theta = theta - 135.0 / 180 * M_PI; //  여기 135 는 절대 불변 회전축 Offset 에해 회전하면서 반사점의 이동을 계산

                                RotationR(theta, psi, ref Rr);

                                //N0 = Rr * (N0org - Rc)' + Rc';
                                for (int w = 0; w < 3; w++)
                                    N0res[w] = N0org[w] - Rc[w];

                                MatrixCross(ref Rr, ref N0res, ref N0, 3);
                                for (int w = 0; w < 3; w++)
                                    N0[w] = N0[w] + Rc[w];

                                ///////////////////////////////////////////////////////////////////
                                ///////////////////////////////////////////////////////////////////


                                // Cross Point
                                //t1 = ((N0 - L1) * Nr) / (Xo' * Nr );
                                for (int w = 0; w < 3; w++)
                                    N0_L1[w] = N0[w] - L1[w];
                                tmpN0L1Nr = MatrixInnerProduct(ref N0_L1, ref Nr, 3);
                                tmpXoNr = MatrixInnerProduct(ref Xo, ref Nr, 3);
                                t1 = tmpN0L1Nr / tmpXoNr;


                                //Pr1 = t1 * Xo' + L1;
                                for (int w = 0; w < 3; w++)
                                    Pr1[w] = t1 * Xo[w] + L1[w];

                                //t2 = ((N0 - L2) * Nr) / (Xo' * Nr );
                                for (int w = 0; w < 3; w++)
                                    N0_L2[w] = N0[w] - L2[w];
                                tmpN0L2Nr = MatrixInnerProduct(ref N0_L2, ref Nr, 3);
                                t2 = tmpN0L2Nr / tmpXoNr;

                                //Pr2 = t2 * Xo' + L2;
                                for (int w = 0; w < 3; w++)
                                    Pr2[w] = t2 * Xo[w] + L2[w];

                                // 여기까지가 프리즘 굴절율을 1.0 으로 한 상태에서 카메라에서 얻어지는 좌표(mm)
                                //  Pr2 : L2 로부터 굴절율 1.0 프리즘을 통과하여 sensor 면으로 투사된 점의 WCS 좌표
                                //  Pr1 : L1 로부터 굴절율 1.0 프리즘을 통과하여 sensor 면으로 투사된 점의 WCS 좌표

                                //  굴절율 0 일 때 Pr1 이 L1 에 매칭되는지 검증
                                //ImgPtoMarkP(1.0, Pr1, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L1, ref resMmirror1);
                                ////  굴절율 0 일 때 Pr2 이 L2 에 매칭되는지 검증
                                //ImgPtoMarkP(1.0, Pr2, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L2, ref resMmirror2);

                                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                ////  여기서부터는 상기좌표로부터 다시 굴절율을 고려한 프리즘을 통과하여 마크평면과 교차하는 점의 좌표를 계산하는 과정

                                ////  `굴절율이 적용됬을 때 L1 이 센서면으로 투영되는 점  좌표 계산
                                uErr1[k] = imgDPtoMarkDP(rIndex, Pr1, L1, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L1, ref comp_imgP_L1);
                                //  `굴절율이 적용됬을 때 Pr2 를 투영시키는 마크평면상의 점 좌표 계산
                                uErr2[k] = imgDPtoMarkDP(rIndex, Pr2, L2, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, L2, ref comp_imgP_L2);

                                for (int w = 0; w < 3; w++)
                                {
                                    Pr1[w] = comp_imgP_L1[w];
                                    Pr2[w] = comp_imgP_L2[w];
                                }

                                // 여기까지가 프리즘 굴절율을 1.717 로 한 상태에서 카메라에서 얻어지는 좌표(um)
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                                X1[0] = 90.909 * Pr1[1];
                                X1[1] = -90.909 * Pr1[2];  // Y -> imgX , Z -> -imgY
                                X2[0] = 90.909 * Pr2[1];
                                X2[1] = -90.909 * Pr2[2];  // Y -> imgX , Z -> -imgY

                                u_tmp[0, k] = X1[0];
                                u_tmp[1, k] = X1[1];
                                u_tmp[2, k] = X2[0];
                                u_tmp[3, k] = X2[1];

                                X1[0] = X1[0] - X1org[0];
                                X1[1] = X1[1] - X1org[1];
                                X2[0] = X2[0] - X2org[0];
                                X2[1] = X2[1] - X2org[1];

                                u[0, k] = X2[0] - X1[0];    //  X 방향 원마크이동량 - 근마크 이동량
                                u[1, k] = X2[1] - X1[1];    //  Y 방향 원마크이동량 - 근마크 이동량
                                u[2, k] = (X2org[0] - X1org[0]);// 40도 일 때의 원마크X좌표 빼기 근마크X좌표       
                                u[3, k] = (X2org[1] - X1org[1]);// 40도 일 때의 원마크Y좌표 빼기 근마크Y좌표 에서 표준거리를 뺀 값
                                k = k + 1;
                            }
                        }
                    }
                }
            }
            StreamWriter wr = new StreamWriter(srcFile);
            //wr.WriteLine("dX\tdy\tdX0\tdY0\ttheta\tpsi\tH\tNear X\tNear Y\tFar X\tFar Y");
            for (int line = 0; line < k; line++)
                wr.WriteLine(u[0, line].ToString("F9") + "\t" + u[1, line].ToString("F9") + "\t" + u[2, line].ToString("F9") + "\t" + u[3, line].ToString("F9") + "\t" + Xtheta[line].ToString("F9") + "\t" + Xpsi[line].ToString("F9") + "\t" + Xheight[line].ToString("F9") + "\t" + u_tmp[0, line].ToString("F9") + "\t" + u_tmp[1, line].ToString("F9") + "\t" + u_tmp[2, line].ToString("F9") + "\t" + u_tmp[3, line].ToString("F9"));// + "\t" + uErr1[line].ToString("E5") + "\t" + uErr2[line].ToString("E5"));

            wr.Close();

            ReadDataFile(srcFile);
            File.Delete(srcFile);
        }

        public void RotationR(double theta_rad, double psi_rad, ref double[,] Rr)
        {
            double ct = 0;
            double st = 0;
            double cp = 0;
            double sp = 0;
            double[,] Ry = new double[3, 3];
            double[,] Rz = new double[3, 3];

            ct = Math.Cos(theta_rad);   //  radian
            st = Math.Sin(theta_rad);   //  radian
            cp = Math.Cos(psi_rad);
            sp = Math.Sin(psi_rad);

            Ry[0, 0] = ct; Ry[0, 2] = st;
            Ry[1, 1] = 1;
            Ry[2, 0] = -st; Ry[2, 2] = ct;

            //Rz = [cp - sp 0; sp cp 0; 0 0 1];
            Rz[0, 0] = cp; Rz[0, 1] = -sp;
            Rz[1, 0] = sp; Rz[1, 1] = cp;
            Rz[2, 2] = 1;
            //R = Rz * Ry;
            MatrixCross(ref Rz, ref Ry, ref Rr, 3);
        }

        public void RefractionVector(double[] surfN, double[] beamV, double rIndex, ref double[] refractedV)
        {
            //  cos θi = N * Xi / ( | N | * | Xi | )
            //  NC  = cos θi | Xi | * N / | N |
            //  NC + NUI = Xi
            //  NUI = Xi – NC
            //  NUI = Xi - cos θi | Xi | *N / | N |
            //  NUO = -NUI * sin θo / sin θi = -NUI / n
            //  XO  = -NC + NUO

            double l_NV = 0;
            double cosT = 0;
            double l_Nsum = 0;
            double l_Vsum = 0;
            for (int i = 0; i < 3; i++)
            {
                l_NV += surfN[i] * beamV[i];
                l_Nsum += surfN[i] * surfN[i];
                l_Vsum += beamV[i] * beamV[i];
            }
            double absN = Math.Sqrt(l_Nsum);
            double absV = Math.Sqrt(l_Vsum);
            cosT = l_NV / (absN * absV);

            double[] Nc = new double[3];
            double[] Nui = new double[3];
            for (int i = 0; i < 3; i++)
            {
                Nc[i] = cosT * absV / absN * surfN[i];
                Nui[i] = beamV[i] - Nc[i];
                refractedV[i] = -Nc[i] - Nui[i] / rIndex;
            }
        }
        public void CPofBeamToSurfaceN(double[] surfN, double[] surfP, double[] beamV, double[] beamP, ref double[] resP)
        {
            //  Nin (P – PR) = 0
            //  PR = R[D / 2 0 0]’
            //  N0 = [1 0 0]
            //  P = PL1 + t N0
            //  Nin (PL1 + t N0 - PR ) = 0
            //  t Nin *N0 = Nin * (PR – PL1 )
            //  t = (Nin * (PR – PL1 ) ) / (Nin * N0)
            double l_surfN_beamV = 0;
            double t = 0;

            for (int i = 0; i < 3; i++)
                l_surfN_beamV += surfN[i] * beamV[i];

            for (int i = 0; i < 3; i++)
                t += surfN[i] * (surfP[i] - beamP[i]) / l_surfN_beamV;

            for (int i = 0; i < 3; i++)
                resP[i] = beamP[i] + t * beamV[i];

        }
        public void ReflectionVector(double[] surfN, double[] inV, ref double[] outV)
        {
            //  Nout = 2(Nin * Nr) * Nr – Nin
            double innerNinNr = 0;

            for (int i = 0; i < 3; i++)
                innerNinNr += inV[i] * surfN[i];

            for (int i = 0; i < 3; i++)
                outV[i] = innerNinNr * surfN[i] - inV[i];

        }
        public void ImgPtoMarkP(double rIndex, double[] imgP, double[] beamV, double[,] Rr, double[] surfNi0, double[] surfNr0, double[] surfNo0, ref double[] surfPi0, ref double[] surfPo0, double[] markP0, ref double[] resMarkP)
        {
            double[] Ndfr = new double[3];
            double[] Ndfr2 = new double[3];
            double[] Nout = new double[3];
            double[] surfPi = new double[3];
            double[] surfPo = new double[3];
            double[] surfNi = new double[3];
            double[] surfNr = new double[3];
            double[] surfNo = new double[3];
            double[] surfMark = new double[3] { 0, 0, 1 }; //    constant, Normal to Mark Plane
            double[] cpi = new double[3];   //  cross point on prism inlet
            double[] cpr = new double[3];   //  cross point on prism reflection surface
            double[] cpo = new double[3];   //  corss point on prism outlet
            double[] pZero = new double[3] { 0, 0, 0 }; //  constance, Center of rotation without Offset.


            //when center of rotation is [ 0 0 0 ]
            MatrixCross(ref Rr, ref surfNi0, ref surfNi, 3);    //  surfNi0 : prism inlet surface normal vector
            MatrixCross(ref Rr, ref surfNr0, ref surfNr, 3);    //  surfNi0 : prism reflection surface normal vector
            MatrixCross(ref Rr, ref surfNo0, ref surfNo, 3);    //  surfNi0 : prism outlet surface normal vector

            MatrixCross(ref Rr, ref surfPi0, ref surfPi, 3);    //  surfNi0 : A point on prism inlet surface
            MatrixCross(ref Rr, ref surfPo0, ref surfPo, 3);    //  surfNi0 : A point on prism outlet surface 

            /////////////////////////////////////////////////////////////////////
            //  단계별로 Octave 와 비교 검증 필요
            //  프리즘 인렛면과 빔간 교점
            //m_strMsg += "Sensor Point = [\t" + imgP[0].ToString("F4") + "\t" + imgP[1].ToString("F4") + "\t" + imgP[2].ToString("F4") + "\t] \r\n";
            CPofBeamToSurfaceN(surfNi, surfPi, beamV, imgP, ref cpi);
            //m_strMsg += "Sensor->Prism Crosspoint = [\t" + cpi[0].ToString("F4") + "\t" + cpi[1].ToString("F4") + "\t" + cpi[2].ToString("F4") + "\t] \r\n";

            RefractionVector(surfNi, beamV, rIndex, ref Ndfr);
            //m_strMsg += "Refracted Vector in Prism = [\t" + Ndfr[0].ToString("F4") + "\t" + Ndfr[1].ToString("F4") + "\t" + Ndfr[2].ToString("F4") + "\t] \r\n";
            //  프리즘 반사점
            CPofBeamToSurfaceN(surfNr, pZero, Ndfr, cpi, ref cpr);
            //m_strMsg += "Prism Reflection Point = [\t" + cpr[0].ToString("F4") + "\t" + cpr[1].ToString("F4") + "\t" + cpr[2].ToString("F4") + "\t] \r\n";
            //  프리즘 반사 후 빔 벡터
            ReflectionVector(surfNr, Ndfr, ref Ndfr2);
            //m_strMsg += "Prism Reflection Vector = [\t" + Ndfr2[0].ToString("F4") + "\t" + Ndfr2[1].ToString("F4") + "\t" + Ndfr2[2].ToString("F4") + "\t] \r\n";
            //  프리즘 반사 후 빔과 프리즘 아웃렛간 교점
            CPofBeamToSurfaceN(surfNo, surfPo, Ndfr2, cpr, ref cpo);
            //m_strMsg += "Prism to Mark Crosspoint = [\t" + cpo[0].ToString("F4") + "\t" + cpo[1].ToString("F4") + "\t" + cpo[2].ToString("F4") + "\t] \r\n";
            //  프리즘 탈출 후 빔 벡터
            RefractionVector(surfNo, Ndfr2, 1 / rIndex, ref Nout);
            //m_strMsg += "To Mark Vector = [\t" + Nout[0].ToString("F4") + "\t" + Nout[1].ToString("F4") + "\t" + Nout[2].ToString("F4") + "\t] \r\n";
            //  프리즘 탈출빔과 마크면간 교점
            CPofBeamToSurfaceN(surfMark, markP0, Nout, cpo, ref resMarkP);
            //m_strMsg += "Mark Plane Cross Point = [\t" + resMarkP[0].ToString("F4") + "\t" + resMarkP[1].ToString("F4") + "\t" + resMarkP[2].ToString("F4") + "\t] \r\n";

        }
        public double imgDPtoMarkDP(double rIndex, double[] imgP, double[] resMmirror, double[] beamV, double[,] Rr, double[] surfNi0, double[] surfNr0, double[] surfNo0, ref double[] surfPi0, ref double[] surfPo0, double[] markP0, ref double[] comp_imgP)
        {
            double[] imgPdy = new double[3];
            double[] imgPdz = new double[3];

            for (int i = 0; i < 3; i++)
            {
                imgPdy[i] = imgP[i];
                imgPdz[i] = imgP[i];
            }
            imgPdy[1] += 0.01; //  10um to Y dir
            imgPdz[2] += 0.01; //  10um to Z dir

            double[] resMorg = new double[3];
            double[] resMdx = new double[3];
            double[] resMdy = new double[3];

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //  검증의 검증용 코드
            //  굴절율 1.0 가정 imgP 를 형성시키는 Mark 평면상의 점을 구한다.
            //  이 점은 굴절율 1.0 가정 시 imgP 에 투영된다.
            //ImgPtoMarkP( 1.0, imgP, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, markP0, ref resMmirror); //  imgP 를 
            //m_strMsg += "\r\n\r\n";
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            //  Sensor 면의 imgP 점이 굴절율을 가진 프리즘에 의해 markP0 면으로 전사된 점 resMorg 을 구한다.
            ImgPtoMarkP(rIndex, imgP, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, markP0, ref resMorg); //  
            //m_strMsg += "\r\n";
            //  Sensor 면의 imgP 점 기준 10um to Y dir 로 이동한 점이 굴절율을 가진 프리즘에 의해 markP0 면으로 전사된 점 resMdx 을 구한다.
            ImgPtoMarkP(rIndex, imgPdy, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, markP0, ref resMdx);
            //m_strMsg += "\r\n";
            //  Sensor 면의 imgP 점 기준 10um to Z dir 로 이동한 점이 굴절율을 가진 프리즘에 의해 markP0 면으로 전사된 점 resMdy 을 구한다.
            ImgPtoMarkP(rIndex, imgPdz, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, markP0, ref resMdy);
            //m_strMsg += "\r\n";

            double[] dMark = new double[2];
            double[] dXMark = new double[2];
            double[] dYMark = new double[2];
            double[,] toInvP = new double[2, 2];
            double[,] resInvP = new double[2, 2];


            dMark[0] = resMorg[0] - markP0[0];
            dMark[1] = resMorg[1] - markP0[1];

            dXMark[0] = resMdx[0] - resMorg[0]; //  Shift by imgP_y
            dXMark[1] = resMdx[1] - resMorg[1];

            dYMark[0] = resMdy[0] - resMorg[0]; //  Shift by imgP_z
            dYMark[1] = resMdy[1] - resMorg[1];

            toInvP[0, 0] = dXMark[0];
            toInvP[0, 1] = dYMark[0];
            toInvP[1, 0] = dXMark[1];
            toInvP[1, 1] = dYMark[1];

            resInvP = MatrixInverse(toInvP, 2);

            double kx = resInvP[0, 0] * (resMorg[0] - resMmirror[0]) + resInvP[0, 1] * (resMorg[1] - resMmirror[1]);
            double ky = resInvP[1, 0] * (resMorg[0] - resMmirror[0]) + resInvP[1, 1] * (resMorg[1] - resMmirror[1]);
            comp_imgP[0] = imgP[0];
            comp_imgP[1] = imgP[1] - 0.01 * kx;
            comp_imgP[2] = imgP[2] - 0.01 * ky;
            ImgPtoMarkP(rIndex, comp_imgP, beamV, Rr, surfNi0, surfNr0, surfNo0, ref surfPi0, ref surfPo0, markP0, ref resMorg);
            //m_strMsg += "\r\n";

            double errDist = Math.Sqrt((resMorg[0] - resMmirror[0]) * (resMorg[0] - resMmirror[0]) + (resMorg[1] - resMmirror[1]) * (resMorg[1] - resMmirror[1]));

            //m_strMsg += "ErrDist =\t" + errDist.ToString("E5") + "\r\n";
            return errDist;
        }
        public void mcLMS2ndPoly(Point2d[] wp, int length, ref Poly2nd p2nd)
        {
            //  y = a (x - b)^2 + c 
            double[,] XXTinv = new double[3, 3];
            double[] XTA = new double[3];
            double[] A = new double[3];
            for (int i = 0; i < length; i++)
            {
                XXTinv[0, 0] += wp[i].X * wp[i].X * wp[i].X * wp[i].X;
                XXTinv[0, 1] += wp[i].X * wp[i].X * wp[i].X;
                XXTinv[0, 2] += wp[i].X * wp[i].X;

                XXTinv[1, 0] += wp[i].X * wp[i].X * wp[i].X;
                XXTinv[1, 1] += wp[i].X * wp[i].X;
                XXTinv[1, 2] += wp[i].X;

                XXTinv[2, 0] += wp[i].X * wp[i].X;
                XXTinv[2, 1] += wp[i].X;
                XXTinv[2, 2]++;

                XTA[0] += wp[i].Y * wp[i].X * wp[i].X;
                XTA[1] += wp[i].Y * wp[i].X;
                XTA[2] += wp[i].Y;
            }
            InverseU(ref XXTinv, 3);
            MatrixCross(ref XXTinv, ref XTA, ref A, 3);
            p2nd.a = A[0];
            p2nd.b = -A[1] / (2 * A[0]);
            p2nd.c = A[2] - A[1] * A[1] / (4 * A[0]);
        }
        //public void InverseU(ref double[,] invM, int dim)
        //{
        //    //  Calculate Inverse Matrix of matrixU and save result to invM
        //    //double[,] copySrc = new double[dim, dim];

        //    //for (int i = 0; i < dim; ++i) // copy the values
        //    //    for (int j = 0; j < dim; ++j)
        //    //        copySrc[i, j] = invM[i, j];

        //    //double[,] result = MatrixInverse(copySrc, dim);
        //    //for (int i = 0; i < dim; ++i) // copy the values
        //    //    for (int j = 0; j < dim; ++j)
        //    //        invM[i, j] = result[i, j];

        //    int info;
        //    alglib.matinvreport rep;
        //    alglib.rmatrixinverse(ref invM, out info, out rep);
        //}
        //public void MatrixCross(ref double[,] dimbydim, ref double[] dimby1, ref double[] res, int dim)
        //{
        //    //  Calculate [ dim x dim ] x [ dim ]
        //    for (int i = 0; i < dim; i++)
        //    {
        //        res[i] = 0;
        //        for (int j = 0; j < dim; j++)
        //        {
        //            res[i] += dimbydim[i, j] * dimby1[j];
        //        }
        //    }
        //}
    }
}
