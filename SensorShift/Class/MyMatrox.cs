﻿
using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
using Matrox.MatroxImagingLibrary;
using System.Threading;
using System.Windows.Forms;
using System.IO.Ports;
using System.Xml;
using OpenCvSharp;

namespace ActroLib
{
    using System;
    using System.Runtime.InteropServices;
    public class MILlib
    {
        public MicroTimer uTimer = null;
        public FZCoefficient mFZC = null;

        public int me = 0;
        public bool m_bAnotherIdle = true;
        //public int MAX_GRAB_COUNT = 0;
        public bool m_bProcess_Vision = false;
        public double FPS;

        public bool[] mC_pDone = new bool[6000];  //  Left 0,1 ;; Right 2,3 in a camera view
        public double[,] mC_pX = new double[4, 6000];  //  Left 0,1 ;; Right 2,3 in a camera view
        public double[,] mC_pY = new double[4, 6000];  //  Left 0,1 ;; Right 2,3 in a camera view
        public double[,] mC_pTX = new double[4, 6000];  //  Left 0,1 ;; Right 2,3 in a camera view
        public double[,] mC_pTY = new double[4, 6000];  //  Left 0,1 ;; Right 2,3 in a camera view
        public double[,] mC_Area = new double[4, 6000];  //  Left 0,1 ;; Right 2,3 in a camera view

        public long[] CLXGrabTimeY = new long[6000];   //  Max 1.2KHz 4sec
        public long[] CLXGrabTime = new long[6000];   //  Max 1.2KHz 4sec
        public long[] CLYGrabTime = new long[6000];   //  Max 1.2KHz 4sec
        public long[] CLXGrabTimeRes = new long[4000];  //  Max 1.7KHz 0.7sec
        public long[] CLYGrabTimeRes = new long[4000];  //  Max 1.7KHz 0.7sec

        public long[] m_CLXOutTime = new long[10000];
        public long[] m_CLYOutTime = new long[10000];

        public double[] mxZeroXgap = new double[2];    //  Left, right
        public double[] mxZeroYgap = new double[2];    //  Left, right
        public double[] m_Yscale = new double[2] { 1, 1 };

        public long[] GrabT1 = new long[6000];
        //public long[] GrabT2 = new long[6000];
        public int dCLXY_FrameCount = 0;
        public int[] CLFrameCount = new int[10];
        public long mTimerFrequency = 0;
        public static int allocatedSysNum = 0;
        public static int MAX_DEFAULT_GRAB_COUNT = 2;                     // 
        public static int MAX_GRAB_COUNT = 2000;                     // max 1000Frame/sec * 5sec = 5000
        public static int MAX_GRAB_COUNT_STEP = 800;                     // max 2000Frame/sec * 0.4sec = 800

        public int mOptThresh = 32;

        private MIL_INT NbAvailableSystems = MIL.M_NULL;
        public static MIL_ID milApplication = MIL.M_NULL;
        public static MIL_ID milRemoteApplication = MIL.M_NULL;
        public MIL_ID milSystem = MIL.M_NULL;
        private MIL_ID milDigitizer = MIL.M_NULL;
        private MIL_ID milDisplay = MIL.M_NULL;
        public MIL_ID milImageDisp = MIL.M_NULL;
        public MIL_ID milChildDisp = MIL.M_NULL;
        public MIL_INT LicenseModules = 0;

        public long IMAGE_THRESHOLD_VALUE = 26;

        /* Minimum and maximum area of blobs. */
        public static long MIN_BLOB_AREA = 50;
        public static long MAX_BLOB_AREA = 8000;  // 실제로는 대부분 4000 이하. 8000 이상은 두개의 마크가 하나로 합쳐진 경우임.

        /* Radius of the smallest particles to keep. */
        public static long MIN_BLOB_RADIUS = 2;

        /* Minimum hole compactness corresponding to a washer. */
        public double MIN_COMPACTNESS = 1.5;

        public MIL_ID MilGraphicList = MIL.M_NULL;                 // Graphic list identifier.
        public MIL_ID MilBinImage = MIL.M_NULL;                    // Binary image buffer identifier.
        public MIL_ID MilBlobResult = MIL.M_NULL;                  // Blob result buffer identifier.ㄹ
        public MIL_ID MilBlobContext = MIL.M_NULL;
        public MIL_INT TotalBlobs = 0;                             // Total number of blobs.
        public MIL_INT BlobsWithHoles = 0;                         // Number of blobs with holes.
        public MIL_INT BlobsWithRoughHoles = 0;                    // Number of blobs with rough holes.
        public MIL_INT mBlobSizeX = 0;                                  // Size X of the source buffer
        public MIL_INT mBlobSizeY = 0;                                  // Size Y of the source buffer
        public double[] mBlobCogX = new double[4];                               // X coordinate of center of gravity.
        public double[] mBlobCogY = new double[4];                               // Y coordinate of center of gravity.
        public double[] mBlobRadius = null;                               // Y coordinate of center of gravity.
        public int[,] mEachThresh = new int[6, 4];

        public MIL_ID[] milSaveBmp = new MIL_ID[1];
        public MIL_ID milTmp = MIL.M_NULL;
        public MIL_ID[] milImageGrab = new MIL_ID[MAX_DEFAULT_GRAB_COUNT];
        public MIL_ID[] milImageData = new MIL_ID[MAX_DEFAULT_GRAB_COUNT];
        public MIL_ID[] milCommonImageGrab = new MIL_ID[MAX_GRAB_COUNT];
        public MIL_ID[] OISXCalStrokeImg = new MIL_ID[2];
        public MIL_ID[] OISYCalStrokeImg = new MIL_ID[2];
        public MIL_ID[] OISXNonEPAStrokeImg = new MIL_ID[2];
        public MIL_ID[] OISYNonEPAStrokeImg = new MIL_ID[2];
        public MIL_ID MergeImage;
        public MIL_ID[] OISXImageData = new MIL_ID[MAX_GRAB_COUNT];
        public MIL_ID[] OISYImageData = new MIL_ID[MAX_GRAB_COUNT];
        public MIL_ID[] OISRImageData = new MIL_ID[MAX_GRAB_COUNT];
        public MIL_ID[] XstepImageData = new MIL_ID[MAX_GRAB_COUNT_STEP];
        public MIL_ID[] YstepImageData = new MIL_ID[MAX_GRAB_COUNT_STEP];
        public MIL_ID[] InstantImageData;

        private MIL_ID milOverlayImage = MIL.M_NULL;
        private MIL_ID milEvent = MIL.M_NULL;
        private MIL_ID milThread = MIL.M_NULL;
        long keyColor;

        Graphics gr;
        public static Font fontData = new Font("Calibri", 30, FontStyle.Regular);
        public static Font fontOKNG = new Font("Calibri", 200, FontStyle.Bold);

        public int m_nCam = 0;
        public bool bChildSystem = false;
        public bool bInit = false;
        public bool bLive = false;
        public bool bLiveA = false;
        public bool OnGrab = false;

        public bool bFlipX = false;
        public bool bFlipY = false;
        public double dZoomFactorX = 2.0;
        public double dZoomFactorY = 2.0;
        public int nSizeX = 0;
        public int nSizeY = 0;
        public int nSizeYstep = 0;
        public double mMarkGapL = 11000;
        public double mMarkGapR = 11000;
        public int mChannelCount = 2;
        public double[] mOldAx = new double[6];
        public double[] mOldAy = new double[6];
        public double[] mMScaleX = new double[2] { 1, 1 };
        public double[] mMScaleY = new double[2] { 1, 1 };
        public double[] sArea = new double[100];
        public byte[][] p_Value = new byte[2][];
        public byte[][] q_Value = new byte[2][];//[M_HROI / 4 * 340 / 4];    // 1/4 축소이미지
        public int nGrabIndex = 0;
        public int nMaxGrabCount = 0;

        public System.Drawing.Point nBoxP1 = new System.Drawing.Point();
        public System.Drawing.Point nBoxP2 = new System.Drawing.Point();

        public struct sSearchModel
        {
            public int width;
            public int height;
            public int conv;
            public int sub;
            public double subconv;
            public int[] img;
            public int[] diffimg;
        }
        public sSearchModel[] mSearchModel = new sSearchModel[10];
        public sSearchModel[] mNewSearchModel = new sSearchModel[10];
        public int mSearchModelIndex = 0;
        public int mSelectedSubIndex = 0;

        public string mOrgFile = "";

        Mat mSourceImg = new Mat(new OpenCvSharp.Size(1680, 340), MatType.CV_8UC1);
        Mat mQuadImg = new Mat();

        public int mProcSequence = 0;
        public Rect[][] mDetectedRect = new Rect[50][];
        public Rect[][] mDetectedLearnRect = new Rect[200][];
        public int[] mDetectedModel = new int[200];
        public int[][] mDetectedLearned = new int[200][];
        public int mTrainingLength = 0;
        public bool[] mIsRunModel = new bool[200];
        public bool[] mIsRunSingleFile = new bool[16];
        public string[] mTargetFiles;

        public class HookDataObject
        {
            public MIL_ID milImageDisp;
            public MIL_ID[] milImageGrab = new MIL_ID[MAX_DEFAULT_GRAB_COUNT];
            public MIL_ID[] milImageData = new MIL_ID[MAX_DEFAULT_GRAB_COUNT];
            public MIL_ID milEvent;
            public MIL_INT milGrabNumber;
            public MIL_INT milGrabIndex;
            public MIL_INT milExit;
            public int m_nCam;
        }

        public HookDataObject userHookData = null;
        GCHandle userHookDataHandle;
        MIL_DIG_HOOK_FUNCTION_PTR userHookFuncDelegate = null;
        MIL_THREAD_FUNCTION_PTR userThreadDelegate = null;
        public class MyReverserClass : IComparer
        {
            // Calls CaseInsensitiveComparer.Compare with the parameters reversed.
            int IComparer.Compare(Object x, Object y)
            {
                return ((new CaseInsensitiveComparer()).Compare(y, x));
            }

        }
        public IComparer Max2MinComparer;
        public bool IsLiveA { get { return bLiveA; } }
        public int SizeX { get { return nSizeX; } }
        public int SizeY { get { return nSizeY; } }
        public bool IsVirtual = false;
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public MILlib(double ZoomFactor)
        {
            dZoomFactorX = ZoomFactor;
            dZoomFactorY = ZoomFactor;
            nMaxGrabCount = MAX_GRAB_COUNT;
            MergeImage = MIL.M_NULL;
            for (int n = 0; n < MAX_DEFAULT_GRAB_COUNT; n++)
            {
                milImageGrab[n] = MIL.M_NULL;
                milImageData[n] = MIL.M_NULL;
            }
            for (int i = 0; i < 2; i++)
            {
                OISXCalStrokeImg[i] = MIL.M_NULL;
                OISYCalStrokeImg[i] = MIL.M_NULL;
                OISXNonEPAStrokeImg[i] = MIL.M_NULL;
                OISYNonEPAStrokeImg[i] = MIL.M_NULL;
            }
            for (int n = 0; n < MAX_GRAB_COUNT; n++)
            {
                milCommonImageGrab[n] = MIL.M_NULL;
                OISXImageData[n] = MIL.M_NULL;
                OISYImageData[n] = MIL.M_NULL;
                OISRImageData[n] = MIL.M_NULL;
            }
            for (int n = 0; n < MAX_GRAB_COUNT_STEP; n++)
            {
                XstepImageData[n] = MIL.M_NULL;
            }

            userHookData = new HookDataObject();

            p_Value[0] = new byte[400 * 2000];
            p_Value[1] = new byte[400 * 2000];
            q_Value[0] = new byte[100 * 500];
            q_Value[1] = new byte[100 * 500];

        }
        public void ClearPoints()
        {
            for (int i = 0; i < 6000; i++)
                mC_pDone[i] = false;
        }
        public bool Init(int lVROI, int lHROI, int lVROIstep, int nCamIndex, string SystemName, MIL_INT SystemNum, MIL_INT DigNum, string DataFormat = "M_RS170", bool FlipX = false, bool FlipY = false)
        {

            bool bHaveDigitizer = false;

            m_nCam = nCamIndex;

            // Inquire MIL licenses.
            //MIL.MsysInquire(milSystem, MIL.M_OWNER_APPLICATION, ref milRemoteApplication);
            //MIL.MappInquire(milRemoteApplication, MIL.M_LICENSE_MODULES, ref LicenseModules);

            if (milApplication == MIL.M_NULL)
                MIL.MappAlloc(MIL.M_NULL, MIL.M_DEFAULT, ref milApplication);
            MIL.MappInquire(milApplication, MIL.M_CURRENT_APPLICATION, ref NbAvailableSystems);


            //if (MIL.MsysAlloc(MIL.M_SYSTEM_DEFAULT, SystemNum, MIL.M_DEFAULT, ref milSystem) == MIL.M_NULL)
            if (MIL.MsysAlloc(milApplication, MIL.M_SYSTEM_DEFAULT, SystemNum, MIL.M_DEFAULT, ref milSystem) == MIL.M_NULL)
            {

                MessageBox.Show("Check \"Matrox Imaging Adapter\" in Hardware Management Console.");
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Normal,
                    FileName = "cmd.exe",
                    Arguments = "/C mmc /b devmgmt.msc"
                };
                process.StartInfo = startInfo;
                process.Start();
                return false;
            }
            else
            {
                //MessageBox.Show("MsysControl " + DigNum + " milSystem = " + milSystem.ToString());
                MIL.MsysControl(milSystem, MIL.M_MODIFIED_BUFFER_HOOK_MODE, MIL.M_MULTI_THREAD);
                allocatedSysNum++;
            }

            if (SystemName == "M_SYSTEM_HOST" || SystemName == "M_SYSTEM_VGA" || SystemName == "M_SYSTEM_GPU")
                bHaveDigitizer = false;
            else
                bHaveDigitizer = true;

            if (bHaveDigitizer == true)
            {
                MIL.MdigAlloc(milSystem, DigNum, DataFormat, MIL.M_DEFAULT, ref milDigitizer);
                if (milDigitizer == MIL.M_NULL)
                {
                    MessageBox.Show("No More Digitizer");
                    return false;
                }

                MIL.MdigControl(milDigitizer, MIL.M_GRAB_MODE, MIL.M_ASYNCHRONOUS);
                MIL.MdigControl(milDigitizer, MIL.M_GRAB_TIMEOUT, 500);                    //  500 일때 Best

                nSizeX = lHROI;  //  Mirror Tilt OIS
                nSizeY = lVROI;
                nSizeYstep = lVROI - 100;
            }
            if (bHaveDigitizer == true)
            {
                for (int n = 0; n < MAX_DEFAULT_GRAB_COUNT; n++)
                    MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_GRAB + MIL.M_NON_PAGED, ref milImageGrab[n]);

            }
            else
            {
                for (int n = 0; n < MAX_DEFAULT_GRAB_COUNT; n++)
                    MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_GRAB + MIL.M_NON_PAGED, ref milImageGrab[n]);
            }
            for (int n = 0; n < MAX_DEFAULT_GRAB_COUNT; n++)
            {
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref milImageData[n]);
            }
            for (int n = 0; n < MAX_GRAB_COUNT; n++)
            {
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_GRAB + MIL.M_NON_PAGED + MIL.M_PROC, ref milCommonImageGrab[n]);
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref OISXImageData[n]);
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref OISYImageData[n]);
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref OISRImageData[n]);
            }

            for (int n = 0; n < 2; n++)
            {
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref OISXCalStrokeImg[n]);
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref OISYCalStrokeImg[n]);
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref OISXNonEPAStrokeImg[n]);
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref OISYNonEPAStrokeImg[n]);
            }
            MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY * 2, 8, MIL.M_IMAGE + MIL.M_PROC, ref MergeImage);
            for (int n = 0; n < MAX_GRAB_COUNT_STEP; n++)
            {
                //MIL.MbufAlloc2d(milSystem, nSizeX, nSizeYstep, 8, MIL.M_IMAGE + MIL.M_PROC, ref YstepImageData[n]); // 170703 
                //MIL.MbufAlloc2d(milSystem, nSizeX, nSizeYstep, 8, MIL.M_IMAGE + MIL.M_PROC, ref XstepImageData[n]); // 170703 
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref YstepImageData[n]); // 170703 
                MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_PROC, ref XstepImageData[n]); // 170703 

            }
            MIL.MbufAlloc2d(milSystem, nSizeX, nSizeY, 8, MIL.M_IMAGE + MIL.M_GRAB + MIL.M_DISP + MIL.M_PROC, ref milImageDisp);
            MIL.MbufClear(milImageDisp, 0);

            MIL.MdispAlloc(milSystem, MIL.M_DEFAULT, "M_DEFAULT", MIL.M_DEFAULT, ref milDisplay);

            MIL.MdispZoom(milDisplay, dZoomFactorX, dZoomFactorY);
            MIL.MgraControl(MIL.M_DEFAULT, MIL.M_DRAW_ZOOM_X, dZoomFactorX);
            MIL.MgraControl(MIL.M_DEFAULT, MIL.M_DRAW_ZOOM_Y, dZoomFactorY);

            MIL.MthrAlloc(milSystem, MIL.M_EVENT, MIL.M_DEFAULT, MIL.M_NULL, MIL.M_NULL, ref milEvent);

            userHookData.milImageDisp = milImageDisp;
            userHookData.milImageGrab = milImageGrab;
            userHookData.milImageData = milImageData;
            userHookData.milEvent = milEvent;
            userHookData.milGrabNumber = 0;
            userHookData.milGrabIndex = 0;
            userHookData.milExit = 0;
            userHookData.m_nCam = nCamIndex;
            userHookDataHandle = GCHandle.Alloc(userHookData);

            userHookFuncDelegate = new MIL_DIG_HOOK_FUNCTION_PTR(DigHookFunction);
            userThreadDelegate = new MIL_THREAD_FUNCTION_PTR(MyThread__Live);

            MIL.MthrAlloc(milSystem, MIL.M_THREAD, MIL.M_DEFAULT, userThreadDelegate, GCHandle.ToIntPtr(userHookDataHandle), ref milThread);

            bFlipX = FlipX;
            bFlipY = FlipY;
            bInit = true;

            MIL.MappControl(MIL.M_DEFAULT, MIL.M_ERROR, MIL.M_PRINT_DISABLE);

            PrepareBlob();

            //=========
            MIL.MblobAlloc(milSystem, MIL.M_DEFAULT, MIL.M_DEFAULT, ref MilBlobContext);
            MIL.MblobControl(MilBlobContext, MIL.M_CENTER_OF_GRAVITY + MIL.M_BINARY, MIL.M_ENABLE);
            //=========
            // Old Style
            //MIL.MblobAllocFeatureList(milSystem, ref MilBlobContext);
            //MIL.MblobSelectFeature(MilBlobContext, MIL.M_AREA);
            //MIL.MblobSelectFeature(MilBlobContext, MIL.M_CENTER_OF_GRAVITY);
            // Allocate a blob result buffer.
            //MIL.MblobAllocResult(milSystem, ref MilBlobResult);
            //
            //
            //PrepareBlob();
            MIL.MblobAllocResult(milSystem, MIL.M_DEFAULT, MIL.M_DEFAULT, ref MilBlobResult);   //  대체검증완료

            Max2MinComparer = new MyReverserClass();

            SupremeTimer.QueryPerformanceFrequency(ref mTimerFrequency);
            ClearPoints();
            mFZC = new FZCoefficient();
            return true;
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void SelectWindow(IntPtr wndHandle)
        {
            MIL.MdispSelectWindow(milDisplay, milImageDisp, wndHandle);
            MIL.MdispControl(milDisplay, MIL.M_OVERLAY, MIL.M_ENABLE);
            MIL.MdispInquire(milDisplay, MIL.M_OVERLAY_ID, ref milOverlayImage);
            MIL.MdispInquire(milDisplay, MIL.M_TRANSPARENT_COLOR, ref keyColor);
            MIL.MdispControl(milDisplay, MIL.M_OVERLAY_CLEAR, MIL.M_DEFAULT);
            MIL.MdispControl(milDisplay, MIL.M_OVERLAY_SHOW, MIL.M_ENABLE);
            MIL.MdispZoom(milDisplay, dZoomFactorX, dZoomFactorY);
        }
        public void DrawClear()
        {
            MIL.MbufClear(milOverlayImage, keyColor);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void DrawSetDCalloc()
        {
            MIL.MbufControl(milOverlayImage, MIL.M_DC_ALLOC, MIL.M_DEFAULT);
            IntPtr hCustomDC = (IntPtr)MIL.MbufInquire(milOverlayImage, MIL.M_DC_HANDLE, MIL.M_NULL);
            //MessageBox.Show(m_nCam.ToString() + " hCustomDC" + hCustomDC.ToString());
            if (!hCustomDC.Equals(IntPtr.Zero))
            {
                gr = Graphics.FromHdc(hCustomDC);
                //MessageBox.Show("Cam = " + m_nCam);
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void DrawSetDCfree()
        {
            if (gr != null)
                gr.Dispose();
            MIL.MbufControl(milOverlayImage, MIL.M_DC_FREE, MIL.M_DEFAULT);
            MIL.MbufControl(milOverlayImage, MIL.M_MODIFIED, MIL.M_DEFAULT);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void DrawDCCross(Brush color)
        {
            DrawSetDCalloc();

            if (gr != null)
            {
                Pen pen = new Pen(color, 1);
                gr.DrawLine(pen, 0, SizeY / 2, SizeX, SizeY / 2);   //  시작점 - 끝점
            }

            DrawSetDCfree();
        }
        public void LiveA()
        {
            if (IsLiveA == true)
                HaltA();

            bLiveA = true;

            MIL.MdigGrabContinuous(milDigitizer, milImageDisp);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void GrabA()
        {
            if (IsVirtual) return;
            if (IsLiveA == true)
            {
                HaltA();
                Thread.Sleep(10);
            }

            while (OnGrab)
            {
                Thread.Sleep(10);
            }
            OnGrab = true;

            MIL.MbufClear(milImageDisp, 0);  //
            MIL.MdigGrab(milDigitizer, milImageGrab[0]);    //

            Thread.Sleep(1);

            MIL.MdigGrabWait(milDigitizer, MIL.M_GRAB_FRAME_END); //   카메라 없을 경우 에러남. 에러 처리 필요. 

            MIL.MbufCopy(milImageGrab[0], milCommonImageGrab[0]);
            MIL.MbufCopy(milImageGrab[0], milImageDisp);

            //MIL.MbufSave(string.Format("TestImage{0}.bmp", testIndex++), milCommonImageGrab[0]);
            OnGrab = false;
            //            MIL.MbufCopy(milXstepImageGrab[0], milImageDisp);
        }
        public int testIndex = 0;
        public void ClearHallCalStrokeBuf()
        {
            for (int i = 0; i < 2; i++)
            {

                MIL.MbufClear(OISXCalStrokeImg[i], 0);
                MIL.MbufClear(OISYCalStrokeImg[i], 0);
                MIL.MbufClear(OISXNonEPAStrokeImg[i], 0);
                MIL.MbufClear(OISYNonEPAStrokeImg[i], 0);
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void HaltA()
        {
            bLiveA = false;

            MIL.MdigHalt(milDigitizer);
        }
        public void GrabWithTime(int index)
        {
            if (IsVirtual) return;
            MIL.MdigGrab(milDigitizer, milCommonImageGrab[index]);
            SupremeTimer.QueryPerformanceCounter(ref GrabT1[index]);
        }
        public void BufCopy_User(int type)
        {
            if (IsVirtual) return;

            int count = 0;

            switch (type)
            {
                case 0: // OIS X 모드 
                case 3:
                    for (int i = 0; i < CLFrameCount[type]; i++)
                        MIL.MbufCopy(milCommonImageGrab[i], OISXImageData[count++]);
                    break;
                case 1: // OIS Y 모드
                case 5:
                    for (int i = 0; i < CLFrameCount[type]; i++)
                        MIL.MbufCopy(milCommonImageGrab[i], OISYImageData[count++]);
                    break;
                case 2: // OIS R 모드
                    for (int i = 0; i < CLFrameCount[type]; i++)
                        MIL.MbufCopy(milCommonImageGrab[i], OISRImageData[count++]);
                    break;
                //case 3: // Y Step 모드
                //    for (int i = start; i < end; i++)
                //        MIL.MbufCopy(milCommonImageGrab[i], YstepImageData[count++]);
                //   break;
                default: // 모드 없음
                    break;
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------c
        MIL_INT DigHookFunction(MIL_INT HookType, MIL_ID HookId, IntPtr HookDataPtr)
        {
            GCHandle HookDataHandle = GCHandle.FromIntPtr(HookDataPtr);
            HookDataObject UserHookDataPtr = HookDataHandle.Target as HookDataObject;

            MIL_ID ModifiedBufferId = MIL.M_NULL;

            MIL.MdigGetHookInfo(HookId, MIL.M_MODIFIED_BUFFER + MIL.M_BUFFER_ID, ref ModifiedBufferId);

            if (ModifiedBufferId != MIL.M_NULL)
            {
                // Fire Event
                MIL.MthrControl(UserHookDataPtr.milEvent, MIL.M_EVENT_SET, MIL.M_SIGNALED);
            }

            if (UserHookDataPtr.milGrabNumber > 0 && userHookData.milGrabIndex >= userHookData.milGrabNumber)
            {
                MIL.MdigProcess(milDigitizer, milImageGrab, MAX_DEFAULT_GRAB_COUNT, MIL.M_STOP, MIL.M_DEFAULT, userHookFuncDelegate, GCHandle.ToIntPtr(userHookDataHandle));
            }

            return 0;
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        static uint MyThread__Live(IntPtr ThreadParameters)
        {
            GCHandle threadParamHandle = GCHandle.FromIntPtr(ThreadParameters);
            HookDataObject TPar = threadParamHandle.Target as HookDataObject;

            while (TPar.milExit == 0)
            {
                // Wait for the Event
                MIL.MthrWait(TPar.milEvent, MIL.M_EVENT_WAIT);

                if (TPar.milExit == 1)
                    return 1;

                if (TPar.milGrabNumber == 0)    // Live
                {
                    MIL.MbufCopy(TPar.milImageGrab[TPar.milGrabIndex % MAX_GRAB_COUNT], TPar.milImageDisp);
                    //MIL.MbufCopyClip(TPar.milImageGrab[TPar.milGrabIndex % MAX_GRAB_COUNT], TPar.milImageDisp, -512, 0);

                }
                else                            // Record
                {
                    //MessageBox.Show("Here I am");
                    //MIL.MbufCopy(TPar.milImageGrab[TPar.milGrabIndex % MAX_GRAB_COUNT], TPar.milImageData[TPar.milGrabIndex]);
                    //MIL.MbufCopyClip(TPar.milImageGrab[TPar.milGrabIndex % MAX_GRAB_COUNT], TPar.milImageData[TPar.milGrabIndex], -512, 0);

                }
                TPar.milGrabIndex++;
            }

            return 1;
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void SaveImage(string strFileName, int index, int axis = -1)
        {
            switch (axis)
            {
                case 0:
                    MIL.MbufExport(strFileName, MIL.M_BMP, OISXImageData[index]);
                    break;
                case 1:
                    MIL.MbufExport(strFileName, MIL.M_BMP, OISYImageData[index]);
                    break;
                case 2:
                    MIL.MbufExport(strFileName, MIL.M_BMP, OISRImageData[index]);
                    break;
                //case 3:
                //    MIL.MbufExport(strFileName, MIL.M_BMP, YstepImageData[index]);
                //    break;
                default: // 모드 없음
                    MIL.MbufExport(strFileName, MIL.M_BMP, milCommonImageGrab[0]);
                    break;
            }
        }
        public void LoadBMPtoBuf0(string filename)
        {
            MIL_ID resMilID = MIL.MbufImport(filename, MIL.M_BMP, MIL.M_RESTORE, milSystem, ref milCommonImageGrab[0]);
            MIL.MbufCopy(resMilID, milImageDisp);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        public void BufCopy2Disp_OIS(int nIndex, int axis)
        {
            if (axis == 0)
            {
                MIL.MbufCopy(OISXImageData[nIndex], milImageDisp);
            }
            else if (axis == 1)
            {
                MIL.MbufCopy(OISYImageData[nIndex], milImageDisp);
            }
            else if (axis == 2)
            {
                MIL.MbufCopy(OISRImageData[nIndex], milImageDisp);
            }
        }
        public void ClearDisp()
        {
            MIL.MbufClear(milImageDisp, 64);  //  여기 정상
        }
        public void PrepareBlob()
        {
            MIL.MbufInquire(milImageDisp, MIL.M_SIZE_X, ref mBlobSizeX);
            MIL.MbufInquire(milImageDisp, MIL.M_SIZE_Y, ref mBlobSizeY);
            MIL.MbufAlloc2d(milSystem, mBlobSizeX, mBlobSizeY, 1 + MIL.M_UNSIGNED, MIL.M_IMAGE + MIL.M_PROC, ref MilBinImage);
        }
        public Rect[] mSearchRange = new Rect[10];
        public int[] mSearchYfrom = new int[4] { 0, 15, 30, 50 };
        public int[] mSearchYto = new int[4] { 75, 29, 49, 70 };
        public int[] mSearchXfrom = new int[4] { 0, 15, 30, 50 };
        public int[] mSearchXto = new int[4] { 14, 29, 49, 70 };
        public bool mIsDebug = false;
        public bool FineCOG(int index, int axis, ref double[] dTx, ref double[] dTy, ref double[] cx, ref double[] cy, ref long Nfound, bool isSave = false, bool IsDebug = false, int iBuf = 0)
        {
            //  cx,cy,dTx,dTy 공통 [0] : Left Sample, [1] : Right Sample
            //  dTx : Rotation
            //  dTy : reserved
            //  p_Value[][] 에 영상 들어있어야 함
            //  q_Value[][] 에 1/4 영상 들어있어야 함.
            //  mSearchModel[mSearchModelIndex] 에 모델 들어있어야함.
            switch (axis)
            {
                case 0:
                case 3:
                    MIL.MbufGet2d(OISXImageData[index], 0, 0, 1680, 340, p_Value[0]);
                    break;
                case 1:
                case 5:
                    MIL.MbufGet2d(OISYImageData[index], 0, 0, 1680, 340, p_Value[0]);
                    break;
                case 2:
                    MIL.MbufGet2d(OISRImageData[index], 0, 0, 1680, 340, p_Value[0]);
                    break;
                //case 3:
                //    MIL.MbufGet2d(YstepImageData[index], 0, 0, 1680, 340, p_Value[0]);
                //    break;
                default: // 모드 없음
                    MIL.MbufGet2d(milCommonImageGrab[index], 0, 0, 1680, 340, p_Value[0]);
                    break;
            }


            mSourceImg.SetArray(p_Value[0]);

            Mat src = new Mat();
            mSourceImg.CopyTo(src);
            Cv2.Blur(mSourceImg, src, new OpenCvSharp.Size(5, 5), new OpenCvSharp.Point(-1, -1), BorderTypes.Default);
            src.CopyTo(mSourceImg);

            mQuadImg = mSourceImg.Resize(new OpenCvSharp.Size(420, 85), 0.25, 0.25, InterpolationFlags.Linear);
            mQuadImg.GetArray(out q_Value[0]);

            mIsDebug = IsDebug;

            string bmpName = string.Format(FIO.BaseDir + "\\Image\\TestImage_{0}.bmp", index);

            if (isSave)
                SaveImage(bmpName, index, axis);

            if (IsDebug)
            {
                string newDir = "..\\RawData\\";
                if (!Directory.Exists(newDir))
                {
                    Directory.CreateDirectory(newDir);
                }

                int imgWidth = mSearchModel[mSearchModelIndex].width / 4;
                int imgHeight = mSearchModel[mSearchModelIndex].height / 4;
                StreamWriter wr = new StreamWriter(newDir + "QTimg" + m_nCam.ToString() + iBuf.ToString() + ".csv");
                String strTmp = "Y,";
                for (int xi = 0; xi < mSourceImg.Width / 4; xi += 1)
                {
                    strTmp += xi.ToString() + ",";
                }
                wr.WriteLine(strTmp);
                strTmp = "";
                for (int yi = 0; yi < imgHeight; yi += 1)
                {
                    strTmp = yi.ToString() + ",";
                    for (int xi = 0; xi < imgWidth; xi += 1)
                    {
                        strTmp += q_Value[iBuf][xi + yi * imgWidth].ToString() + ",";
                    }
                    wr.WriteLine(strTmp);
                }
                wr.Close();
            }

            int i = 0;
            int j = 0;
            long lconv = 0;
            long maxConv = 0;
            OpenCvSharp.Point[] posFoundS = new OpenCvSharp.Point[5];
            OpenCvSharp.Point[] posFoundT = new OpenCvSharp.Point[5];
            long[] convFoundS = new long[5];
            long[] convFoundT = new long[5];
            int cntFound = 0;
            bool bCand = false;
            bool bFinishViewRegion = false;

            int thresh0 = (int)(mSearchModel[mSearchModelIndex].conv * 0.4);
            int thresh1 = (int)(mSearchModel[mSearchModelIndex].conv * 0.5);
            int thresh2 = (int)(mSearchModel[mSearchModelIndex].conv * 0.6);
            int thresh3 = (int)(mSearchModel[mSearchModelIndex].conv * 0.7);
            int threshT = (int)(mSearchModel[mSearchModelIndex].conv * 0.93);

            //int xSearchRegion = 1100 / 4 ; // mSourceImg.Width / 4 - mSearchModel[mSearchModelIndex].width;
            int xSearchRegion = mSourceImg.Width / 4 - mSearchModel[mSearchModelIndex].width;
            int ySearchRegion = mSourceImg.Height / 4 - mSearchModel[mSearchModelIndex].height;

            int lwidth = mSearchModel[mSearchModelIndex].width;
            int lheight = mSearchModel[mSearchModelIndex].height;
            mConvSrcData = new byte[lwidth * lheight];

            bool IsSkipArea = false;
            double subconv = 0;
            int si = 0;
            bool bSaved = false;
            long lconv1 = 0;
            long lconv2 = 0;
            int prev_i = 0;

            int xSkipWidth = 40;
            int ySkipWidth = 0;

            if (mSearchRange[0].X > 0)
            {
                mSearchYfrom[mSearchModelIndex] = mSearchRange[0].Y;
                mSearchYto[mSearchModelIndex] = mSearchRange[0].Y + mSearchRange[0].Height;
                for (int rj = 0; rj < 4; rj++)
                {
                    if (mSearchRange[rj].X == 0)
                        continue;
                    if (mSearchYfrom[mSearchModelIndex] > mSearchRange[rj].Y)
                        mSearchYfrom[mSearchModelIndex] = mSearchRange[rj].Y;
                    if (mSearchYto[mSearchModelIndex] < mSearchRange[rj].Y + mSearchRange[rj].Height)
                        mSearchYto[mSearchModelIndex] = mSearchRange[rj].Y + mSearchRange[rj].Height;
                }
            }

            j = mSearchYfrom[mSearchModelIndex];

            while (j <= mSearchYto[mSearchModelIndex])
            {
                i = 4;
                if (mSearchRange[cntFound].X > 0)
                {
                    i = mSearchRange[cntFound].X;
                    xSearchRegion = mSearchRange[cntFound].X + mSearchRange[cntFound].Width;
                }

                while (i < xSearchRegion)
                {
                    if (cntFound == 4)
                    {
                        bFinishViewRegion = true;
                        break;
                    }
                    IsSkipArea = false;
                    if (cntFound > 0)
                    {
                        for (si = 0; si < cntFound; si++)
                        {
                            if (Math.Abs(posFoundS[si].X - i) < xSkipWidth && Math.Abs(posFoundS[si].Y - j) < 5)
                                IsSkipArea = true;
                            if (IsSkipArea)
                                break;
                        }
                    }
                    if (IsSkipArea)
                    {
                        i = posFoundS[si].X + xSkipWidth;
                        continue;
                    }
                    lconv = CalcConv(mSearchModelIndex, i, j, iBuf, ref subconv);
                    prev_i = i;
                    if (lconv < -thresh0)
                        i += 6;
                    if (lconv < 1)
                        i += 3;
                    else if (lconv < thresh0)
                        i += 2;
                    else if (lconv < thresh1)
                        i += 1;
                    else
                        i++;


                    if (lconv > threshT)
                    {
                        bCand = true;
                    }
                    if (lconv < thresh3 && bCand == true)
                    {
                        bCand = false;
                        if (bSaved)
                        {
                            bSaved = false;
                            maxConv = 0;

                            mSearchRange[cntFound].X = posFoundS[cntFound].X - 9;
                            mSearchRange[cntFound].Width = 18;
                            mSearchRange[cntFound].Y = posFoundS[cntFound].Y - 9;
                            mSearchRange[cntFound].Height = 18;

                            j = mSearchYfrom[mSearchModelIndex];    //  하나 찾으면 Y검색 시작점을 원상복귀

                            cntFound++;

                            if (cntFound == 2)
                            {
                                j = posFoundS[0].Y + ySkipWidth;
                                break;
                            }
                            else if (cntFound < 5)
                            {
                                i = posFoundS[cntFound - 1].X + xSkipWidth;
                                continue;
                            }
                            else
                            {
                                bFinishViewRegion = true;
                                break;
                            }
                        }
                    }
                    if (bCand)
                    {
                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////
                        bool IsCorrect = false;
                        if (mSearchModel[mSearchModelIndex].sub == 0)
                            IsCorrect = true;

                        else if (mSearchModel[mSearchModelIndex].sub > 100)
                        {
                            if (subconv < mSearchModel[mSearchModelIndex].subconv)
                                IsCorrect = true;
                        }
                        else
                        {
                            if (subconv > mSearchModel[mSearchModelIndex].subconv)
                                IsCorrect = true;
                        }
                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////

                        if (maxConv < lconv && (IsCorrect == true))
                        {
                            bSaved = true;
                            maxConv = lconv;
                            posFoundS[cntFound].X = i - 1;
                            posFoundS[cntFound].Y = j;
                            convFoundS[cntFound] = lconv;
                        }
                    }
                }
                j++;
                if (bFinishViewRegion)
                    break;
            }

            for (si = 0; si < cntFound; si++)
            {
                lconv1 = 0;
                if (posFoundS[si].Y > 0)
                    lconv1 = CalcConv(mSearchModelIndex, posFoundS[si].X, posFoundS[si].Y - 1, iBuf, ref subconv);

                lconv2 = CalcConv(mSearchModelIndex, posFoundS[si].X, posFoundS[si].Y + 1, iBuf, ref subconv);

                if (convFoundS[si] < lconv1)
                {
                    posFoundS[si].Y = posFoundS[si].Y - 1;
                    convFoundS[si] = lconv1;
                }
                if (convFoundS[si] < lconv2)
                {
                    posFoundS[si].Y = posFoundS[si].Y + 1;
                    convFoundS[si] = lconv2;
                }
            }

            int itrSum = 0;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Top View
            //  Sort
            int[] px = new int[4];
            int[] px2 = new int[4];
            int[] py = new int[4];
            long[] pconv = new long[4];

            for (int q = 0; q < 4; q++)
            {
                px[q] = posFoundS[q].X;
                px2[q] = px[q];
                py[q] = posFoundS[q].Y;
                pconv[q] = convFoundS[q];
            }
            Array.Sort(px, py); //  px 값이 작은 것 부터 정렬됨.
            Array.Sort(px2, pconv); //  px 값이 작은 것 부터 정렬됨.
            //   제품이 1개인 경우 배열중 앞쪽 2개는 0 으로 채워진다.
            for (int q = 0; q < 4; q++)
            {
                posFoundS[q].X = px[q];
                posFoundS[q].Y = py[q];
                convFoundS[q] = pconv[q];
            }
            //SaveConvRaw(mSearchModelIndex, posFoundS[0].X, posFoundS[0].Y, iBuf);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //  COG 계산
            //mIsDebug = false;
            cntFound = 0;
            //  Top View cx[0~3], cy[0~3] for Yaw
            double[] fcx = new double[3];
            double[] fcy = new double[3];
            for (int k = 0; k < 4; k++)
            {
                cx[k] = posFoundS[k].X;
                cy[k] = posFoundS[k].Y;
                if (cx[k] > 0)
                {
                    cntFound++;
                    itrSum += CalcCog(mSearchModelIndex, (int)cx[k], (int)cy[k], iBuf, ref fcx, ref fcy);
                    cx[k] = fcx[1];
                    cy[k] = fcy[1];
                }
            }
            for (int k = 0; k < 4; k++)
            {
                if (cx[k] != 0 && cx[k] < 420) //0 Index
                {
                    if (k == 0) continue;
                    cx[0] = cx[k]; cy[0] = cy[k];
                    cx[k] = 0; cy[k] = 0;
                }
                else if (cx[k] != 0 && cx[k] >= 420 && cx[k] < 840) //1 Index
                {
                    if (k == 1) continue;
                    cx[1] = cx[k]; cy[1] = cy[k];
                    cx[k] = 0; cy[k] = 0;
                }
                else if (cx[k] != 0 && cx[k] >= 840 && cx[k] < 1260) //2 Index
                {
                    if (k == 2) continue;
                    cx[2] = cx[k]; cy[2] = cy[k];
                    cx[k] = 0; cy[k] = 0;
                }
                else if (cx[k] != 0 && cx[k] >= 1260)//3 Index
                {
                    if (k == 3) continue;
                    cx[3] = cx[k]; cy[3] = cy[k];
                    cx[k] = 0; cy[k] = 0;
                }
            }

            //  Clockwise => +


            Nfound = cntFound;
            return true;
        }

        //public bool FineCOG(int index, int axis, ref double[] dTx, ref double[] dTy, ref double[] cx, ref double[] cy, ref long Nfound, bool isSave = false, bool IsDebug = false, int iBuf = 0)
        //{
        //    //  cx,cy,dTx,dTy 공통 [0] : Left Sample, [1] : Right Sample
        //    //  dTx : Rotation
        //    //  dTy : reserved
        //    //  p_Value[][] 에 영상 들어있어야 함
        //    //  q_Value[][] 에 1/4 영상 들어있어야 함.
        //    //  mSearchModel[mSearchModelIndex] 에 모델 들어있어야함.
        //    switch (axis)
        //    {
        //        case 0:
        //            MIL.MbufGet2d(OISXImageData[index], 0, 0, 1680, 340, p_Value[0]);
        //            break;
        //        case 1:
        //            MIL.MbufGet2d(OISYImageData[index], 0, 0, 1680, 340, p_Value[0]);
        //            break;
        //        case 2:
        //            MIL.MbufGet2d(OISRImageData[index], 0, 0, 1680, 340, p_Value[0]);
        //            break;
        //        //case 3:
        //        //    MIL.MbufGet2d(YstepImageData[index], 0, 0, 1680, 340, p_Value[0]);
        //        //    break;
        //        default: // 모드 없음
        //            MIL.MbufGet2d(milCommonImageGrab[index], 0, 0, 1680, 340, p_Value[0]);
        //            break;
        //    }


        //    mSourceImg.SetArray(p_Value[0]);
        //    mQuadImg = mSourceImg.Resize(new OpenCvSharp.Size(420, 85), 0.25, 0.25, InterpolationFlags.Linear);
        //    mQuadImg.GetArray(out q_Value[0]);

        //    mIsDebug = IsDebug;

        //    string bmpName = string.Format(FIO.BaseDir + "\\Image\\TestImage_{0}.bmp", index);

        //    if(isSave)
        //        SaveImage(bmpName, index, axis);

        //    if (IsDebug)
        //    {
        //        string newDir = "..\\RawData\\";
        //        if (!Directory.Exists(newDir))
        //        {
        //            Directory.CreateDirectory(newDir);
        //        }

        //        int imgWidth = mSearchModel[mSearchModelIndex].width / 4;
        //        int imgHeight = mSearchModel[mSearchModelIndex].height / 4;
        //        StreamWriter wr = new StreamWriter(newDir + "QTimg" + m_nCam.ToString() + iBuf.ToString() + ".csv");
        //        String strTmp = "Y,";
        //        for (int xi = 0; xi < mSourceImg.Width / 4; xi += 1)
        //        {
        //            strTmp += xi.ToString() + ",";
        //        }
        //        wr.WriteLine(strTmp);
        //        strTmp = "";
        //        for (int yi = 0; yi < imgHeight; yi += 1)
        //        {
        //            strTmp = yi.ToString() + ",";
        //            for (int xi = 0; xi < imgWidth; xi += 1)
        //            {
        //                strTmp += q_Value[iBuf][xi + yi * imgWidth].ToString() + ",";
        //            }
        //            wr.WriteLine(strTmp);
        //        }
        //        wr.Close();
        //    }

        //    int i = 0;
        //    int j = 0;
        //    long lconv = 0;
        //    long maxConv = 0;
        //    OpenCvSharp.Point[] posFoundS = new OpenCvSharp.Point[5];
        //    OpenCvSharp.Point[] posFoundT = new OpenCvSharp.Point[5];
        //    long[] convFoundS = new long[5];
        //    long[] convFoundT = new long[5];
        //    int cntFound = 0;
        //    bool bCand = false;
        //    bool bFinishViewRegion = false;

        //    int thresh0 = (int)(mSearchModel[mSearchModelIndex].conv * 0.4);
        //    int thresh1 = (int)(mSearchModel[mSearchModelIndex].conv * 0.5);
        //    int thresh2 = (int)(mSearchModel[mSearchModelIndex].conv * 0.6);
        //    int thresh3 = (int)(mSearchModel[mSearchModelIndex].conv * 0.7);
        //    int threshT = (int)(mSearchModel[mSearchModelIndex].conv * 0.93);

        //    //int xSearchRegion = 1100 / 4 ; // mSourceImg.Width / 4 - mSearchModel[mSearchModelIndex].width;
        //    int xSearchRegion = mSourceImg.Width / 4 - mSearchModel[mSearchModelIndex].width;
        //    int ySearchRegion = mSourceImg.Height / 4 - mSearchModel[mSearchModelIndex].height;

        //    int lwidth = mSearchModel[mSearchModelIndex].width;
        //    int lheight = mSearchModel[mSearchModelIndex].height;
        //    mConvSrcData = new byte[lwidth * lheight];

        //    bool IsSkipArea = false;
        //    double subconv = 0;
        //    int si = 0;
        //    bool bSaved = false;
        //    long lconv1 = 0;
        //    long lconv2 = 0;
        //    int prev_i = 0;

        //    int xSkipWidth = 40;

        //    //if (mSearchRange[0].X > 0)
        //    //{
        //    //    mSearchYfrom[mSearchModelIndex] = mSearchRange[0].Y;
        //    //    mSearchYto[mSearchModelIndex] = mSearchRange[0].Y + mSearchRange[0].Height;
        //    //    for (int rj = 0; rj < 4; rj++)
        //    //    {
        //    //        if (mSearchRange[rj].X == 0)
        //    //            continue;
        //    //        if (mSearchYfrom[mSearchModelIndex] > mSearchRange[rj].Y)
        //    //            mSearchYfrom[mSearchModelIndex] = mSearchRange[rj].Y;
        //    //        if (mSearchYto[mSearchModelIndex] < mSearchRange[rj].Y + mSearchRange[rj].Height)
        //    //            mSearchYto[mSearchModelIndex] = mSearchRange[rj].Y + mSearchRange[rj].Height;
        //    //    }
        //    //}

        //    j = mSearchYfrom[mSearchModelIndex];

        //    while (j <= mSearchYto[mSearchModelIndex])
        //    {
        //        i = 4;
        //        //if (mSearchRange[cntFound].X > 0)
        //        //{
        //        //    i = mSearchRange[cntFound].X;
        //        //    xSearchRegion = mSearchRange[cntFound].X + mSearchRange[cntFound].Width;
        //        //}
        //        if (j == 32 || j==31 || j== 33)
        //            i = 4;

        //        while (i < xSearchRegion)
        //        {
        //            if (cntFound == 4)
        //            {
        //                bFinishViewRegion = true;
        //                break;
        //            }
        //            IsSkipArea = false;
        //            if ( i==140)
        //                IsSkipArea = false;

        //            if (cntFound > 0)
        //            {
        //                for (si = 0; si < cntFound; si++)
        //                {
        //                    if (Math.Abs(posFoundS[si].X - i) < xSkipWidth && Math.Abs(posFoundS[si].Y - j) < 5)
        //                        IsSkipArea = true;
        //                    if (IsSkipArea)
        //                        break;
        //                }
        //            }
        //            if (IsSkipArea)
        //            {
        //                i = posFoundS[si].X + xSkipWidth;
        //                continue;
        //            }
        //            lconv = CalcConv(mSearchModelIndex, i, j, iBuf, ref subconv);
        //            prev_i = i;
        //            if (lconv < -thresh0)
        //                i += 6;
        //            if (lconv < 1)
        //                i += 3;
        //            else if (lconv < thresh0)
        //                i += 2;
        //            else if (lconv < thresh1)
        //                i += 1;
        //            else
        //                i++;


        //            if (lconv > threshT)
        //            {
        //                bCand = true;
        //            }
        //            if (lconv < thresh3 && bCand == true)
        //            {
        //                bCand = false;
        //                if (bSaved)
        //                {
        //                    bSaved = false;
        //                    maxConv = 0;

        //                    //mSearchRange[cntFound].X = posFoundS[cntFound].X - 9;
        //                    //mSearchRange[cntFound].Width = 18;
        //                    //mSearchRange[cntFound].Y = posFoundS[cntFound].Y - 9;
        //                    //mSearchRange[cntFound].Height = 18;

        //                    //j = mSearchYfrom[mSearchModelIndex];    //  ?? ??? Y?? ???? ????
        //                    j = 0;

        //                    cntFound++;

        //                    if (cntFound < 5)
        //                    {
        //                        i = posFoundS[cntFound - 1].X + xSkipWidth;
        //                        continue;
        //                    }
        //                    else
        //                    {
        //                        bFinishViewRegion = true;
        //                        break;
        //                    }
        //                }
        //            }
        //            if (bCand)
        //            {
        //                //////////////////////////////////////////////////////////////////////////////
        //                //////////////////////////////////////////////////////////////////////////////
        //                bool IsCorrect = false;
        //                if (mSearchModel[mSearchModelIndex].sub == 0)
        //                    IsCorrect = true;

        //                else if (mSearchModel[mSearchModelIndex].sub > 100)
        //                {
        //                    if (subconv < mSearchModel[mSearchModelIndex].subconv)
        //                        IsCorrect = true;
        //                }
        //                else
        //                {
        //                    if (subconv > mSearchModel[mSearchModelIndex].subconv)
        //                        IsCorrect = true;
        //                }
        //                //////////////////////////////////////////////////////////////////////////////
        //                //////////////////////////////////////////////////////////////////////////////

        //                if (maxConv < lconv && (IsCorrect == true))
        //                {
        //                    bSaved = true;
        //                    maxConv = lconv;
        //                    posFoundS[cntFound].X = i - 1;
        //                    posFoundS[cntFound].Y = j;
        //                    convFoundS[cntFound] = lconv;
        //                }
        //            }
        //        }
        //        j++;
        //        if (bFinishViewRegion)
        //            break;
        //    }

        //    for (si = 0; si < cntFound; si++)
        //    {
        //        lconv1 = 0;
        //        if (posFoundS[si].Y > 0)
        //            lconv1 = CalcConv(mSearchModelIndex, posFoundS[si].X, posFoundS[si].Y - 1, iBuf, ref subconv);

        //        lconv2 = CalcConv(mSearchModelIndex, posFoundS[si].X, posFoundS[si].Y + 1, iBuf, ref subconv);

        //        if (convFoundS[si] < lconv1)
        //        {
        //            posFoundS[si].Y = posFoundS[si].Y - 1;
        //            convFoundS[si] = lconv1;
        //        }
        //        if (convFoundS[si] < lconv2)
        //        {
        //            posFoundS[si].Y = posFoundS[si].Y + 1;
        //            convFoundS[si] = lconv2;
        //        }
        //    }

        //    int itrSum = 0;

        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    /// Top View
        //    //  Sort
        //    int[] px = new int[4];
        //    int[] px2 = new int[4];
        //    int[] py = new int[4];
        //    long[] pconv = new long[4];

        //    for (int q = 0; q < 4; q++)
        //    {
        //        px[q] = posFoundS[q].X;
        //        px2[q] = px[q];
        //        py[q] = posFoundS[q].Y;
        //        pconv[q] = convFoundS[q];
        //    }
        //    Array.Sort(px, py); //  px ?? ?? ? ?? ???.
        //    Array.Sort(px2, pconv); //  px ?? ?? ? ?? ???.
        //    //   ??? 1?? ?? ??? ?? 2?? 0 ?? ????.
        //    for (int q = 0; q < 4; q++)
        //    {
        //        posFoundS[q].X = px[q];
        //        posFoundS[q].Y = py[q];
        //        convFoundS[q] = pconv[q];
        //    }
        //    //SaveConvRaw(mSearchModelIndex, posFoundS[0].X, posFoundS[0].Y, iBuf);
        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    //  COG 계산
        //    //mIsDebug = false;
        //    cntFound = 0;
        //    //  Top View cx[0~3], cy[0~3] for Yaw
        //    double[] fcx = new double[3];
        //    double[] fcy = new double[3];
        //    for (int k = 0; k < 4; k++)
        //    {
        //        cx[k] = posFoundS[k].X;
        //        cy[k] = posFoundS[k].Y;
        //        if (cx[k] > 0)
        //        {
        //            cntFound++;
        //            itrSum += CalcCog(mSearchModelIndex, (int)cx[k], (int)cy[k], iBuf, ref fcx, ref fcy);
        //            cx[k] = fcx[1];
        //            cy[k] = fcy[1];
        //        }
        //    }


        //    for (int k = 0; k < 4; k++)
        //    {
        //        if (cx[k] != 0 && cx[k] < 420) //0 Index
        //        {
        //            if (k == 0) continue;
        //            cx[0] = cx[k]; cy[0] = cy[k];
        //            cx[k] = 0; cy[k] = 0;
        //        }
        //        else if (cx[k] != 0 && cx[k] >= 420 && cx[k] < 840) //1 Index
        //        {
        //            if (k == 1) continue;
        //            cx[1] = cx[k]; cy[1] = cy[k];
        //            cx[k] = 0; cy[k] = 0;
        //        }
        //        else if (cx[k] != 0 && cx[k] >= 840 && cx[k] < 1260) //2 Index
        //        {
        //            if (k == 2) continue;
        //            cx[2] = cx[k]; cy[2] = cy[k];
        //            cx[k] = 0; cy[k] = 0;
        //        }
        //        else if (cx[k] != 0 && cx[k] >= 1260)//3 Index
        //        {
        //            if (k == 3) continue;
        //            cx[3] = cx[k]; cy[3] = cy[k];
        //            cx[k] = 0; cy[k] = 0;
        //        }
        //    }

        //    //  Clockwise => +
        //    Nfound = cntFound;
        //    return true;
        //}
        public void ConvertXYtheta(double[] cx, double[] cy, double[] tx, ref double[] resCx, ref double[] resCy, ref double[] resThetaMin)
        {
            //  um, min
            //if (cx[0] > 420)
            //{
            //    //  Left Sample Only
            //    resThetaMin[0] = (cy[1] - cy[0]) / (cx[1] - cx[0]);
            //    resCx[0] = (cx[0] + cx[1]) / 2;
            //    resCy[0] = (cy[0] + cy[1]) / 2;
            //}
            //else if (cx[1] > 840)
            //{
            //    //  Right Sample Only
            //    resThetaMin[1] = (cy[1] - cy[0]) / (cx[1] - cx[0]);
            //    resCx[1] = (cx[0] + cx[1]) / 2;
            //    resCy[1] = (cy[0] + cy[1]) / 2;
            //}
            //else if (cx[0] > 0)
            //{
            //    //  Both Samples
            //    resThetaMin[0] = (cy[3] - cy[1]) / (cx[3] - cx[1]);
            //    resThetaMin[1] = (cy[2] - cy[0]) / (cx[2] - cx[0]);
            //    resCx[0] = (cx[3] + cx[1]) / 2;
            //    resCy[0] = (cy[3] + cy[1]) / 2;
            //    resCx[1] = (cx[0] + cx[2]) / 2;
            //    resCy[1] = (cy[0] + cy[2]) / 2;
            //}
            //else if (cx[0]<1)
            //{
            //    if (cx[2]<420)
            //    {
            //        resThetaMin[1] = (cy[3] - cy[2]) / (cx[3] - cx[2]);
            //        resCx[1] = (cx[2] + cx[3]) / 2;
            //        resCy[1] = (cy[2] + cy[3]) / 2;
            //    }else if (cx[2]>420)
            //    {
            //        resThetaMin[0] = (cy[3] - cy[2]) / (cx[3] - cx[2]);
            //        resCx[0] = (cx[2] + cx[3]) / 2;
            //        resCy[0] = (cy[2] + cy[3]) / 2;
            //    }
            //}
            //  Both Samples
            resThetaMin[0] = (cy[3] - cy[1]) / (cx[3] - cx[1]);
            resThetaMin[1] = (cy[2] - cy[0]) / (cx[2] - cx[0]);
            resCx[0] = (cx[3] + cx[1]) / 2;
            resCy[0] = (cy[3] + cy[1]) / 2;
            resCx[1] = (cx[0] + cx[2]) / 2;
            resCy[1] = (cy[0] + cy[2]) / 2;


            resCx[0] = resCx[0] * 11;// um
            resCy[0] = resCy[0] * 11;// um
            resCx[1] = resCx[1] * 11;// um
            resCy[1] = resCy[1] * 11;// um

            resThetaMin[0] = resThetaMin[0] * 60 * 180 / Math.PI;//min
            resThetaMin[1] = resThetaMin[1] * 60 * 180 / Math.PI;//min
        }
        public int CalcCog(int model, int x, int y, int iBuf, ref double[] cx, ref double[] cy)
        {
            //  Preprocess for q_Value to have -1,0,1 only
            int fWidth = mSourceImg.Width;
            int fHeight = mSourceImg.Height;
            int lwidth = mSearchModel[model].width * 4;
            int lheight = mSearchModel[model].height * 4;
            //double mx = 0;
            //double my = 0;
            //double smx = 0;
            //double smy = 0;
            //int cnt = 0;

            int[] srcData = new int[lwidth * lheight];
            int[] sortedData = new int[lwidth * lheight];
            int[] xDiffData = new int[lwidth * lheight];
            int[] yDiffData = new int[lwidth * lheight];
            //int xdiff = 0;
            //int ydiff = 0;

            double[] Vavg = new double[lheight];
            lheight = lheight > (fHeight - y) ? (fHeight - y) : lheight;
            int pos = 0;
            cx[1] = 4 * x + lwidth / 2;
            cy[1] = 4 * y + lheight / 2;
            cx[0] = 4 * x + lwidth / 2;
            cy[0] = 4 * y + lheight / 2;
            double[] oldcx = new double[3];
            double[] oldcy = new double[3];
            int srcPt = 0;
            int itr = 0;
            int oldx = 0;
            int oldy = 0;
            //string lstr = "";
            for (itr = 0; itr < 4; itr++)
            {
                x = (int)(cx[0] - lwidth / 2);
                y = (int)(cy[0] - lheight / 2);
                double xrsd = (cx[0] - lwidth / 2) - x;
                double yrsd = (cy[0] - lheight / 2) - y;

                if (oldx != x || oldy != y)
                {
                    //StreamWriter wr = new StreamWriter("Raw" + itr.ToString() + ".csv");
                    for (int j = 0; j < lheight; j++)
                    {
                        //lstr = "";
                        for (int i = 0; i < lwidth; i++)
                        {
                            pos = i + j * lwidth;
                            srcData[pos] = 255 - p_Value[iBuf][x + i + (y + j) * fWidth];
                            //lstr += srcData[pos].ToString() + ",";
                        }
                        //wr.WriteLine(lstr);
                    }
                    //wr.Close();

                    Array.Copy(srcData, sortedData, srcData.Length);
                    Array.Sort(sortedData);
                    oldx = x;
                    oldy = y;
                }
                int binBtm = sortedData[(int)(srcData.Length * 0.7)];    //  반전영상에서 어두운 X % 지점에서 끊는다.
                List<FZCoefficient.Point2d> xLeft = new List<FZCoefficient.Point2d>();
                List<FZCoefficient.Point2d> xRight = new List<FZCoefficient.Point2d>();
                List<FZCoefficient.Point2d> yTop = new List<FZCoefficient.Point2d>();
                List<FZCoefficient.Point2d> yBtm = new List<FZCoefficient.Point2d>();
                int hfrom = lheight / 4;
                int hto = (3 * lheight) / 4;
                for (int i = 0; i < lwidth; i++)
                {
                    FZCoefficient.Point2d pt = new FZCoefficient.Point2d();
                    pt.X = i;
                    for (int j = hfrom; j < hto; j++)
                    {
                        pos = i + j * lwidth;
                        if (Math.Abs(i - lwidth / 2) < 4)
                            continue;
                        if (srcData[pos] <= binBtm)
                            continue;
                        else
                            srcPt = srcData[pos] - binBtm;

                        pt.Y += srcPt;
                    }
                    if (i < lwidth / 2)
                        xLeft.Add(pt);
                    else
                        xRight.Add(pt);
                }
                double max = 0;
                int maxIndexL = 0;
                int maxIndexR = 0;
                for (int i = 0; i < xLeft.Count; i++)
                {
                    if (max < xLeft[i].Y)
                    {
                        max = xLeft[i].Y;
                        maxIndexL = i;
                    }
                }
                max = 0;
                for (int i = 0; i < xRight.Count; i++)
                {
                    if (max < xRight[i].Y)
                    {
                        max = xRight[i].Y;
                        maxIndexR = i;
                    }
                }
                FZCoefficient.Point2d[] xL = new FZCoefficient.Point2d[11];
                FZCoefficient.Point2d[] xR = new FZCoefficient.Point2d[11];
                for (int i = 0; i < 11; i++)
                {
                    xL[i].X = xLeft[maxIndexL - 5 + i].X;
                    xL[i].Y = xLeft[maxIndexL - 5 + i].Y;

                    xR[i].X = xRight[maxIndexR - 5 + i].X;
                    xR[i].Y = xRight[maxIndexR - 5 + i].Y;
                }

                FZCoefficient.Poly2nd coefL = new FZCoefficient.Poly2nd();
                FZCoefficient.Poly2nd coefR = new FZCoefficient.Poly2nd();
                mFZC.mcLMS2ndPoly(xL, xL.Length, ref coefL);
                mFZC.mcLMS2ndPoly(xR, xR.Length, ref coefR);
                cx[1] = (coefL.b + coefR.b) / 2 + x;

                int xfrom = lwidth / 4;
                int xto = (3 * lwidth) / 4;
                for (int j = 0; j < lheight; j++)
                {
                    FZCoefficient.Point2d pt = new FZCoefficient.Point2d();
                    pt.X = j;
                    for (int i = xfrom; i < xto; i++)
                    {
                        pos = i + j * lwidth;
                        if (Math.Abs(i - lheight / 2) < 4)
                            continue;
                        if (srcData[pos] <= binBtm)
                            continue;
                        else
                            srcPt = srcData[pos] - binBtm;

                        pt.Y += srcPt;
                    }
                    if (j < lheight / 2)
                        yTop.Add(pt);
                    else
                        yBtm.Add(pt);
                }
                max = 0;
                int maxIndexT = 0;
                int maxIndexB = 0;
                for (int i = 0; i < yTop.Count; i++)
                {
                    if (max < yTop[i].Y)
                    {
                        max = yTop[i].Y;
                        maxIndexT = i;
                    }
                }
                max = 0;
                for (int i = 0; i < yBtm.Count; i++)
                {
                    if (max < yBtm[i].Y)
                    {
                        max = yBtm[i].Y;
                        maxIndexB = i;
                    }
                }
                FZCoefficient.Point2d[] yT = new FZCoefficient.Point2d[11];
                FZCoefficient.Point2d[] yB = new FZCoefficient.Point2d[11];
                for (int i = 0; i < 11; i++)
                {
                    yT[i].X = yTop[maxIndexT - 5 + i].X;
                    yT[i].Y = yTop[maxIndexT - 5 + i].Y;

                    yB[i].X = yBtm[maxIndexB - 5 + i].X;
                    yB[i].Y = yBtm[maxIndexB - 5 + i].Y;
                }

                FZCoefficient.Poly2nd coefT = new FZCoefficient.Poly2nd();
                FZCoefficient.Poly2nd coefB = new FZCoefficient.Poly2nd();
                mFZC.mcLMS2ndPoly(yT, yT.Length, ref coefT);
                mFZC.mcLMS2ndPoly(yB, yB.Length, ref coefB);
                cy[1] = (coefT.b + coefB.b) / 2 + y;

                cx[0] = cx[1];
                cy[0] = cy[1];

                double dcx = oldcx[0] - cx[0];
                double dcy = oldcy[0] - cy[0];
                if (Math.Abs(dcx) < 0.001 && Math.Abs(dcy) < 0.001)
                    break;

                oldcx[0] = cx[0];
                oldcx[1] = cx[1];
                oldcy[0] = cy[0];
                oldcy[1] = cy[1];
            }

            cx[1] = cx[0];
            cy[1] = cy[0];
            return itr;
        }
        public void GetCommonBuf(int index, byte[] livebuf, int axis)
        {
            switch (axis)
            {
                case 0:
                    MIL.MbufGet2d(OISXImageData[index], 0, 0, 1680, 340, livebuf);
                    break;
                case 1:
                    MIL.MbufGet2d(OISYImageData[index], 0, 0, 1680, 340, livebuf);
                    break;
                case 2:
                    MIL.MbufGet2d(OISRImageData[index], 0, 0, 1680, 340, livebuf);
                    break;
                //case 3:
                //    MIL.MbufGet2d(YstepImageData[index], 0, 0, 1680, 340, livebuf);
                //    break;
                default: // 모드 없음
                    MIL.MbufGet2d(milImageDisp, 0, 0, 1680, 340, livebuf);
                    break;
            }

        }
        byte[] mConvSrcData = null;
        public long CalcConv(int model, int x, int y, int iBuf, ref double subconv, bool blast = false)
        {
            //  Preprocess for q_Value to have -1,0,1 only
            double avgBin = 0;
            double avgBinC = 0;
            int avgBinCcnt = 0;
            int quaterWidth = mSourceImg.Width / 4;
            int quaterHeight = mSourceImg.Height / 4;
            int lwidth = mSearchModel[model].width;
            int lheight = mSearchModel[model].height;
            int minBin = 99;
            int maxBin = 0;
            int zeroCnt = 0;
            int pix = 0;
            double pv = 0;
            //byte[] mConvSrcData = new byte[lwidth * lheight];

            double[] Vavg = new double[lheight];

            minBin = 99;
            maxBin = 0;
            avgBin = 0;

            lheight = lheight > (quaterHeight - y) ? (quaterHeight - y) : lheight;

            for (int j = 0; j < lheight; j++)
                for (int i = 0; i < lwidth; i++)
                {
                    pix = q_Value[iBuf][x + i + (y + j) * quaterWidth];
                    avgBin += pix;
                    if ((j > 2 && j < lheight - 1) && (i > 2 && i < lwidth - 3))
                    {
                        avgBinC += pix;
                        avgBinCcnt++;
                    }

                    if (minBin > pix)
                        minBin = pix;
                    if (i > 3 && i < lwidth - 3)
                    {
                        if (maxBin < pix)
                            maxBin = pix;
                    }
                    if (pix < 2)
                        zeroCnt++;

                    if (i < 2 || i > lwidth - 3)
                        Vavg[j] += pix / 4.0; //     Vavg[j] 에는 BOX 좌우측 끝단 각 세로 2줄에 대해서 같은 Y 좌표를 가지는 Pixel 들의 평균개조값 - 2 가 저장되게 된다.
                                              //Vavg[j] += pix / 4.0 - 0.5; //     Vavg[j] 에는 BOX 좌우측 끝단 각 세로 2줄에 대해서 같은 Y 좌표를 가지는 Pixel 들의 평균개조값 - 2 가 저장되게 된다.
                }
            if (maxBin - minBin < 17)
                return 0;
            if (maxBin - minBin < 24 && model == 1)
                return 0;

            avgBin = avgBin / (lwidth * lheight);
            avgBinC = avgBinC / avgBinCcnt;

            minBin = (int)((avgBin + minBin) / 2);
            double lmfactor = 1;

            if (avgBin < 22)
                lmfactor = 22 / avgBin;


            long res = 0;
            if (model == 0)    //  Side View
            {
                double VavgBin = 0;
                for (int j = 1; j < lheight; j++)
                    Vavg[j] = (Vavg[j - 1] + 3 < Vavg[j] ? Vavg[j - 1] + 3 : Vavg[j]);  // Vavg 값중에 Y 촤표의 증가에 따라 갑자기 튀는 값이 있으면 필터링해서 3 이상 튀지 않도록 해준다.

                for (int j = 0; j < lheight; j++)
                    VavgBin += Vavg[j]; //  VavgBin Vavg 의 합

                avgBin = avgBin - VavgBin / lheight;    // avgBin = 전체 평균 - (좌우측 세로 2열의 평균값 -2)

                if (blast)
                    maxBin = 0;

                for (int j = 0; j < lheight; j++)
                    for (int i = 0; i < lwidth; i++)
                    {
                        mConvSrcData[i + j * lwidth] = q_Value[iBuf][x + i + (y + j) * quaterWidth];

                        pv = q_Value[iBuf][x + i + (y + j) * quaterWidth] - Vavg[j];    //  
                        if (pv < avgBin - 0.5)
                            res -= mSearchModel[model].img[i + lwidth * j];  //  * -1
                        else if (pv > 4 * avgBin)
                            res += 2 * mSearchModel[model].img[i + lwidth * j];  //  * +2
                        else if (pv > avgBin + 0.5)
                            res += mSearchModel[model].img[i + lwidth * j];  //  * +1
                    }
                if (res > mSearchModel[model].conv * 0.9)
                {
                    if ((avgBinC - avgBin) < 1.5)
                        res = (res * 4) / 5;
                }

            }
            else    //  Top View
            {

                for (int j = 0; j < lheight; j++)
                    for (int i = 0; i < lwidth; i++)
                    {
                        mConvSrcData[i + j * lwidth] = q_Value[iBuf][x + i + (y + j) * quaterWidth];

                        if (q_Value[iBuf][x + i + (y + j) * quaterWidth] < minBin - 1)
                            res -= mSearchModel[model].img[i + lwidth * j];  //  * -1
                        else if (q_Value[iBuf][x + i + (y + j) * quaterWidth] > minBin + 2)
                            res += mSearchModel[model].img[i + lwidth * j];  //  * +1
                    }
            }
            subconv = 0;
            if (res > mSearchModel[model].conv)
            {
                switch (mSearchModel[model].sub % 100)
                {
                    case 1:
                        mCalcDiffConv(mConvSrcData, model, ref subconv);
                        break;
                    case 2:
                        mCalcXDiffConv(mConvSrcData, model, ref subconv);
                        break;
                    case 3:
                        mCalcYDiffConv(mConvSrcData, model, ref subconv);
                        break;
                    case 4:
                        mCalcInOutAvg(mConvSrcData, model, ref subconv);
                        break;
                    case 5:
                        mCalcLeftRightAvg(mConvSrcData, model, ref subconv);
                        break;
                    case 6:
                        mCalcTopBottomAvg(mConvSrcData, model, ref subconv);
                        break;
                    case 7:
                        mCalcCenterToLeftRightAvg(mConvSrcData, model, ref subconv);
                        break;
                    case 8:
                        mCalcCenterToTopBottomAvg(mConvSrcData, model, ref subconv);
                        break;
                    case 9:
                        mCalcFullStdev(mConvSrcData, model, ref subconv);
                        break;
                    case 10:
                        mCalcInFullStdev(mConvSrcData, model, ref subconv);
                        break;
                    default:
                        break;
                }
            }
            return res;
        }
        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        void mCalcDiffConv(byte[] srcData, int modelIndex, ref double res)
        {
            int data = 0;
            int model = 0;
            double avgSrc = 0;
            //double avgModel = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 1; i < lwidth - 1; i++)
            {
                for (int j = 1; j < lheight - 1; j++)
                {
                    data = srcData[i - 1 + j * lwidth] + srcData[i + (j - 1) * lwidth];
                    data = data - srcData[i + 1 + j * lwidth] - srcData[i + (j + 1) * lwidth];
                    model = mSearchModel[modelIndex].img[i - 1 + j * lwidth] + mSearchModel[modelIndex].img[i + (j - 1) * lwidth];
                    model = model - mSearchModel[modelIndex].img[i + 1 + j * lwidth] - mSearchModel[modelIndex].img[i + (j + 1) * lwidth];

                    res += data * model;
                    avgSrc += srcData[i + j * lwidth];
                }
            }
            avgSrc = avgSrc / ((lwidth - 2) * (lheight - 2));   //  Avg of Src

            res = res / ((lwidth - 2) * (lheight - 2));   //  Avg of SrcDiff * ModelDiff
                                                          //            res = res / (avgSrc * avgModel);    //   normalize
            res = res / avgSrc;    //   normalize
        }
        void mCalcXDiffConv(byte[] srcData, int modelIndex, ref double res)
        {
            int data = 0;
            int model = 0;
            double avgSrc = 0;
            //double avgModel = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 1; i < lwidth - 1; i++)
            {
                for (int j = 1; j < lheight - 1; j++)
                {
                    data = srcData[i - 1 + j * lwidth] + srcData[i - 1 + (j - 1) * lwidth] + srcData[i - 1 + (j + 1) * lwidth];
                    data = data - srcData[i + 1 + j * lwidth] - srcData[i + 1 + (j - 1) * lwidth] - srcData[i + 1 + (j + 1) * lwidth];
                    model = mSearchModel[modelIndex].img[i - 1 + j * lwidth] + mSearchModel[modelIndex].img[i - 1 + (j - 1) * lwidth] + mSearchModel[modelIndex].img[i - 1 + (j + 1) * lwidth];
                    model = model - mSearchModel[modelIndex].img[i + 1 + j * lwidth] - mSearchModel[modelIndex].img[i + 1 + (j - 1) * lwidth] - mSearchModel[modelIndex].img[i + 1 + (j + 1) * lwidth];

                    res += data * model;
                    avgSrc += srcData[i + j * lwidth];
                }
            }
            avgSrc = avgSrc / ((lwidth - 2) * (lheight - 2));   //  Avg of Src

            res = res / ((lwidth - 2) * (lheight - 2));   //  Avg of SrcDiff * ModelDiff
            res = res / avgSrc;    //   normalize
        }
        void mCalcYDiffConv(byte[] srcData, int modelIndex, ref double res)
        {
            int data = 0;
            int model = 0;
            double avgSrc = 0;
            //double avgModel = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 1; i < lwidth - 1; i++)
            {
                for (int j = 1; j < lheight - 1; j++)
                {
                    data = srcData[i - 1 + (j - 1) * lwidth] + srcData[i + (j - 1) * lwidth] + srcData[i + 1 + (j - 1) * lwidth];
                    data = data - srcData[i - 1 + (j + 1) * lwidth] - srcData[i + (j + 1) * lwidth] - srcData[i + 1 + (j + 1) * lwidth];
                    model = mSearchModel[modelIndex].img[i - 1 + (j - 1) * lwidth] + mSearchModel[modelIndex].img[i + (j - 1) * lwidth] + mSearchModel[modelIndex].img[i + 1 + (j - 1) * lwidth];
                    model = model - mSearchModel[modelIndex].img[i - 1 + (j + 1) * lwidth] - mSearchModel[modelIndex].img[i + (j + 1) * lwidth] - mSearchModel[modelIndex].img[i + 1 + (j + 1) * lwidth];

                    res += data * model;
                    avgSrc += srcData[i + j * lwidth];
                    //avgModel += mSearchModel[modelIndex].img[i - 1 + j * lwidth];
                }
            }
            avgSrc = avgSrc / ((lwidth - 2) * (lheight - 2));   //  Avg of Src
            //avgModel = avgModel / ((lwidth - 2) * (lheight - 2));   //  Avg of Model

            res = res / ((lwidth - 2) * (lheight - 2));   //  Avg of SrcDiff * ModelDiff
            //res = res / (avgSrc * avgModel);    //   normalize
            res = res / avgSrc;    //   normalize  
        }
        void mCalcInOutAvg(byte[] srcData, int modelIndex, ref double res)
        {
            int icount = 0;
            int ocount = 0;
            double avgSrc = 0;
            double isrc = 0;
            double osrc = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 0; i < lwidth; i++)
                for (int j = 0; j < lheight; j++)
                    avgSrc += srcData[i + j * lwidth];

            avgSrc = avgSrc / (lwidth * lheight);   //  Avg of Src

            for (int i = 0; i < lwidth; i++)
            {
                for (int j = 0; j < lheight; j++)
                {
                    if (i < 2 || i > lwidth - 3 || j < 2 || j > lheight - 3)
                    {
                        isrc += srcData[i + j * lwidth];
                        icount++;
                    }
                    else
                    {
                        osrc += srcData[i + j * lwidth];
                        ocount++;
                    }
                }
            }
            isrc = isrc / icount;
            osrc = osrc / ocount;

            res = (isrc - osrc) / avgSrc;
        }
        void mCalcLeftRightAvg(byte[] srcData, int modelIndex, ref double res)
        {
            int Lcount = 0;
            int Rcount = 0;
            double avgSrc = 0;
            double Lsrc = 0;
            double Rsrc = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;


            for (int i = 0; i < lwidth; i++)
                for (int j = 0; j < lheight; j++)
                    avgSrc += srcData[i + j * lwidth];

            avgSrc = avgSrc / (lwidth * lheight);   //  Avg of Src

            for (int i = 0; i < lwidth; i++)
            {
                for (int j = 0; j < lheight; j++)
                {
                    if (i < lwidth / 2)
                    {
                        Lsrc += srcData[i + j * lwidth];
                        Lcount++;
                    }
                    else
                    {
                        Rsrc += srcData[i + j * lwidth];
                        Rcount++;
                    }
                }
            }
            Lsrc = Lsrc / Lcount;
            Rsrc = Rsrc / Rcount;

            res = (Lsrc - Rsrc) / avgSrc;
        }
        void mCalcTopBottomAvg(byte[] srcData, int modelIndex, ref double res)
        {
            int Tcount = 0;
            int Bcount = 0;
            double avgSrc = 0;
            double Tsrc = 0;
            double Bsrc = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 0; i < lwidth; i++)
                for (int j = 0; j < lheight; j++)
                    avgSrc += srcData[i + j * lwidth];

            avgSrc = avgSrc / (lwidth * lheight);   //  Avg of Src

            for (int i = 0; i < lwidth; i++)
            {
                for (int j = 0; j < lheight; j++)
                {
                    if (j < lheight / 2)
                    {
                        Tsrc += srcData[i + j * lwidth];
                        Tcount++;
                    }
                    else
                    {
                        Bsrc += srcData[i + j * lwidth];
                        Bcount++;
                    }
                }
            }
            Tsrc = Tsrc / Tcount;
            Bsrc = Bsrc / Bcount;

            res = (Tsrc - Bsrc) / avgSrc;
        }
        void mCalcCenterToLeftRightAvg(byte[] srcData, int modelIndex, ref double res)
        {
            int LRcount = 0;
            int Ccount = 0;
            double avgSrc = 0;
            double LRsrc = 0;
            double Csrc = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 0; i < lwidth; i++)
                for (int j = 0; j < lheight; j++)
                    avgSrc += srcData[i + j * lwidth];

            avgSrc = avgSrc / (lwidth * lheight);   //  Avg of Src

            for (int i = 0; i < lwidth; i++)
            {
                for (int j = 0; j < lheight; j++)
                {
                    if (i < 2 || i > lwidth - 3)
                    {
                        LRsrc += srcData[i + j * lwidth];
                        LRcount++;
                    }
                    else if (i > 2 && i < lwidth - 3)
                    {
                        Csrc += srcData[i + j * lwidth];
                        Ccount++;
                    }
                }
            }
            LRsrc = LRsrc / LRcount;
            Csrc = Csrc / Ccount;

            res = (LRsrc - Csrc) / avgSrc;
        }
        void mCalcCenterToTopBottomAvg(byte[] srcData, int modelIndex, ref double res)
        {
            int TBcount = 0;
            int Ccount = 0;
            double avgSrc = 0;
            double TBsrc = 0;
            double Csrc = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;


            for (int i = 0; i < lwidth; i++)
                for (int j = 0; j < lheight; j++)
                    avgSrc += srcData[i + j * lwidth];

            avgSrc = avgSrc / (lwidth * lheight);   //  Avg of Src

            for (int i = 0; i < lwidth; i++)
            {
                for (int j = 0; j < lheight; j++)
                {
                    if (j < 2 || j > lheight - 3)
                    {
                        TBsrc += srcData[i + j * lwidth];
                        TBcount++;
                    }
                    else if (j > 2 && j < lheight - 3)
                    {
                        Csrc += srcData[i + j * lwidth];
                        Ccount++;
                    }
                }
            }
            TBsrc = TBsrc / TBcount;
            Csrc = Csrc / Ccount;

            res = (TBsrc - Csrc) / avgSrc;
        }
        void mCalcFullStdev(byte[] srcData, int modelIndex, ref double res)
        {
            int data = 0;
            int count = 0;
            double sum = 0;
            double ssum = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 0; i < lwidth; i++)
            {
                for (int j = 0; j < lheight; j++)
                {
                    data = srcData[i + j * lwidth];
                    sum += data;
                    ssum += data * data;
                    count++;
                }
            }
            res = Math.Sqrt(ssum / count - sum / count * sum / count) / (sum / count);
        }
        void mCalcInFullStdev(byte[] srcData, int modelIndex, ref double res)
        {
            int data = 0;
            int i_count = 0;
            double i_sum = 0;
            double i_ssum = 0;
            double i_avg = 0;
            int f_count = 0;
            double f_sum = 0;
            double f_ssum = 0;
            double f_avg = 0;
            int lwidth = mSearchModel[modelIndex].width;
            int lheight = mSearchModel[modelIndex].height;

            for (int i = 0; i < lwidth; i++)
            {
                for (int j = 0; j < lheight; j++)
                {
                    data = srcData[i + j * lwidth];
                    if (i > 3 && i < lwidth - 4 && j > 2 && j < lheight - 3)
                    {
                        i_sum += data;
                        i_ssum += data * data;
                        i_count++;
                    }
                    f_sum += data;
                    f_ssum += data * data;
                    f_count++;
                }
            }
            f_avg = f_sum / f_count;
            i_avg = i_sum / i_count;

            double i_std = Math.Sqrt(i_ssum / i_count - i_avg * i_avg);
            double f_std = Math.Sqrt(f_ssum / f_count - f_avg * f_avg);
            res = (i_std - f_std) / ((f_sum + i_sum) / (i_count + f_count));
        }
    }

}
