﻿using Basler.Pylon;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActroLib
{

    public delegate void FunctionPointer(int port, string testItem);
    public class LogEvent : EventArgs
    {
        public class Params
        {
            public int Ch { get; set; }
            public string Msg { get; set; }
            public bool IsUse { get; set; }
        }

        public event EventHandler<Params> Evented = null;

        public virtual void AddLog(int ch, string msg, bool isUse = false)
        {
            Evented?.Invoke(this, new Params() { Ch = ch, Msg = msg, IsUse = isUse });
        }
    }
    public class ShowEvent : EventArgs
    {
        public class Params
        {
            public int Ch { get; set; }
            public string Key { get; set; }
        }

        public event EventHandler<Params> Evented = null;

        public virtual void Show(int ch, string key)
        {
            Evented?.Invoke(this, new Params() { Ch = ch, Key = key });
        }
    }
    public static class PROCESS
    {
        public static string SWVersion = "22061701";
        public static LogEvent LogEvent = new LogEvent();
        public static ShowEvent ShowEvent = new ShowEvent();
        public static Model Model = new Model();
        public static Recipe Rcp = new Recipe();
        public static Spec Spec = new Spec();
        public static Option Option = new Option();
        public static SEM1217S DrvIC = new SEM1217S(Model.IsVirtual);
        public static CurrentPath Current = new CurrentPath();
        public static VisionParam VParam = new VisionParam();
        public static ChartList Chart = new ChartList();
        public static CurrentLed CurrentLed = new CurrentLed();
        public static Process Process = new Process();
    }
    public class HallParam
    {
        public int gain;
        public int Offset;
        public int Bias;
        public int min;
        public int max;
        public int mid;
        public int measure_Min;
        public int measure_Max;
        public void Clear()
        {
            gain = 0;
            Offset = 0;
            Bias = 0;
            min = 0;
            max = 0;
            mid = 0;
            measure_Min = 0;
            measure_Max = 0;
        }
    };
    public class CalLine_fit
    {
        public class Item
        {
            public double x;
            public double y;
        }
        public List<Item> Point = new List<Item>();
        public double dSlope;
        public double dYintercept;
        public CalLine_fit(int count)
        {
            //for (int i = 0; i < count; i++)
            //    Point.Add(new Item());
        }
        public void Line_fitting()
        {
            if (Point.Count < 1) return;
            double SUMx = 0;
            double SUMy = 0;
            double SUMxy = 0;
            double SUMxx = 0;
            //calculate various sums 
            for (int i = 0; i < Point.Count; i++)
            {
                SUMx += Point[i].x;
                SUMy += Point[i].y;
                SUMxy += Point[i].x * Point[i].y;
                SUMxx += Point[i].x * Point[i].x;
            }
            //calculate the means of x and y
            double AVGy = SUMy / Point.Count;
            double AVGx = SUMx / Point.Count;
            //slope or a1
            dSlope = (Point.Count * SUMxy - SUMx * SUMy) / (Point.Count * SUMxx - SUMx * SUMx);
            //y itercept or a0
            dYintercept = AVGy - dSlope * AVGx;
        }
    }
    public enum AO_TYPE { _CLX, _CLY, _CLR, _CLXStep, _CLYStep, _CLRStep, };
    public class Process
    {
        //Class Reference ===================================================================
        public LogEvent Log { get { return PROCESS.LogEvent; } }
        public ShowEvent ShowEvent { get { return PROCESS.ShowEvent; } }
        public Recipe Rcp { get { return PROCESS.Rcp; } }
        public Spec Spec { get { return PROCESS.Spec; } }
        public Option Option { get { return PROCESS.Option; } }
        public SEM1217S DrvIC { get { return PROCESS.DrvIC; } }
        public Model Model { get { return PROCESS.Model; } }
        public CurrentPath Current { get { return PROCESS.Current; } }
        public ChartList Chart { get { return PROCESS.Chart; } }
        public CurrentLed CurrentLed { get { return PROCESS.CurrentLed; } }
        public VisionParam VParam { get { return PROCESS.VParam; } }
        //App Variable ======================================================================
        public const int LGMON_AMP_N_PHASE = 0x0C08;
        public const int LGCTRL = 0x0C00;
        public const int LG_AMPLITUDE = 0x0C01;
        public const int LG_FREQUENCY = 0x0C02;
        public const int LG_SKIPNUM = 0x0C03;
        public const int LG_TRYNUM = 0x0C04;
        public const byte LG_CTRL_MASK_ENABLE = 0x01;
        public const byte LOOPGAIN_X_AXIS = 0x01;
        public const byte LOOPGAIN_Y_AXIS = 0x11;
        public const int PID_CF_16_667_KHZ = 16667;
        public const int GAIN_PLUS = 1;
        public const int GAIN_MINUS = 0;
        public const int TEMP_INIT_VALUE = 1234;
        public const int PHASE_MARGIN_NG_THRESHOLD = 30;

        public bool IsVirtual = true;
        public bool TestVal = false;
        public int RepeatRun = 0;
        public int CurrentRun = 0;
        public bool SuddenStop = false;
        public bool IsLastInspect = false;
        public bool mbHavePH = false;
        public long TimerFrequency = 0;
        public int OISHallTestInspIndex = 0;
        public byte CrlDftFlag = 0;
        public int PMInspIndex = 0;
        public bool PM_OISUseX = false;
        public bool PM_OISUseY = false;
        public byte[] FWCode = null;
        public string[] PIDLine = null;
        public string[] MESLine = null;
        public int lXNonEPA_H_min = 0;
        public int lXNonEPA_H_max = 0;
        public int lYNonEPA_H_min = 0;
        public int lYNonEPA_H_max = 0;

        public event EventHandler<int> RunStart = null;
        public event EventHandler<int> RepeatEnd = null;
        public event EventHandler<int> RunEnd = null;
        public class Items
        {
            public string Name { get; set; }
            public FunctionPointer Func { get; set; }
            public bool IsScan { get; set; }
            public string Time = "0";
        }
        public ObservableCollection<Items> ItemList = new ObservableCollection<Items>();

        public bool m_bAllLEDOn = false;
        public bool m_bLeftLEDOn = false;
        public bool m_bRightLEDOn = false;
        public bool m_bLEDMarkCheck = false;
        public double[] GrabTime = new double[10000];
        public double[] GrabHall = new double[10000];
        //Port Variable ====================================================================
        public int PortCnt = 1;
        public List<AutoResetEvent> AnalogOut_DoneEvent = new List<AutoResetEvent>();
        public List<AutoResetEvent> AnalogIn_DoneEvent = new List<AutoResetEvent>();
        public List<MicroTimer> microTimer_AO = new List<MicroTimer>();
        public List<MicroTimer> microTimer_AI = new List<MicroTimer>();
        public List<int> Count_AI = new List<int>();
        public List<int> Count_AO = new List<int>();
        public List<int> MaxCount_AI = new List<int>();
        public List<int> MaxCount_AO = new List<int>();
        public List<int> AOType = new List<int>();
        public List<bool> IsRun = new List<bool>();
        public List<bool> mbDontInterrupt = new List<bool>();
        public List<int> mCurrentSequence = new List<int>();
        public List<bool> mPortIdle = new List<bool>();
        //Type Variable =====================================================================
        public int AxisCnt = 6;
        public List<long[]> CLOutTime = new List<long[]>();
        public List<long[]> CLInTime = new List<long[]>();
        public List<double[]> Waveform_CL = new List<double[]>();
        public List<long> CLPeakTime = new List<long>();
        public List<int> CL_DataCount = new List<int>();
        public List<string> CLName = new List<string>();
        //Channel Variable ====================================================================
        public int ChannelCount { get; set; }
        public List<string> ErrMsg = new List<string>();
        public List<bool> ChannelOn = new List<bool>();
        public List<string> FWVersion = new List<string>();
        public List<string> StrIndex = new List<string>();
        public List<string> StrBarcode = new List<string>();
        public List<double[]> Hall_X1 = new List<double[]>();
        public List<double[]> Hall_X2 = new List<double[]>();
        public List<double[]> Hall_Y = new List<double[]>();
        public List<double[]> SensCurrent = new List<double[]>();
        public List<int> XCrossOffset = new List<int>();
        public List<int> YCrossOffset = new List<int>();
        public List<bool> isHallCalFailforChOff = new List<bool>();
        public List<bool> HallCalFinish = new List<bool>();
        public List<bool> IsFirstReadNVM = new List<bool>();
        public List<bool> IsHallTestDone = new List<bool>();
        public List<bool> IsHallTestFail = new List<bool>();
        public List<bool> IsHallTestResult = new List<bool>();
        public List<bool> IsHallTestFinishState = new List<bool>();
        public List<int> OISHallTestCheckIndex = new List<int>();
        public List<int> OISHallTestRes = new List<int>();
        public List<int[]> OISHallTestResValue = new List<int[]>();
        public List<List<HallParam>> HallParam = new List<List<HallParam>>();

        public List<double> XGainThr = new List<double>();
        public List<double> YGainThr = new List<double>();
        public List<double> XGainThr_2nd = new List<double>();
        public List<double> YGainThr_2nd = new List<double>();
        public class LoopGainResult
        {   
            public double phaseMargin;
            public double pmFreq;
            public double maxDiffGain;
            public void Clear()
            {
                phaseMargin = 0;
                pmFreq = 0;
                maxDiffGain = 0;
            }
        }
        public List<LoopGainResult> LG_Result = new List<LoopGainResult>();
        public List<int> PMCheckIndex = new List<int>();
        public List<double> XH_OFST = new List<double>();
        public List<double> YH_OFST = new List<double>();
        //=======================================================================================
        public Process()
        {
            //Port Variable =============================================================
            for (int i = 0; i < PortCnt; i++)
            {
                AnalogOut_DoneEvent.Add(new AutoResetEvent(false));
                AnalogIn_DoneEvent.Add(new AutoResetEvent(false));
                microTimer_AO.Add(new MicroTimer());
                microTimer_AI.Add(new MicroTimer());
                Count_AI.Add(0);
                Count_AO.Add(0);
                MaxCount_AI.Add(0);
                MaxCount_AO.Add(0);
                AOType.Add(0);
                IsRun.Add(false);
                mbDontInterrupt.Add(false);
                mCurrentSequence.Add(0);
                mPortIdle.Add(true);
            }

            microTimer_AO[0].MicroTimerElapsed += new MicroTimer.MicroTimerElapsedEventHandler(OnTimedEvent_AO_L);
            microTimer_AI[0].MicroTimerElapsed += new MicroTimer.MicroTimerElapsedEventHandler(OnTimedEvent_AI_L);
            if (PortCnt > 1)
            {
                microTimer_AO[1].MicroTimerElapsed += new MicroTimer.MicroTimerElapsedEventHandler(OnTimedEvent_AO_R);
                microTimer_AI[1].MicroTimerElapsed += new MicroTimer.MicroTimerElapsedEventHandler(OnTimedEvent_AI_R);
            }
            //Type Variable ============================================================
            for (int i = 0; i < AxisCnt; i++)
            {
                CLOutTime.Add(new long[10000]);
                CLInTime.Add(new long[10000]);
                Waveform_CL.Add(new double[10000]);
                CLPeakTime.Add(0);
                CL_DataCount.Add(0);
            }
            CLName.Add("OIS X");
            CLName.Add("OIS Y");
            CLName.Add("OIS R");
            CLName.Add("Step X");
            CLName.Add("Step Y");
            CLName.Add("Step R");
            //Chnnel Variable =============================================================
            ChannelCount = 2;
            if (PortCnt > 1) ChannelCount = 4;
            for (int i = 0; i < ChannelCount; i++)
            {
                ErrMsg.Add("");
                ChannelOn.Add(true);
                FWVersion.Add("");
                StrIndex.Add("");
                StrBarcode.Add("");
                Hall_X1.Add(new double[10000]);
                Hall_X2.Add(new double[10000]);
                Hall_Y.Add(new double[10000]);
                SensCurrent.Add(new double[10000]);
                XCrossOffset.Add(0);
                YCrossOffset.Add(0);
                isHallCalFailforChOff.Add(false);
                HallCalFinish.Add(false);
                IsFirstReadNVM.Add(false);
                IsHallTestDone.Add(false);
                IsHallTestFail.Add(false);
                IsHallTestResult.Add(false);
                IsHallTestFinishState.Add(false);
                OISHallTestCheckIndex.Add(0);
                OISHallTestRes.Add(0);
                OISHallTestResValue.Add(new int[6]);
                HallParam.Add(new List<HallParam>());
                for (int j = 0; j < AxisCnt; j++)
                {
                    HallParam[i].Add(new HallParam());
                }
                XGainThr.Add(0);
                YGainThr.Add(0);
                XGainThr_2nd.Add(0);
                YGainThr_2nd.Add(0);
                LG_Result.Add(new LoopGainResult());
                PMCheckIndex.Add(0);
                XH_OFST.Add(0);
                YH_OFST.Add(0);
            }
            //ActionList Add ===============================================================
            ItemList.Add(new Items() { Name = "IC Check", Func = IC_Check });
            ItemList.Add(new Items() { Name = "FW Download", Func = FW_Download });
            ItemList.Add(new Items() { Name = "Read FW Version", Func = Read_FW_Versione });
            ItemList.Add(new Items() { Name = "PID Update", Func = PID_Update });
            ItemList.Add(new Items() { Name = "Barcode Write", Func = Barcode_Write });
            ItemList.Add(new Items() { Name = "Hall Calibration", Func = Hall_Calibration, IsScan = true });
            ItemList.Add(new Items() { Name = "OIS EPA", Func = OIS_EPA });
            ItemList.Add(new Items() { Name = "Hall NVM Read Only", Func = Hall_NVM_Read_Only });
            ItemList.Add(new Items() { Name = "OIS X", Func = OIS_X, IsScan = true });
            ItemList.Add(new Items() { Name = "OIS Y", Func = OIS_Y, IsScan = true });
            ItemList.Add(new Items() { Name = "OIS R", Func = OIS_R, IsScan = true });
            ItemList.Add(new Items() { Name = "XYAging", Func = XYAging });
            //ItemList.Add(new Items() { Name = "FRA Aging 2", Func = FRA_Aging });
            ItemList.Add(new Items() { Name = "XY PhaseMargin", Func = XY_PhaseMargin });
            //ItemList.Add(new Items() { Name = "XY PhaseMargin 2nd", Func = XY_PhaseMargin });
            ItemList.Add(new Items() { Name = "LoopGain @ 10Hz", Func = LoopGain_10Hz });
            ItemList.Add(new Items() { Name = "Hall Deviation", Func = Hall_Deviation, IsScan = true });
            ItemList.Add(new Items() { Name = "OIS Hall Test", Func = OIS_Hall_Test });
            ItemList.Add(new Items() { Name = "Test Circuit Open Short", Func = Test_Circuit_Open_Short });
            ItemList.Add(new Items() { Name = "Hall Stdev XY", Func = Hall_Stdev_XY });
            ItemList.Add(new Items() { Name = "Hall Decenter XY", Func = Hall_Decenter_XY });
            ItemList.Add(new Items() { Name = "X Step Response", Func = Step_X, IsScan = true });
            //ItemList.Add(new Items() { Name = "Y Step Response", Func = Y_Step_Response, IsScan = true });
            ItemList.Add(new Items() { Name = "XY Hall Mid Tilt Test", Func = XY_Hall_Mid_Tilt_Test, IsScan = true });
            ItemList.Add(new Items() { Name = "Linearity Compensation", Func = Linearity_Compensation, IsScan = true });
            ItemList.Add(new Items() { Name = "XY Displacement Accuracy", Func = XY_Displacement_Accuracy, IsScan = true });

            IsVirtual = DrvIC.IsVirtual = Model.IsVirtual;
        }
        private void OnTimedEvent_AI_L(object sender, MicroTimerEventArgs timerEventArgs)
        {
            OnTimedEvent_AI(0);
        }
        private void OnTimedEvent_AI_R(object sender, MicroTimerEventArgs timerEventArgs)
        {
            OnTimedEvent_AI(1);
        }
        private void OnTimedEvent_AO_L(object sender, MicroTimerEventArgs timerEventArgs)
        {
            OnTimedEvent_AO(0);
        }
        private void OnTimedEvent_AO_R(object sender, MicroTimerEventArgs timerEventArgs)
        {
            OnTimedEvent_AO(1);
        }
        private void OnTimedEvent_AI(int port)
        {
            if (Count_AI[port] >= MaxCount_AI[port])
            {
                microTimer_AI[port].Enabled = false;
                AnalogIn_DoneEvent[port].Set();
                return;
            }
            long CurTime = 0;
            SupremeTimer.QueryPerformanceCounter(ref CurTime);

            CLInTime[AOType[port]][Count_AI[port]] = CurTime;

            int ch = port * 2;

            for (int i = ch; i < ch + 2; i++)
            {
                if (ChannelOn[i])
                    SensCurrent[i][Count_AI[port]] = DrvIC.GetCurrent(i);   //Current Sensing,  unit : mA
            }
            Count_AI[port]++;
        }
        private void OnTimedEvent_AO(int port)
        {
            int ch = port * 2;
            long CurTime = 0;
            long CurTime2 = 0;

            SupremeTimer.QueryPerformanceCounter(ref CurTime);

            if (Count_AO[port] >= MaxCount_AO[port]) //Last Time
            {
                for (int i = ch; i < ch + 2; i++)
                {
                    if (ChannelOn[i])
                    {
                        Hall_X1[i][Count_AO[port] - 1] = DrvIC.ReadHall(i, 0);
                        Hall_X2[i][Count_AO[port] - 1] = DrvIC.ReadHall(i, 1);
                        Hall_Y[i][Count_AO[port] - 1] = DrvIC.ReadHall(i, 2);
                        Log.AddLog(i, string.Format("Target : {0}, X1 hall : {1}, X2 hall : {2}, Y hall : {3}",
                            Waveform_CL[AOType[port]][Count_AO[port] - 1], Hall_X1[i][Count_AO[port] - 1], Hall_X2[i][Count_AO[port] - 1], Hall_Y[i][Count_AO[port] - 1]));
                        SensCurrent[i][Count_AO[port] - 1] = DrvIC.GetCurrent(i);   //Current Sensing,  unit : mA
                    }
                }
                SupremeTimer.QueryPerformanceCounter(ref CurTime2);
                oCam[port].FPS = 1010 + ((CurTime2 - CurTime) % 90) / 10.0;

                microTimer_AO[port].Enabled = false;
                AnalogOut_DoneEvent[port].Set();
                return;
            }
            if (AOType[port] < (int)AO_TYPE._CLXStep)
            {
                if (ChannelOn[ch] || ChannelOn[ch + 1])
                    oCam[port].GrabWithTime(Count_AO[port]);
            }

            for (int i = ch; i < ch + 2; i++)
            {
                if (ChannelOn[i])
                {
                    if (Count_AO[port] == 0) continue;
                    Hall_X1[i][Count_AO[port] - 1] = DrvIC.ReadHall(i, 0);
                    Hall_X2[i][Count_AO[port] - 1] = DrvIC.ReadHall(i, 1);
                    Hall_Y[i][Count_AO[port] - 1] = DrvIC.ReadHall(i, 2);
                    Log.AddLog(i, string.Format("Target : {0}, X1 hall : {1}, X2 hall : {2}, Y hall : {3}",
                        Waveform_CL[AOType[port]][Count_AO[port] - 1], Hall_X1[i][Count_AO[port] - 1], Hall_X2[i][Count_AO[port] - 1], Hall_Y[i][Count_AO[port] - 1]));
                    SensCurrent[i][Count_AO[port] - 1] = DrvIC.GetCurrent(i);   //Current Sensing,  unit : mA
                }
            }

            SupremeTimer.QueryPerformanceCounter(ref CurTime);
            CLInTime[AOType[port]][Count_AO[port]] = CurTime;

            for (int i = ch; i < ch + 2; i++)
            {
                if (ChannelOn[i])
                    DrvIC.Move(i, AOType[port], (short)(Waveform_CL[AOType[port]][Count_AO[port]]));
            }

            SupremeTimer.QueryPerformanceCounter(ref CurTime);
            CLOutTime[AOType[port]][Count_AO[port]] = CurTime;

            if (Count_AO[port] > 1 && CLPeakTime[AOType[port]] == 0)
                if (Waveform_CL[AOType[port]][Count_AO[port]] < Waveform_CL[AOType[port]][Count_AO[port] - 1])  //  방향 반전되는 시점을 Peak Time 으로 지정한다. 일단 지정되면 다시 지정하는 일은 없도록 한다.
                    CLPeakTime[AOType[port]] = CLOutTime[AOType[port]][Count_AO[port] - 1] + TimerFrequency / 500;

            Count_AO[port]++;
        }
        private void Process_Function(int port, string testItem)
        {
            mbDontInterrupt[port] = false;

            long startTime = 0;
            SupremeTimer.QueryPerformanceCounter(ref startTime);
            SupremeTimer.QueryPerformanceFrequency(ref TimerFrequency);

            int index = 0;
            for (int i = 0; i < ItemList.Count; i++)
            {
                if (testItem.Contains(ItemList[i].Name))
                {
                    index = i; break;
                }
            }

            int ch = port * 2;
            if (!ChannelOn[ch] && !ChannelOn[ch + 1])
                return;

            Task Func1 = null, Func2 = null;

            if (ItemList[index].IsScan)
            {
                if (ChannelOn[ch])
                {
                    Log.AddLog(ch, testItem + " Start", false);
                }
                if (ChannelOn[ch + 1])
                {
                    Log.AddLog(ch + 1, testItem + " Start", false);
                }
                Func1 = new Task(() => ItemList[index].Func(port, testItem));
                Func1.Start();
                if (Func1 != null) Task.WaitAll(Func1);
            }
            else
            {
                if (ChannelOn[ch])
                {
                    Func1 = new Task(() => ItemList[index].Func(ch, testItem));
                    Func1.Start();
                    Log.AddLog(ch, testItem + " Start", false);
                }
                if (ChannelOn[ch + 1])
                {
                    Func2 = new Task(() => ItemList[index].Func(ch + 1, testItem));
                    Func2.Start();
                    Log.AddLog(ch + 1, testItem + " Start", false);
                }
                if (Func1 != null && Func2 != null) Task.WaitAll(Func1, Func2);
                else
                {
                    if (Func1 != null) Task.WaitAll(Func1);
                    if (Func2 != null) Task.WaitAll(Func2);
                }
            }

            long endTime = 0;
            SupremeTimer.QueryPerformanceCounter(ref endTime);
            double ellipse = (endTime - startTime) / (double)(TimerFrequency);

            Log.AddLog(ch, testItem + "\t" + ellipse.ToString("F3") + "sec", true);

            ItemList[index].Time = ellipse.ToString("F3");
        }
        public void SetSearchYRegion()
        {
            oCam[0].mSearchYfrom[0] = 0;

            oCam[0].mSearchYto[0] = 65;
            oCam[0].mSearchRange = new Rect[10];
        }
        private void Process_Start(int port)
        {
            try
            {
                if (!IsVirtual)
                {
                    //if (oCam[port].IsLiveA == true)
                    //    oCam[port].HaltA();
                    oCam[port].ClearHallCalStrokeBuf();
                }
                else
                {
                    Log.AddLog(0, "Virtual Process_Start =========");
                    Log.AddLog(1, "Virtual Process_Start =========");
                }
                PM_OISUseX = false;
                PM_OISUseY = false;
                //IsTesting[port] = true;

                int ch = port * 2;
                int count = Rcp.ToDoList.Count;
                if (count == 0)
                {
                    Thread.Sleep(2000);
                    for (int i = ch; i < ch + 2; i++)
                        ErrMsg[i] = "Test Item is Empty";
                    return;
                }

                for (int i = ch; i < ch + 2; i++)
                {
                    ChannelOn[i] = true;
                    ErrMsg[i] = "";
                    Spec.PassFails[i].FirstFailIndex = 0;
                }

                if (ErrMsg[ch] != "" && ErrMsg[ch + 1] != "")
                {
                    return;
                }

                if (!ChannelOn[ch])
                    ErrMsg[ch] = "Socket Empty";

                if (!ChannelOn[ch + 1])
                    ErrMsg[ch + 1] = "Socket Empty";



                long startTime = 0;
                SupremeTimer.QueryPerformanceCounter(ref startTime);
                SupremeTimer.QueryPerformanceFrequency(ref TimerFrequency);

                bool loopContinue = true;

                int todoCnt = 0;

                MakeWaveform(port);
                mbHavePH = false;

                for (int i = ch; i < ch + 2; i++)
                {
                    if (ChannelOn[i])
                    {
                        StrIndex[i] = (Spec.LastSampleNum + i + 1).ToString();
                    }
                }

                SetSearchYRegion();

                while (todoCnt < count)
                {
                    if (SuddenStop)
                    {
                        ErrMsg[ch] = "SuddenStop";
                        ErrMsg[ch + 1] = "SuddenStop";
                        break;
                    }

                    mCurrentSequence[port] = todoCnt;

                    if (todoCnt == count - 1)
                        IsLastInspect = true;
                    else
                        IsLastInspect = false;

                    string testItem = Rcp.ToDoList[todoCnt];

                    for (int i = ch; i < ch + 2; i++)
                    {
                        if (ChannelOn[i])
                        {
                            Log.AddLog(i, StrIndex[i] + ">> " + testItem, true);
                        }
                    }

                    if (testItem.Contains("Phase"))
                        mbHavePH = true;
                    if (testItem.Contains("OIS X"))
                        PM_OISUseX = true;
                    if (testItem.Contains("OIS Y"))
                        PM_OISUseY = true;

                    Process_Function(port, testItem);

                    if (ErrMsg[ch] != "" && ErrMsg[ch + 1] != "")
                    {
                        loopContinue = false;
                        Log.AddLog(ch, ErrMsg[ch], false);
                        Log.AddLog(ch + 1, ErrMsg[ch + 1], false);
                    }

                    if (!loopContinue) break;
                    else todoCnt++;
                    Thread.Sleep(100);
                }

                LEDOn(port, false);

                long endTime = 0;
                SupremeTimer.QueryPerformanceCounter(ref endTime);
                double ellipse = (endTime - startTime) / (double)(TimerFrequency);
                for (int i = ch; i < ch + 2; i++)
                {
                    DrvIC.OIS_On(i, false);
                    if (!ErrMsg[i].Contains("Socket Empty") && !ErrMsg[i].Contains("SuddenStop")) Spec.LastSampleNum++;
                    Log.AddLog(i, "Total Test Time\t" + ellipse.ToString("F3") + "sec", true);
                    Spec.PassFails[i].TotalTime = ellipse.ToString("F3");
                    //if (!SuddenStop) ICProcess.Write_Result(i, m_ChannelOn[i]);
                }
                if (!SuddenStop)
                {
                    WriteResult(port);

                    int[] lPassFail = new int[2] { 0, 0 };
                    if (Spec.PassFails[ch].TotalFail == "")
                        lPassFail[0] = 2;
                    else
                        lPassFail[0] = 9;

                    if (Spec.PassFails[ch + 1].TotalFail == "")
                        lPassFail[1] = 2;
                    else
                        lPassFail[1] = 9;

                    if (Option.m_bWriteResultToDriverIC)
                    {
                        WriteResultToDriverIC(ch, lPassFail[0]);
                        WriteResultToDriverIC(ch + 1, lPassFail[1]);
                    }
                }

                return;
            }
            catch
            {

            }
        }
        public void MakeWaveform(int port)
        {
            ClearWaveformBuffer(port);
            for (int i = 0; i < AxisCnt; i++)
                MakeCLWaveform(i);
        }
        public void MakeCLWaveform(int type = 0)
        {
            int pos_min = 0, pos_max = 0, Step = 0;
            int i, k = 0;

            switch (type)
            {
                case (int)AO_TYPE._CLX:
                    pos_min = Rcp.iXDrvCodeMin;
                    pos_max = Rcp.iXDrvCodeMax;
                    Step = Rcp.iDrvXStep;
                    Waveform_CL[type][k++] = pos_min;
                    Waveform_CL[type][k++] = pos_min;
                    break;
                case (int)AO_TYPE._CLY:
                    pos_min = Rcp.iYDrvCodeMin;
                    pos_max = Rcp.iYDrvCodeMax;
                    Step = Rcp.iDrvYStep;
                    Waveform_CL[type][k++] = pos_min;
                    Waveform_CL[type][k++] = pos_min;
                    break;
                case (int)AO_TYPE._CLR:
                    pos_min = Rcp.iRDrvCodeMin;
                    pos_max = Rcp.iRDrvCodeMax;
                    Step = Rcp.iDrvRStep;
                    Waveform_CL[type][k++] = pos_min;
                    Waveform_CL[type][k++] = pos_min;
                    break;
                case (int)AO_TYPE._CLXStep:
                case (int)AO_TYPE._CLYStep:
                    Waveform_CL[type][0] = Rcp.iSettlingOffset;
                    Waveform_CL[type][1] = Rcp.iSettlingOffset + Rcp.iSettlingStep;
                    Waveform_CL[type][2] = Rcp.iSettlingOffset + Rcp.iSettlingStep;
                    Waveform_CL[type][3] = Rcp.iSettlingOffset + Rcp.iSettlingStep;
                    Waveform_CL[type][4] = Rcp.iSettlingOffset + Rcp.iSettlingStep;
                    CL_DataCount[type] = 5;
                    return;
            }

            if (Step < 1)
                Step = 240; //  default step
            do
            {
                Waveform_CL[type][k++] = pos_min;
                pos_min += Step;
            } while (pos_min < pos_max);


            Waveform_CL[type][k++] = pos_max;
            Waveform_CL[type][k] = pos_max;

            for (i = 1; i <= k - 1; i++)
                Waveform_CL[type][k + i] = Waveform_CL[type][k - i - 1];
            if (type == (int)AO_TYPE._CLR) Waveform_CL[type][k + i] = -pos_max;
            else Waveform_CL[type][k + i] = Waveform_CL[type][0];  //  Back to neutral Point
            CL_DataCount[type] = k + i;

        }
        public void ClearWaveformBuffer(int port)
        {
            for (int i = 0; i < 6000; i++)
            {
                SensCurrent[2 * port][i] = 0;
                SensCurrent[2 * port + 1][i] = 0;
            }
            for (int i = 0; i < Waveform_CL[0].Count(); i++)
            {
                for (int j = 0; j < Waveform_CL.Count(); j++)
                    Waveform_CL[j][i] = 0;
            }
        }
        public void LoadTestUnload(int port)
        {
            try
            {
                int ch = port * 2;

                if (DrvIC.IsSafeOn && Option.m_bSafeSensor)
                {
                    Log.AddLog(ch, "Safe Sensor Detected. Push Start Button Again..", true);
                    IsRun[port] = false;
                    return;
                }

                if (IsVirtual) Log.AddLog(ch, "Virtual Load Socket==");
                else
                {
                    DrvIC.ToggleAll(true);
                }

                Thread.Sleep(2000);

                DrvIC.PowerReset(port);

                RunStart?.Invoke(null, port);

                Process_Start(port);

                RepeatEnd?.Invoke(null, port);

                if (DrvIC.IsSafeOn && Option.m_bSafeSensor)
                {
                    Log.AddLog(ch, "Safe Sensor Detected. Push Start Button Again..", true);

                    DrvIC.IsCoverLoad = false;
                    DrvIC.UnloadCover(0);
                    DrvIC.LoadSocket(0);

                    IsRun[port] = false;
                    RunEnd?.Invoke(null, port);
                    return;
                }

                if (IsVirtual) Log.AddLog(ch, "Virtual UnLoad Socket==");
                else
                {
                    DrvIC.ToggleAll(false);
                }

                RunEnd?.Invoke(null, port);

                IsRun[port] = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void RunTest()
        {
            if (RepeatRun == 1)
            {
                if (IsRun[0]) return;
                if (!IsRun[0])
                {
                    IsRun[0] = true;
                    LoadTestUnload(0);
                }
            }
            else
            {
                CurrentRun = 1;
                while (true)
                {
                    LoadTestUnload(0);
                    if (CurrentRun >= RepeatRun || SuddenStop) break;
                    CurrentRun++;
                    Thread.Sleep(1500);
                }
            }
            SuddenStop = false;
        }
        public void SwitchRun()
        {
            RepeatRun = CurrentRun = 1;
            Task.Factory.StartNew(() => RunTest());
        }
        public void LEDOn(int port, bool isOn = true)
        {
            int ch = port * 2;

            string strOn = "";
            if (isOn) strOn = "On";
            else strOn = "Off";
            if (IsVirtual)
            {
                Log.AddLog(ch, string.Format("Virtual LED {0} ==", strOn));
                return;
            }
            int Value = 0;
            if (isOn) Value = (int)(CurrentLed.Val[ch] * 1240);
            else Value = 0;

            for (int i = 0; i < 4; i++) DrvIC.SetLEDpower(i, Value);

            m_bAllLEDOn = m_bRightLEDOn = m_bLeftLEDOn = isOn;
        }
        public void IC_Check(int ch, string testItem)
        {
            //int ch = port * 2;
            try
            {
                byte[] rBuffer = new byte[4] { 0xFF, 0xFF, 0xFF, 0xFF };
                bool res = false;
                rBuffer = DrvIC.ReadAnyByte(ch, 0x6014, 4);
                if (rBuffer != null)
                {
                    Log.AddLog(ch, "IC Version= " + rBuffer[0].ToString("X") + " " + rBuffer[1].ToString("X") + " " + rBuffer[2].ToString("X") + " " + rBuffer[3].ToString("X"), false);
                    if (IsVirtual) return;
                    if (Model.DriverIC == "SEM1215SA")
                    {
                        if (rBuffer[0] == 0x41 && rBuffer[1] == 0x53)
                            return;
                        else
                        {
                            ChannelOn[ch] = false;
                            ErrMsg[ch] = "IC_CHECK";
                        }
                    }
                    else
                    {
                        if (rBuffer[0] == 0x00 && rBuffer[1] == 0x00)
                            return;
                        else
                        {
                            ChannelOn[ch] = false;
                            ErrMsg[ch] = "IC_CHECK";
                        }
                    }
                }
                else
                {
                    Thread.Sleep(100);
                    rBuffer = DrvIC.ReadAnyByte(ch, 0x6014, 4);
                    if (!res)
                    {
                        ChannelOn[ch] = false;
                        ErrMsg[ch] = "I2C_FAIL";
                    }
                    else
                    {
                        Log.AddLog(ch, "IC Version= " + rBuffer[0].ToString("X") + " " + rBuffer[1].ToString("X") + " " + rBuffer[2].ToString("X") + " " + rBuffer[3].ToString("X"), false);
                        if (Model.DriverIC == "SEM1215SA")
                        {
                            if (rBuffer[0] == 0x41 && rBuffer[1] == 0x53)
                                return;
                            else
                            {
                                ChannelOn[ch] = false;
                                ErrMsg[ch] = "IC_CHECK";
                            }
                        }
                        else
                        {
                            if (rBuffer[0] == 0x00 && rBuffer[1] == 0x00)
                                return;
                            else
                            {
                                ChannelOn[ch] = false;
                                ErrMsg[ch] = "IC_CHECK";
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void FW_Download(int ch, string testItem)
        {
            try
            {
                Log.AddLog(ch, "OIS FW Path : " + Current.FWPath);

                if (Current.FWPath != "" && FWCode != null)
                {
                    //if(!DrvIC.Update_FW(ch, FWCode))
                    //{
                    //    ErrMsg[ch] = "FW_DOWNLOAD";
                    //    ChannelOn[ch] = false;
                    //}
                    DriverICFWUpdate(ch, FWCode, FWCode.Length);
                }
                else
                {
                    Log.AddLog(ch, "Need to assign FW File", true);
                    ErrMsg[ch] = "FW_DOWNLOAD";
                    ChannelOn[ch] = false;
                    return;
                }

            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void DriverICFWUpdate(int ch, byte[] progCode, int progLength)
        {

            byte[] wBuffer = new byte[256];
            byte[] rBuffer1 = new byte[1];
            byte[] rBuffer = new byte[2];
            byte[] rBufferL = new byte[4];
            byte[] csBuffer = new byte[2];

            short checksum = 0;

            //  28KB 버젼인지 32KB 버젼인지 확인하기
            byte res = 0;
            ////////////////////////////////////////////////////////////////////////////////////
            //  Erase FLASH

            byte[] sBuffer = new byte[1];
            sBuffer[0] = 0x07;
            DrvIC.WriteAnyByte(ch, 0x1000, sBuffer);
            Thread.Sleep(200);
            ////////////////////////////////////////////////////////////////////////////////////
            //  Check FW Update Mode
            //Log.AddLog(ch, "Check FW Update Mode");
            res = DrvIC.RetryRead(ch, 0x0001, 0x02, 20);
            if (res != 0x02)
            {
                //  errMsg[ch] = "OISSTS not FW Update Mode after FWUPDATECTRL_REG Set : 0x" + res.ToString("X");
                Log.AddLog(ch, "OISSTS not FW Update Mode after FWUPDATECTRL_REG Set : 0x" + res.ToString("X"), false);
                ChannelOn[ch] = false;
                ErrMsg[ch] = "I2C_FAIL";
                return;
            }
            //Log.AddLog(ch, "Write FW " + progLength.ToString() + "byte");
            ////////////////////////////////////////////////////////////////////////////////////
            //  Write FW


            //Log.AddLog(ch, "progLength = " + progLength, false);
            //Log.AddLog(ch, "iFWwriteInterval = " + Rcp.iFWwriteInterval, false);


            for (int i = 0; i < progLength / 256; i++)   //  RumbaS4
            {
                Array.Copy(progCode, 256 * i, wBuffer, 0, 256);
                DrvIC.WriteAnyByte(ch, 0x1100, wBuffer);    //  20180828 수정
                //DriverIC.FWriteToSlave(ch, 0x1100, wBuffer);    //  20180828 수정
                if (Rcp.iFWwriteInterval > 5)
                    Thread.Sleep(Rcp.iFWwriteInterval - 5);	//	 Wait for flash ROM writing
                else
                    Thread.Sleep(5);	//	 Wait for flash ROM writing
            }


            ushort sprogCode1 = 0;
            ushort sprogCode2 = 0;
            for (int i = 0; i < progLength; i += 2)
            {
                sprogCode1 = progCode[i];
                sprogCode2 = progCode[i + 1];
                checksum += (short)(sprogCode1 | (sprogCode2 << 8));
            }

            csBuffer[0] = (byte)(checksum & 0x00FF);
            csBuffer[1] = (byte)((checksum & 0xFF00) >> 8);
            DrvIC.WriteAnyByte(ch, 0x1002, csBuffer);
            Thread.Sleep(200);
            rBuffer1 = DrvIC.ReadAnyByte(ch, 0x1001, 1);
            if (rBuffer1[0] == 0)
            {
                //  Succeed
                sBuffer[0] = 0x80;
                DrvIC.WriteAnyByte(ch, 0x1000, sBuffer);


                Thread.Sleep(300);  //  20181026 1000msec -> 300msec 로 변경
                //bool tmpbool = Standby_Sequence(ch);
                rBufferL = DrvIC.ReadAnyByte(ch, 0x1008, 4);
                FWVersion[ch] = rBufferL[3].ToString("X") + " " + rBufferL[2].ToString("X") + " " + rBufferL[1].ToString("X") + " " + rBufferL[0].ToString("X");
                Log.AddLog(ch, "FW Update Ver = " + FWVersion[ch], false);
            }
            else
            {
                //  errMsg[ch] = "FW Update fail : CheckSum Error";
                Log.AddLog(ch, "FW Update fail : CheckSum Error ", false);
                ChannelOn[ch] = false;
                ErrMsg[ch] = "FW_DOWNLOAD";
            }

            byte bres = 0;
            bres = DrvIC.CheckIfOISSTS_IDLE(ch);
            if (bres > 0)
            {
                //   errMsg[ch] = "Fail OISSTS_IDLE";
                Log.AddLog(ch, "Fail OISSTS_IDLE", false);
                ChannelOn[ch] = false;
                ErrMsg[ch] = "STS_IDLE";

                return;
            }

            //  Run Drv IC
            //  20181026 아래 Skip 처리하기에는 위험부담이 커서 일단 내버려움.
            //m_bProcess_FWupdate_OK[ch] = 1;
        }
        public void Read_FW_Versione(int ch, string testItem)
        {
            try
            {
                //ReadFWVersion(ch, false);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void PID_Update(int ch, string testItem)
        {
            try
            {
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Barcode_Write(int ch, string testItem)
        {
            try
            {
                if (WriteBarcode(ch)) Log.AddLog(ch, "Fail to Write Barcode " + ch.ToString(), true);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public bool WriteBarcode(int ch)
        {
            //if (MachineType == (int)FZ4P.MachineType.Slave)
            //    histIndex = sCIndex[ch] + 5000;
            //else
            //histIndex = sCIndex[ch];

            DateTime dtNow = DateTime.Now;
            byte[] wBuffer128 = new byte[128];
            byte[] wBuffer128_confirm = new byte[128];

            byte lyear = (byte)(dtNow.Year - 2000);
            byte lmonth_high = (byte)(dtNow.Month & 0x0C);
            byte lmonth_low = (byte)(dtNow.Month & 0x03);
            byte lday = (byte)(dtNow.Day);
            byte lHour_high = (byte)(dtNow.Hour & 0x10);
            byte lHour_low = (byte)(dtNow.Hour & 0x0F);
            byte lMinute_high = (byte)(dtNow.Minute & 0x3C);
            byte lMinute_low = (byte)(dtNow.Minute & 0x03);
            byte lSec = (byte)dtNow.Second;


            StrBarcode[ch] = "";
            wBuffer128 = DrvIC.ReadAnyByte(ch, 0xAE00, 128);    //  190531  0xDF00 -> 0xBF00    로 변경
            if (wBuffer128[112] != 0x01)
            {
                wBuffer128 = DrvIC.ReadAnyByte(ch, 0xAE00, 128);  //  190531  0xDF00 -> 0xBF00    로 변경
            }
            if (wBuffer128[112] == 0x01 && Option.m_bBarcodeOverwriteDisable)
            {
                //m_nIndexSkipped[ch] = sCIndex[ch];
                Log.AddLog(ch, "Already Barcode written, Overwrite Disabled", false);

                for (int i = 112; i < 121; i++)
                {
                    StrBarcode[ch] += wBuffer128[i].ToString("X2");

                }
                return true;
            }

            wBuffer128[112] = 0x01;     //  Barcode Flag
                                        // wBuffer128[113] = (byte)lbxBarcodeMaker.SelectedIndex;     //  Lot No _Maker

            if (Model.TesterNo == "") Model.TesterNo = "0";
            int l_TesterNumber = Convert.ToInt32(Model.TesterNo);
            if (l_TesterNumber > 0xFF)
            {
                wBuffer128[113] = (byte)(l_TesterNumber / 256);   //  검사장비 번호 High
                wBuffer128[114] = (byte)(l_TesterNumber % 256);   //  검사장비 번호 Low
            }
            else
            {
                wBuffer128[113] = 0x00;                  //  검사장비 번호 High
                wBuffer128[114] = (byte)(l_TesterNumber % 256);    //  검사장비 번호 Low
            }
            wBuffer128[115] = (byte)((lyear << 2) | (lmonth_high >> 2));             //  상위6bit 년, 하위 2bit 월 high값
            wBuffer128[116] = (byte)((lmonth_low << 6) | (lday << 1) | (lHour_high >> 4));     //  월low값 상위2bit, day 5bit, hour상위 1bit
            wBuffer128[117] = (byte)((lHour_low << 4) | (lMinute_high >> 2));                  // hour하위 4bit, minute 상위 4bit
            wBuffer128[118] = (byte)((lMinute_low << 6) | (lSec));                           // minute 하위 2bit , second 6bit
            wBuffer128[119] = (byte)Model.Maker.ToCharArray()[0];
            wBuffer128[120] = (byte)(ch + 1);


            for (int i = 112; i < 121; i++)
            {
                StrBarcode[ch] += wBuffer128[i].ToString("X2");
            }

            //byte[] rbuffer11 = new byte[11];
            //DrvIC.ReadAnyByte(ch, 0x1014, rbuffer11);

            //for (int i = 0; i < 11; i++)
            //    wBuffer128[113 + i] = rbuffer11[i];

            //wBuffer128[124] = 0x00;     //  Reserved
            //wBuffer128[125] = 0x00;     //  Reserved
            //wBuffer128[126] = (byte)(ch + '1');     //  Reserved

            byte lCheckSum = 0;
            string strAccu = "";
            for (int i = 112; i < 121; i++)
            {
                lCheckSum += wBuffer128[i];
                strAccu += "0xAE" + i.ToString("X2") + " " + wBuffer128[i].ToString("F0") + "\r\n";
            }
            Log.AddLog(ch, strAccu, false);
            wBuffer128[127] = (byte)(lCheckSum % 255 + 1);  //  Barcode Checksum


            DrvIC.WriteAnyByte(ch, 0xAE00, wBuffer128); //  190531   0xDF00 -> 0xBF64  로 변경
            Thread.Sleep(100);
            wBuffer128_confirm = DrvIC.ReadAnyByte(ch, 0xAE00, 128);

            //string lmsg = "W   : R \r\n";
            for (int i = 0; i < 128; i++)
            {
                //lmsg += wBuffer128[i].ToString() + " " + wBuffer128_confirm[i].ToString("F0") + "\r\n";
                if (wBuffer128[i] != wBuffer128_confirm[i])
                {
                    if (IsVirtual)
                    {
                        Log.AddLog(ch, "Virtual Barcode Write==");
                        return true;
                    }
                    Log.AddLog(ch, i.ToString() + "th in 128 byte is not correct!!", false);
                    ErrMsg[ch] = "BARCODE_WRITE";
                    ChannelOn[ch] = false;
                    // errMsg[ch] = "Barcode Write Error";
                    return false;
                }
            }
            //Log.AddLog(ch, lmsg);
            Log.AddLog(ch, "Barcode written : " + StrBarcode[ch].ToString(), false); //+ " ch:" + (wBuffer128[126]).ToString(), false);

            return true;
        }
        public void Hall_Calibration(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                //Reset Epa
                //for (int j = ch; j < ch + 2; j++)
                //    DrvIC.EPAReset(j);
                for (int j = ch; j < ch + 2; j++) Log.AddLog(j, string.Format("=== Cal Stroke Start ==="));
                CalStroke(port);

                Task Func1 = null, Func2 = null;
                if (ChannelOn[ch])
                {
                    Func1 = new Task(() => Hall_Calibration(ch));
                    Func1.Start();
                }
                if (ChannelOn[ch + 1])
                {
                    Func2 = new Task(() => Hall_Calibration(ch + 1));
                    Func2.Start();
                }
                if (Func1 != null && Func2 != null) Task.WaitAll(Func1, Func2);
                else
                {
                    if (Func1 != null) Task.WaitAll(Func1);
                    if (Func2 != null) Task.WaitAll(Func2);
                }

                Thread.Sleep(100);
                for (int j = ch; j < ch + 2; j++) Log.AddLog(j, string.Format("=== Mecha Stroke  Start ==="));
                CalStroke(port, true);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Hall_Calibration(int ch)
        {
            Log.AddLog(ch, string.Format("=== Hall Calibration  Start ==="));
            if (!DrvIC.HallCalStart(ch))
            {
                ErrMsg[ch] = "HALL_CALIBRATION";
                Spec.PassFails[ch].FirstFailIndex = 0;
                ChannelOn[ch] = false;
            }

            for (int i = 0; i < 2; i++)
            {
                if (!ChannelOn[ch]) continue;
                HallParam[ch][i] = DrvIC.HallCalRead(ch, i);

                Log.AddLog(ch, string.Format("{0} Hall Gain : {1}", CLName[i], HallParam[ch][i].gain));
                Log.AddLog(ch, string.Format("{0} Hall Offset : {1}", CLName[i], HallParam[ch][i].Offset));
                Log.AddLog(ch, string.Format("{0} Hall min : {1}", CLName[i], HallParam[ch][i].min));
                Log.AddLog(ch, string.Format("{0} Hall max : {1}", CLName[i], HallParam[ch][i].max));
                Log.AddLog(ch, string.Format("{0} Hall mid : {1}", CLName[i], HallParam[ch][i].mid));
 
            }

            Log.AddLog(ch, string.Format("=== Hall Calibration  Store Start ==="));
            if (!DrvIC.Store_Hall_Cal(ch))
            {
                Log.AddLog(ch, string.Format("=== Hall Calibration  Store Failed ==="));
            }

            if (Option.m_bUpdateHallCalData)
            {
                Log.AddLog(ch, string.Format("=== Hall Calibration Data Update ==="));

                //Write Hall Data to NVM
                int memAddr = 0x0300;
                byte[] buffer = new byte[1];
                buffer[0] = 1;
                DrvIC.WriteAnyByte(ch, memAddr, buffer);

                if (buffer[0] != 1)
                {
                    Log.AddLog(ch, string.Format("Fail Write Hall Data to NVM"));
                }
                else
                    Log.AddLog(ch, string.Format("Done NVM Write Hall Data"));

            }
        }
        public void CalStroke(int port, bool isMecha = false)
        {
            int ch = port * 2;
            //OIS On Off
            for (int j = ch; j < ch + 2; j++)
            {
                if (isMecha) OIS_StartMode(j, 0);
                else
                {
                    DrvIC.OIS_On(j, true);
                    DrvIC.Write2Byte(j, DrvIC.REG_OIS_MODE, 0x0E);
                }
            }

            //LED On
            LEDOn(port);
            Thread.Sleep(100);

            double[] strokeMin = new double[2];
            double[] strokeMax = new double[2];

            double[] cx = new double[4];
            double[] cy = new double[4];
            double[] rescx = new double[2];
            double[] rescy = new double[2];
            double[] resTheta = new double[2];

            // X Move ====
            for (int j = ch; j < ch + 2; j++)
            {
                if (isMecha)
                {
                    DrvIC.Move(j, 0, 5000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Min Near X : {0}, X Hall : {1}", 5000, DrvIC.ReadHall(j, 0)));
                    DrvIC.Move(j, 0, 4000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Min X : {0}, X Hall : {1}", 4000, DrvIC.ReadHall(j, 0)));
                }
                else
                {
                    DrvIC.MoveCal(j, 1, (short)Rcp.CalStrokeMidY);
                    Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Cal Move Mid Y : {0}, Y Hall : {1}", Rcp.CalStrokeMidY, DrvIC.ReadHall(j, 2)));
                    DrvIC.MoveCal(j, 0, (short)Rcp.CalStrokeMinX);
                    Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Cal Move Min X : {0}, X Hall : {1}", Rcp.CalStrokeMinX, DrvIC.ReadHall(j, 0)));
                }
            }

            //Grab and Measure Stroke X Min
            Thread.Sleep(Rcp.calStrokeDelay);
            oCam[0].GrabWithTime(0);
            Thread.Sleep(Rcp.calStrokeDelay);
            SetSearchYRegion();
            MeasureXYT(0, -1, ref cx, ref cy, ref rescx, ref rescy, ref resTheta);

            for (int j = ch; j < ch + 2; j++) Log.AddLog(j, string.Format("Stroke X Min : {0:0.00}", strokeMin[j] = rescy[j]));

            for (int j = ch; j < ch + 2; j++)
            {
                if (isMecha)
                {
                    DrvIC.Move(j, 0, 27000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Max Near X : {0}, X Hall : {1}", 27000, DrvIC.ReadHall(j, 0)));
                    DrvIC.Move(j, 0, 28000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Max X : {0}, X Hall : {1}", 28000, DrvIC.ReadHall(j, 0)));
                }
                else
                {
                    DrvIC.MoveCal(j, 0, (short)Rcp.CalStrokeMaxX);
                    Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Cal Move Max X : {0}, X Hall : {1}", Rcp.CalStrokeMaxX, DrvIC.ReadHall(j, 0)));
                }
            }
            //Grab and Measure Stroke X Max
            Thread.Sleep(Rcp.calStrokeDelay);
            oCam[0].GrabWithTime(1);
            Thread.Sleep(Rcp.calStrokeDelay);
            SetSearchYRegion();
            MeasureXYT(1, -1, ref cx, ref cy, ref rescx, ref rescy, ref resTheta);

            for (int j = ch; j < ch + 2; j++) Log.AddLog(j, string.Format("Stroke X Max : {0:0.00}", strokeMax[j] = rescy[j]));

            for (int j = ch; j < ch + 2; j++)
            {
                if (isMecha)
                {
                    Log.AddLog(j, string.Format("============X Mecha Stroke : {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_NonEPAStroke].Val = Math.Abs(strokeMax[j] - strokeMin[j])));
                    ShowEvent.Show(j, "X NonEPA");
                }
                else
                {
                    Log.AddLog(j, string.Format("============X Cal Stroke : {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_CalStroke].Val = Math.Abs(strokeMax[j] - strokeMin[j])));
                    ShowEvent.Show(j, "X Cal");
                }
            }
            //// Y Move ====
            ////OIS On Off
            //for (int j = ch; j < ch + 2; j++)
            //{
            //    DrvIC.OIS_On(j, false);
            //    if (isMecha) OIS_StartMode(j, 1);
            //}

            for (int j = ch; j < ch + 2; j++)
            {
                if (isMecha)
                {
                    DrvIC.Move(j, 1, 5000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Min Near Y : {0}, Y Hall : {1}", 12000, DrvIC.ReadHall(j, 2)));
                    DrvIC.Move(j, 1, 4000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Min Y : {0}, Y Hall : {1}", 4000, DrvIC.ReadHall(j, 2)));
                }
                else
                {
                    DrvIC.MoveCal(j, 0, (short)Rcp.CalStrokeMidX);
                    Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Cal Move Mid X : {0}, X Hall : {1}", Rcp.CalStrokeMidX, DrvIC.ReadHall(j, 0)));
                    DrvIC.MoveCal(j, 1, (short)Rcp.CalStrokeMinY);
                    Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Cal Move Min Y : {0}, Y Hall : {1}", Rcp.CalStrokeMinY, DrvIC.ReadHall(j, 2)));
                }
            }
            //Grab and Measure Stroke Y Min
            Thread.Sleep(Rcp.calStrokeDelay);
            oCam[0].GrabWithTime(2);
            Thread.Sleep(Rcp.calStrokeDelay);

            SetSearchYRegion();
            MeasureXYT(2, -1, ref cx, ref cy, ref rescx, ref rescy, ref resTheta);

            for (int j = ch; j < ch + 2; j++) Log.AddLog(j, string.Format("Stroke Y Min : {0:0.00}", strokeMin[j] = rescx[j]));

            for (int j = ch; j < ch + 2; j++)
            {
                if (isMecha)
                {
                    DrvIC.Move(j, 1, 27000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Max Near Y : {0}, Y Hall : {1}", 27000, DrvIC.ReadHall(j, 2)));
                    DrvIC.Move(j, 1, 28000); Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Mecha Move Max Y : {0}, Y Hall : {1}", 28000, DrvIC.ReadHall(j, 2)));
                }
                else
                {
                    DrvIC.MoveCal(j, 1, (short)Rcp.CalStrokeMaxY);
                    Thread.Sleep(Rcp.calStrokeDelay);
                    Log.AddLog(j, string.Format("Cal Move Max Y : {0}, Y Hall : {1}", Rcp.CalStrokeMaxY, DrvIC.ReadHall(j, 2)));
                }
            }
            //Grab and Measure Stroke Y Max
            Thread.Sleep(Rcp.calStrokeDelay);
            oCam[0].GrabWithTime(3);
            Thread.Sleep(Rcp.calStrokeDelay);
            SetSearchYRegion();
            MeasureXYT(3, -1, ref cx, ref cy, ref rescx, ref rescy, ref resTheta);

            for (int j = ch; j < ch + 2; j++) Log.AddLog(j, string.Format("Stroke Y Max : {0:0.00}", strokeMax[j] = rescx[j]));

            for (int j = ch; j < ch + 2; j++)
            {
                if (isMecha)
                {
                    Log.AddLog(j, string.Format("============Y Mecha Stroke : {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_NonEPAStroke].Val = Math.Abs(strokeMax[j] - strokeMin[j])));
                    ShowEvent.Show(j, "Y NonEPA");
                }
                else
                {
                    Log.AddLog(j, string.Format("============Y Cal Stroke : {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_CalStroke].Val = Math.Abs(strokeMax[j] - strokeMin[j])));
                    ShowEvent.Show(j, "Y Cal");
                }
            }
            oCam[0].CLFrameCount[0] = 4;
            oCam[0].BufCopy_User(0);
        }
        public void OIS_EPA(int ch, string testItem)
        {
            try
            {
                DrvIC.EPAControl(ch);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Hall_NVM_Read_Only(int ch, string testItem)
        {
            try
            {
                //ReadNVMHall(ch, false, false);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void OIS_X(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                LEDOn(port);
                for (int i = ch; i < ch + 2; i++)
                    OIS_StartMode(i, 0);

                Process_CLXYTest(port, (int)AO_TYPE._CLX);
                LEDOn(port, false);

                if (!ChannelOn[ch] && !ChannelOn[ch + 1]) return;

                Thread.Sleep(160);
                oCam[port].BufCopy_User((int)AO_TYPE._CLX);
                Process_CalcCLXY(port, (int)AO_TYPE._CLX);

                FPS[port] = oCam[port].FPS;
                Log.AddLog(ch, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);
                Log.AddLog(ch + 1, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);

            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void OIS_Y(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                LEDOn(port);
                for (int i = ch; i < ch + 2; i++)
                    OIS_StartMode(i, 1);

                Process_CLXYTest(port, 1);
                LEDOn(port, false);

                if (!ChannelOn[ch] && !ChannelOn[ch + 1]) return;

                Thread.Sleep(160);
                oCam[port].BufCopy_User(1);
                Process_CalcCLXY(port, 1);

                FPS[port] = oCam[port].FPS;
                Log.AddLog(ch, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);
                Log.AddLog(ch + 1, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);

            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void OIS_R(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                LEDOn(port);
                for (int i = ch; i < ch + 2; i++)
                    OIS_StartMode(i, 2);

                Process_CLXYTest(port, 2);
                LEDOn(port, false);

                if (!ChannelOn[ch] && !ChannelOn[ch + 1]) return;

                Thread.Sleep(160);
                oCam[port].BufCopy_User(2);
                Process_CalcCLXY(port, 2);

                FPS[port] = oCam[port].FPS;
                Log.AddLog(ch, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);
                Log.AddLog(ch + 1, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);

            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Step_X(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                LEDOn(port);
                for (int i = ch; i < ch + 2; i++)
                    OIS_StartMode(i, 0);

                Process_CLXYTest(port, (int)AO_TYPE._CLXStep, true);
                LEDOn(port, false);

                if (!ChannelOn[ch] && !ChannelOn[ch + 1]) return;

                Thread.Sleep(160);
                oCam[port].BufCopy_User((int)AO_TYPE._CLXStep);
                Process_CalcCLXY(port, (int)AO_TYPE._CLXStep);

                FPS[port] = oCam[port].FPS;
                Log.AddLog(ch, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);
                Log.AddLog(ch + 1, testItem + "\t" + FPS[port].ToString("F1") + "fps", false);

            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Process_CLXYTest(int port, int axis, bool isStep = false)
        {
            int ch = port * 2;
            FPS[port] = 1100;

            for (int i = ch; i < ch + 2; i++)
            {
                if (ChannelOn[i])
                {
                    if (DrvIC.RetryRead(i, 0x0001, 0x02, 10) > 0x02)
                    {
                        ErrMsg[i] = "I2C_FAIL";
                        ChannelOn[i] = false;
                        Log.AddLog(i, "I2C Fail in Process_CLXYTest() #" + i, false);
                    }
                }
                for (int j = 0; j < Hall_X1[i].Count(); j++)
                {
                    Hall_X1[i][j] = 0;
                    Hall_X2[i][j] = 0;
                    Hall_Y[i][j] = 0;
                }
            }

            if (!ChannelOn[ch] && !ChannelOn[ch + 1])
                return;

            Task Func1 = null;
            if (isStep)
            {
                Func1 = new Task(() => Process_WaveformCLXYStep(port, axis));
                Func1.Start();
            }
            else
            {
                Func1 = new Task(() => Process_WaveformCLXYOut(port, axis));
                Func1.Start();
            }
            if (Func1 != null) Task.WaitAll(Func1);
        }
        public void Process_WaveformCLXYOut(int port, int axis = 0)
        {
            Count_AO[port] = 0;
            Count_AI[port] = 0;

            AOType[port] = axis;
            CLPeakTime[0] = 0;

            MaxCount_AO[port] = CL_DataCount[axis];
            oCam[port].CLFrameCount[axis] = CL_DataCount[axis];
            MaxCount_AI[port] = CL_DataCount[axis];

            microTimer_AO[port].Interval = Rcp.iDrvStepInterval * 1000;  //  30000µs = 30msec
            microTimer_AO[port].Enabled = true; // Start timer

            Thread.Sleep(1000);

            // 출력시작 시점 지정.
            AnalogOut_DoneEvent[port].WaitOne();       //   Type1 Sampling : 163840개 출력 또는 Type 2 Sampling : 10000개 데이타 출력 완료

            if (axis == 0 || axis == 2)
            {
                Array.Copy(oCam[port].GrabT1, oCam[port].CLXGrabTime, (long)(oCam[port].CLFrameCount[axis]));
            }
            else if (axis == 1 || axis == 3)
            {
                Array.Copy(oCam[port].GrabT1, oCam[port].CLYGrabTime, (long)(oCam[port].CLFrameCount[axis]));
            }
        }
        public void Process_WaveformCLXYStep(int port, int axis)
        {
            Count_AO[port] = 0;
            AOType[port] = axis;
            CLPeakTime[0] = 0;

            MaxCount_AO[port] = CL_DataCount[axis];
            oCam[port].CLFrameCount[axis] = 2000;
            MaxCount_AI[port] = CL_DataCount[axis];

            microTimer_AO[port].Interval = 51000;
            microTimer_AO[port].Enabled = true;

            Task.Factory.StartNew(() => Process_VisionGrab(port, axis));

            AnalogOut_DoneEvent[port].WaitOne();

            //if (axis == 0 || axis == 2)
            //{
            //    Array.Copy(oCam[port].GrabT1, oCam[port].CLXGrabTime, (long)(oCam[port].CLFrameCount[axis]));
            //}
            //else if (axis == 1 || axis == 3)
            //{
            //    Array.Copy(oCam[port].GrabT1, oCam[port].CLYGrabTime, (long)(oCam[port].CLFrameCount[axis]));
            //}
        }
        public void Process_VisionGrab(int port, int axis)
        {
            Stopwatch sw = new Stopwatch();
            sw.Reset(); sw.Start();
            for (int i = 0; i < oCam[port].CLFrameCount[axis]; i++)
            {
                oCam[port].GrabWithTime(i);
                GrabTime[i] = sw.ElapsedMilliseconds;
                GrabHall[i] = DrvIC.ReadHall(0, 0);
                if (!microTimer_AO[port].Enabled)
                {
                    oCam[port].CLFrameCount[axis] = i;
                    break;
                }
            }
            sw.Stop();
        }
        public bool Process_CalcCLXY(int port, int type = 0)
        {
            int framecount;
            int ch = port * 2;
            framecount = oCam[port].CLFrameCount[type];

            List<double[]> cxList = new List<double[]>();
            List<double[]> cyList = new List<double[]>();
            List<double[]> rescxList = new List<double[]>();
            List<double[]> rescyList = new List<double[]>();
            List<double[]> resThetaList = new List<double[]>();

            for (int j = 0; j < 4; j++)
            {
                cxList.Add(new double[framecount]);
                cyList.Add(new double[framecount]);
            }

            for (int j = ch; j < ch + 2; j++)
            {
                rescxList.Add(new double[framecount]);
                rescyList.Add(new double[framecount]);
                resThetaList.Add(new double[framecount]);
            }

            for (int i = 0; i < framecount; i++)
            {
                double[] cx = new double[4];
                double[] cy = new double[4];
                double[] rescx = new double[2];
                double[] rescy = new double[2];
                double[] resTheta = new double[2];
                //SetSearchYRegion();
                if (!MeasureXYT(i, type, ref cx, ref cy, ref rescx, ref rescy, ref resTheta))
                {
                    Thread.Sleep(100);
                    MeasureXYT(i, type, ref cx, ref cy, ref rescx, ref rescy, ref resTheta);
                }
                for (int j = 0; j < 4; j++)
                {
                    cxList[j][i] = cx[j];
                    cyList[j][i] = cy[j];
                }
                for (int j = ch; j < ch + 2; j++)
                {
                    rescxList[j][i] = rescy[j];
                    rescyList[j][i] = rescx[j];
                    resThetaList[j][i] = resTheta[j] / 60;
                }
            }

            if (Option.m_bSaveRawData)
            {
                StreamWriter wt = new StreamWriter(FIO.RowDataDir + "__P" + port.ToString() + "_" + CLName[type] + "_mark.txt");
                for (int i = 0; i < framecount; i++)
                {
                    wt.WriteLine(string.Format("{0:0.00}\t{1:0.00}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5:0.00}\t{6:0.00}\t{7:0.00}\t",
                        cxList[0][i], cyList[0][i], cxList[1][i], cyList[1][i], cxList[2][i], cyList[2][i], cxList[3][i], cyList[3][i]));
                }
                wt.Close();
            }

            List<double[]> strokeX = new List<double[]>();
            List<double[]> strokeY = new List<double[]>();
            List<double[]> Theta = new List<double[]>();
            List<double> CenterOffsetX = new List<double>() { 0, 0 };
            List<double> maxStrokeX = new List<double>();
            List<double> minStrokeX = new List<double>();
            List<double> CenterOffsetY = new List<double>();
            List<double> maxStrokeY = new List<double>();
            List<double> minStrokeY = new List<double>();

            for (int j = ch; j < ch + 2; j++)
            {
                strokeX.Add(new double[framecount]);
                strokeY.Add(new double[framecount]);
                Theta.Add(new double[framecount]);
                CenterOffsetX.Add(0);
                maxStrokeX.Add(-10);
                minStrokeX.Add(10);
                CenterOffsetY.Add(0);
                maxStrokeY.Add(-10);
                minStrokeY.Add(10);
            }

            double[] gbTime = new double[framecount];
            double[] csTime = new double[framecount];


            double[] InitStrokeX = new double[2];
            double[] InitStrokeY = new double[2];
            double[] InitTheth = new double[2];

            for (int i = 0; i < framecount; i++)
            {
                //Time
                gbTime[i] = ((oCam[port].CLXGrabTime[i] - CLOutTime[0][2]) / (double)(TimerFrequency));   // 초 단위
                csTime[i] = ((CLInTime[0][i] - CLOutTime[0][2]) / (double)(TimerFrequency));   // 초 단위 Current Sensing Time

                for (int j = ch; j < ch + 2; j++)
                {
                    if (i == 0) continue;
                    if (i == 1)
                    {
                        InitStrokeX[j] = rescxList[j][i];
                        InitStrokeY[j] = rescyList[j][i];
                        InitTheth[j] = resThetaList[j][i];

                    }
                    strokeX[j][i - 1] = rescxList[j][i] - InitStrokeX[j];
                    strokeY[j][i - 1] = InitStrokeY[j] - rescyList[j][i];
                    Theta[j][i - 1] = resThetaList[j][i] - InitTheth[j];
                }
            }

            if (IsVirtual)
            {
                for (int j = ch; j < ch + 2; j++)
                {
                    if (type == (int)AO_TYPE._CLX)
                    {
                        strokeX[j][0] = -80;
                        strokeY[j][0] = 0.1;
                        Theta[j][0] = -4;
                        SensCurrent[j][0] = 50;
                        Hall_X1[j][0] = -80;
                    }
                    else
                    {
                        strokeX[j][0] = 0.1;
                        strokeY[j][0] = -82;
                        Theta[j][0] = 4;
                        SensCurrent[j][0] = 60;
                        Hall_Y[j][0] = -82;
                    }

                    for (int k = 1; k < framecount / 2; k++)
                    {
                        if (type == (int)AO_TYPE._CLX)
                        {
                            strokeX[j][k] = strokeX[j][k - 1] + 4;
                            strokeY[j][k] = 0.1;
                            Theta[j][k] = Theta[j][k - 1] + 0.2;
                        }
                        else
                        {
                            strokeY[j][k] = strokeY[j][k - 1] + 4;
                            strokeX[j][k] = 0.1;
                            Theta[j][k] = Theta[j][k - 1] - 0.2;
                        }
                        if (k % 2 == 0) SensCurrent[j][k] = SensCurrent[j][k - 1] - 2;
                        else SensCurrent[j][k] = SensCurrent[j][k - 1] + 2;
                        if (type == (int)AO_TYPE._CLX)
                        {
                            Hall_X1[j][k] = Waveform_CL[type][k] / 100;
                        }
                        else
                        {
                            Hall_Y[j][k] = Waveform_CL[type][k] / 100;
                        }
                    }
                    strokeX[j][framecount / 2] = strokeX[j][framecount / 2 - 1];
                    strokeY[j][framecount / 2] = strokeY[j][framecount / 2 - 1];
                    Theta[j][framecount / 2] = Theta[j][framecount / 2 - 1];
                    SensCurrent[j][framecount / 2] = SensCurrent[j][framecount / 2 - 1];
                    if (type == (int)AO_TYPE._CLX)
                    {
                        Hall_X1[j][framecount / 2] = Hall_X1[j][framecount / 2 - 1];
                    }
                    else
                    {
                        Hall_Y[j][framecount / 2] = Hall_Y[j][framecount / 2 - 1];
                    }
                    for (int k = framecount / 2 + 1; k < framecount; k++)
                    {
                        if (type == (int)AO_TYPE._CLX)
                        {
                            strokeX[j][k] = strokeX[j][k - 1] - 4.02;
                            strokeY[j][k] = 0.1;
                            Theta[j][k] = Theta[j][k - 1] - 0.2;
                        }
                        else
                        {
                            strokeY[j][k] = strokeY[j][k - 1] - 4.02;
                            strokeX[j][k] = 0.1;
                            Theta[j][k] = Theta[j][k - 1] + 0.2;
                        }
                        if (k % 2 == 0) SensCurrent[j][k] = SensCurrent[j][k - 1] - 2;
                        else SensCurrent[j][k] = SensCurrent[j][k - 1] + 2;
                        if (type == (int)AO_TYPE._CLX)
                        {
                            Hall_X1[j][k] = Waveform_CL[type][k] / 100;
                        }
                        else
                        {
                            Hall_Y[j][k] = Waveform_CL[type][k] / 100;
                        }
                    }
                }
            }

            for (int j = ch; j < ch + 2; j++)
            {
                double[] results = new double[(int)ResultItem.Count];

                if (ChannelOn[j])
                {
                    if (type == (int)AO_TYPE._CLX)
                    {
                        CalcCLXYPerformance(j, strokeX[j], strokeY[j], Theta[j], gbTime, framecount, csTime, results, type);

                        Log.AddLog(j, string.Format("OISX_Ratedstroke    = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_Ratedstroke].Val = results[(int)ResultItem.Ratedstroke]));
                        Log.AddLog(j, string.Format("OISX_Forwardstroke  = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_Forwardstroke].Val = results[(int)ResultItem.Forwardstroke]));
                        Log.AddLog(j, string.Format("OISX_Backwardstroke = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_Backwardstroke].Val = results[(int)ResultItem.Backwardstroke]));
                        Log.AddLog(j, string.Format("OISX_Sensitivity    = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_Sensitivity].Val = results[(int)ResultItem.Sensitivity]));
                        Log.AddLog(j, string.Format("OISX_Linearity      = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_Linearity].Val = results[(int)ResultItem.Linearity]));
                        Log.AddLog(j, string.Format("OISX_Hysteresis     = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_Hysteresis].Val = results[(int)ResultItem.Hysteresis]));
                        Log.AddLog(j, string.Format("OISX_CenterCurrent  = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_CenterCurrent].Val = results[(int)ResultItem.CenterCurrent]));
                        Log.AddLog(j, string.Format("OISX_MaxCurrent     = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISX_MaxCurrent].Val = results[(int)ResultItem.MaxCurrent]));

                        ShowEvent.Show(j, "X");
                    }
                    else if (type == (int)AO_TYPE._CLY)
                    {
                        CalcCLXYPerformance(j, strokeY[j], strokeX[j], Theta[j], gbTime, framecount, csTime, results, type);

                        Log.AddLog(j, string.Format("OISY_Ratedstroke    = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_Ratedstroke].Val = results[(int)ResultItem.Ratedstroke]));
                        Log.AddLog(j, string.Format("OISY_Forwardstroke  = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_Forwardstroke].Val = results[(int)ResultItem.Forwardstroke]));
                        Log.AddLog(j, string.Format("OISY_Backwardstroke = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_Backwardstroke].Val = results[(int)ResultItem.Backwardstroke]));
                        Log.AddLog(j, string.Format("OISY_Sensitivity    = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_Sensitivity].Val = results[(int)ResultItem.Sensitivity]));
                        Log.AddLog(j, string.Format("OISY_Linearity      = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_Linearity].Val = results[(int)ResultItem.Linearity]));
                        Log.AddLog(j, string.Format("OISY_Hysteresis     = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_Hysteresis].Val = results[(int)ResultItem.Hysteresis]));
                        Log.AddLog(j, string.Format("OISY_CenterCurrent  = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_CenterCurrent].Val = results[(int)ResultItem.CenterCurrent]));
                        Log.AddLog(j, string.Format("OISY_MaxCurrent     = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISY_MaxCurrent].Val = results[(int)ResultItem.MaxCurrent]));

                        ShowEvent.Show(j, "Y");
                    }
                    else if (type == (int)AO_TYPE._CLR)
                    {
                        CalcCLXYPerformance(j, Theta[j], strokeX[j], Theta[j], gbTime, framecount, csTime, results, type);

                        Log.AddLog(j, string.Format("OISR_InitDegree     = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISR_InitDegree].Val = results[(int)ResultItem.InitDegree]));
                        Log.AddLog(j, string.Format("OISR_MaxDegree      = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISR_MaxDegree].Val = results[(int)ResultItem.MaxDegree]));
                        Log.AddLog(j, string.Format("OISR_Sensitivity    = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISR_Sensitivity].Val = results[(int)ResultItem.Sensitivity]));
                        Log.AddLog(j, string.Format("OISR_Linearity      = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISR_Linearity].Val = results[(int)ResultItem.Linearity]));
                        Log.AddLog(j, string.Format("OISR_Hysteresis     = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISR_Hysteresis].Val = results[(int)ResultItem.Hysteresis]));
                        Log.AddLog(j, string.Format("OISR_MaxCurrent     = {0:0.00}", Spec.PassFails[j].Results[(int)SpecItem.OISR_MaxCurrent].Val = results[(int)ResultItem.MaxCurrent]));

                        ShowEvent.Show(j, "R");
                    }

                    if (type == (int)AO_TYPE._CLX)
                    {
                        Chart.Stroke[j].RangeMin = Chart.Theta[j].RangeMin = Chart.Trajectory[j].RangeMin = Rcp.iXPlotRangeMin;
                        Chart.Stroke[j].RangeMax = Chart.Theta[j].RangeMax = Chart.Trajectory[j].RangeMax = Rcp.iXPlotRangeMax;

                        Chart.Stroke[j].SafeAddChart(type, framecount, Waveform_CL[type], strokeX[j], SensCurrent[j], Hall_X1[j]);
                        Chart.Theta[j].SafeAddChart(type, framecount, Waveform_CL[type], Theta[j]);
                        Chart.Trajectory[j].SafeAddChart(type, framecount, strokeX[j], strokeY[j], Waveform_CL[type]);
                    }
                    else if (type == (int)AO_TYPE._CLY)
                    {
                        Chart.Stroke[j].RangeMin = Chart.Theta[j].RangeMin = Chart.Trajectory[j].RangeMin = Rcp.iYPlotRangeMin;
                        Chart.Stroke[j].RangeMax = Chart.Theta[j].RangeMax = Chart.Trajectory[j].RangeMax = Rcp.iYPlotRangeMax;

                        Chart.Stroke[j].SafeAddChart(type, framecount, Waveform_CL[type], strokeY[j], SensCurrent[j], Hall_Y[j]);
                        Chart.Theta[j].SafeAddChart(type, framecount, Waveform_CL[type], Theta[j]);
                        Chart.Trajectory[j].SafeAddChart(type, framecount, strokeX[j], strokeY[j], Waveform_CL[type]);
                    }
                    else if (type == (int)AO_TYPE._CLR)
                    {
                        Chart.Rotation[j].RangeMin = Rcp.iRPlotRangeMin;
                        Chart.Rotation[j].RangeMax = Rcp.iRPlotRangeMax;
                        Chart.Rotation[j].SafeAddChart(type, framecount, Waveform_CL[type], Theta[j], SensCurrent[j], Hall_X1[j], Hall_X2[j]);
                        //Chart.Trajectory[j].SafeAddChart(type, framecount, strokeX[j], strokeY[j]);
                    }
                    else if (type == (int)AO_TYPE._CLXStep)
                    {

                        Chart.Step[j].SafeAddChart(type, framecount, GrabTime, strokeX[j], GrabHall);
                    }
                }
            }
            return true;
        }
        public bool CalcCLXYPerformance(int ch, double[] stroke, double[] cross_stroke, double[] theta, double[] spltime, int count, double[] csTime, double[] results, int axis = 0)
        {

            if (Option.m_bSaveRawData)
            {
                StreamWriter wr;
                string newDir = FIO.CheckResultFolder();
                newDir = newDir + "DrivingData\\";
                if (!Directory.Exists(newDir))
                {
                    Directory.CreateDirectory(newDir);
                }

                string sFilePath = string.Format("{0}{1}_{2}.csv", newDir + CLName[axis], ch, StrIndex[ch]);

                wr = new StreamWriter(sFilePath);
                DateTime dtNow = DateTime.Now;
                wr.WriteLine(dtNow.ToString("MM:dd:hh:mm:ss"));
                if (axis == (int)AO_TYPE._CLX) wr.WriteLine("i,Code,X Stroke,Y Stroke,Theta,Current,Hall X1,Hall X2,Hall Y");
                else if (axis == (int)AO_TYPE._CLY) wr.WriteLine("i,Code,X Stroke,Y Stroke,Theta,Current,Hall X1,Hall X2,Hall Y");
                else if (axis == (int)AO_TYPE._CLR) wr.WriteLine("i,Code,Theta,Current,Hall X1,Hall X2,Hall Y");
                for (int i = 0; i < count; i++)
                {
                    if (axis == (int)AO_TYPE._CLX)
                        wr.WriteLine(string.Format("{0},{1},{2:0.000},{3:0.000},{4:0.0},{5},{6},{7},{8}", i, Waveform_CL[axis][i], stroke[i], cross_stroke[i], theta[i], SensCurrent[ch][i], Hall_X1[ch][i], Hall_X2[ch][i], Hall_Y[ch][i]));
                    else if (axis == (int)AO_TYPE._CLY)
                        wr.WriteLine(string.Format("{0},{1},{2:0.000},{3:0.000},{4:0.0},{5},{6},{7},{8}", i, Waveform_CL[axis][i], cross_stroke[i], stroke[i], theta[i], SensCurrent[ch][i], Hall_X1[ch][i], Hall_X2[ch][i], Hall_Y[ch][i]));
                    else if (axis == (int)AO_TYPE._CLR)
                        wr.WriteLine(string.Format("{0},{1},{2:0.0},{3},{4},{5},{6}", i, Waveform_CL[axis][i], theta[i], SensCurrent[ch][i], Hall_X1[ch][i], Hall_X2[ch][i], Hall_Y[ch][i]));
                }
                wr.Close();
            }
            int harfIndex = 0;
            for (int i = 1; i < count; i++)
            {
                if (Waveform_CL[axis][i - 1] > Waveform_CL[axis][i]) break;
                harfIndex = i;
            }
            if (axis == 2)
            {
                //Theta ===============================================================================================================================
                results[(int)ResultItem.InitDegree] = theta[2];
                double max = -999;
                for (int i = 2; i < count - 1; i++)
                    if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                    {
                        max = Math.Max(max, theta[i]);
                    }

                double min = 999;
                for (int i = 2; i < count - 1; i++)
                    if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                    {
                        min = Math.Min(min, theta[i]);
                    }

                results[(int)ResultItem.MaxDegree] = max;

                CalLine_fit fwd = new CalLine_fit(harfIndex - 1);
                CalLine_fit bwd = new CalLine_fit(harfIndex - 1);

                //Linearity , Sensitivity, Hysteresis  ==================================================================================
                Log.AddLog(ch, "RawData for Linearity , Sensitivity, Hysteresis >>");
                Log.AddLog(ch, " FWD >>");

                int startFIndex = 1;
                int endFIndex = harfIndex - 1;

                for (int i = startFIndex; i < endFIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                    {
                        fwd.Point.Add(new CalLine_fit.Item() { x = Waveform_CL[axis][i], y = theta[i] });
                        Log.AddLog(ch, Waveform_CL[axis][i] + "\t" + theta[i].ToString("F3") + "\t");
                    }
                }

                fwd.Line_fitting();
                Log.AddLog(ch, " FWD Driving_resolution = " + fwd.dSlope.ToString("F4")); //   FWD Sensitivity
                Log.AddLog(ch, " BWD >>");

                int startBIndex = harfIndex;
                int endBIndex = count - 1;

                for (int i = startBIndex; i < endBIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                    {
                        bwd.Point.Add(new CalLine_fit.Item() { x = Waveform_CL[axis][i], y = theta[i] });
                        Log.AddLog(ch, Waveform_CL[axis][i] + "\t" + theta[i].ToString("F3") + "\t");
                    }
                }

                bwd.Line_fitting();
                Log.AddLog(ch, " BWD Driving_resolution = " + bwd.dSlope.ToString("F4"), false);  //   BWD Sensitivity

                //Sensitivity
                results[(int)ResultItem.Sensitivity] = 100 * (fwd.dSlope + bwd.dSlope) / 2;

                //Linearity
                results[(int)ResultItem.Linearity] = 0;
                for (int i = startFIndex; i < endFIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                    {
                        double newS = fwd.dSlope * Waveform_CL[axis][i] + fwd.dYintercept;
                        if (results[(int)ResultItem.Linearity] < Math.Abs(newS - theta[i]))
                            results[(int)ResultItem.Linearity] = Math.Abs(newS - theta[i]);
                    }
                }
                for (int i = startBIndex; i < endBIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                    {
                        double newS = bwd.dSlope * Waveform_CL[axis][i] + bwd.dYintercept;
                        if (results[(int)ResultItem.Linearity] < Math.Abs(newS - theta[i]))
                            results[(int)ResultItem.Linearity] = Math.Abs(newS - theta[i]);
                    }
                }

                double Hyst = 0;
                double oldHyst = 0;

                for (int i = 1; i < count; i++)
                {
                    if (i >= startFIndex && i <= endFIndex)
                    {
                        for (int j = count - 1; j > i + 2; j--)
                        {
                            if (Waveform_CL[axis][i] == Waveform_CL[axis][j])
                            {
                                if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                                {
                                    Hyst = Math.Abs(theta[i] - theta[j]);
                                    if (oldHyst < Hyst)
                                        oldHyst = Hyst;
                                }
                            }
                        }
                    }
                }
                //Hysteresis
                results[(int)ResultItem.Hysteresis] = oldHyst;

                //Max Current
                max = -999;
                for (int i = 1; i < count; i++)
                    if (Waveform_CL[axis][i] >= Rcp.iRMeasureRangeMin && Waveform_CL[axis][i] <= Rcp.iRMeasureRangeMax)
                    {
                        max = Math.Max(max, SensCurrent[ch][i]);
                    }
                results[(int)ResultItem.MaxCurrent] = max;

                if (double.IsNaN(results[(int)ResultItem.Sensitivity]))
                    results[(int)ResultItem.Sensitivity] = -1;

                if (results[(int)ResultItem.Sensitivity] < 0)
                {
                    results[(int)ResultItem.Sensitivity] = 999;
                    results[(int)ResultItem.Linearity] = 999;
                    results[(int)ResultItem.Hysteresis] = 999;
                }
            }
            else
            {
                int measureMin = 0;
                int measureMax = 0;

                if (axis == 0) // axis X
                {
                    measureMin = Rcp.iXMeasureRangeMin;
                    measureMax = Rcp.iXMeasureRangeMax;
                }
                if (axis == 1) // axis Y
                {
                    measureMin = Rcp.iYMeasureRangeMin;
                    measureMax = Rcp.iYMeasureRangeMax;
                }

                //Stroke ===============================================================================================================================
                //Forward Stroke =====================================================
                double max = -999;
                for (int i = 2; i < harfIndex - 1; i++)
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        max = Math.Max(max, stroke[i]);
                    }
                double min = 999;
                for (int i = 2; i < harfIndex - 1; i++)
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        min = Math.Min(min, stroke[i]);
                    }

                results[(int)ResultItem.Forwardstroke] = max - min;

                //Backward Stroke =====================================================
                max = -999;
                for (int i = harfIndex; i < count - 1; i++)
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        max = Math.Max(max, stroke[i]);
                    }
                min = 999;
                for (int i = harfIndex; i < count - 1; i++)
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        min = Math.Min(min, stroke[i]);
                    }

                results[(int)ResultItem.Backwardstroke] = max - min;

                //Full Stroke =====================================================
                results[(int)ResultItem.Ratedstroke] = results[(int)ResultItem.Forwardstroke] + results[(int)ResultItem.Backwardstroke];

                CalLine_fit fwd = new CalLine_fit(harfIndex - 1);
                CalLine_fit bwd = new CalLine_fit(harfIndex - 1);
                CalLine_fit Cross = new CalLine_fit(harfIndex - 1);

                //Linearity , Sensitivity, Hysteresis, Crosstalk  ==================================================================================
                double cross_min = 999;
                double cross_max = -999;

                Log.AddLog(ch, "RawData for Linearity , Sensitivity, Hysteresis, Crosstalk >>");
                Log.AddLog(ch, " FWD >>");

                int startFIndex = 1;
                int endFIndex = harfIndex;

                for (int i = startFIndex; i < endFIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        Cross.Point.Add(new CalLine_fit.Item() { x = stroke[i], y = cross_stroke[i] });
                        fwd.Point.Add(new CalLine_fit.Item() { x = Waveform_CL[axis][i], y = stroke[i] });

                        if (cross_min > cross_stroke[i])
                            cross_min = cross_stroke[i];

                        if (cross_max < cross_stroke[i])
                            cross_max = cross_stroke[i];

                        Log.AddLog(ch, Waveform_CL[axis][i] + "\t" + stroke[i].ToString("F3") + "\t" + cross_stroke[i].ToString("F3"));
                    }
                }
                fwd.Line_fitting();
                Log.AddLog(ch, " FWD Driving_resolution = " + fwd.dSlope.ToString("F4")); //   FWD Sensitivity
                Log.AddLog(ch, " BWD >>");

                int startBIndex = harfIndex;
                int endBIndex = count - 1;

                for (int i = startBIndex; i < endBIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        Cross.Point.Add(new CalLine_fit.Item() { x = stroke[i], y = cross_stroke[i] });
                        bwd.Point.Add(new CalLine_fit.Item() { x = Waveform_CL[axis][i], y = stroke[i] });

                        if (cross_min > cross_stroke[i])
                            cross_min = cross_stroke[i];

                        if (cross_max < cross_stroke[i])
                            cross_max = cross_stroke[i];

                        Log.AddLog(ch, Waveform_CL[axis][i] + "\t" + stroke[i].ToString("F3") + "\t" + cross_stroke[i].ToString("F3"));
                    }
                }

                results[(int)ResultItem.Crosstalk] = cross_max - cross_min;
                Log.AddLog(ch, "CrossMinMax:" + cross_min.ToString("F3") + " - " + cross_max.ToString("F3"));

                bwd.Line_fitting();
                Log.AddLog(ch, " BWD Driving_resolution = " + bwd.dSlope.ToString("F4"), false);  //   BWD Sensitivity

                //Sensitivity
                results[(int)ResultItem.Sensitivity] = 100 * (fwd.dSlope + bwd.dSlope) / 2;

                Cross.Line_fitting();

                double ltheta = Math.Atan(Cross.dSlope);
                //Crosstalk
                results[(int)ResultItem.Crosstalk] = Math.Cos(ltheta) * results[(int)ResultItem.Crosstalk];   // 각도 보상

                //Linearity
                results[(int)ResultItem.Linearity] = 0;
                for (int i = startFIndex; i < endFIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        double newS = fwd.dSlope * Waveform_CL[axis][i] + fwd.dYintercept;
                        if (results[(int)ResultItem.Linearity] < Math.Abs(newS - stroke[i]))
                            results[(int)ResultItem.Linearity] = Math.Abs(newS - stroke[i]);
                    }
                }
                for (int i = startBIndex; i < endBIndex; i++)
                {
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        double newS = bwd.dSlope * Waveform_CL[axis][i] + bwd.dYintercept;
                        if (results[(int)ResultItem.Linearity] < Math.Abs(newS - stroke[i]))
                            results[(int)ResultItem.Linearity] = Math.Abs(newS - stroke[i]);
                    }
                }

                double Hyst = 0;
                double oldHyst = 0;

                for (int i = 1; i < count; i++)
                {
                    if (i >= startFIndex && i <= endFIndex)
                    {
                        if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                        {
                            for (int j = count - 1; j > i + 2; j--)
                            {
                                if (Waveform_CL[axis][i] == Waveform_CL[axis][j])
                                {
                                    Hyst = Math.Abs(stroke[i] - stroke[j]);
                                    if (oldHyst < Hyst)
                                        oldHyst = Hyst;
                                }
                            }
                        }
                    }
                }
                //Hysteresis
                results[(int)ResultItem.Hysteresis] = oldHyst;

                //Center Current
                max = -999;
                for (int i = 1; i < count - 1; i++)
                {
                    if (Waveform_CL[axis][i] >= measureMin && Waveform_CL[axis][i] <= measureMax)
                    {
                        if ((stroke[i] < results[(int)ResultItem.Ratedstroke] / 2) && (stroke[i + 1] >= results[(int)ResultItem.Ratedstroke] / 2))
                        {
                            max = (SensCurrent[ch][i] + SensCurrent[ch][i + 1]) / 2;
                            break;
                        }
                    }
                }
                results[(int)ResultItem.CenterCurrent] = max;

                //Max Current
                max = -999;
                for (int i = 1; i < count; i++)
                    max = Math.Max(max, SensCurrent[ch][i]);
                results[(int)ResultItem.MaxCurrent] = max;

                if (double.IsNaN(results[(int)ResultItem.Sensitivity]))
                    results[(int)ResultItem.Sensitivity] = -1;

                if (results[(int)ResultItem.Sensitivity] < 0)
                {
                    results[(int)ResultItem.Sensitivity] = 999;
                    results[(int)ResultItem.Linearity] = 999;
                    results[(int)ResultItem.Hysteresis] = 999;
                }
            }


            return true;
        }
        public void XYAging(int ch, string testItem)
        {
            try
            {
                OIS_StartMode(ch, 0);
                OIS_StartMode(ch, 1);

                for (int i = 0; i < Rcp.iAgingLoop; i++)
                {
                    DrvIC.Move(ch, 0, (short)Rcp.iXDrvCodeMin);
                    Thread.Sleep(Rcp.iAgingDelay);
                    Log.AddLog(ch, string.Format("X Target : {0} , X Hall : {1}", (short)Rcp.iXDrvCodeMin, DrvIC.ReadHall(ch, 0)));
                    DrvIC.Move(ch, 0, (short)Rcp.iXDrvCodeMax);
                    Thread.Sleep(Rcp.iAgingDelay);
                    Log.AddLog(ch, string.Format("X Target : {0} , X Hall : {1}", (short)Rcp.iXDrvCodeMax, DrvIC.ReadHall(ch, 0)));
                }
                for (int i = 0; i < Rcp.iAgingLoop; i++)
                {
                    DrvIC.Move(ch, 1, (short)Rcp.iYDrvCodeMin);
                    Thread.Sleep(Rcp.iAgingDelay);
                    Log.AddLog(ch, string.Format("Y Target : {0} , Y Hall : {1}", (short)Rcp.iYDrvCodeMin, DrvIC.ReadHall(ch, 2)));
                    DrvIC.Move(ch, 1, (short)Rcp.iXDrvCodeMax);
                    Thread.Sleep(Rcp.iAgingDelay);
                    Log.AddLog(ch, string.Format("Y Target : {0} , Y Hall : {1}", (short)Rcp.iYDrvCodeMax, DrvIC.ReadHall(ch, 2)));
                }


            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void XY_PhaseMargin(int ch, string testItem)
        {
            try
            {
                LG_Result[ch].Clear();
                DrvIC.MeasureLoopGain(ch, 0); //Fra X1
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX_PMFreq].Val = LG_Result[ch].pmFreq;
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX_PhaseMargin].Val = LG_Result[ch].phaseMargin;
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX_MaxDiffGain].Val = LG_Result[ch].maxDiffGain;

                LG_Result[ch].Clear();
                DrvIC.MeasureLoopGain(ch, 1); //Fra X2
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX2_PMFreq].Val = LG_Result[ch].pmFreq;
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX2_PhaseMargin].Val = LG_Result[ch].phaseMargin;
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX2_MaxDiffGain].Val = LG_Result[ch].maxDiffGain;

                LG_Result[ch].Clear();
                DrvIC.MeasureLoopGain(ch, 2); //Fra Y
                Spec.PassFails[ch].Results[(int)SpecItem.FRAY_PMFreq].Val = LG_Result[ch].pmFreq;
                Spec.PassFails[ch].Results[(int)SpecItem.FRAY_PhaseMargin].Val = LG_Result[ch].phaseMargin;
                Spec.PassFails[ch].Results[(int)SpecItem.FRAY_MaxDiffGain].Val = LG_Result[ch].maxDiffGain;

                ShowEvent.Show(ch, "FRA X");
                ShowEvent.Show(ch, "FRA X2");
                ShowEvent.Show(ch, "FRA Y");
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        //public void Process_PhaseMargin(int ch)
        //{
        //    string message = "";
        //    if (ch == 1) return;
        //    byte loop_num, step, i;
        //    ushort Xloop_freq, Xend_freq, Yloop_freq, Yend_freq;
        //    ushort step_delay;
        //    double lg_num;

        //    loop_num = (byte)Rcp.iFRAloop;
        //    Xloop_freq = (ushort)Rcp.iXChirpFrom;
        //    Xend_freq = (ushort)Rcp.iXChirpTo;
        //    Yloop_freq = (ushort)Rcp.iYChirpFrom;
        //    Yend_freq = (ushort)Rcp.iYChirpTo;
        //    step = (byte)Rcp.iFRAstep;
        //    step_delay = (ushort)Rcp.iFRAdelay;

        //    if (PM_OISUseX)
        //    {
        //        short data = (short)Math.Min(Spec.PassFails[ch + 1].Results[(int)SpecItem.OISX_Ratedstroke].Val * 20, 255);

        //        DrvIC.Write2Byte(ch, 0x0064, data);
        //        Log.AddLog(ch, string.Format("Write to 0x0064 = 0x{0:X4}", data));
        //    }
        //    if (PM_OISUseY)
        //    {
        //        short data = (byte)Math.Min(Spec.PassFails[ch + 1].Results[(int)SpecItem.OISY_Ratedstroke].Val * 20, 255);

        //        DrvIC.Write2Byte(ch, 0x0066, data);
        //        Log.AddLog(ch, string.Format("Write to 0x0066 = 0x{0:X4}", data));
        //    }

        //    DrvIC.OIS_On(ch, false);
        //    Thread.Sleep(1);
        //    byte res = DrvIC.CheckIfOISSTS_IDLE(ch);
        //    if (res > 0)
        //    {
        //        ErrMsg[ch] = "STS_IDLE";
        //        ChannelOn[ch] = false;
        //        // errMsg[ch] = "Fail OISSTS_IDLE";
        //        Log.AddLog(ch, "Fail CheckIfOISSTS_IDLE", false);
        //        return;
        //    }

        //    Thread.Sleep(1);
        //    lg_num = Math.Pow(2.0, DrvIC.ReadByte(ch, LG_TRYNUM));

        //    if (Option.m_bDFTphasemargin)
        //        CrlDftFlag = 0x08;
        //    else
        //        CrlDftFlag = 0x00;

        //    if (PMCheckIndex[ch] == 0)
        //    {
        //        PMInspIndex += 1;
        //        PMCheckIndex[ch] = PMInspIndex;
        //    }

        //    byte[] rbuffer2 = new byte[2];
        //    byte[] rbuffer4 = new byte[4];
        //    for (i = 0; i < loop_num; i++)
        //    {
        //        Log.AddLog(ch, "LOOP_NUM = " + i.ToString() + "/" + loop_num.ToString(), false);
        //        if (!GetPhaseMarginOfAxis(ch, LOOPGAIN_X_AXIS, Xloop_freq, Xend_freq, step, lg_num, Rcp.iFRAXgainTH, false))
        //            continue;

        //        if (!GetPhaseMarginOfAxis(ch, LOOPGAIN_Y_AXIS, Yloop_freq, Yend_freq, step, lg_num, Rcp.iFRAYgainTH, false))
        //            continue;

        //        message = LG_Result[ch][LOOPGAIN_X_AXIS].phaseMargin.ToString("F1") + "\t" + LG_Result[ch][LOOPGAIN_Y_AXIS].phaseMargin.ToString("F1") + "\t"
        //                + LG_Result[ch][LOOPGAIN_X_AXIS].pmFreq.ToString("F1") + "\t" + LG_Result[ch][LOOPGAIN_Y_AXIS].pmFreq.ToString("F1") + "\t"
        //                + LG_Result[ch][LOOPGAIN_X_AXIS].maxDiffGain.ToString("F1") + "\t" + LG_Result[ch][LOOPGAIN_Y_AXIS].maxDiffGain.ToString("F1") + "\r\n";
        //        Log.AddLog(ch, message, false);
        //    }

        //    if (!ChannelOn[ch]) return;

        //    Spec.PassFails[ch].Results[(int)SpecItem.FRAX_PMFreq].Val = LG_Result[ch][0x01].pmFreq;
        //    Spec.PassFails[ch].Results[(int)SpecItem.FRAX_PhaseMargin].Val = LG_Result[ch][0x01].phaseMargin;
        //    Spec.PassFails[ch].Results[(int)SpecItem.FRAX_MaxDiffGain].Val = LG_Result[ch][0x01].maxDiffGain;
        //    ShowEvent.Show(ch, "FRA X");
        //    Spec.PassFails[ch].Results[(int)SpecItem.FRAY_PMFreq].Val = LG_Result[ch][0x11].pmFreq;
        //    Spec.PassFails[ch].Results[(int)SpecItem.FRAY_PhaseMargin].Val = LG_Result[ch][0x11].phaseMargin;
        //    Spec.PassFails[ch].Results[(int)SpecItem.FRAY_MaxDiffGain].Val = LG_Result[ch][0x11].maxDiffGain;
        //    ShowEvent.Show(ch, "FRA Y");
        //    if (PM_OISUseX)
        //    {
        //        rbuffer2 = DrvIC.ReadAnyByte(ch, 0x0064, 2);
        //        Log.AddLog(ch, "Read from 0x0064 = " + rbuffer2[0].ToString("X") + " " + rbuffer2[1].ToString("X"), false);
        //        rbuffer4 = DrvIC.ReadAnyByte(ch, 0x0038, 4);
        //        Log.AddLog(ch, "Read from 0x0038 = " + rbuffer4[0].ToString("X") + " " + rbuffer4[1].ToString("X") + " " + rbuffer4[2].ToString("X") + " " + rbuffer4[3].ToString("X"), false);
        //    }
        //    if (PM_OISUseY)
        //    {
        //        rbuffer2 = DrvIC.ReadAnyByte(ch, 0x0066, 2);
        //        Log.AddLog(ch, "Read from 0x0066 = " + rbuffer2[0].ToString("X") + " " + rbuffer2[1].ToString("X"), false);
        //        rbuffer4 = DrvIC.ReadAnyByte(ch, 0x003C, 4);
        //        Log.AddLog(ch, "Read from 0x003C = " + rbuffer4[0].ToString("X") + " " + rbuffer4[1].ToString("X") + " " + rbuffer4[2].ToString("X") + " " + rbuffer4[3].ToString("X"), false);
        //    }
        //}
        //public bool GetPhaseMarginOfAxis(int ch, byte lgAxis, ushort loop_freq, ushort end_freq, byte step, double lg_num, double GainThr, bool isSecondInsp)
        //{
        //    string strPM = string.Empty;
        //    string PMLog = string.Empty;
        //    string PMLogDir = string.Empty;
        //    StreamWriter sw = null;
        //    byte[] rbuffer = new byte[1];
        //    byte[] rbuffer2 = new byte[2];
        //    byte[] rbuffer8 = new byte[8];
        //    byte[] rbuffer16 = new byte[16];
        //    byte[] wbuffer = new byte[1];
        //    byte[] wbuffer2 = new byte[2];

        //    byte lp_count, b_startsign;//, ng_amp_0;
        //    int pre_start_freq, start_freq, freq_255 = 0;
        //    double pm = 0;
        //    double gain_amp, pre_gain, gain = 0;
        //    double gain_255, pm_255;
        //    double[] amp = new double[2];
        //    double[] phase = new double[2];
        //    double final_freq = 0;
        //    double tmp_phase = 0;
        //    double pre_pm = 0;
        //    string strAxis = "";
        //    UInt16 loop_amp = 0;
        //    int cCount = 0;
        //    double[] ampDft = new double[2];
        //    double[] phaseDft = new double[2];
        //    double phaseDelay = 0;
        //    int iSSSum = 0;
        //    Int32 iSCSum = 0;
        //    Int32 oHSSum = 0;
        //    Int32 oHCSum = 0;
        //    double M_PI = Math.Asin(1) * 2;

        //    double[] Freq = new double[(loop_freq - end_freq) / step + 1];
        //    double[] Gain = new double[(loop_freq - end_freq) / step + 1];
        //    double[] PM = new double[(loop_freq - end_freq) / step + 1];
        //    double[] Amp0 = new double[(loop_freq - end_freq) / step + 1];
        //    double[] Amp1 = new double[(loop_freq - end_freq) / step + 1];
        //    double[] Phase0 = new double[(loop_freq - end_freq) / step + 1];
        //    double[] Phase1 = new double[(loop_freq - end_freq) / step + 1];
        //    bool FindZeroGain = false;

        //    Log.AddLog(ch, "StartF=" + loop_freq + " EndF=" + end_freq, false);
        //    pre_gain = TEMP_INIT_VALUE; // Temp value Initialize
        //    start_freq = loop_freq;
        //    pre_start_freq = start_freq;
        //    lp_count = 0;
        //    b_startsign = GAIN_MINUS;
        //    gain_255 = TEMP_INIT_VALUE;
        //    pm_255 = 0;
        //    //ng_amp_0 = 0;

        //    LG_Result[ch][lgAxis].status = false;
        //    LG_Result[ch][lgAxis].phaseMargin = 0;
        //    LG_Result[ch][lgAxis].pmFreq = 0;
        //    LG_Result[ch][lgAxis].maxDiffGain = 0;

        //    if (Option.m_bSaveRawData)
        //    {
        //        PMLogDir = FIO.CheckResultFolder();
        //        PMLogDir = PMLogDir + "PMData\\";
        //        if (!Directory.Exists(PMLogDir))
        //            Directory.CreateDirectory(PMLogDir);

        //        if (lgAxis == LOOPGAIN_X_AXIS)
        //        {
        //            if (isSecondInsp)
        //                PMLogDir = PMLogDir + "PM_X_2nd_" + PMCheckIndex[ch] + ".csv";
        //            else
        //                PMLogDir = PMLogDir + "PM_X_" + PMCheckIndex[ch] + ".csv";
        //        }

        //        else if (lgAxis == LOOPGAIN_Y_AXIS)
        //        {
        //            if (isSecondInsp)
        //                PMLogDir = PMLogDir + "PM_Y_2nd_" + PMCheckIndex[ch] + ".csv";
        //            else
        //                PMLogDir = PMLogDir + "PM_Y_" + PMCheckIndex[ch] + ".csv";
        //        }

        //        sw = new StreamWriter(PMLogDir);
        //        sw.WriteLine("FREQ, GAIN, PM, Gain THD, AMP_0, AMP_1, PHASE_0, PHASE_1");

        //    }

        //    if (lgAxis == LOOPGAIN_X_AXIS)
        //    {
        //        loop_amp = (ushort)(Rcp.iXAmplitude);
        //        strAxis = "X ";
        //    }
        //    else if (lgAxis == LOOPGAIN_Y_AXIS)
        //    {
        //        loop_amp = (ushort)(Rcp.iYAmplitude);
        //        strAxis = "Y ";
        //    }
        //    Log.AddLog(ch, "Loop amp = " + loop_amp, false);
        //    if (loop_amp > 255)
        //    {
        //        Log.AddLog(ch, "Loop amp set = 255", false);
        //        loop_amp = 255;
        //    }

        //    wbuffer[0] = (byte)loop_amp;
        //    DrvIC.WriteAnyByte(ch, LG_AMPLITUDE, wbuffer);

        //    while (true)
        //    {
        //        if (LoopGainTrigger_CheckDone(ch, lgAxis, (ushort)start_freq) == false)
        //        {
        //            Log.AddLog(ch, "LGT_CHK returns false", false);
        //            return false;
        //        }

        //        Thread.Sleep(50);
        //        if (!Option.m_bDFTphasemargin)
        //        {
        //            //  Gain Crossing Method
        //            rbuffer8 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 8);
        //            if (rbuffer8 != null)
        //            {
        //                Thread.Sleep(1);
        //                rbuffer8 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 8);
        //            }

        //            amp[0] = (rbuffer8[1] << 8) | (rbuffer8[0] & 0xFF);
        //            amp[1] = (rbuffer8[3] << 8) | (rbuffer8[2] & 0xFF);
        //            phase[0] = (rbuffer8[5] << 8) | (rbuffer8[4] & 0xFF);
        //            phase[1] = (rbuffer8[7] << 8) | (rbuffer8[6] & 0xFF);

        //            if ((amp[0] + amp[1] == 0) || (phase[0] + phase[1] == 0))
        //            {
        //                Log.AddLog(ch, strAxis + start_freq.ToString() + "Hz", false);
        //                Log.AddLog(ch, " Err> Amp : " + rbuffer8[0].ToString("X") + "\t" + rbuffer8[1].ToString("X") + "\t" + rbuffer8[2].ToString("X") + "\t" + rbuffer8[3].ToString("X"), false);
        //                Log.AddLog(ch, " Err> phase : " + rbuffer8[4].ToString("X") + "\t" + rbuffer8[5].ToString("X") + "\t" + rbuffer8[6].ToString("X") + "\t" + rbuffer8[7].ToString("X"), false);
        //                if (cCount++ < 2)  ////////// 10 -> 2
        //                    continue;
        //            }
        //            // Phase Margin, Gain 값 계산식 적용하여 표시
        //            if (amp[1] > 0)
        //                gain_amp = (double)(amp[0]) / (double)(amp[1]);
        //            else
        //                gain_amp = 99999999;

        //            if (gain_amp == 0)
        //            {
        //                //ng_amp_0 = 1;
        //            }
        //            gain = 20 * Math.Log10(gain_amp);
        //            pm = ((phase[0] - phase[1]) / lg_num) * ((double)start_freq / (double)PID_CF_16_667_KHZ) * (-360);

        //            Freq[lp_count] = start_freq;
        //            Gain[lp_count] = gain;
        //            PM[lp_count] = pm;
        //            Amp0[lp_count] = amp[0];
        //            Amp1[lp_count] = amp[1];
        //            Phase0[lp_count] = phase[0];
        //            Phase1[lp_count] = phase[1];
        //        }
        //        else
        //        {
        //            //  DFT Method
        //            rbuffer16 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 16);
        //            if (rbuffer16 != null)
        //            {
        //                Thread.Sleep(1);
        //                rbuffer16 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 16);
        //            }
        //            iSSSum = (rbuffer16[0] + (rbuffer16[1] << 8) + (rbuffer16[2] << 16) + (rbuffer16[3] << 24));
        //            iSCSum = (rbuffer16[4] + (rbuffer16[5] << 8) + (rbuffer16[6] << 16) + (rbuffer16[7] << 24));
        //            oHSSum = (rbuffer16[8] + (rbuffer16[9] << 8) + (rbuffer16[10] << 16) + (rbuffer16[11] << 24));
        //            oHCSum = (rbuffer16[12] + (rbuffer16[13] << 8) + (rbuffer16[14] << 16) + (rbuffer16[15] << 24));

        //            ampDft[0] = (Math.Sqrt(Math.Pow((double)(iSCSum), 2) + Math.Pow((double)(iSSSum), 2)) / ((2048 * loop_amp) >> 8)) / (((double)PID_CF_16_667_KHZ / ((double)start_freq) + 1) * 8) * 2;
        //            ampDft[1] = (Math.Sqrt(Math.Pow((double)(oHCSum), 2) + Math.Pow((double)(oHSSum), 2)) / ((2048 * loop_amp) >> 8)) / (((double)PID_CF_16_667_KHZ / ((double)start_freq) + 1) * 8) * 2;
        //            phaseDft[0] = Math.Atan2((double)iSSSum, (double)iSCSum) * 180 / M_PI;
        //            phaseDft[1] = Math.Atan2((double)oHSSum, (double)oHCSum) * 180 / M_PI;
        //            /* Phase Margin, Gain 값 계산식 적용하여 표시 */
        //            gain_amp = (double)(ampDft[1]) / (double)(ampDft[0]);
        //            gain = 20 * Math.Log10(gain_amp);
        //            phaseDelay = phaseDft[1] - phaseDft[0];
        //            pm = 180 - phaseDelay;
        //            if (pm > 360)
        //            {
        //                pm = pm - 360;
        //            }
        //            else if (pm < -360)
        //            {
        //                pm = pm + 360;
        //            }

        //            Freq[lp_count] = start_freq;
        //            Gain[lp_count] = gain;
        //            PM[lp_count] = pm;
        //            Amp0[lp_count] = ampDft[0];
        //            Amp1[lp_count] = ampDft[1];
        //            Phase0[lp_count] = phaseDft[0];
        //            Phase1[lp_count] = phaseDft[1];

        //        }

        //        if ((gain > 0.0) && (gain < gain_255))
        //        {
        //            gain_255 = gain;            //  전체 Scan 영역에서 Gain 이 최소일때의 주파수와 PM 을 저장한다.
        //            freq_255 = start_freq;
        //            pm_255 = pm;
        //        }

        //        lp_count++;

        //        //Log.AddLog(ch, "Ch " + ch + " : on way.. " + strAxis + " \tPM = " + pm_255.ToString("F3") + "\t" + "@ Freq = " + start_freq.ToString() + "\tGain = " + gain.ToString("F2") + "\tPre_Gain = " + pre_gain.ToString("F2") + " StartF=" + start_freq.ToString("F0") + " EndF=" + end_freq.ToString("F0"));
        //        if (strAxis == "X ")
        //            strPM = strPM + "X =\t" + Freq[lp_count - 1].ToString() + "\t" + Gain[lp_count - 1].ToString("F2") + "\t" + PM[lp_count - 1].ToString("F2") + "\t" + Amp0[lp_count - 1].ToString("F2") + "\t" + Amp1[lp_count - 1].ToString("F2") + "\t" + Phase0[lp_count - 1].ToString("F2") + "\t" + Phase1[lp_count - 1].ToString("F2") + "\r\n";
        //        else
        //            strPM = strPM + "Y =\t" + Freq[lp_count - 1].ToString() + "\t" + Gain[lp_count - 1].ToString("F2") + "\t" + PM[lp_count - 1].ToString("F2") + "\t" + Amp0[lp_count - 1].ToString("F2") + "\t" + Amp1[lp_count - 1].ToString("F2") + "\t" + Phase0[lp_count - 1].ToString("F2") + "\t" + Phase1[lp_count - 1].ToString("F2") + "\r\n";


        //        if (lp_count == 1)      // First Time
        //        {
        //            if (gain > 0.0)     // exceptional case //  첫번째 주파수로써 gain 이 0보다 크면 gain_plus 를 할당해준다. 다음주파수에서 Gain 도 0보다 크면 종료된다.
        //            {
        //                b_startsign = GAIN_PLUS;
        //                start_freq = start_freq - step;
        //                continue;
        //            }
        //        }
        //        if (b_startsign == GAIN_PLUS)
        //        {
        //            if (gain < 0.0)
        //            {
        //                b_startsign = GAIN_MINUS;   // 첫번쨰 주파수에서 gain 이 0보다 크고, 두번째 주파수에서의 Gain 이 0보다 작으면 두번째 주파수의 gain 은 0 으로 강제할당한다.
        //            }
        //            else
        //            {
        //                start_freq = start_freq - step;     // 첫번째 주파수의 Gain 이 0 보다 크고 두번째 주파수에서의 Gain 도 0보다 크면 이쪽으로 들어온다.
        //                if (start_freq < end_freq)
        //                {
        //                    //  주파수 검색이 끝난상태
        //                    //errMsg[ch] = "";   //   Finish Test
        //                    //Log.AddLog( ch , "Ch " + ch + " : PM Test Done " + strAxis + " \tPM = " + pm_255.ToString("F3") + "\t" + "@ Freq = " + freq_255.ToString() + "\tGain = " + gain.ToString("F2") + "\tPre_Gain = " + pre_gain.ToString("F2") + " StartF=" + start_freq.ToString("F0") + " EndF=" + end_freq.ToString("F0"));
        //                    LG_Result[ch][lgAxis].phaseMargin = pm_255;
        //                    LG_Result[ch][lgAxis].pmFreq = freq_255;
        //                    LG_Result[ch][lgAxis].maxDiffGain = gain;
        //                    break;
        //                }
        //                continue;
        //            }
        //        }
        //        if ((pre_gain != TEMP_INIT_VALUE) && (Math.Abs(gain - pre_gain) > LG_Result[ch][lgAxis].maxDiffGain))
        //        {
        //            LG_Result[ch][lgAxis].maxDiffGain = Math.Abs(gain - pre_gain);
        //        }


        //        //pre_start_freq = start_freq;
        //        //pre_gain = gain;
        //        //pre_pm = pm;
        //        //start_freq = start_freq - step;
        //        //if (start_freq < end_freq)
        //        //    break;
        //        //else
        //        //    continue;

        //        if (gain >= 0.0)
        //        {
        //            if (isSecondInsp)
        //            {
        //                if (lgAxis == LOOPGAIN_X_AXIS)
        //                    XGainThr_2nd[ch] = gain;
        //                else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                    YGainThr_2nd[ch] = gain;
        //            }
        //            else
        //            {
        //                if (lgAxis == LOOPGAIN_X_AXIS)
        //                    XGainThr[ch] = gain;
        //                else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                    YGainThr[ch] = gain;
        //            }

        //            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //            /////   수정 / 추가한 부분
        //            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //            final_freq = (int)(((gain * pre_start_freq) - (pre_gain * start_freq)) / (gain - pre_gain));                                            //  2019.3.23 수정
        //            LG_Result[ch][lgAxis].pmFreq = final_freq;                                                                                      //  2019.3.23 추가
        //            LG_Result[ch][lgAxis].phaseMargin = pre_pm + (pm - pre_pm) * (final_freq - pre_start_freq) / (start_freq - pre_start_freq);     //  2019.3.23 추가
        //            FindZeroGain = true;
        //            for (int i = 0; i < lp_count; i++)
        //            {
        //                if (Option.m_bSaveRawData)
        //                    PMLog += Freq[i].ToString() + "," + Gain[i].ToString("F2") + "," + PM[i].ToString("F2") + ", " + "," + Amp0[i].ToString("F2") + "," + Amp1[i].ToString("F2") + "," + Phase0[i].ToString("F2") + "," + Phase1[i].ToString("F2") + "\r\n";
        //            }
        //            if (Option.m_bPhaseInterpolation)
        //                break;
        //            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //            bool res = LoopGainTrigger_CheckDone(ch, lgAxis, (ushort)LG_Result[ch][lgAxis].pmFreq);
        //            if (!Option.m_bDFTphasemargin)
        //            {
        //                rbuffer8 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 8);
        //                amp[0] = (rbuffer8[1] << 8) | (rbuffer8[0] & 0xFF);
        //                amp[1] = (rbuffer8[3] << 8) | (rbuffer8[2] & 0xFF);
        //                phase[0] = (rbuffer8[5] << 8) | (rbuffer8[4] & 0xFF);
        //                phase[1] = (rbuffer8[7] << 8) | (rbuffer8[6] & 0xFF);
        //                gain_amp = (double)amp[0] / (double)amp[1];
        //                gain = 20 * Math.Log10(gain_amp);

        //                if ((phase[0] + phase[1]) == 0)
        //                {
        //                    //Log.AddLog(ch, "illegal PhaseMargin : P0= " + phase[0].ToString("F0") + " P1= " + phase[1].ToString("F0") + " res=" + res.ToString());
        //                    continue;
        //                }
        //                tmp_phase = ((phase[0] - phase[1]) / lg_num) * ((double)start_freq / (double)PID_CF_16_667_KHZ) * (-360);
        //                if (tmp_phase > 0)
        //                {
        //                    LG_Result[ch][lgAxis].phaseMargin = tmp_phase;
        //                    LG_Result[ch][lgAxis].maxDiffGain = gain;
        //                }
        //                else
        //                {
        //                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                    /////   추가한 부분
        //                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                    LG_Result[ch][lgAxis].phaseMargin = pre_pm;
        //                    LG_Result[ch][lgAxis].pmFreq = pre_start_freq;
        //                    LG_Result[ch][lgAxis].maxDiffGain = pre_gain;
        //                    strPM += "pre_start_freq= " + pre_start_freq.ToString("F0") + " pre_pm= " + pre_pm.ToString("F1") + " now_freq=" + final_freq.ToString("F0") + " now_pm =" + tmp_phase.ToString("F1");
        //                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                }

        //                break;
        //            }
        //            else
        //            {
        //                //  DFT Method
        //                rbuffer16 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 15);
        //                if (rbuffer16 != null)
        //                {
        //                    Thread.Sleep(1);
        //                    rbuffer16 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 15);
        //                }
        //                iSSSum = (rbuffer16[0] + (rbuffer16[1] << 8) + (rbuffer16[2] << 16) + (rbuffer16[3] << 24));
        //                iSCSum = (rbuffer16[4] + (rbuffer16[5] << 8) + (rbuffer16[6] << 16) + (rbuffer16[7] << 24));
        //                oHSSum = (rbuffer16[8] + (rbuffer16[9] << 8) + (rbuffer16[10] << 16) + (rbuffer16[11] << 24));
        //                oHCSum = (rbuffer16[12] + (rbuffer16[13] << 8) + (rbuffer16[14] << 16) + (rbuffer16[15] << 24));

        //                ampDft[0] = (Math.Sqrt(Math.Pow((double)(iSCSum), 2) + Math.Pow((double)(iSSSum), 2)) / ((2048 * loop_amp) >> 8)) / (((double)PID_CF_16_667_KHZ / ((double)start_freq) + 1) * 8) * 2;
        //                ampDft[1] = (Math.Sqrt(Math.Pow((double)(oHCSum), 2) + Math.Pow((double)(oHSSum), 2)) / ((2048 * loop_amp) >> 8)) / (((double)PID_CF_16_667_KHZ / ((double)start_freq) + 1) * 8) * 2;
        //                phaseDft[0] = Math.Atan2((double)iSSSum, (double)iSCSum) * 180 / M_PI;
        //                phaseDft[1] = Math.Atan2((double)oHSSum, (double)oHCSum) * 180 / M_PI;
        //                /* Phase Margin, Gain 값 계산식 적용하여 표시 */
        //                gain_amp = (double)(ampDft[1]) / (double)(ampDft[0]);
        //                gain = 20 * Math.Log10(gain_amp);
        //                phaseDelay = phaseDft[1] - phaseDft[0];
        //                tmp_phase = 180 - phaseDelay;
        //                if (tmp_phase > 360)
        //                {
        //                    tmp_phase = tmp_phase - 360;
        //                }
        //                else if (tmp_phase < -360)
        //                {
        //                    tmp_phase = tmp_phase + 360;
        //                }
        //                LG_Result[ch][lgAxis].phaseMargin = tmp_phase;
        //                LG_Result[ch][lgAxis].pmFreq = start_freq;
        //                LG_Result[ch][lgAxis].maxDiffGain = gain;

        //                strPM += "pre_start_freq= " + pre_start_freq.ToString("F0") + " pre_pm= " + pre_pm.ToString("F1") + " now_freq=" + final_freq.ToString("F0") + " now_pm =" + tmp_phase.ToString("F1");

        //                break;
        //            }
        //        }

        //        pre_start_freq = start_freq;
        //        pre_gain = gain;
        //        pre_pm = pm;
        //        start_freq = start_freq - step;

        //        if (start_freq < end_freq)
        //        {

        //            if (GainThr != 0 && !FindZeroGain)
        //            {
        //                int GainThrindex = 0;
        //                double max = Gain[0];
        //                for (int i = 0; i < lp_count; i++)
        //                {
        //                    if (Gain[i] > max && Gain[i] >= GainThr)
        //                    {
        //                        max = Gain[i];
        //                        GainThrindex = i;

        //                    }
        //                }
        //                for (int i = 0; i < lp_count; i++)
        //                {
        //                    if (Option.m_bSaveRawData)
        //                    {
        //                        if (GainThrindex == i && Gain[GainThrindex] >= GainThr)
        //                            PMLog += Freq[i].ToString() + "," + Gain[i].ToString("F2") + "," + PM[i].ToString("F2") + "," + "Gain THD" + "," + Amp0[i].ToString("F2") + "," + Amp1[i].ToString("F2") + "," + Phase0[i].ToString("F2") + "," + Phase1[i].ToString("F2") + "\r\n";
        //                        else
        //                            PMLog += Freq[i].ToString() + "," + Gain[i].ToString("F2") + "," + PM[i].ToString("F2") + "," + " " + "," + Amp0[i].ToString("F2") + "," + Amp1[i].ToString("F2") + "," + Phase0[i].ToString("F2") + "," + Phase1[i].ToString("F2") + "\r\n";
        //                    }

        //                }
        //                if (Gain[GainThrindex] >= GainThr)
        //                {
        //                    LG_Result[ch][lgAxis].phaseMargin = PM[GainThrindex];
        //                    LG_Result[ch][lgAxis].pmFreq = Freq[GainThrindex];

        //                    if (isSecondInsp)
        //                    {
        //                        if (lgAxis == LOOPGAIN_X_AXIS)
        //                            XGainThr_2nd[ch] = Gain[GainThrindex];
        //                        else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                            YGainThr_2nd[ch] = Gain[GainThrindex];
        //                    }
        //                    else
        //                    {
        //                        if (lgAxis == LOOPGAIN_X_AXIS)
        //                            XGainThr[ch] = Gain[GainThrindex];
        //                        else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                            YGainThr[ch] = Gain[GainThrindex];
        //                    }


        //                }
        //                else
        //                {

        //                    if (isSecondInsp)
        //                    {
        //                        if (lgAxis == LOOPGAIN_X_AXIS)
        //                            XGainThr_2nd[ch] = 0;
        //                        else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                            YGainThr_2nd[ch] = 0;
        //                    }
        //                    else
        //                    {
        //                        if (lgAxis == LOOPGAIN_X_AXIS)
        //                            XGainThr[ch] = 0;
        //                        else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                            YGainThr[ch] = 0;
        //                    }

        //                    if (lgAxis == LOOPGAIN_X_AXIS)        //khkim_170920
        //                    {
        //                        Log.AddLog(ch, "X Freq Range Not Correct.", false);
        //                    }
        //                    else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                    {
        //                        Log.AddLog(ch, "Y Freq Range Not Correct.", false);
        //                    }
        //                }

        //                break;


        //            }
        //            else
        //            {

        //                if (lgAxis == LOOPGAIN_X_AXIS)        //khkim_170920
        //                {
        //                    Log.AddLog(ch, "X Freq Range Not Correct.", false);
        //                }
        //                else if (lgAxis == LOOPGAIN_Y_AXIS)
        //                {
        //                    Log.AddLog(ch, "Y Freq Range Not Correct.", false);
        //                }
        //                break;
        //            }

        //        }
        //    }
        //    Log.AddLog(ch, strPM, false);
        //    LG_Result[ch][lgAxis].status = true;

        //    if (Option.m_bSaveRawData)
        //    {
        //        sw.WriteLine(PMLog);
        //        sw.Close();
        //    }
        //    return true;
        //}
        //public bool LoopGainTrigger_CheckDone(int ch, byte axisCmd, ushort testFreq)
        //{
        //    byte[] rbuffer = new byte[1];
        //    byte[] rbuffer2 = new byte[2];
        //    byte[] wbuffer = new byte[1];
        //    byte[] wbuffer2 = new byte[2];

        //    //theApp.m_FTDII2C.txData[0] = testFreq;
        //    wbuffer2[0] = (byte)(testFreq & 0xff);
        //    wbuffer2[1] = (byte)(testFreq >> 8);
        //    //pDlg->WriteData(LG_FREQUENCY, theApp.m_FTDII2C.txData, NUMBER_1);
        //    DrvIC.WriteAnyByte(ch, LG_FREQUENCY, wbuffer2);

        //    //Log.AddLog( ch , "W 0x" + LG_FREQUENCY.ToString("X") + " 0x" + wbuffer[0].ToString("X"));
        //    //theApp.m_FTDII2C.txData[0] = axisCmd;

        //    wbuffer[0] = (byte)(axisCmd + CrlDftFlag);
        //    //pDlg->WriteData(LGCTRL, theApp.m_FTDII2C.txData, NUMBER_1);
        //    DrvIC.WriteAnyByte(ch, LGCTRL, wbuffer);
        //    //Log.AddLog( ch , "W 0x" + LGCTRL.ToString("X") + " 0x" + wbuffer[0].ToString("X"));

        //    /* Check LoopGain Done */
        //    //wrSytemLog("LGT_CHK(" + axisCmd.ToString() + ") \t testFreq=" + testFreq.ToString() + "Hz");
        //    Thread.Sleep(120);
        //    int j = 0;
        //    do
        //    {
        //        Thread.Sleep(30);
        //        // Loop Gain 상태가 Stop 인지 확인
        //        //pDlg->ReadData(LGCTRL, theApp.m_FTDII2C.rxData, NUMBER_1);
        //        rbuffer = DrvIC.ReadAnyByte(ch, LGCTRL, 1);

        //        if (j++ > 200)
        //        {
        //            Log.AddLog(ch, "Fail LoopGainTrigger_CheckDone : " + axisCmd.ToString("X") + "axis " + testFreq.ToString("F0") + "Hz", false);
        //            return false;
        //        }
        //    } while ((rbuffer[0] & LG_CTRL_MASK_ENABLE) != 0);

        //    return true;
        //}
        public void LoopGain_10Hz(int ch, string testItem)
        {
            try
            {
                LG_Result[ch].Clear();
                DrvIC.MeasureLoop10HzGain(ch, 0); 
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX_Gain10Hz].Val = LG_Result[ch].maxDiffGain;

                LG_Result[ch].Clear();
                DrvIC.MeasureLoop10HzGain(ch, 1); 
                Spec.PassFails[ch].Results[(int)SpecItem.FRAX2_Gain10Hz].Val = LG_Result[ch].maxDiffGain;

                LG_Result[ch].Clear();
                DrvIC.MeasureLoop10HzGain(ch, 2); 
                Spec.PassFails[ch].Results[(int)SpecItem.FRAY_Gain10Hz].Val = LG_Result[ch].maxDiffGain;
                ShowEvent.Show(ch, "FRA X");
                ShowEvent.Show(ch, "FRA X2");
                ShowEvent.Show(ch, "FRA Y");
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        //public bool LoopGainSingle(int ch, int axis, ushort Freq)
        //{
        //    byte[] rbuffer = new byte[1];
        //    byte[] wbuffer = new byte[1];
        //    byte[] wbuffer2 = new byte[2];
        //    double lg_num;
        //    ushort loop_amp = 0;
        //    string strAxis = "";
        //    byte[] rbuffer8 = new byte[8];
        //    double[] amp_res = new double[2];
        //    double gain_amp, gain = 0;
        //    byte[] rbuffer16 = new byte[16];
        //    int iSSSum = 0;
        //    int iSCSum = 0;
        //    int oHSSum = 0;
        //    int oHCSum = 0;
        //    int axis1 = 2;

        //    if (axis == LOOPGAIN_X_AXIS)
        //        axis1 = 0;
        //    else
        //        axis1 = 1;

        //    mGain10Hz[ch][axis1] = 0;

        //    DrvIC.OIS_On(ch, false);
        //    Thread.Sleep(1);
        //    byte res = DrvIC.CheckIfOISSTS_IDLE(ch);
        //    if (res > 0)
        //    {
        //        ErrMsg[ch] = "STS_IDLE";
        //        ChannelOn[ch] = false;
        //        // errMsg[ch] = "Fail OISSTS_IDLE";
        //        Log.AddLog(ch, "Fail CheckIfOISSTS_IDLE", false);
        //        return false;
        //    }
        //    Thread.Sleep(1);
        //    rbuffer = DrvIC.ReadAnyByte(ch, LG_TRYNUM, 1);
        //    Thread.Sleep(1);
        //    lg_num = Math.Pow(2.0, (double)rbuffer[0]);

        //    if (Option.m_bDFTphasemargin)
        //        CrlDftFlag = 0x08;
        //    else
        //        CrlDftFlag = 0x00;



        //    if (axis == LOOPGAIN_X_AXIS)
        //    {
        //        loop_amp = (ushort)(Rcp.iLoppgainXAmp);  //   Code
        //        strAxis = "X ";
        //    }
        //    else if (axis == LOOPGAIN_Y_AXIS)
        //    {
        //        loop_amp = (ushort)(Rcp.iLoppgainYAmp);  //   Code
        //        strAxis = "Y ";
        //    }
        //    Log.AddLog(ch, "Loop amp = " + loop_amp, false);
        //    if (loop_amp > 255)
        //    {

        //        Log.AddLog(ch, "Loop amp set = 255", false);
        //        loop_amp = 255;
        //    }

        //    wbuffer[0] = (byte)loop_amp;
        //    DrvIC.WriteAnyByte(ch, LG_AMPLITUDE, wbuffer);

        //    if (!LoopGainTrigger_CheckDone(ch, (byte)axis, Freq))
        //    {
        //        Log.AddLog(ch, "LGT_CHK returns false", false);
        //        return false;
        //    }

        //    Thread.Sleep(50);

        //    if (!Option.m_bDFTphasemargin)
        //    {
        //        //  Gain Crossing Method
        //        rbuffer8 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 8);
        //        if (rbuffer8 != null)
        //        {
        //            Thread.Sleep(1);
        //            rbuffer8 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 8);
        //        }

        //        amp_res[0] = (rbuffer8[1] << 8) | (rbuffer8[0] & 0xFF);
        //        amp_res[1] = (rbuffer8[3] << 8) | (rbuffer8[2] & 0xFF);


        //        if ((amp_res[0] + amp_res[1] == 0))
        //        {
        //            Log.AddLog(ch, strAxis + Freq.ToString() + "Hz", false);
        //            Log.AddLog(ch, " Err> Amp : " + rbuffer8[0].ToString("X") + "\t" + rbuffer8[1].ToString("X") + "\t" + rbuffer8[2].ToString("X") + "\t" + rbuffer8[3].ToString("X"), false);
        //        }
        //        // Phase Margin, Gain 값 계산식 적용하여 표시
        //        if (amp_res[1] > 0)
        //            gain_amp = (double)(amp_res[0]) / (double)(amp_res[1]);
        //        else
        //            gain_amp = 99999999;

        //        gain = 20 * Math.Log10(gain_amp);
        //        mGain10Hz[ch][axis1] = gain;
        //        if (axis1 == 0)
        //            Log.AddLog(ch, "X = " + Freq.ToString() + "Hz, " + gain.ToString("F3"), false);
        //        else
        //            Log.AddLog(ch, "Y = " + Freq.ToString() + "Hz, " + gain.ToString("F3"), false);
        //    }
        //    else
        //    {
        //        //  DFT Method
        //        rbuffer16 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 16);
        //        if (rbuffer16 != null)
        //        {
        //            Thread.Sleep(1);
        //            rbuffer16 = DrvIC.ReadAnyByte(ch, LGMON_AMP_N_PHASE, 16);
        //        }
        //        iSSSum = (rbuffer16[0] + (rbuffer16[1] << 8) + (rbuffer16[2] << 16) + (rbuffer16[3] << 24));
        //        iSCSum = (rbuffer16[4] + (rbuffer16[5] << 8) + (rbuffer16[6] << 16) + (rbuffer16[7] << 24));
        //        oHSSum = (rbuffer16[8] + (rbuffer16[9] << 8) + (rbuffer16[10] << 16) + (rbuffer16[11] << 24));
        //        oHCSum = (rbuffer16[12] + (rbuffer16[13] << 8) + (rbuffer16[14] << 16) + (rbuffer16[15] << 24));

        //        amp_res[0] = (Math.Sqrt(Math.Pow((double)(iSCSum), 2) + Math.Pow((double)(iSSSum), 2)) / ((2048 * loop_amp) >> 8)) / (((double)PID_CF_16_667_KHZ / ((double)Freq) + 1) * 8) * 2;
        //        amp_res[1] = (Math.Sqrt(Math.Pow((double)(oHCSum), 2) + Math.Pow((double)(oHSSum), 2)) / ((2048 * loop_amp) >> 8)) / (((double)PID_CF_16_667_KHZ / ((double)Freq) + 1) * 8) * 2;

        //        /* Phase Margin, Gain 값 계산식 적용하여 표시 */
        //        gain_amp = (double)(amp_res[1]) / (double)(amp_res[0]);
        //        gain = 20 * Math.Log10(gain_amp);
        //        mGain10Hz[ch][axis1] = gain;
        //        if (axis1 == 0)
        //            Log.AddLog(ch, "X = " + Freq.ToString() + "Hz, " + gain.ToString("F3"), false);
        //        else
        //            Log.AddLog(ch, "Y = " + Freq.ToString() + "Hz, " + gain.ToString("F3"), false);

        //    }
        //    return true;

        //}
        public void Hall_Deviation(int ch, string testItem)
        {
            try
            {
                OIS_StartMode(ch, 0);
                OIS_StartMode(ch, 1);

                byte[] rbuffer = new byte[4];
                byte[] wbuffer = new byte[4];
                byte[] wbuf2 = new byte[2];

                string strlineCh1 = "";
                double lhx = 0, lhy = 0;
                StreamWriter wr = null;

                wbuf2[0] = (byte)(Rcp.HallDEV_GainFactorX % 256);
                wbuf2[1] = (byte)(Rcp.HallDEV_GainFactorX >> 8);
                DrvIC.WriteAnyByte(ch, 0x0854, wbuf2);  // x,y 동시 구동
                wbuf2[0] = (byte)(Rcp.HallDEV_GainFactorY % 256);
                wbuf2[1] = (byte)(Rcp.HallDEV_GainFactorY >> 8);
                DrvIC.WriteAnyByte(ch, 0x0856, wbuf2);  // x,y 동시 구동

                Log.AddLog(ch, "GainFactor X = " + Rcp.HallDEV_GainFactorX, false);
                Log.AddLog(ch, "GainFactor Y = " + Rcp.HallDEV_GainFactorY, false);

                if (DrvIC.RetryRead(ch, 0x0001, 0x02, 10) > 0x02)
                {
                    ErrMsg[ch] = "I2C_FAIL";
                    ChannelOn[ch] = false;
                    Log.AddLog(ch, "I2C Fail in Process_CLXYTest() #" + ch, false);
                }
                else
                    DrvIC.OIS_On(ch, true);

                string newDir = FIO.CheckResultFolder();
                newDir += "HallDeviation\\";
                if (!Directory.Exists(newDir))
                {
                    Directory.CreateDirectory(newDir);
                }
                string sFileNameCh1 = newDir + "\\HallDev_" + StrIndex[ch].ToString() + ".csv";

                double[] CenterOffsetTx = new double[2];
                double[] CenterOffsetTy = new double[2];
                double[] Tx = new double[2];
                double[] Ty = new double[2];

                if (Option.m_bSaveRawData)
                {
                    wr = new StreamWriter(sFileNameCh1);
                    wr.WriteLine("Hall_X" + "," + "Hall_Y");
                }

                // move target position start
                wbuffer[0] = (byte)(((int)Rcp.HallDEV_TargetX) % 256);
                wbuffer[1] = (byte)(((int)Rcp.HallDEV_TargetX) >> 8);
                wbuffer[2] = (byte)(((int)Rcp.HallDEV_TargetY) % 256);
                wbuffer[3] = (byte)(((int)Rcp.HallDEV_TargetY) >> 8);

                DrvIC.WriteAnyByte(ch, 0x0720, wbuffer);    // x,y 동시 구동
                // move target position end

                Thread.Sleep((int)Rcp.HallDEV_InitialDelay);

                double hall_ch1_min_x = 99999;
                double hall_ch1_max_x = 0;
                double hall_ch1_min_y = 99999;
                double hall_ch1_max_y = 0;

                double sumch1x = 0;
                double sumch1y = 0;

                double sumch1x_sqr = 0;
                double sumch1y_sqr = 0;

                double avg_ch1_hall_x = 0;
                double avg_ch1_hall_y = 0;

                double avg_ch1_hall_sqr_x = 0;
                double avg_ch1_hall_sqr_y = 0;

                double Halldeviation_ch1x = 0;
                double Halldeviation_ch1y = 0;
                //int port, double InitDelay, double Interval, double sampling, double TargetX, double TargetY)
                Log.AddLog(ch, "InitDelay=  " + Rcp.HallDEV_InitialDelay + " ms", false);
                Log.AddLog(ch, "Interval =  " + Rcp.HallDEV_Interval + " ms", false);
                Log.AddLog(ch, "sampling =  " + Rcp.HallDEV_sampling + " spl", false);
                Log.AddLog(ch, "TargetX  =  " + Rcp.HallDEV_TargetX + " code", false);
                Log.AddLog(ch, "TargetY  =  " + Rcp.HallDEV_TargetY + " code", false);

                for (int j = 0; j < Rcp.HallDEV_sampling; j++)
                {
                    rbuffer = DrvIC.ReadAnyByte(ch, 0x0B2C, 4);
                    lhx = rbuffer[0] + (rbuffer[1] << 8);   //  X Hall Value    20181011

                    lhy = rbuffer[2] + (rbuffer[3] << 8);   //  Y Hall Value    20181011
                                                            // Log.AddLog(0, "lhx =  " + lhx + "   lhy =  " + lhy, false);
                    sumch1x += lhx;
                    sumch1y += lhy;
                    //  Log.AddLog(0, "sumch1x =  " + sumch1x + "   sumch1y =  " + sumch1y, false);
                    sumch1x_sqr += lhx * lhx;
                    sumch1y_sqr += lhy * lhy;
                    //   Log.AddLog(0, "sumch1x_sqr =  " + sumch1x_sqr + "   sumch1y_sqr =  " + sumch1y_sqr, false);
                    if (hall_ch1_min_x > lhx)
                        hall_ch1_min_x = lhx;
                    if (hall_ch1_max_x < lhx)
                        hall_ch1_max_x = lhx;
                    if (hall_ch1_min_y > lhy)
                        hall_ch1_min_y = lhy;
                    if (hall_ch1_max_y < lhy)
                        hall_ch1_max_y = lhy;
                    // Log.AddLog(0, "hall_ch1_min =  " + hall_ch1_min + "   hall_ch1_max =  " + hall_ch1_max, false);
                    strlineCh1 += lhx + "," + lhy + "\r\n";
                    //  wr.WriteLine(strlineCh1);
                    Thread.Sleep((int)Rcp.HallDEV_Interval);
                }
                avg_ch1_hall_x = sumch1x / Rcp.HallDEV_sampling;
                avg_ch1_hall_y = sumch1y / Rcp.HallDEV_sampling;
                Log.AddLog(ch, "Hall Average x= " + avg_ch1_hall_x, false);
                Log.AddLog(ch, "Hall Average y= " + avg_ch1_hall_y, false);
                Log.AddLog(ch, "Hall min x = " + hall_ch1_min_x, false);
                Log.AddLog(ch, "Hall max x = " + hall_ch1_max_x, false);
                Log.AddLog(ch, "Hall min y = " + hall_ch1_min_y, false);
                Log.AddLog(ch, "Hall max y = " + hall_ch1_max_y, false);
                avg_ch1_hall_sqr_x = sumch1x_sqr / Rcp.HallDEV_sampling;
                avg_ch1_hall_sqr_y = sumch1y_sqr / Rcp.HallDEV_sampling;
                Halldeviation_ch1x = Math.Sqrt(Math.Abs(avg_ch1_hall_sqr_x - avg_ch1_hall_x * avg_ch1_hall_x));
                Halldeviation_ch1y = Math.Sqrt(Math.Abs(avg_ch1_hall_sqr_y - avg_ch1_hall_y * avg_ch1_hall_y));
                Log.AddLog(ch, "Hall DEV x= " + Halldeviation_ch1x, false);
                Log.AddLog(ch, "Hall DEV y= " + Halldeviation_ch1y, false);

                Spec.PassFails[ch].Results[(int)SpecItem.OISX_HALLDEV].Val = Halldeviation_ch1x;
                Spec.PassFails[ch].Results[(int)SpecItem.OISY_HALLDEV].Val = Halldeviation_ch1y;

                ShowEvent.Show(ch, "Hall Dev");

                if (Option.m_bSaveRawData)
                {
                    wr.WriteLine(strlineCh1);
                }

                if (Option.m_bSaveRawData)
                {
                    wr.Close();
                }

                wbuf2[0] = (512 % 256);
                wbuf2[1] = (512 >> 8);
                DrvIC.WriteAnyByte(ch, 0x0854, wbuf2);  // x,y 동시 구동
                wbuf2[0] = (512 % 256);
                wbuf2[1] = (512 >> 8);
                DrvIC.WriteAnyByte(ch, 0x0856, wbuf2);  // x,y 동시 구동
                Log.AddLog(ch, "GainFactor X = " + 512, false);
                Log.AddLog(ch, "GainFactor Y = " + 512, false);
                Log.AddLog(ch, "Hall Deviation Test Finish", true);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void OIS_Hall_Test(int ch, string testItem)
        {
            try
            {
                int tmplPassFail = 0;
                if (Spec.PassFails[ch].FirstFailIndex == 0)
                    tmplPassFail = 2;
                else
                    tmplPassFail = 9;

                if (IsHallTestFail[ch])
                    tmplPassFail = (tmplPassFail & 0x0F) + 0x90;
                else if (IsHallTestDone[ch])
                    tmplPassFail = (tmplPassFail & 0x0F) + 0x20;

                if (Option.m_bWriteResultToDriverIC)
                    WriteResultToDriverIC(ch, tmplPassFail);

                if (ChannelOn[ch])
                {
                    IsHallTestFinishState[ch] = false;
                    IsHallTestResult[ch] = true;
                    //OIS_HallTest(ch, Rcp.circleThr, Rcp.circleErrCnt, Rcp.circleFreq, Rcp.circleAmp, Rcp.circleCycle, Rcp.oscEnable, Rcp.oscThr, Rcp.oscErrTime, Rcp.oscPos, Rcp.saveFlag);
                }
                else
                    IsHallTestFinishState[ch] = true;

                Thread.Sleep(100);

                if (ChannelOn[ch])
                {
                    if (!IsHallTestResult[ch])
                    {
                        Spec.PassFails[ch].Results[(int)SpecItem.OIS_HallTest].Val = 0;
                    }
                    else
                    {
                        Spec.PassFails[ch].Results[(int)SpecItem.OIS_HallTest].Val = 1;

                    }
                    ShowEvent.Show(ch, "Hall");
                }
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Test_Circuit_Open_Short(int ch, string testItem)
        {
            try
            {
                DrvIC.Circuit_Open_Short(ch);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Hall_Stdev_XY(int ch, string testItem)
        {
            try
            {
                DrvIC.Hall_Stdev(ch, 0);
                DrvIC.Hall_Stdev(ch, 1);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Hall_Decenter_XY(int ch, string testItem)
        {
            try
            {
                DrvIC.Hall_Decenter(ch, 0);
                DrvIC.Hall_Decenter(ch, 1);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }

        public bool WriteResultToDriverIC(int ch, int result)
        {
            //int histIndex = sCIndex[ch];

            if (!Option.m_bWriteResultToDriverIC) return true;
            byte[] rbuffer = new byte[1];
            byte[] wBuffer128 = new byte[128];
            byte[] wBuffer128_confirm = new byte[128];

            rbuffer = DrvIC.ReadAnyByte(ch, 0x0850, 1);
            wBuffer128 = DrvIC.ReadAnyByte(ch, 0xBE00, 128);    //  190531  0xDF00 -> 0xBF00 으로 변경됨.

            for (int i = 0; i < 42; i++)
                wBuffer128[i] = 0x00;
            for (int i = 46; i < 64; i++)
                wBuffer128[i] = 0x00;

            int l_TesterNumber = Convert.ToInt16(Model.TesterNo);
            if (l_TesterNumber > 0XFF)
            {
                wBuffer128[0] = (byte)(l_TesterNumber % 256);  //  검사장비 번호 Low
                wBuffer128[1] = (byte)(l_TesterNumber / 256);  //  검사장비 번호 High
            }
            else
            {
                wBuffer128[0] = (byte)(l_TesterNumber % 256);  //  검사장비 번호 Low
                wBuffer128[1] = 0x00;                          //  검사장비 번호 High
            }

            DateTime dtNow = DateTime.Now;
            byte lcoorperate = 0x01;
            byte lday = (byte)(dtNow.Day);
            byte lmonth = (byte)(dtNow.Month);
            byte lyear = (byte)(dtNow.Year - 2020);
            lyear = (byte)((lyear & 0x3) << 1);

            wBuffer128[2] = (byte)result; //  0 : before test, 2 : Good, 9: Fail
            wBuffer128[3] = rbuffer[0];
            wBuffer128[4] = (byte)(lXNonEPA_H_min % 256);
            wBuffer128[5] = (byte)(lXNonEPA_H_min / 256);
            wBuffer128[6] = (byte)(lXNonEPA_H_max % 256);
            wBuffer128[7] = (byte)(lXNonEPA_H_max / 256);
            wBuffer128[8] = (byte)(lYNonEPA_H_min % 256);
            wBuffer128[9] = (byte)(lYNonEPA_H_min / 256);
            wBuffer128[10] = (byte)(lYNonEPA_H_max % 256);
            wBuffer128[11] = (byte)(lYNonEPA_H_max / 256);
            wBuffer128[12] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISX_Ratedstroke].Val * 20, 255);   //  X Stroke / 2
            wBuffer128[13] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISX_Forwardstroke].Val * 20, 255);   //  X Stroke / 2
            wBuffer128[14] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISX_Backwardstroke].Val * 20, 255);   //  X Stroke / 2
            wBuffer128[15] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISY_Ratedstroke].Val * 20, 255);   //  Y Stroke / 2
            wBuffer128[16] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISY_Forwardstroke].Val * 20, 255);   //  Y Stroke / 2
            wBuffer128[17] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISY_Backwardstroke].Val * 20, 255);   //  Y Stroke / 2
            wBuffer128[18] = (byte)HallParam[ch][0].Bias;   //  X Hall bias
            wBuffer128[19] = (byte)HallParam[ch][1].Bias;   //  Y Hall bias
            wBuffer128[20] = (byte)HallParam[ch][0].Offset;   //  X Hall offset
            wBuffer128[21] = (byte)HallParam[ch][1].Offset;   //  Y Hall offset
            wBuffer128[22] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISX_Hysteresis].Val * 500, 255);   //  X Hysteresis
            wBuffer128[23] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISY_Hysteresis].Val * 500, 255);   //  X Hysteresis
            wBuffer128[24] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISX_Linearity].Val * 500, 255);   //  Y Lineraity
            wBuffer128[25] = (byte)Math.Min(Spec.PassFails[ch].Results[(int)SpecItem.OISY_Linearity].Val * 500, 255);   //  Y Lineraity

            int tmpint = (int)Math.Round(Spec.PassFails[ch].Results[(int)SpecItem.OISX_CenterCurrent].Val);
            wBuffer128[26] = (byte)tmpint;   //  X Phase Margin
            tmpint = (int)Math.Round(Spec.PassFails[ch].Results[(int)SpecItem.OISY_CenterCurrent].Val);
            wBuffer128[27] = (byte)tmpint;   //  Y Phase Margin
            wBuffer128[28] = (byte)Spec.PassFails[ch].Results[(int)SpecItem.FRAX_PhaseMargin].Val;   //  X Phase Margin
            wBuffer128[29] = (byte)Spec.PassFails[ch].Results[(int)SpecItem.FRAY_PhaseMargin].Val;   //  Y Phase Margin
            wBuffer128[30] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.FRAX_PMFreq].Val / 4);
            wBuffer128[31] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.FRAY_PMFreq].Val / 4);
            wBuffer128[32] = (byte)Spec.PassFails[ch].Results[(int)SpecItem.FRAX_Gain10Hz].Val;
            wBuffer128[33] = (byte)Spec.PassFails[ch].Results[(int)SpecItem.FRAY_Gain10Hz].Val;
            wBuffer128[34] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISX_MidTiltTest].Val * 256 / 5);
            wBuffer128[35] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISY_MidTiltTest].Val * 256 / 5);
            wBuffer128[36] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISX_CodeMid2MechaMid].Val * 500);
            wBuffer128[37] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISY_CodeMid2MechaMid].Val * 500);
            wBuffer128[38] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISX_SettlingTime].Val * 10);
            wBuffer128[39] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISY_SettlingTime].Val * 10);
            wBuffer128[40] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISX_Sensitivity].Val * 500);
            wBuffer128[41] = (byte)(Spec.PassFails[ch].Results[(int)SpecItem.OISY_Sensitivity].Val * 500);

            double tmpdouble = Spec.PassFails[ch].Results[(int)SpecItem.OISX_Crosstalk].Val;
            double tmpdouble2 = Spec.PassFails[ch].Results[(int)SpecItem.OISY_Crosstalk].Val;

            int dXCrossTalk = (int)(tmpdouble * 1000);
            int dYCrossTalk = (int)(tmpdouble2 * 1000);
            if (dXCrossTalk > 65535)
            {
                dXCrossTalk = 65535;
            }
            if (dYCrossTalk > 65535)
            {
                dYCrossTalk = 65535;
            }

            wBuffer128[46] = (byte)(dYCrossTalk % 256);
            wBuffer128[47] = (byte)(dYCrossTalk >> 8);
            wBuffer128[48] = (byte)(dXCrossTalk % 256);
            wBuffer128[49] = (byte)(dXCrossTalk >> 8);
            wBuffer128[50] = 0x00;//(byte)(Croostalk Flag);
            tmpint = (int)Math.Round(Spec.PassFails[ch].Results[(int)SpecItem.OISX_MaxCurrent].Val);
            wBuffer128[51] = (byte)tmpint;   //  X Phase Margin
            tmpint = (int)Math.Round(Spec.PassFails[ch].Results[(int)SpecItem.OISY_MaxCurrent].Val);
            wBuffer128[52] = (byte)tmpint;
            tmpint = (int)((Spec.PassFails[ch].Results[(int)SpecItem.OISX_HALLDEV].Val) * 10);
            wBuffer128[53] = (byte)tmpint;   //  X Phase Margin
            tmpint = (int)((Spec.PassFails[ch].Results[(int)SpecItem.OISY_HALLDEV].Val) * 10);
            wBuffer128[54] = (byte)tmpint;

            wBuffer128[59] = (byte)((byte)(((byte)lcoorperate << 3) | lyear) | (lmonth >> 3));     //  7~3bit : 업체명,  2~1bit : 년, 0 bit : 4bit 월의 상위 0bit
            wBuffer128[60] = (byte)((byte)(lmonth << 5) | (lday & 0x1F));                              //  7~5bit : 4bit 월의 하위3bit, 4 ~ 0bit : 일
            if (Model.Supplier == "Optrontech")
                wBuffer128[61] = 1/* 프리즘 업체 */;
            else
                wBuffer128[61] = 2/* 프리즘 업체 */;
            wBuffer128[62] = 0x33; //OIS Class

            int lCheckSum = 0;
            for (int i = 0; i < 63; i++)
            {
                lCheckSum += wBuffer128[i];
            }

            wBuffer128[63] = (byte)(lCheckSum % 255 + 1);
            lCheckSum = wBuffer128[63];

            DrvIC.OIS_On(ch, false);
            byte bres = 0;
            bres = DrvIC.CheckIfOISSTS_IDLE(ch);
            if (bres > 0)
            {
                ErrMsg[ch] = "STS_IDLE";
                ChannelOn[ch] = false;
                Log.AddLog(ch, "OISSTS not IDLE", false);
                return false;
            }
            DrvIC.WriteAnyByte(ch, 0xBE00, wBuffer128);
            Thread.Sleep(100);
            wBuffer128_confirm = DrvIC.ReadAnyByte(ch, 0xBE00, 128);
            string lmsg = "";

            Log.AddLog(ch, StrIndex[ch] + ">> WriteResultToDriverIC", false);
            for (int i = 0; i < 128; i++)
            {
                if (i < 64)
                    Log.AddLog(ch, string.Format("Addr : 0xBE{0:X2}, Write {1}, Read {2}",i, wBuffer128[i], wBuffer128_confirm[i] ));


                if (wBuffer128[i] != wBuffer128_confirm[i])
                {
                    lmsg = wBuffer128[i].ToString("X2") + ">" + wBuffer128_confirm[i].ToString("X2") + "\r\n";
                    Log.AddLog(ch, i.ToString() + "th in 128 byte Fail : " + lmsg, false);
                }
            }

            if (wBuffer128[63] != (byte)lCheckSum)
            {

                DrvIC.WriteAnyByte(ch, 0xBE00, wBuffer128);
                Thread.Sleep(50);
                wBuffer128_confirm = DrvIC.ReadAnyByte(ch, 0xBE00, 128);
                for (int i = 0; i < 128; i++)
                {
                    if (wBuffer128[i] != wBuffer128_confirm[i])
                    {
                        for (int j = 0; j < 128; j++)
                        {
                            wBuffer128[j] = 0xFA;
                        }
                        DrvIC.WriteAnyByte(ch, 0xBE00, wBuffer128);
                        Log.AddLog(ch, "Fail in Writing Result to Driver IC", false);
                        ErrMsg[ch] = "WRITE_DRIVERIC";
                        ChannelOn[ch] = false;

                        return false;
                    }
                }
            }

            Log.AddLog(ch, "Success WriteResultToDriverIC", false);
            return true;
        }
        public void XY_Hall_Mid_Tilt_Test(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                //HallMidTiltTest(port);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void Linearity_Compensation(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                for (int j = ch; j < ch + 2; j++)
                {
                    if (!ChannelOn[j]) continue;
                    Log.AddLog(j, string.Format("Linearity Cal Init = Mem : 0x{0:X4}, WData : 0x{1:X4}", 0x0860, 0x01));
                    DrvIC.WriteByte(j, 0x0860, 0x01);
                    Log.AddLog(j, string.Format("Init Check = Mem : 0x{0:X4}, RData : 0x{1:X4}", 0x0860, DrvIC.ReadByte(j, 0x0860)));

                    short target = (short)((DrvIC.Read2Byte(j, 0x0B18) + DrvIC.Read2Byte(j, 0x0B1A)) / 2);
                    Log.AddLog(j, string.Format("Move X Pos : {0}, X Hall : {1}", target, DrvIC.ReadHall(j, 0)));
                    DrvIC.Move(j, 0, target);
                    target = DrvIC.Read2Byte(j, 0x0B16);
                    Log.AddLog(j, string.Format("Move Y Pos : {0}, Y Hall : {1}", target, DrvIC.ReadHall(j, 2)));
                    DrvIC.Move(j, 1, target);
                }
                OIS_LinearityCal(port, 0);
                OIS_LinearityCal(port, 1);

                for (int j = ch; j < ch + 2; j++)
                {
                    if (!ChannelOn[j]) continue;
                    Log.AddLog(j, string.Format("Linearity Cal Write = Mem : 0x{0:X4}, WData : 0x{1:X4}", 0x0860, 0x10));
                    DrvIC.WriteByte(j, 0x0860, 0x10);
                    Log.AddLog(j, string.Format("NVM Write = Mem : 0x{0:X4}, WData : 0x{1:X4}", 0x0306, 0x2000));
                    DrvIC.Write2Byte(j, 0x0306, 0x2000);
                }
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public class LinCompVal
        {
            public ushort[] Target = new ushort[255];
            public double[] HallScale = new double[255];
            public double[] Stroke = new double[255];
            public double[] StrokeNomalized = new double[255];
            public double[] HallNomalized = new double[255];
            public double[] error = new double[255];
            public double[] LinComp_dCoeff = new double[4];
            public int[] LinComp_Coeff = new int[4];
            public byte samplingCnt = 0;
            public double R2 = 0;
        }
        public class LDVHallScale_t
        {
            public ushort HallMin = 0;
            public ushort HallMax = 0;
            public double LDVMin = 0;
            public double LDVMax = 0;
            public float scaleGain = 0;
        }
        public class LinCalc
        {
            public const int MATRIXMAX = 255;
            public const int COL = 4;
            public const int CC = COL * COL;
            public double[] a = new double[MATRIXMAX];
            public double[] aT = new double[MATRIXMAX];
            public double[] aTa = new double[CC];
            public double[] aTai = new double[CC];
            public double[] aTaiaT = new double[MATRIXMAX];
            public void swap(double a, double b)
            {
                double temp = 0;
                temp = a;
                a = b;
                b = temp;
            }
            /* Matrix Functions */
            public void mtrans(double[] a, double[] b, int r, int c)
            {
                int i, j;

                for (i = 0; i < c; i++)
                {
                    for (j = 0; j < r; j++)
                    {
                        b[i * r + j] = a[j * c + i];
                    }
                }
            }
            public void mmult(double[] x, double[] y, ref double[] z, int l, int m, int n)
            {
                int i, j, k;

                for (i = 0; i < l; i++)
                {
                    for (j = 0; j < n; j++)
                    {
                        z[i * n + j] = 0;
                        for (k = 0; k < m; k++)
                        {
                            z[i * n + j] += x[i * m + k] * y[k * n + j];
                        }
                    }
                }
            }
            public int mminv(double[] x, double[] y, int n)
            {
                int i, j, k;
                double[] z = new double[1024];

                for (i = 0; i < n; i++)
                {
                    for (j = 0; j < n; j++) z[i * 2 * n + j] = x[i * n + j];
                    z[i * 2 * n + n + i] = 1;
                }
                for (i = 0; i < n; i++)
                {
                    int pivot = i;
                    while (Math.Abs(x[pivot * n + i]) < 0.00001)
                    {
                        pivot++;
                        if (pivot >= n) return -1;
                    }
                    for (j = 0; j < 2 * n; j++) swap(z[i * 2 * n + j], z[pivot * 2 * n + j]);

                    double pval = z[i * 2 * n + i];
                    for (j = 0; j < 2 * n; j++) z[i * 2 * n + j] = z[i * 2 * n + j] / pval;
                    for (j = 0; j < n; j++)
                    {
                        if (j == i) continue;
                        double p2val = z[j * 2 * n + i];
                        for (k = 0; k < 2 * n; k++) z[j * 2 * n + k] -= z[i * 2 * n + k] * p2val;
                    }
                }
                for (i = 0; i < n; i++) for (j = 0; j < n; j++) y[i * n + j] = z[i * 2 * n + n + j];

                return 0;
            }
            public void RunLinearityCal(ref LinCompVal LinearityCal)
            {
                int i;

                for (i = 0; i < LinearityCal.samplingCnt; i++)
                {
                    a[COL * i + 0] = 1;
                    a[COL * i + 1] = LinearityCal.HallNomalized[i];
                    a[COL * i + 2] = a[COL * i + 1] * a[COL * i + 1];
                    a[COL * i + 3] = a[COL * i + 1] * a[COL * i + 1] * a[COL * i + 1];
                }

                mtrans(a, aT, LinearityCal.samplingCnt, COL);
                mmult(aT, a, ref aTa, COL, LinearityCal.samplingCnt, COL);
                mminv(aTa, aTai, COL);
                mmult(aTai, aT, ref aTaiaT, COL, COL, LinearityCal.samplingCnt);
                mmult(aTaiaT, LinearityCal.error, ref LinearityCal.LinComp_dCoeff, COL, LinearityCal.samplingCnt, 1);

                // 결정계수 R2 계산 //
                double sstot = 0;
                double ssres = 0;
                double[] est = new double[255];
                double avg = 0;
                //double R2 = 0;

                mmult(a, LinearityCal.LinComp_dCoeff, ref est, LinearityCal.samplingCnt, COL, 1);
                for (i = 0; i < LinearityCal.samplingCnt; i++)
                    avg += LinearityCal.error[i];
                avg = avg / LinearityCal.samplingCnt;
                for (i = 0; i < LinearityCal.samplingCnt; i++)
                {
                    sstot += (avg - LinearityCal.error[i]) * (avg - LinearityCal.error[i]);
                    ssres += (LinearityCal.error[i] - est[i]) * (LinearityCal.error[i] - est[i]);
                }
                LinearityCal.R2 = 1 - ssres / sstot;
            }

        }
        public void OIS_LinearityCal(int port, int axis = 0)
        {

            int ch = port * 2;

            OIS_StartMode(ch, axis);

            int MoveStep = (Rcp.LinEnd - Rcp.LinStart) / Rcp.LinSamplingSize;

            LinCompVal[] oisLinearityCal = new LinCompVal[2];
            LDVHallScale_t[] oisLDVScale = new LDVHallScale_t[2];
            LinCalc[] LinCal = new LinCalc[2];

            for (int j = ch; j < ch + 2; j++)
            {
                oisLinearityCal[j] = new LinCompVal();
                oisLDVScale[j] = new LDVHallScale_t();
                LinCal[j] = new LinCalc();

                if (axis == 0)
                {
                    Log.AddLog(j, "== X LinearityCal ==");
                }
                else if (axis == 1)
                {
                    Log.AddLog(j, "== Y LinearityCal ==");
                }
            }

            LEDOn(port);
            Thread.Sleep(100);
            //Moving And Grab ============================================================
            int samplingCnt = Rcp.LinSamplingSize + 1;
            for (int j = ch; j < ch + 2; j++)
            {
                if (ChannelOn[j])
                {
                    oisLinearityCal[j].samplingCnt = (byte)(samplingCnt);
                    Log.AddLog(j, "StartCode = " + Rcp.LinStart, false);
                    Log.AddLog(j, "EndCode = " + Rcp.LinEnd, false);
                    Log.AddLog(j, "SamplingCnt = " + samplingCnt, false);
                    Log.AddLog(j, "MoveStep = " + MoveStep, false);
                    Log.AddLog(j, "MoveDelay = " + Rcp.LinTargetDelay + "\r\n", false);

                    for (int i = 0; i < oisLinearityCal[j].samplingCnt; i++)
                    {
                        oisLinearityCal[j].Target[i] = (ushort)(i * MoveStep + Rcp.LinStart);
                        if (oisLinearityCal[j].Target[i] > Rcp.LinEnd)
                            oisLinearityCal[j].Target[i] = (ushort)Rcp.LinEnd;
                    }
                }
            }

            double[] InitStrokeX = new double[2];

            for (int i = 0; i < samplingCnt; i++)
            {
                for (int j = ch; j < ch + 2; j++)
                    DrvIC.Move(j, axis, (short)oisLinearityCal[j].Target[i]);
                Thread.Sleep(300);
                for (int j = ch; j < ch + 2; j++)
                {
                    if (axis == 0)
                    {
                        oisLinearityCal[j].HallScale[i] = (ushort)DrvIC.ReadHall(j, 1);
                    }
                    else if (axis == 1)
                    {
                        oisLinearityCal[j].HallScale[i] = (ushort)DrvIC.ReadHall(j, 2);
                    }
                }

                oCam[0].GrabWithTime(i);
                Thread.Sleep(300);

                double[] cx = new double[4];
                double[] cy = new double[4];
                double[] rescx = new double[2];
                double[] rescy = new double[2];
                double[] resTheta = new double[2];
                SetSearchYRegion();
                MeasureXYT(i, -1, ref cx, ref cy, ref rescx, ref rescy, ref resTheta);
                Thread.Sleep(300);

                for (int j = ch; j < ch + 2; j++)
                {
                    if (axis == 0)
                    {
                        oisLinearityCal[j].Stroke[i] = rescy[j];
                    }
                    else if (axis == 1)
                    {
                        oisLinearityCal[j].Stroke[i] = rescx[j];
                    }

                    //if (i == 0) oisLinearityCal[j].Stroke[i] = 0;
                    if (i == 0) InitStrokeX[j] = oisLinearityCal[j].Stroke[i];

                    if (axis == 0)
                    {
                        //if (i > 0)
                        oisLinearityCal[j].Stroke[i] = oisLinearityCal[j].Stroke[i] - InitStrokeX[j];
                    }
                    else if (axis == 1)
                    {
                        //if (i > 0)
                        oisLinearityCal[j].Stroke[i] = InitStrokeX[j] - oisLinearityCal[j].Stroke[i];
                    }
                    Log.AddLog(j, "Target = " + oisLinearityCal[j].Target[i] + "\t" + "HallScale = " + oisLinearityCal[j].HallScale[i] + "\t" + "Stroke = " + oisLinearityCal[j].Stroke[i].ToString("F3"), false);
                }
            }
            //==================================================================
            Thread.Sleep(10);

            for (int j = ch; j < ch + 2; j++)
            {
                if (!ChannelOn[j]) continue;
                oisLDVScale[j].HallMin = (ushort)oisLinearityCal[j].HallScale[0];
                oisLDVScale[j].HallMax = (ushort)oisLinearityCal[j].HallScale[samplingCnt - 1];
                oisLDVScale[j].LDVMin = oisLinearityCal[j].Stroke[0];
                oisLDVScale[j].LDVMax = oisLinearityCal[j].Stroke[samplingCnt - 1];
                oisLDVScale[j].scaleGain = -((oisLDVScale[j].HallMax - oisLDVScale[j].HallMin)) / (float)(oisLDVScale[j].LDVMax - oisLDVScale[j].LDVMin);

                for (int i = 0; i < samplingCnt; i++)
                {
                    /* 변위를 Target(Hall) Range로 Scaling */
                    oisLinearityCal[j].Stroke[i] = -(((oisLinearityCal[j].Stroke[i] - oisLDVScale[j].LDVMin) * oisLDVScale[j].scaleGain) - oisLDVScale[j].HallMin);

                    /* Error 계산 */
                    oisLinearityCal[j].error[i] = oisLinearityCal[j].Stroke[i] - oisLinearityCal[j].HallScale[i];

                    /* 변위, Target(Hall) Normalize (for Q13) */
                    oisLinearityCal[j].StrokeNomalized[i] = oisLinearityCal[j].Stroke[i] / 8192;
                    oisLinearityCal[j].HallNomalized[i] = oisLinearityCal[j].HallScale[i] / 8192;

                    string tmpstr = oisLinearityCal[j].Stroke[i].ToString("F3") + " " + "\t" + oisLinearityCal[j].StrokeNomalized[i].ToString("F3") + "\t" + oisLinearityCal[j].HallNomalized[i].ToString("F3") + "\t" + oisLinearityCal[j].error[i].ToString("F3");
                    Log.AddLog(j, tmpstr);
                }

                LinCal[j].RunLinearityCal(ref oisLinearityCal[j]);
            }

            for (int j = ch; j < ch + 2; j++)
            {
                if (!ChannelOn[j]) continue;

                Log.AddLog(j, "\r\n" + "R2 = " + oisLinearityCal[j].R2.ToString("F7") + "\r\n", false);

                oisLinearityCal[j].LinComp_Coeff[0] = (int)(oisLinearityCal[j].LinComp_dCoeff[0]);
                oisLinearityCal[j].LinComp_Coeff[1] = (int)(oisLinearityCal[j].LinComp_dCoeff[1]);
                oisLinearityCal[j].LinComp_Coeff[2] = (int)(oisLinearityCal[j].LinComp_dCoeff[2]);
                oisLinearityCal[j].LinComp_Coeff[3] = (int)(oisLinearityCal[j].LinComp_dCoeff[3]);
                string tmpstr = oisLinearityCal[j].LinComp_dCoeff[0].ToString("F3") + "\t" + oisLinearityCal[j].LinComp_dCoeff[1].ToString("F3") + "\t" + oisLinearityCal[j].LinComp_dCoeff[2].ToString("F3") + "\t" + oisLinearityCal[j].LinComp_dCoeff[3].ToString("F3");
                Log.AddLog(j, tmpstr, false);
                tmpstr = oisLinearityCal[j].LinComp_Coeff[0].ToString("F3") + "\t" + oisLinearityCal[j].LinComp_Coeff[1].ToString("F3") + "\t" + oisLinearityCal[j].LinComp_Coeff[2].ToString("F3") + "\t" + oisLinearityCal[j].LinComp_Coeff[3].ToString("F3");
                Log.AddLog(j, tmpstr, false);

                if (axis == 0)
                {
                    //X1
                    DrvIC.Write2Byte(j, 0x0880, (short)oisLinearityCal[j].LinComp_Coeff[0]);
                    DrvIC.Write2Byte(j, 0x0884, (short)oisLinearityCal[j].LinComp_Coeff[1]);
                    DrvIC.Write2Byte(j, 0x0890, (short)oisLinearityCal[j].LinComp_Coeff[2]);
                    DrvIC.Write2Byte(j, 0x089A, (short)oisLinearityCal[j].LinComp_Coeff[3]);

                    //X2
                    DrvIC.Write2Byte(j, 0x089C, (short)oisLinearityCal[j].LinComp_Coeff[0]);
                    DrvIC.Write2Byte(j, 0x08A2, (short)oisLinearityCal[j].LinComp_Coeff[1]);
                    DrvIC.Write2Byte(j, 0x08AE, (short)oisLinearityCal[j].LinComp_Coeff[2]);
                    DrvIC.Write2Byte(j, 0x0886, (short)oisLinearityCal[j].LinComp_Coeff[3]);

                    Log.AddLog(j, string.Format("Raed X1 = C0 : {0:X4}, C1 : {1:X4}, C2 : {2:X4},C3 : {3:X4},",
                        DrvIC.Read2Byte(j, 0x0880), DrvIC.Read2Byte(j, 0x0884),
                    DrvIC.Read2Byte(j, 0x0890), DrvIC.Read2Byte(j, 0x089A)));

                    Log.AddLog(j, string.Format("Raed X2 = C0 : {0:X4}, C1 : {1:X4}, C2 : {2:X4},C3 : {3:X4},",
                        DrvIC.Read2Byte(j, 0x089C), DrvIC.Read2Byte(j, 0x08A2),
                        DrvIC.Read2Byte(j, 0x08AE), DrvIC.Read2Byte(j, 0x08B6)));
                }
                else if (axis == 1)
                {
                    DrvIC.Write2Byte(j, 0x0864, (short)oisLinearityCal[j].LinComp_Coeff[0]);
                    DrvIC.Write2Byte(j, 0x0866, (short)oisLinearityCal[j].LinComp_Coeff[1]);
                    DrvIC.Write2Byte(j, 0x0872, (short)oisLinearityCal[j].LinComp_Coeff[2]);
                    DrvIC.Write2Byte(j, 0x087E, (short)oisLinearityCal[j].LinComp_Coeff[3]);

                    Log.AddLog(j, string.Format("Raed Y = C0 : {0:X4}, C1 : {1:X4}, C2 : {2:X4},C3 : {3:X4},",
                        DrvIC.Read2Byte(j, 0x0864), DrvIC.Read2Byte(j, 0x0866),
                        DrvIC.Read2Byte(j, 0x0872), DrvIC.Read2Byte(j, 0x087E)));

                }
            }
            LEDOn(port, false);
        }
        public void XY_Displacement_Accuracy(int port, string testItem)
        {
            int ch = port * 2;
            try
            {
                OISDisplacementAccuracy(port);
            }
            catch (Exception e)
            {
                Log.AddLog(ch, testItem + " Exception : " + e.ToString() + " ch : " + ch.ToString());
                ErrMsg[ch] = testItem + " Error";
                ChannelOn[ch] = false;
            }
        }
        public void OISDisplacementAccuracy(int port)
        {
            int ch = port * 2;

            string strlineCh1 = "";
            string strlineCh2 = "";

            byte[] wbuffer2 = new byte[2];
            double[] dTx = new double[10];
            double[] dTy = new double[10];
            double[] cx = new double[10];
            double[] cy = new double[10];
            double[] tx = new double[4];
            double[] ty = new double[4];
            double[] resTheta = new double[2];
            double[] rescx = new double[2];
            double[] rescy = new double[2];

            StreamWriter wr = null;
            StreamWriter wr1 = null;

            string newDir = FIO.CheckResultFolder();
            newDir = newDir + "DisplacementAccuracy\\";

            if (!Directory.Exists(newDir))
            {
                Directory.CreateDirectory(newDir);
            }

            string sFileNameCh1 = newDir + "\\DisplacementAccuracy_" + StrIndex[ch].ToString() + ".csv";
            string sFileNameCh2 = newDir + "\\DisplacementAccuracy_" + StrIndex[ch + 1].ToString() + ".csv";

            Log.AddLog(ch, "OIS Displacement Accuracy Test Start", false);
            Log.AddLog(ch + 1, "OIS Displacement Accuracy Test Start", false);

            Log.AddLog(ch, "X Target: " + Rcp.iXDisplacement + "\t" + "Y Target: " + Rcp.iYDisplacement, false);
            Log.AddLog(ch + 1, "X Target: " + Rcp.iXDisplacement + "\t" + "Y Target: " + Rcp.iYDisplacement, false);


            LEDOn(port);

            for (int i = ch; i < ch + 2; i++)
            {
                OIS_StartMode(i, 0);
                OIS_StartMode(i, 1);
            }

            Thread.Sleep(50);
            wbuffer2[0] = (byte)(Rcp.iXDisplacement % 256);
            wbuffer2[1] = (byte)(Rcp.iXDisplacement >> 8);

            if (ChannelOn[ch])
                DrvIC.WriteAnyByte(ch, 0x0720, wbuffer2);

            if (ChannelOn[ch + 1])
                DrvIC.WriteAnyByte(ch + 1, 0x0720, wbuffer2);
            Thread.Sleep(100);
            wbuffer2[0] = (byte)(Rcp.iYDisplacement % 256);
            wbuffer2[1] = (byte)(Rcp.iYDisplacement >> 8);

            if (ChannelOn[ch])
                DrvIC.WriteAnyByte(ch, 0x0722, wbuffer2);

            if (ChannelOn[ch + 1])
                DrvIC.WriteAnyByte(ch + 1, 0x0722, wbuffer2);

            Thread.Sleep(100);


            if (ChannelOn[ch])
            {
                if (Option.m_bSaveRawData)
                {
                    wr = new StreamWriter(sFileNameCh1);
                    wr.WriteLine("Count" + "," + "X_Angle" + "," + "Y_Angle" + "," + "Time");
                }
            }
            if (ChannelOn[ch + 1])
            {
                if (Option.m_bSaveRawData)
                {
                    wr1 = new StreamWriter(sFileNameCh2);
                    wr1.WriteLine("Count" + "," + "X_Angle" + "," + "Y_Angle" + "," + "Time");
                }
            }


            Stopwatch sw;
            sw = new Stopwatch();
            sw.Start();
            int count = 1;

            while (sw.ElapsedMilliseconds / 1000f < Rcp.TimeDisplacement)
            {

                oCam[port].GrabA();
                Thread.Sleep(50);

                //long nFound = 0;
                ////oCam[port].FineCOG(0, ref dTx, ref dTy, ref cx, ref cy, ref nFound, false);    // btnMeasureTXTY_Click()
                //oCam[port].ConvertXYtheta(cx, cy, dTx, ref rescx, ref rescy, ref resTheta);
                //oCam[port].ForceCPXY(0, ref cx, ref cy);
                //double tmph = 0;
                //oCam[port].CalcXYZT_L(0, ref tmph, ref tx[0], ref ty[0], Option.m_bXDirReverse, Option.m_bYDirReverse);
                //oCam[port].CalcXYZT_R(0, ref tmph, ref tx[1], ref ty[1], Option.m_bXDirReverse, Option.m_bYDirReverse);

                if (ChannelOn[ch])
                {
                    Log.AddLog(ch, "Count : " + count, false);
                    Log.AddLog(ch, "X Angle(deg): " + tx[0].ToString("F3"), false);
                    Log.AddLog(ch, "Y Angle(deg): " + ty[0].ToString("F3"), false);

                    strlineCh1 += count + "," + tx[0].ToString("F3") + "," + ty[0].ToString("F3") + "," + sw.ElapsedMilliseconds / 1000f + "\r\n";

                }

                if (ChannelOn[ch + 1])
                {
                    Log.AddLog(ch + 1, "Count : " + count, false);
                    Log.AddLog(ch + 1, "X Angle(deg): " + tx[1].ToString("F3"), false);
                    Log.AddLog(ch + 1, "Y Angle(deg): " + ty[1].ToString("F3"), false);

                    strlineCh2 += count + "," + tx[1].ToString("F3") + "," + ty[1].ToString("F3") + "," + sw.ElapsedMilliseconds / 1000f + "\r\n";

                }
                count++;
            }

            sw.Stop();

            LEDOn(port, false);
            Log.AddLog(ch, "OIS Displacement Accuracy Test Done!!", false);
            Log.AddLog(ch + 1, "OIS Displacement Accuracy Test Done!!", false);


            if (ChannelOn[ch])
            {
                if (Option.m_bSaveRawData)
                {
                    wr.WriteLine(strlineCh1);
                    wr.Close();
                }
            }
            if (ChannelOn[ch + 1])
            {
                if (Option.m_bSaveRawData)
                {
                    wr1.WriteLine(strlineCh2);
                    wr1.Close();
                }
            }
        }
        public void OIS_StartMode(int ch, int axis)
        {
            DrvIC.Move(ch, axis, 0x3E80);
            DrvIC.SetFixMode(ch);
            if (axis == (int)AO_TYPE._CLR)
            {
                //Move Mid Position
                DrvIC.Move(ch, 2, 0);
                Log.AddLog(ch, string.Format("Move Mid R Position : {0}", 0));
            }
            else
            {
                //Move Mid Position
                DrvIC.Move(ch, 0, 16000);
                Log.AddLog(ch, string.Format("Move Mid X Position : {0}", 16000));
                DrvIC.Move(ch, 1, 16000);
                Log.AddLog(ch, string.Format("Move Mid Y Position : {0}", 16000));
            }


            DrvIC.ReadHall(ch, 0);
            DrvIC.ReadHall(ch, 1);
            DrvIC.ReadHall(ch, 2);

            Thread.Sleep(100);
        }
        //private void Process_CLXYStepTest(int port, int axis = 0)
        //{
        //    int ch = port * 2;
        //    if (ChannelOn[ch])
        //    {
        //        if (DriverIC.RetryRead(ch, 0x0001, 0x02, 10) > 0x02)
        //        {
        //            ErrMsg[ch] = "I2C_FAIL";
        //            Log.AddLog(ch, "Step Drv Error", false);
        //            ChannelOn[ch] = false;
        //            //  errMsg[ch] = "Step Drv Error";

        //        }
        //    }
        //    if (ChannelOn[ch + 1])
        //    {
        //        if (DriverIC.RetryRead(ch + 1, 0x0001, 0x02, 10) > 0x02)
        //        {
        //            ErrMsg[ch + 1] = "I2C_FAIL";
        //            Log.AddLog(ch + 1, "Step Drv Error", false);
        //            ChannelOn[ch + 1] = false;
        //            //    errMsg[ch + 1] = "Step Drv Error";

        //        }
        //    }

        //    Task Func1 = null;
        //    Func1 = new Task(() => Process_WaveformCLXYStepOut(port, axis));
        //    Func1.Start();

        //    if (Func1 != null) Task.WaitAll(Func1);
        //}
        //public void Process_WaveformCLXYStepOut(int port, int axis = 0/*X=0, Y=1*/)
        //{
        //    microTimer_AO[port].Enabled = false; // Start timer

        //    Count_AO[port] = 0;
        //    int ch = port * 2;
        //    double sec = 0.5;  // 0.5sec is enough
        //    int frameCount = 0;
        //    if (axis == 0)
        //    {
        //        AOType[port] = (int)AO_TYPE._CLR;
        //        MaxCount_AO[port] = CL_DataCount[2];
        //        oCam[port].CLFrameCount[2] = (int)(FPS[port] * sec); //  저장되는 최대 프레임 수
        //        frameCount = oCam[port].CLFrameCount[2];
        //    }
        //    else
        //    {
        //        AOType[port] = (int)AO_TYPE._CLYStep;
        //        MaxCount_AO[port] = CL_DataCount[3];
        //        oCam[port].CLFrameCount[3] = (int)(FPS[port] * sec); //  저장되는 최대 프레임 수
        //        frameCount = oCam[port].CLFrameCount[3];
        //    }

        //    //  ManualResetEvent Instance 를 Process_VisonGrab 에 넘겨줘야 한다.

        //    //Log.AddLog( ch , frameCount + " frame reserved");
        //    int grabbedcount = 0;
        //    Thread ThreadVisionData = new Thread(() => oCam[port].Process_VisionGrab(2 /* AFRes CLAFRes */, frameCount, ref grabbedcount));
        //    ThreadVisionData.Start();


        //    //microTimer_AO[port].Interval = 100000;  //  100000µs = 100msec, Default Value
        //    microTimer_AO[port].Interval = 51000;  //  100000µs = 100msec, Default Value
        //    microTimer_AO[port].Enabled = true; // Start timer

        //    AnalogOut_DoneEvent[port].WaitOne();

        //    //SupremeTimer.QueryPerformanceCounter(ref m_dEndTime);
        //    //m_dElapsedTime = (m_dEndTime - m_dStartTime) / (double)TimerFrequency;

        //    if (!oCam[port].Process_VisionGrabWait()) return;

        //    if (axis == 0)
        //    {
        //        oCam[port].CLFrameCount[2] = grabbedcount;
        //        Array.Copy(oCam[port].GrabT1, oCam[port].CLXGrabTimeRes, (long)(oCam[port].CLFrameCount[2]));
        //    }
        //    else
        //    {
        //        oCam[port].CLFrameCount[3] = grabbedcount;
        //        Array.Copy(oCam[port].GrabT1, oCam[port].CLYGrabTimeRes, (long)(oCam[port].CLFrameCount[3]));
        //    }

        //    microTimer_AI[port].Interval = 2000;  //  25µs = 40Khz
        //    microTimer_AO[port].Interval = 10000;  //  25µs = 40Khz
        //    //m_bProcess_WaveformCLXYStepOut_OK[port] = true;
        //}
        //public bool Process_CalcCLXYSettling(int port, int axis = 0)
        //{
        //    int framecount = 0;

        //    int i, EffectiveFrameCount;
        //    StreamWriter writer = null;

        //    if (axis == 0)
        //        framecount = (int)oCam[port].CLFrameCount[2];
        //    else
        //        framecount = (int)oCam[port].CLFrameCount[3];

        //    int ch = port * 2;
        //    //int lHindex = sCIndex[ch];

        //    //MessageBox.Show(mPortIdle[port].ToString() + " " + mPortIdle[(port + 1) % 2].ToString());
        //    //oCam[port].m_bAnotherIdle = IsIdle((port + 1) % 2);

        //    string lTestItem = (axis == 0) ? "X Step" : "Y Step";
        //    if (!oCam[port].Process_VisionData(framecount, false, lTestItem, ChannelCount))
        //    {
        //        ErrMsg[ch] = "VISION_MARK_FAIL";
        //        ErrMsg[ch + 1] = "VISION_MARK_FAIL";
        //        ChannelOn[ch] = false;
        //        ChannelOn[ch + 1] = false;

        //        return false;
        //    }

        //    double[] XArray_Avg = new double[2000];
        //    double[] YArray_Avg = new double[2000];
        //    double[] XArray_AvgR = new double[2000];
        //    double[] YArray_AvgR = new double[2000];

        //    double[] spltime = new double[2000];
        //    double[] spltime1 = new double[2000];

        //    double _Pi = Math.Asin(1) * 2;

        //    EffectiveFrameCount = 0;

        //    //int i0 = 0;
        //    string strResults = "";
        //    EffectiveFrameCount = 0;


        //    double theTime = 0;
        //    double tx = 0;
        //    double ty = 0;
        //    double txR = 0;
        //    double tyR = 0;
        //    double[] lmin = new double[2] { 999, 999 };

        //    //string filename = "";
        //    //if (axis == 0)
        //    //    filename = "StepData_X.txt";
        //    //else
        //    //    filename = "StepData_Y.txt";
        //    //StreamWriter swr = new StreamWriter(filename);

        //    for (i = 0; i < framecount; i++)
        //    {
        //        if (axis == 0)
        //            theTime = ((oCam[port].CLXGrabTimeRes[i] - CLOutTime[2][1]) / (double)(TimerFrequency));
        //        else
        //            theTime = ((oCam[port].CLYGrabTimeRes[i] - CLOutTime[3][1]) / (double)(TimerFrequency));
        //        if (theTime > -0.05)
        //            break;
        //    }
        //    //Log.AddLog(ch, "SkipCount = " + i.ToString());
        //    //  여기까지 검증 완료

        //    for (; i < framecount; i++)
        //    {
        //        if (axis == 0)
        //        {
        //            spltime1[EffectiveFrameCount] = ((oCam[port].CLXGrabTimeRes[i] - CLOutTime[2][1]) / (double)(TimerFrequency));   // 초 단위
        //            if ((CLOutTime[2][3] - oCam[port].CLXGrabTimeRes[i]) / (double)(TimerFrequency) < (Rcp.iTfinalDelay + 2 * Rcp.iTmovingAvg) / 1000.0) break;
        //        }
        //        else
        //        {
        //            spltime1[EffectiveFrameCount] = ((oCam[port].CLYGrabTimeRes[i] - CLOutTime[3][1]) / (double)(TimerFrequency));   // 초 단위
        //            if ((CLOutTime[3][3] - oCam[port].CLYGrabTimeRes[i]) / (double)(TimerFrequency) < (Rcp.iTfinalDelay + 2 * Rcp.iTmovingAvg) / 1000.0) break;
        //        }
        //        spltime[EffectiveFrameCount] = spltime1[EffectiveFrameCount];
        //        double tmph = 0;
        //        oCam[port].CalcXYZT_L(i, ref tmph, ref tx, ref ty, Option.m_bXDirReverse, Option.m_bYDirReverse);
        //        oCam[port].CalcXYZT_R(i, ref tmph, ref txR, ref tyR, Option.m_bXDirReverse, Option.m_bYDirReverse);
        //        //oCam[port].CalcXYZT_L(i, ref tx, ref ty, true, m_bXDirReverse, m_bYDirReverse);
        //        //oCam[port].CalcXYZT_R(i, ref txR, ref tyR, true, m_bXDirReverse, m_bYDirReverse);

        //        XArray_Avg[EffectiveFrameCount] = tx;
        //        YArray_Avg[EffectiveFrameCount] = ty;
        //        XArray_AvgR[EffectiveFrameCount] = txR;
        //        YArray_AvgR[EffectiveFrameCount] = tyR;

        //        if (axis == 0)
        //        {
        //            if (lmin[0] > tx)
        //                lmin[0] = tx;
        //            if (lmin[1] > txR)
        //                lmin[1] = txR;
        //        }
        //        else
        //        {
        //            if (lmin[0] > ty)
        //                lmin[0] = ty;
        //            if (lmin[1] > tyR)
        //                lmin[1] = tyR;
        //        }

        //        EffectiveFrameCount++;
        //    }
        //    //Log.AddLog(ch, "Axis " + axis.ToString() + " CalcXYZT ");

        //    if (Option.m_bSaveRawData)
        //    {
        //        string newDir = FIO.CheckResultFolder();
        //        newDir = newDir + "DrivingData\\";
        //        if (!Directory.Exists(newDir))
        //        {
        //            Directory.CreateDirectory(newDir);
        //        }
        //        string sFilePath;
        //        //if (CurrentInspPosBlock != "Unknown" || CurrentInspPosSocket != "Unknown")
        //        //    sFilePath = newDir + lTestItem + "_" + ch.ToString() + "_" + lHindex.ToString() + "_" + CurrentInspPosBlock + "_" + CurrentInspPosSocket + ".csv";
        //        //else
        //        sFilePath = newDir + lTestItem + "_" + ch.ToString() + "_" + StrIndex[ch].ToString() + ".csv";

        //        if (FIO.IsAccessAbleFile(sFilePath))
        //        {

        //            writer = new StreamWriter(sFilePath);
        //            DateTime dtNow = DateTime.Now;   // 현재 날짜, 시간 얻기
        //            string strTime = dtNow.ToString("MM:dd:hh:mm:ss");
        //            writer.WriteLine(strTime);
        //            writer.WriteLine("index,Time, X Stroke, Y Stroke");
        //            for (i = 0; i < EffectiveFrameCount; i++)
        //            {
        //                strResults = i + "," + spltime[i].ToString("F6") + "," + XArray_Avg[i].ToString("F4") + "," + YArray_Avg[i].ToString("F4");
        //                writer.WriteLine(strResults);
        //            }
        //            writer.Close();
        //            Log.AddLog(ch, "Save Raw Data ", false);
        //        }
        //    }


        //    int EffectiveDataCount = EffectiveFrameCount - 1;
        //    if (EffectiveDataCount < 1)
        //    {
        //        ErrMsg[ch] = "VISION_MARK_FAIL";
        //        ErrMsg[ch + 1] = "VISION_MARK_FAIL";
        //        ChannelOn[ch] = false;
        //        ChannelOn[ch + 1] = false;

        //        //errMsg[ch] = "No Effective Data";
        //        Log.AddLog(ch, "No Effective Data", false);
        //        return false;
        //    }
        //    //Log.AddLog(ch, "EffectiveDataCount = " + EffectiveDataCount.ToString());

        //    double[] results = new double[2];
        //    double[] resultsR = new double[2];
        //    if (axis == 0)
        //    {
        //        for (i = 0; i < EffectiveDataCount; i++)
        //        {
        //            XArray_Avg[i] = XArray_Avg[i] - lmin[0];
        //            XArray_AvgR[i] = XArray_AvgR[i] - lmin[1];
        //        }
        //        if (ChannelOn[ch])
        //        {
        //            CalcCLXYSettlingPerformance(XArray_Avg, spltime, EffectiveDataCount, results, axis);
        //        }
        //        if (ChannelOn[ch + 1])
        //        {
        //            CalcCLXYSettlingPerformance(XArray_AvgR, spltime, EffectiveDataCount, resultsR, axis);
        //        }
        //    }
        //    else
        //    {
        //        for (i = 0; i < EffectiveDataCount; i++)
        //        {
        //            YArray_Avg[i] = YArray_Avg[i] - lmin[0];
        //            YArray_AvgR[i] = YArray_AvgR[i] - lmin[1];
        //        }
        //        if (ChannelOn[ch])
        //        {
        //            CalcCLXYSettlingPerformance(YArray_Avg, spltime, EffectiveDataCount, results, axis);
        //        }
        //        if (ChannelOn[ch + 1])
        //        {
        //            CalcCLXYSettlingPerformance(YArray_AvgR, spltime, EffectiveDataCount, resultsR, axis);
        //        }
        //    }

        //    string logText = "SettlingTime  = " + results[0].ToString("F3") + "\r\n" +
        //                     "Overshoot     = " + results[1].ToString("F3") + "\r\n";

        //    Log.AddLog(ch, logText, false);

        //    logText = "SettlingTime  = " + resultsR[0].ToString("F3") + "\r\n" +
        //                     "Overshoot     = " + resultsR[1].ToString("F3") + "\r\n";

        //    Log.AddLog(ch + 1, logText, false);
        //    //  여기까지 검증됨.

        //    if (axis == 0)
        //    {
        //        if (ChannelOn[ch])
        //        {
        //            Spec.PassFails[ch].Results[(int)SpecItem.OISX_SettlingTime].Val = results[0];
        //            Spec.PassFails[ch].Results[(int)SpecItem.OISX_Overshoot].Val = results[1];

        //            //Chart.AddChart(ch, (int)Axis.X, (int)GraphMode.Step, EffectiveDataCount, spltime, XArray_Avg);
        //            ShowEvent.Show(ch, "X Step");
        //        }
        //    }
        //    else
        //    {
        //        if (ChannelOn[ch])
        //        {
        //            Spec.PassFails[ch].Results[(int)SpecItem.OISY_SettlingTime].Val = results[0];
        //            Spec.PassFails[ch].Results[(int)SpecItem.OISY_Overshoot].Val = results[1];
        //            //Chart.AddChart(ch, (int)Axis.Y, (int)GraphMode.Step, EffectiveDataCount, spltime, YArray_Avg);
        //            ShowEvent.Show(ch, "Y Step");
        //        }
        //    }

        //    if (axis == 0)
        //    {
        //        if (ChannelOn[ch + 1])
        //        {
        //            Spec.PassFails[ch + 1].Results[(int)SpecItem.OISX_SettlingTime].Val = resultsR[0];
        //            Spec.PassFails[ch + 1].Results[(int)SpecItem.OISX_Overshoot].Val = resultsR[1];

        //            //Chart.AddChart(ch + 1, (int)Axis.X, (int)GraphMode.Step, EffectiveDataCount, spltime, XArray_AvgR);
        //            ShowEvent.Show(ch + 1, "X Step");
        //        }
        //    }
        //    else
        //    {
        //        if (ChannelOn[ch])
        //        {
        //            Spec.PassFails[ch + 1].Results[(int)SpecItem.OISY_SettlingTime].Val = resultsR[0];
        //            Spec.PassFails[ch + 1].Results[(int)SpecItem.OISY_Overshoot].Val = resultsR[1];
        //            //Chart.AddChart(ch + 1, (int)Axis.Y, (int)GraphMode.Step, EffectiveDataCount, spltime, YArray_AvgR);
        //            ShowEvent.Show(ch + 1, "Y Step");
        //        }
        //    }

        //    for (int u = 0; u < 4000; u++)
        //    {
        //        oCam[port].CLXGrabTimeRes[u] = 0;
        //        oCam[port].CLYGrabTimeRes[u] = 0;
        //    }
        //    return true;
        //}
        //public void CalcCLXYSettlingPerformance(double[] stroke, double[] spltime, int count, double[] results, int axis = 0)
        //{
        //    //  출력 시점 : m_CLAFStepOutTime[]
        //    //  출력 : m_bufWaveform_CLAFStep[]
        //    //  Sampling 변위 : stroke[]
        //    //  Sampling 시점 : spltime[]
        //    //  Settling Time 계산 
        //    //  출력시점 변위 계산은 출력 시점으로부터 과거로 TmovingAverage 시간동안 변위 평균 계산 : InitialStroke
        //    //  출력시점으로부터 TfinalDelay 경과 뒤 TmovingAverage 시간 동안 변위 평균 계산 : FinalStroke , StepStroke = InitialStroke - FInalStroke
        //    //  TfinalDelay 로부터 시간을 과거로 indexing 하면서 Indexing 시점에서의 변위가 FinalStroke 로부터 1.5um (StepStroke < 5um 이면 1.0um) 편차를 처음으로 넘어서는 시점을 Tsettled
        //    //  SettlingTIme = Tsettled - m_CLAFStepOutTime[3]

        //    double InitialStroke = 0, FinalStroke = 0, StepStroke = 0;
        //    double SettlingDeviation;
        //    double SettlingDeviationDeg;
        //    double FinalTime1, FinalTime2, InitialTime1, InitialTime2, Tsettled, SettlingTime;
        //    double overshootStroke = -999;

        //    int i = 0, j = 0, k = 0;
        //    InitialTime1 = -Rcp.iTfinalDelay / 1000.0;
        //    InitialTime2 = 0;
        //    FinalTime1 = Rcp.iTfinalDelay / 1000.0;
        //    FinalTime2 = FinalTime1 + Rcp.iTmovingAvg / 1000.0;

        //    if (axis == 0)
        //    {
        //        SettlingDeviation = Rcp.iXSettlingUnderPnt;
        //        SettlingDeviationDeg = Rcp.iXSettlingUnderDeg;
        //    }
        //    else
        //    {
        //        SettlingDeviation = Rcp.iYSettlingUnderPnt;
        //        SettlingDeviationDeg = Rcp.iYSettlingUnderDeg;
        //    }

        //    for (i = 0; i < count; i++)
        //    {
        //        if (spltime[i] > FinalTime1 && spltime[i] < FinalTime2)
        //        {
        //            FinalStroke += stroke[i];
        //            j++;
        //        }
        //        if (spltime[i] > InitialTime1 && spltime[i] < InitialTime2)
        //        {
        //            InitialStroke += stroke[i];
        //            k++;
        //        }
        //        if (spltime[i] > InitialTime2 && spltime[i] < FinalTime1)
        //        {
        //            if (overshootStroke < stroke[i])
        //                overshootStroke = stroke[i];
        //        }
        //    }
        //    if (j == 0)
        //    {
        //        FinalStroke = stroke[count - 1];
        //        j = 1;
        //    }

        //    FinalStroke = FinalStroke / j;
        //    InitialStroke = InitialStroke / k;
        //    StepStroke = Math.Abs(FinalStroke - InitialStroke);
        //    overshootStroke = overshootStroke - InitialStroke;

        //    if (axis == 0)
        //    {
        //        SettlingDeviation = StepStroke * Rcp.iXSettlingUnderPnt / 100.0;
        //    }
        //    else
        //    {
        //        SettlingDeviation = StepStroke * Rcp.iYSettlingUnderPnt / 100.0;
        //    }

        //    if (SettlingDeviation > SettlingDeviationDeg)
        //    {
        //        SettlingDeviation = SettlingDeviationDeg;   //  SettlingDeviationDeg 와 iSettlingUnderPnt 중 작은 값을 기준으로 측정한다.
        //    }


        //    Tsettled = -1;
        //    for (i = count - 1; i > 1; i--)
        //    {
        //        if (spltime[i] < FinalTime1)
        //            if (Math.Abs(stroke[i] - FinalStroke) > SettlingDeviation)
        //            {
        //                Tsettled = spltime[i + 1];
        //                break;
        //            }
        //    }
        //    if (Tsettled < 0)
        //        SettlingTime = spltime[count - 1] - InitialTime2;
        //    else
        //        SettlingTime = Tsettled - InitialTime2;

        //    results[0] = SettlingTime * 1000;         //  msec
        //    results[1] = ((overshootStroke / StepStroke) - 1) * 100;   //  Percent Overshoot
        //}
        public void WriteResult(int port)
        {
            DateTime dtNow = DateTime.Now;   // 현재 날짜, 시간 얻기
            string filename = "res_" + dtNow.ToString("yyMMdd") + ".csv";

            string sLotDir = FIO.CheckResultFolder();
            //if (Model.LotID != "" && Model.LotID != null)
            //    sLotDir += Model.LotID;

            string sFilePath = sLotDir + "\\" + filename;

            int ch = port * 2;

            if (!File.Exists(sFilePath))
            {
                AddHeadLineToResult(sFilePath);
            }

            string[] sLog = new string[2] { "", "" };

            for (int chCnt = 0; chCnt < 2; chCnt++)
            {
                ch += chCnt;

                if (ErrMsg[ch] == "Socket Empty") { Spec.PassFails[ch].FirstFailIndex = -1; }
                else
                {
                    for (int k = 0; k < ItemList.Count; k++)
                    {
                        if (ErrMsg[ch].Contains(ItemList[k].Name))
                        {
                            Spec.PassFails[ch].FirstFailIndex = (-(k + 2));
                        }
                    }
                }
                Log.AddLog(ch, "Barcode = " + StrBarcode[ch], true);
                Log.AddLog(ch, string.Format("ch : {0}, msg : {1}, PassFail : {2}", ch, ErrMsg[ch], Spec.PassFails[ch].FirstFailIndex), false);

                //"Time,Index,BarCode,LotID,Channel,PassFail,
                sLog[chCnt] += string.Format("'{0},{1},{2},{3},{4},{5},",
                    dtNow.ToString("yyyy-MM-dd HH:mm:ss.fff"), StrIndex[ch], StrBarcode[ch], Model.LotID, ch + 1, Spec.PassFails[ch].TotalFail);
                //"1st Fail Item,;
                //1st Fail Item
                if (Spec.PassFails[ch].FirstFailIndex > 0)
                {
                    ErrMsg[ch] = Spec.PassFails[ch].FirstFail;

                    Log.AddLog(ch, "Fail : " + ErrMsg[ch]);
                    sLog[chCnt] += ErrMsg[ch] + ",";  //  First Fail Item
                }
                else if (Spec.PassFails[ch].FirstFailIndex < 0)
                {
                    sLog[chCnt] += ErrMsg[ch] + ",";
                }
                else
                {
                    if (ChannelOn[ch])
                    {
                        sLog[chCnt] += "PASS" + ",";
                    }
                    else
                    {
                        sLog[chCnt] += "NONE" + ",";
                    }
                }
                //"XH Offset,XH Bias,XH min,XH max,XH mid,
                //NonEPA XH min,NonEPA XH max,NonEPA XH mid,EPA X Gain,EPA X Offset,";
                //"X Drv min,X Drv max,X measure min,X measure max,X step offset,X step,";
                sLog[chCnt] += string.Format("{0},{1},{2},{3},{4}," +
                    "{5},{6},{7},{8},{9}," +
                    "{10},{11},{12},{13},{14},{15},"
                , HallParam[ch][0].Offset, HallParam[ch][0].Bias, HallParam[ch][0].min, HallParam[ch][0].max, HallParam[ch][0].mid
                , "", "", "", "", ""
                , Rcp.iXDrvCodeMin, Rcp.iXDrvCodeMax, HallParam[ch][0].measure_Min, HallParam[ch][0].measure_Max, "", "");

                //X Results
                for (int i = (int)SpecItem.OISX_CalStroke; i < (int)SpecItem.OISY_CalStroke; i++)
                {
                    sLog[chCnt] += string.Format("{0:0.000},", Spec.PassFails[ch].Results[i].Val);
                }

                sLog[chCnt] += string.Format("{0},{1},{2},{3},{4}," +
                    "{5},{6},{7},{8},{9}," +
                    "{10},{11},{12},{13},{14},{15},"
                , HallParam[ch][1].Offset, HallParam[ch][1].Bias, HallParam[ch][1].min, HallParam[ch][1].max, HallParam[ch][1].mid
                , "", "", "", "", ""
                , Rcp.iYDrvCodeMin, Rcp.iYDrvCodeMax, HallParam[ch][1].measure_Min, HallParam[ch][1].measure_Max, "", "");

                //Y Results ~ end
                for (int i = (int)SpecItem.OISY_CalStroke; i < Spec.Param.Count; i++)
                {
                    sLog[chCnt] += string.Format("{0:0.000},", Spec.PassFails[ch].Results[i].Val);
                }
                //Time Results
                for (int i = 0; i < ItemList.Count; i++)
                {
                    sLog[chCnt] += string.Format("{0},", ItemList[i].Time);
                }
                //Total Test Time,FW Ver,IC Type,SW Ver,Hall Test Index,PM Index,";
                sLog[chCnt] += string.Format("{0},{1},{2},{3},{4},{5}",
                    Spec.PassFails[ch].TotalTime, FWVersion[ch], Model.DriverIC, Path.GetFileNameWithoutExtension(Application.ExecutablePath), OISHallTestCheckIndex[ch], "");

                if (ErrMsg[ch] != "")
                    Spec.TotlaFailed++;
                else Spec.TotlaPassed++;
                Spec.TotlaTested++;
            }
            if (!FIO.IsAccessAbleFile(sFilePath))
            {
                MessageBox.Show("Unable to Write Result File. Check if it is open! : \n" + sFilePath);
            }
            else
            {
                StreamWriter writer;
                writer = File.AppendText(sFilePath);
                if (sLog[0] != "")
                    writer.WriteLine(sLog[0]);
                if (sLog[1] != "")
                    writer.WriteLine(sLog[1]);
                writer.Close();
            }
        }
        public void AddHeadLineToResult(string sFilePath)
        {
            StreamWriter writer = File.AppendText(sFilePath);

            string sHeader = "Time,Index,BarCode,LotID,Channel,PassFail,1st Fail Item,";

            sHeader += "XH Offset,XH Bias,XH min,XH max,XH mid,NonEPA XH min,NonEPA XH max,NonEPA XH mid,EPA X Gain,EPA X Offset,";
            sHeader += "X Drv min,X Drv max,X measure min,X measure max,X step offset,X step,";

            string sParam = "";
            for (int i = (int)SpecItem.OISX_CalStroke; i < (int)SpecItem.OISY_CalStroke; i++)
            {
                sParam += string.Format("X {0},", Spec.Param[i][1]);
            }

            sHeader += sParam;

            sHeader += "YH Offset,YH Bias,YH min,YH max,YH mid,NonEPA YH min,NonEPA YH max,NonEPA YH mid,EPA Y Gain,EPA Y Offset,";
            sHeader += "Y Drv min,Y Drv max,Y measure min,Y measure max,Y step offset,Y step,";

            sParam = "";
            for (int i = (int)SpecItem.OISY_CalStroke; i < (int)SpecItem.FRAX_PMFreq; i++)
            {
                sParam += string.Format("Y {0},", Spec.Param[i][1]);
            }
            sHeader += sParam;

            sParam = "";
            for (int i = (int)SpecItem.FRAX_PMFreq; i < (int)SpecItem.FRAY_PMFreq; i++)
            {
                sParam += string.Format("X {0},", Spec.Param[i][1]);
            }
            sHeader += sParam;

            sParam = "";
            for (int i = (int)SpecItem.FRAY_PMFreq; i < (int)SpecItem.OIS_HallTest; i++)
            {
                sParam += string.Format("Y {0},", Spec.Param[i][1]);
            }
            sHeader += sParam;

            sParam = "";
            for (int i = (int)SpecItem.OIS_HallTest; i < Spec.Param.Count; i++)
            {
                sParam += string.Format("{0},", Spec.Param[i][1]);
            }
            sHeader += sParam;

            //Time header
            sParam = "";
            for (int i = 0; i < ItemList.Count; i++)
            {
                sParam += string.Format("{0} Time,", ItemList[i].Name);
            }
            sHeader += sParam;

            sHeader += "Total Test Time,FW Ver,IC Type,SW Ver,Hall Test Index,PM Index,";
            writer.Write(sHeader);
            writer.Write("\r\n");

            //Unit
            sHeader = "Unit,,,,,,,";

            sHeader += ",,,,,,,,,,";
            sHeader += ",,,,,,";

            sParam = "";
            for (int i = (int)SpecItem.OISX_CalStroke; i < (int)SpecItem.OISY_CalStroke; i++)
            {
                sParam += string.Format("({0}),", Spec.Param[i][9]);
            }

            sHeader += sParam;

            sHeader += ",,,,,,,,,,";
            sHeader += ",,,,,,";

            sParam = "";
            for (int i = (int)SpecItem.OISY_CalStroke; i < (int)SpecItem.FRAX_PMFreq; i++)
            {
                sParam += string.Format("({0}),", Spec.Param[i][9]);
            }
            sHeader += sParam;

            sParam = "";
            for (int i = (int)SpecItem.FRAX_PMFreq; i < (int)SpecItem.FRAY_PMFreq; i++)
            {
                sParam += string.Format("X {0},", Spec.Param[i][9]);
            }
            sHeader += sParam;

            sParam = "";
            for (int i = (int)SpecItem.FRAY_PMFreq; i < (int)SpecItem.OIS_HallTest; i++)
            {
                sParam += string.Format("({0}),", Spec.Param[i][9]);
            }
            sHeader += sParam;

            sParam = "";
            for (int i = (int)SpecItem.OIS_HallTest; i < Spec.Param.Count; i++)
            {
                sParam += string.Format("({0}),", Spec.Param[i][9]);
            }
            sHeader += sParam;

            //Time Unit
            sParam = "";
            for (int i = 0; i < ItemList.Count; i++)
            {
                sParam += string.Format("(msec),");
            }
            sHeader += sParam;

            sHeader += "(msec),,,,,,";
            writer.Write(sHeader);
            writer.Write("\r\n");

            writer.Close();
        }
        public bool CheckDriverIC(int ch)
        {
            try
            {
                byte[] rFWBuffer = new byte[4];
                byte[] rICBuffer = new byte[4];

                rFWBuffer = DrvIC.ReadAnyByte(ch, 0x1008, 4);

                //int value = rFWBuffer[0] + (rFWBuffer[1] << 8) + (rFWBuffer[2] << 16) + (rFWBuffer[3] << 24);  
                Log.AddLog(ch, "FW Ver " + " : " + rFWBuffer[3].ToString("X") + " " + rFWBuffer[2].ToString("X") + " " + rFWBuffer[1].ToString("X") + " " + rFWBuffer[0].ToString("X"), false);

                rICBuffer = DrvIC.ReadAnyByte(ch, 0x6014, 4);
                Log.AddLog(ch, "IC Ver " + " : " + rICBuffer[0].ToString("X") + " " + rICBuffer[1].ToString("X") + " " + rICBuffer[2].ToString("X") + " " + rICBuffer[3].ToString("X"), false);

                return true;
            }
            catch (Exception)
            {
                Log.AddLog(ch, "Contact Fail", false);
                return false;
                throw;
            }

        }
        public int LoadBaseModel(string sFileName)
        {
            if (!File.Exists(sFileName)) return 0;
            string all = "";
            try
            {
                StreamReader wr = new StreamReader(sFileName);
                all = wr.ReadToEnd();
                wr.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to access file : " + sFileName);
                return 0;
            }
            int[] basewidth = new int[10];
            int[] baseheight = new int[10];

            string[] allModel = all.Split("[HEAD]".ToCharArray());
            int modelNo = 0;
            for (int i = 0; i < allModel.Length; i++)
            {
                if (allModel[i].Length < 5) continue;
                string[] allLines = allModel[i].Split("\n".ToCharArray());
                string[] lhead = allLines[0].Split(',');
                oCam[0].mSearchModel[modelNo].width = Convert.ToInt16(lhead[1]);
                oCam[0].mSearchModel[modelNo].height = Convert.ToInt16(lhead[2]);
                oCam[0].mSearchModel[modelNo].conv = Convert.ToInt16(lhead[3]);
                oCam[0].mSearchModel[modelNo].sub = Convert.ToInt16(lhead[4]);
                if (lhead.Length > 5)
                    if (lhead[5].Length > 1)
                        oCam[0].mSearchModel[modelNo].subconv = Convert.ToDouble(lhead[5]);  //  이 항목이 없을수도 있으므로.

                oCam[0].mSearchModel[modelNo].img = new int[oCam[0].mSearchModel[modelNo].width * oCam[0].mSearchModel[modelNo].height];
                oCam[0].mSearchModel[modelNo].diffimg = new int[(oCam[0].mSearchModel[modelNo].width - 2) * (oCam[0].mSearchModel[modelNo].height - 2)];
                if (allLines.Length <= oCam[0].mSearchModel[modelNo].height)
                    return modelNo;

                int cnt = 0;
                for (int j = 1; j <= oCam[0].mSearchModel[modelNo].height; j++)
                {
                    string[] ldata = allLines[j].Split(',');
                    if (ldata.Length < oCam[0].mSearchModel[modelNo].width)
                        return modelNo;

                    for (int k = 0; k < oCam[0].mSearchModel[modelNo].width; k++)
                        oCam[0].mSearchModel[modelNo].img[cnt++] = Convert.ToInt16(ldata[k]);
                }

                for (int yi = 0; yi < oCam[0].mSearchModel[modelNo].height - 2; yi++)
                    for (int xi = 0; xi < oCam[0].mSearchModel[modelNo].width - 2; xi++)
                    {
                        if (xi < 4 || xi > oCam[0].mSearchModel[modelNo].width - 8)
                            oCam[0].mSearchModel[modelNo].diffimg[xi + yi * (oCam[0].mSearchModel[modelNo].width - 2)] = oCam[0].mSearchModel[modelNo].img[xi + yi * oCam[0].mSearchModel[modelNo].width] - oCam[0].mSearchModel[modelNo].img[xi + 2 + yi * oCam[0].mSearchModel[modelNo].width];
                        else if (yi < 4 || yi > oCam[0].mSearchModel[modelNo].height - 7)
                            oCam[0].mSearchModel[modelNo].diffimg[xi + yi * (oCam[0].mSearchModel[modelNo].width - 2)] = oCam[0].mSearchModel[modelNo].img[xi + yi * oCam[0].mSearchModel[modelNo].width] - oCam[0].mSearchModel[modelNo].img[xi + (yi + 2) * oCam[0].mSearchModel[modelNo].width];
                    }

                basewidth[modelNo] = oCam[0].mSearchModel[modelNo].width;
                baseheight[modelNo] = oCam[0].mSearchModel[modelNo].height;
                modelNo++;
            }
            return modelNo;
        }
        //Vision Variable ===========================================================================================================
        public Camera Cam { get; set; }
        public int CamCount { get; set; }
        public double[] FPS = new double[2] { 1000, 1000 };   //  Default Value
        public MILlib[] oCam = new MILlib[2];
        public bool IsMeasureTXTYRun = false;
        public int mVROI = 340;
        public int mHROI = 1680;
        public int mVROIstep = 150;
        public Mat mSourceImg = new Mat(new OpenCvSharp.Size(1680, 340), MatType.CV_8UC1);
        //Vision function
        public void Initial_Vision()
        {
            try
            {
                //Cam Open ===
                if (IsVirtual)
                {
                    Log.AddLog(0, "Virtual Cam Open");
                    CamCount = 1;
                }
                else
                {
                    List<ICameraInfo> allCameras = CameraFinder.Enumerate();
                    CamCount = allCameras.Count;
                    if (CamCount < 1) return;
                    Cam = new Camera(allCameras[0]);
                    Cam.CameraOpened += Configuration.AcquireContinuous;
                    Cam.Open();
                    Task.Factory.StartNew(() => Cam.Parameters.Load(FIO.RootDir + "CameraConfig.pfs", ParameterPath.CameraDevice));

                    //Cam.Parameters[PLCamera.ExposureAuto].SetValue("Off");

                }

                //Matrox Init =====
                oCam[0] = new MILlib(1.0);
                string strPath = FIO.RootDir + "Basler_10tap_" + mVROI.ToString() + "_" + mHROI.ToString() + ".dcf";
                if (IsVirtual)
                {
                    Log.AddLog(0, "Virtual Mil Init Dcf : " + strPath);
                    oCam[0].IsVirtual = IsVirtual;
                }
                else
                {
                    oCam[0].Init(mVROI, mHROI, mVROIstep, 0, "M_SYSTEM_RADIENTEVCL", 0, 0, strPath);
                }
            }
            catch
            {

            }
        }
        public bool SetRoi(int offsetX, int offsetY)
        {
            if (offsetX < 0 || offsetY < 0 || offsetX > 2040 - mHROI || offsetY >= 1088 - mVROI || CamCount < 1) return false;

            Cam.Parameters[PLCamera.Width].SetValue(mHROI);
            Cam.Parameters[PLCamera.Height].SetValue(mVROI);
            Cam.Parameters[PLCamera.OffsetX].SetValue(offsetX);
            Cam.Parameters[PLCamera.OffsetY].SetValue(offsetY);

            return true;
        }
        public bool SetExposure(int value)
        {
            Cam.Parameters[PLCamera.ExposureTimeAbs].SetValue(value);

            return true;
        }
        public bool MeasureXYT(int index, int axis, ref double[] cx, ref double[] cy, ref double[] rescx, ref double[] rescy, ref double[] resTheta, bool isSave = false)
        {
            while (IsMeasureTXTYRun)
                Thread.Sleep(100);

            IsMeasureTXTYRun = true;

            double[] dTx = new double[2];
            double[] dTy = new double[2];

            long nFound = 0;
            bool res = false;

            try
            {
                res = oCam[0].FineCOG(index, axis, ref dTx, ref dTy, ref cx, ref cy, ref nFound, isSave);
                oCam[0].ConvertXYtheta(cx, cy, dTx, ref rescx, ref rescy, ref resTheta);
            }
            catch
            {
                IsMeasureTXTYRun = false;
                return false;
            }


            rescx[0] = rescx[0] * VParam.XYScale[0];
            rescx[1] = rescx[1] * VParam.XYScale[1];
            rescy[0] = rescy[0] * VParam.XYScale[0];
            rescy[1] = rescy[1] * VParam.XYScale[1];

            IsMeasureTXTYRun = false;
            return true;
        }
        public Bitmap ShowReconfigImage(int index = -1, int axis = -1)
        {
            byte[] buf = new byte[1680 * 340];
            oCam[0].GetCommonBuf(index, buf, axis);
            return ReconfigImg(buf);
        }
        public Bitmap ReconfigImg(byte[] buf)
        {
            mSourceImg.SetArray(buf);
            Rect roi1 = new Rect(0, 0, 420, 340);
            Rect roi2 = new Rect(420, 0, 420, 340);
            Rect roi3 = new Rect(840, 0, 420, 340);
            Rect roi4 = new Rect(1260, 0, 420, 340);
            Mat subImg1 = mSourceImg.SubMat(roi1);
            Mat subImg2 = mSourceImg.SubMat(roi2);
            Mat subImg3 = mSourceImg.SubMat(roi3);
            Mat subImg4 = mSourceImg.SubMat(roi4);
            Mat imgTmp = new Mat();
            Mat imgTmp2 = new Mat();
            Cv2.HConcat(subImg2, subImg4, imgTmp);
            Cv2.HConcat(subImg1, subImg3, imgTmp2);
            Cv2.HConcat(imgTmp, imgTmp2, mSourceImg);

            return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(mSourceImg);
        }
        public Mat OverlayedImg = new Mat();
        public Bitmap DrawBoxesOnTarget(double[] cx, double[] cy)
        {
            Rect[] lDetectedRect = new Rect[4];    //    모델 최대 4종, 기존 코드에 중복 있는지 확인

            Cv2.CvtColor(mSourceImg, OverlayedImg, ColorConversionCodes.GRAY2RGB);
            int[] dx = new int[4] { 840, -420, 420, -840 };
            int k = 0;
            for (int i = 0; i < 4; i++)
            {
                if (cx[i] < 1)
                    continue;
                if (cx[i] < 420)
                    k = 0;
                else if (cx[i] < 840)
                    k = 1;
                else if (cx[i] < 1320)
                    k = 2;
                else
                    k = 3;

                lDetectedRect[i].X = (int)cx[i] + dx[k];
                lDetectedRect[i].Y = (int)cy[i];
                lDetectedRect[i].Width = (int)(4 * oCam[0].mSearchModel[oCam[0].mSearchModelIndex].width);
                lDetectedRect[i].Height = (int)(4 * oCam[0].mSearchModel[oCam[0].mSearchModelIndex].height);

                lDetectedRect[i].X -= lDetectedRect[i].Width / 2;
                lDetectedRect[i].Y -= lDetectedRect[i].Height / 2;

                OverlayedImg.Rectangle(lDetectedRect[i], Scalar.Cyan, 1);
            }
            return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(OverlayedImg);
        }
        //==========================================================================================================================

    }
}
