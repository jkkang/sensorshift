﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActroLib
{
    public static class FIO
    {
        public static string BaseDir = "C:\\SensorShift\\";
        public static string RootDir = "C:\\SensorShift\\DoNotTouch\\";
        public static string RowDataDir = "C:\\SensorShift\\Result\\RawData\\";
        public static string UserScriptDir = "C:\\SensorShift\\UserScript\\";
        public static void SetTextLine(string path, List<string> list)
        {
            try
            {
                string FilePath = path;
                //if (!File.Exists(FilePath)) return;
                StreamWriter sw = new StreamWriter(FilePath);
                for (int i = 0; i < list.Count; i++)
                { sw.WriteLine(list[i]); }
                sw.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static List<string> GetTextAll(string path)
        {
            List<string> result = new List<string>();
            string FilePath = path;
            if (!File.Exists(FilePath)) return null;
            StreamReader sr = new StreamReader(FilePath);
            while (sr.Peek() >= 0)
            {
                result.Add(sr.ReadLine());
            }
            sr.Close();
            return result;
        }
        public static string OpenFile(string InitDir, string ext, bool save = false)
        {
            FileDialog op;
            if (save) op = new SaveFileDialog();
            else op = new OpenFileDialog();

            op.InitialDirectory = InitDir;
            if (ext != "") ext = ext.Remove(0, 1);
            op.Filter = "*." + ext + "|*." + ext;
            if (op.ShowDialog() == DialogResult.OK)
                return op.FileName;
            else return null;
        }
        public static string CheckResultFolder()
        {
            DateTime dt = DateTime.Now;
            string resDirectory = BaseDir + "\\Data\\" + dt.Year + "\\" + dt.Month + "\\" + dt.Day + "\\";
            if (!Directory.Exists(resDirectory))
                Directory.CreateDirectory(resDirectory);
            return resDirectory;
        }
        public static bool IsAccessAbleFile(string filePath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //에러가 발생한 이유는 이미 다른 프로세서에서 점유중이거나.
                //혹은 파일이 존재하지 않기 때문이다.
                return false;
            }
            finally
            {
                if (fs != null)
                    //만약에 파일이 정상적으로 열렸다면 점유중이 아니다.
                    //다시 파일을 닫아줘야 한다.
                    fs.Close();
            }
            return true;
        }
    }
    public class BaseRecipe
    {
        public List<string> List = new List<string>();
        public List<string> ReadList = new List<string>();
        public string FilePath { get; set; }

        //Recipe, Spec Variable =========================================
        public List<object[]> Param = new List<object[]>();
        public bool bChange = false;
        public string CurrentName { get; set; }   
        public string[] ReadArry { get; set; }
        public string InitDir { get; set; }
        public string Ext { get; set; }
        public virtual void Init(string current, string subDir)
        {
            if (!Directory.Exists(FIO.BaseDir + "\\Result\\")) Directory.CreateDirectory(FIO.BaseDir + "\\Result\\");
            if (!Directory.Exists(FIO.UserScriptDir)) Directory.CreateDirectory(FIO.UserScriptDir);
            if (!Directory.Exists(FIO.RowDataDir)) Directory.CreateDirectory(FIO.RowDataDir);
    
            InitDir = FIO.BaseDir + subDir;
            Ext = Path.GetExtension(current);
            if (!Directory.Exists(InitDir)) Directory.CreateDirectory(InitDir);
            FilePath = FIO.BaseDir + subDir + current;

            CurrentName = current;
            if (!File.Exists(FilePath)) Save();

            Read();
        }
        //=================================================================
        public virtual void Init()
        {
        }
        public virtual void Save(string filePath = "")
        {
        }
        public virtual void Read(string filePath = "")
        {
            if (!File.Exists(FilePath))
            {
                FIO.SetTextLine(FilePath, List);
            }
            else
            {
                StreamReader sr = new StreamReader(FilePath);
                ReadArry = sr.ReadToEnd().Split('\r');

                int Paramindex = 0;

                for (int i = 0; i < ReadArry.Length; i++)
                {
                    string[] rArr = ReadArry[i].Split('\t');
                    rArr[0] = rArr[0].Trim();
                    for (int j = Paramindex; j < Param.Count; j++)
                    {
                        if (rArr[0] == Param[j][0].ToString())
                        {
                            Param[j][1] = rArr[1];
                            Paramindex = j + 1;
                            break;
                        }
                        bChange = true;
                    }
                }

                if (Param.Count != ReadArry.Length - 1) bChange = true;
                sr.Close();
            }
            
            SetParam();
        }
        public virtual void SetParam()
        {
        }
    }
    public enum RecipeItem
    {
        TsettleDelay,
        TfinalDelay,
        TmovingAv,
        DrvXStep,
        DrvYStep,
        DrvRStep,
        DrvStepInterval,
        NegMargin,
        PosMargin,
        XDrvCodeMin,
        XDrvCodeMax,
        XPlotRangeMin,
        XPlotRangeMax,
        XMeasureRangeMin,
        XMeasureRangeMax,
        YDrvCodeMin,
        YDrvCodeMax,
        YPlotRangeMin,
        YPlotRangeMax,
        YMeasureRangeMin,
        YMeasureRangeMax,
        RDrvCodeMin,
        RDrvCodeMax,
        RPlotRangeMin,
        RPlotRangeMax,
        RMeasureRangeMin,
        RMeasureRangeMax,
        SettlingOffset,
        SettlingStep,
        HallCalCount,
        XHallRangeThr,
        YHallRangeThr,
        CalStrokeMinX,
        CalStrokeMaxX,
        CalStrokeMidX,
        CalStrokeMinY,
        CalStrokeMaxY,
        CalStrokeMidY,
        calStrokeDelay,
        LinSamplingSize,
        LinStart,
        LinEnd,
        LinTargetDelay,
        OISEPACtrl,
        XEPACutBottom,
        XEPACutTop,
        YEPACutBottom,
        YEPACutTop,
        AgingLoop,
        AgingDelay,
        FRAloop,
        FRAstep,
        FRAdelay,
        FRAXgainTH,
        FRAYgainTH,
        XChirpFrom,
        XChirpTo,
        XAmplitude,
        XOffset,
        XCrossOffset,
        YChirpFrom,
        YChirpTo,
        YAmplitude,
        YOffset,
        YCrossOffset,
        circleThr,
        circleErrCnt,
        circleFreq,
        circleAmp,
        circleCycle,
        oscEnable,
        oscThr,
        oscErrTime,
        oscPos,
        saveFlag,
        LoppgainXAmp,
        LoppgainYAmp,
        XDisplacement,
        YDisplacement,
        TimeDisplacement,
        HallDEV_InitialDelay,
        HallDEV_Interval,
        HallDEV_sampling,
        HallDEV_TargetX,
        HallDEV_TargetY,
        HallDEV_GainFactorX,
        HallDEV_GainFactorY,
        MidTiltDelay,
        MidTiltRepeat,
        I2Cclock,
        I2CslaveAddr,
        HallCalTimeLimit,
        FWwriteInterval,
        ItrCount,
        CurrentLedL1,
        CurrentLedR1,
        CurrentLedL2,
        CurrentLedR2,

    };
    public class Recipe : BaseRecipe
    {

        public ObservableCollection<string> ToDoList = new ObservableCollection<string>();

        public int iTsettleDelay;
        public int iTfinalDelay;
        public int iTmovingAvg;
        public int iDrvXStep;
        public int iDrvYStep;
        public int iDrvRStep;
        public int iDrvStepInterval;
        public double iNegMargin;
        public double iPosMargin;
        public int iXDrvCodeMin;
        public int iXDrvCodeMax;
        public int iXPlotRangeMin;
        public int iXPlotRangeMax;
        public int iXMeasureRangeMin;
        public int iXMeasureRangeMax;
        public int iYDrvCodeMin;
        public int iYDrvCodeMax;
        public int iYPlotRangeMin;
        public int iYPlotRangeMax;
        public int iYMeasureRangeMin;
        public int iYMeasureRangeMax;
        public int iRDrvCodeMin;
        public int iRDrvCodeMax;
        public int iRPlotRangeMin;
        public int iRPlotRangeMax;
        public int iRMeasureRangeMin;
        public int iRMeasureRangeMax;
        public int iSettlingOffset;
        public int iSettlingStep;
        public int HallCalCount;
        public int XHallRangeThr;
        public int YHallRangeThr;
        public int CalStrokeMinX;
        public int CalStrokeMaxX;
        public int CalStrokeMidX;
        public int CalStrokeMinY;
        public int CalStrokeMaxY;
        public int CalStrokeMidY;
        public int calStrokeDelay;
        public int LinSamplingSize;
        public int LinStart;
        public int LinEnd;
        public int LinTargetDelay;
        public int iOISEPACtrl;
        public int iXEPACutBottom;
        public int iXEPACutTop;
        public int iYEPACutBottom;
        public int iYEPACutTop;
        public int iAgingLoop;
        public int iAgingDelay;
        public int iFRAloop;
        public int iFRAstep;
        public int iFRAdelay;
        public double iFRAXgainTH;
        public double iFRAYgainTH;
        public int iXChirpFrom;
        public int iXChirpTo;
        public double iXAmplitude;
        public double iXOffset;
        public int iXCrossOffset;
        public int iYChirpFrom;
        public int iYChirpTo;
        public double iYAmplitude;
        public double iYOffset;
        public int iYCrossOffset;
        public double i10HzXAmp;
        public double i10HzYAmp;
        public byte circleThr;
        public byte circleErrCnt;
        public byte circleFreq;
        public byte circleAmp;
        public byte circleCycle;
        public ushort oscEnable;
        public byte oscThr;
        public byte oscErrTime;
        public byte oscPos;
        public byte saveFlag;
        public int iXDisplacement;
        public int iYDisplacement;
        public double TimeDisplacement;
        public double HallDEV_InitialDelay;
        public double HallDEV_Interval;
        public double HallDEV_sampling;
        public double HallDEV_TargetX;
        public double HallDEV_TargetY;
        public int HallDEV_GainFactorX;
        public int HallDEV_GainFactorY;
        public int MidTiltDelay;
        public int MidTiltRepeat;
        public int iI2Cclock;
        public int iI2CslaveAddr;
        public int iHallCalTimeLimit;
        public int iFWwriteInterval;
        public int iItrCount;

        public List<double> iLEDcurrent = new List<double>();

        public Recipe()
        {
            Param.Add(new object[] { "Common", "Time To Settle", "25", "msec", true });              
            Param.Add(new object[] { "Common", "Delay to Final Pos.", "50", "msec", true });              
            Param.Add(new object[] { "Common", "Moving Avg. Time", "2", "msec", true });              
            Param.Add(new object[] { "Common", "Drv X Step", "400", "code", true });              
            Param.Add(new object[] { "Common", "Drv Y Step", "400", "code", true });
            Param.Add(new object[] { "Common", "Drv R Step", "100", "code", true });
            Param.Add(new object[] { "Common", "Drv Step Interval", "40", "msec", true });              
            Param.Add(new object[] { "Common", "Margin To Neg. Stop", "0", "um", true });              
            Param.Add(new object[] { "Common", "Margin To Pos. Stop", "0", "um", true });              
            Param.Add(new object[] { "X", "Drv Code Min", "4000", "code", true });              
            Param.Add(new object[] { "X", "Drv Code Max", "28000", "code", true });              
            Param.Add(new object[] { "X", "Plot Range Min", "6000", "code", true });
            Param.Add(new object[] { "X", "Plot Range Max", "26000", "code", true });
            Param.Add(new object[] { "X", "Measuring Min", "6000", "code", true });
            Param.Add(new object[] { "X", "Measuring Max", "26000", "code", true });
            Param.Add(new object[] { "Y", "Drv Code Min", "4000", "code", true });
            Param.Add(new object[] { "Y", "Drv Code Max", "28000", "code", true });
            Param.Add(new object[] { "Y", "Plot Range Min", "6000", "code", true });
            Param.Add(new object[] { "Y", "Plot Range Max", "26000", "code", true });
            Param.Add(new object[] { "Y", "Measuring Min", "6000", "code", true });
            Param.Add(new object[] { "Y", "Measuring Max", "26000", "code", true });
            Param.Add(new object[] { "R", "Drv Code Min", "-8000", "code", true });
            Param.Add(new object[] { "R", "Drv Code Max", "8000", "code", true });
            Param.Add(new object[] { "R", "Plot Range Min", "-5000", "code", true });
            Param.Add(new object[] { "R", "Plot Range Max", "5000", "code", true });
            Param.Add(new object[] { "R", "Measuring Min", "-5000", "code", true });
            Param.Add(new object[] { "R", "Measuring Max", "5000", "code", true });
            Param.Add(new object[] { "Settling", "Settling Offset", "4000", "code", true });
            Param.Add(new object[] { "Settling", "Settling Step", "1000", "code", true });
            Param.Add(new object[] { "Hall Cal", "Hall Cal Count", "1", "cnt", true });              
            Param.Add(new object[] { "Hall Cal", "X Hall Range Thr", "18000", "-", true });              
            Param.Add(new object[] { "Hall Cal", "Y Hall Range Thr", "18000", "-", true });              
            Param.Add(new object[] { "Cal Stroke", "X Code Min", "0", "code", true });              
            Param.Add(new object[] { "Cal Stroke", "X Code Max", "8190", "code", true });              
            Param.Add(new object[] { "Cal Stroke", "X Code Mid", "0", "code", true });              
            Param.Add(new object[] { "Cal Stroke", "Y Code Min", "0", "code", true });              
            Param.Add(new object[] { "Cal Stroke", "Y Code Max", "8190", "code", true });              
            Param.Add(new object[] { "Cal Stroke", "Y Code Mid", "0", "code", true });              
            Param.Add(new object[] { "Cal Stroke", "Delay", "100", "ms", true });              
            Param.Add(new object[] { "Linearity Comp", "Sampling Size", "13", "cnt", true });              
            Param.Add(new object[] { "Linearity Comp", "Start Code", "0", "code", true });              
            Param.Add(new object[] { "Linearity Comp", "End Code", "16384", "code", true });              
            Param.Add(new object[] { "Linearity Comp", "Move Delay", "100", "ms", true });              
            Param.Add(new object[] { "EPA", "OIS EPA Cntl", "1", "On(1)/Off(0)", true });              
            Param.Add(new object[] { "EPA", "X EPA Cut Bottom", "10", "%", true });              
            Param.Add(new object[] { "EPA", "X EPA Cut Top", "10", "%", true });              
            Param.Add(new object[] { "EPA", "Y EPA Cut Bottom", "10", "%", true });              
            Param.Add(new object[] { "EPA", "Y EPA Cut Top", "10", "%", true });                          
            Param.Add(new object[] { "Aging", "Aging Loop", "20", "#", true });              
            Param.Add(new object[] { "Aging", "Aging Interval", "200", "msec", true });                          
            Param.Add(new object[] { "FRA", "Loop", "1", "#", true });              
            Param.Add(new object[] { "FRA", "Step", "5", "Hz", true });              
            Param.Add(new object[] { "FRA", "Delay", "10", "msec", true });              
            Param.Add(new object[] { "FRA", "X Gain TH", "0", "db", true });              
            Param.Add(new object[] { "FRA", "Y Gain TH", "0", "db", true });                                     
            Param.Add(new object[] { "X FRA", "Chirp from", "250", "Hz", true });              
            Param.Add(new object[] { "X FRA", "Chirp to", "100", "Hz", true });              
            Param.Add(new object[] { "X FRA", "Drv Amplitude", "4000", "code(mV)", true });                                 
            Param.Add(new object[] { "X FRA", "Drv Offset", "28000", "code(mV)", true });              
            Param.Add(new object[] { "X FRA", "Cross Axis Offset", "16000", "code", true });              
            Param.Add(new object[] { "Y FRA", "Chirp from", "250", "Hz", true });              
            Param.Add(new object[] { "Y FRA", "Chirp to", "100", "Hz", true });              
            Param.Add(new object[] { "Y FRA", "Drv Amplitude", "4000", "code(mV)", true });                                      
            Param.Add(new object[] { "Y FRA", "Drv Offset", "28000", "code(mV)", true });              
            Param.Add(new object[] { "Y FRA", "Cross Axis Offset", "16000", "code", true });              
            Param.Add(new object[] { "LG @ 10Hz", "X Amp", "146", "code(mV)", true });              
            Param.Add(new object[] { "LG @ 10Hz", "Y Amp", "146", "code(mV)", true });              
            Param.Add(new object[] { "Hall Test", "Circle TH", "75", "_", true });              
            Param.Add(new object[] { "Hall Test", "Circle Err Cnt", "0", "cnt", true });              
            Param.Add(new object[] { "Hall Test", "Circle Freq", "5", "Hz", true });              
            Param.Add(new object[] { "Hall Test", "Circle Amp Lev", "0", "%", true });              
            Param.Add(new object[] { "Hall Test", "Circle Cycle", "2", "cnt", true });              
            Param.Add(new object[] { "Hall Test", "OSC Enable", "1", "_", true });              
            Param.Add(new object[] { "Hall Test", "OSC Thr", "100", "_", true });              
            Param.Add(new object[] { "Hall Test", "OSC Err Time", "100", "msec", true });              
            Param.Add(new object[] { "Hall Test", "OSC Position", "80", "%", true });              
            Param.Add(new object[] { "Hall Test", "Save", "1", "bool", true });              
            Param.Add(new object[] { "Displacement Accuracy", "Time", "1", "sec", true });              
            Param.Add(new object[] { "Displacement Accuracy", "Target X", "4095", "code", true });              
            Param.Add(new object[] { "Displacement Accuracy", "Target Y", "4095", "code", true });              
            Param.Add(new object[] { "Hall Deviation", "Initial Delay", "1000", "ms", true });              
            Param.Add(new object[] { "Hall Deviation", "Read Interval", "10", "ms", true });              
            Param.Add(new object[] { "Hall Deviation", "Sampling", "100", "samples", true });              
            Param.Add(new object[] { "Hall Deviation", "Target X", "8192", "code", true });              
            Param.Add(new object[] { "Hall Deviation", "Target Y", "8192", "code", true });              
            Param.Add(new object[] { "Hall Deviation", "Gain Factor X", "512", "dec", true });              
            Param.Add(new object[] { "Hall Deviation", "Gain Factor Y", "512", "dec", true });              
            Param.Add(new object[] { "Mid Tilt", "Delay", "100", "ms", true });              
            Param.Add(new object[] { "Mid Tilt", "Repeat", "1", "Cnt", true });              
            Param.Add(new object[] { "I2C", "I2C Clock", "400", "KHz", true });              
            Param.Add(new object[] { "I2C", "Slave Addr", "61", "Hex", true });              
            Param.Add(new object[] { "Others", "Hall Cal Time Lmt", "20", "sec", true });              
            Param.Add(new object[] { "Others", "FW Write Interval", "10", "msec", true });              
            Param.Add(new object[] { "Others", "Repeat Test", "1", "#", true });              
            Param.Add(new object[] { "Others", "LED Power L-L", "2.8", "V", true });              
            Param.Add(new object[] { "Others", "LED Power L-R", "2.8", "V", true });              
            Param.Add(new object[] { "Others", "LED Power R-L", "2.8", "V", true });              
            Param.Add(new object[] { "Others", "LED Power R-R", "2.8", "V", true });

            iLEDcurrent.Add(0);
            iLEDcurrent.Add(0);
            iLEDcurrent.Add(0);
            iLEDcurrent.Add(0);
        }
        public override void Save(string filePath = "")
        {
            if (filePath == null) return;
            if (filePath != "") FilePath = filePath;
            StreamWriter sw = new StreamWriter(FilePath);

            string data = "";
            for (int i = 0; i < ToDoList.Count; i++)
            {
                data += string.Format("{0}\t", ToDoList[i]);
            }
            if (data != "") data = data.Remove(data.Length - 1);
            sw.WriteLine(data);

            for (int i = 0; i < Param.Count; i++)
            {
                data = string.Format("{0}\t{1}", Param[i][1], Param[i][2]);
                sw.WriteLine(data);
            }
            sw.Close();

            bChange = false;

            Read();
        }
        public override void Read(string filePath = "")
        {
            if (filePath == null) return;
            if (filePath != "")
            {
                FilePath = filePath;
                CurrentName = Path.GetFileName(FilePath);
            }
            StreamReader sr = new StreamReader(FilePath);

            ReadArry = sr.ReadToEnd().Split('\r');
            ToDoList.Clear();

            string[] actionArry = ReadArry[0].Split('\t');
            for (int i = 0; i < actionArry.Length; i++)
            {
                if (actionArry[i] != "") ToDoList.Add(actionArry[i]);
            }

            int Paramindex = 0;

            for (int i = 1; i < ReadArry.Length; i++)
            {
                string[] arr = ReadArry[i].Split('\t');
                for (int j = Paramindex; j < Param.Count; j++)
                {
                    arr[0] = arr[0].Trim();
                    if (arr[0] == Param[j][1].ToString())
                    {
                        Param[j][2] = arr[1];
                        Paramindex = j + 1;
                        break;
                    }
                    bChange = true;
                }
            }

            if (Param.Count != ReadArry.Length - 2) bChange = true;

            sr.Close();
            SetParam();
        }
        public override void SetParam()
        {
            int index = 0;
            iTsettleDelay = Convert.ToInt32(Param[index++][2]);
            iTfinalDelay = Convert.ToInt32(Param[index++][2]);
            iTmovingAvg = Convert.ToInt32(Param[index++][2]);
            iDrvXStep = Convert.ToInt32(Param[index++][2]);
            iDrvYStep = Convert.ToInt32(Param[index++][2]);
            iDrvRStep = Convert.ToInt32(Param[index++][2]);
            iDrvStepInterval = Convert.ToInt32(Param[index++][2]);
            iNegMargin = Convert.ToDouble(Param[index++][2]);
            iPosMargin = Convert.ToDouble(Param[index++][2]);
            iXDrvCodeMin = Convert.ToInt32(Param[index++][2]);
            iXDrvCodeMax = Convert.ToInt32(Param[index++][2]);
            iXPlotRangeMin = Convert.ToInt32(Param[index++][2]);
            iXPlotRangeMax = Convert.ToInt32(Param[index++][2]);
            iXMeasureRangeMin = Convert.ToInt32(Param[index++][2]);
            iXMeasureRangeMax = Convert.ToInt32(Param[index++][2]);
            iYDrvCodeMin = Convert.ToInt32(Param[index++][2]);
            iYDrvCodeMax = Convert.ToInt32(Param[index++][2]);
            iYPlotRangeMin = Convert.ToInt32(Param[index++][2]);
            iYPlotRangeMax = Convert.ToInt32(Param[index++][2]);
            iYMeasureRangeMin = Convert.ToInt32(Param[index++][2]);
            iYMeasureRangeMax = Convert.ToInt32(Param[index++][2]);
            iRDrvCodeMin = Convert.ToInt32(Param[index++][2]);
            iRDrvCodeMax = Convert.ToInt32(Param[index++][2]);
            iRPlotRangeMin = Convert.ToInt32(Param[index++][2]);
            iRPlotRangeMax = Convert.ToInt32(Param[index++][2]);
            iRMeasureRangeMin = Convert.ToInt32(Param[index++][2]);
            iRMeasureRangeMax = Convert.ToInt32(Param[index++][2]);
            iSettlingOffset = Convert.ToInt32(Param[index++][2]);
            iSettlingStep = Convert.ToInt32(Param[index++][2]);
            HallCalCount = Convert.ToInt32(Param[index++][2]);
            XHallRangeThr = Convert.ToInt32(Param[index++][2]);
            YHallRangeThr = Convert.ToInt32(Param[index++][2]);
            CalStrokeMinX = Convert.ToInt32(Param[index++][2]);
            CalStrokeMaxX = Convert.ToInt32(Param[index++][2]);
            CalStrokeMidX = Convert.ToInt32(Param[index++][2]);
            CalStrokeMinY = Convert.ToInt32(Param[index++][2]);
            CalStrokeMaxY = Convert.ToInt32(Param[index++][2]);
            CalStrokeMidY = Convert.ToInt32(Param[index++][2]);
            calStrokeDelay = Convert.ToInt32(Param[index++][2]);
            LinSamplingSize = Convert.ToInt32(Param[index++][2]);
            LinStart = Convert.ToInt32(Param[index++][2]);
            LinEnd = Convert.ToInt32(Param[index++][2]);
            LinTargetDelay = Convert.ToInt32(Param[index++][2]);
            iOISEPACtrl = Convert.ToInt32(Param[index++][2]);
            iXEPACutBottom = Convert.ToInt32(Param[index++][2]);
            iXEPACutTop = Convert.ToInt32(Param[index++][2]);
            iYEPACutBottom = Convert.ToInt32(Param[index++][2]);
            iYEPACutTop = Convert.ToInt32(Param[index++][2]);
            iAgingLoop = Convert.ToInt32(Param[index++][2]);
            iAgingDelay = Convert.ToInt32(Param[index++][2]);
            iFRAloop = Convert.ToInt32(Param[index++][2]);
            iFRAstep = Convert.ToInt32(Param[index++][2]);
            iFRAdelay = Convert.ToInt32(Param[index++][2]);
            iFRAXgainTH = Convert.ToDouble(Param[index++][2]);
            iFRAYgainTH = Convert.ToDouble(Param[index++][2]);
            iXChirpFrom = Convert.ToInt32(Param[index++][2]);
            iXChirpTo = Convert.ToInt32(Param[index++][2]);
            iXAmplitude = Convert.ToDouble(Param[index++][2]);
            iXOffset = Convert.ToDouble(Param[index++][2]);
            iXCrossOffset = Convert.ToInt32(Param[index++][2]);
            iYChirpFrom = Convert.ToInt32(Param[index++][2]);
            iYChirpTo = Convert.ToInt32(Param[index++][2]);
            iYAmplitude = Convert.ToDouble(Param[index++][2]);
            iYOffset = Convert.ToDouble(Param[index++][2]);
            iYCrossOffset = Convert.ToInt32(Param[index++][2]);
            i10HzXAmp = Convert.ToDouble(Param[index++][2]);
            i10HzYAmp = Convert.ToDouble(Param[index++][2]);
            circleThr = Convert.ToByte(Param[index++][2]);
            circleErrCnt = Convert.ToByte(Param[index++][2]);
            circleFreq = Convert.ToByte(Param[index++][2]);
            circleAmp = Convert.ToByte(Param[index++][2]);
            circleCycle = Convert.ToByte(Param[index++][2]);
            oscEnable = Convert.ToUInt16(Param[index++][2]);
            oscThr = Convert.ToByte(Param[index++][2]);
            oscErrTime = Convert.ToByte(Param[index++][2]);
            oscPos = Convert.ToByte(Param[index++][2]);
            saveFlag = Convert.ToByte(Param[index++][2]);
            iXDisplacement = Convert.ToInt32(Param[index++][2]);
            iYDisplacement = Convert.ToInt32(Param[index++][2]);
            TimeDisplacement = Convert.ToDouble(Param[index++][2]);
            HallDEV_InitialDelay = Convert.ToDouble(Param[index++][2]);
            HallDEV_Interval = Convert.ToDouble(Param[index++][2]);
            HallDEV_sampling = Convert.ToDouble(Param[index++][2]);
            HallDEV_TargetX = Convert.ToDouble(Param[index++][2]);
            HallDEV_TargetY = Convert.ToDouble(Param[index++][2]);
            HallDEV_GainFactorX = Convert.ToInt32(Param[index++][2]);
            HallDEV_GainFactorY = Convert.ToInt32(Param[index++][2]);
            MidTiltDelay = Convert.ToInt32(Param[index++][2]);
            MidTiltRepeat = Convert.ToInt32(Param[index++][2]);
            iI2Cclock = Convert.ToInt32(Param[index++][2]);
            iI2CslaveAddr = Convert.ToInt32(Param[index++][2]);
            iHallCalTimeLimit = Convert.ToInt32(Param[index++][2]);
            iFWwriteInterval = Convert.ToInt32(Param[index++][2]);
            iItrCount = Convert.ToInt32(Param[index++][2]);

            iLEDcurrent[0] = Convert.ToDouble(Param[index++][2]);
            iLEDcurrent[1] = Convert.ToDouble(Param[index++][2]);
            iLEDcurrent[2] = Convert.ToDouble(Param[index++][2]);
            iLEDcurrent[3] = Convert.ToDouble(Param[index++][2]);

            if (bChange) Save();
        }
    }
    public enum SpecItem
    {
        YEILD,
        OISX_CalStroke,
        OISX_NonEPAStroke,
        OISX_Ratedstroke,
        OISX_Forwardstroke,
        OISX_Backwardstroke,
        OISX_Sensitivity,
        OISX_Linearity,
        OISX_Hysteresis,
        OISX_CenterCurrent,
        OISX_MaxCurrent,
        OISX_CodeMid2MechaMid,
        OISX_Crosstalk,
        OISX_SettlingTime,
        OISX_Overshoot,
        OISX_FWDRatedStroke,
        OISX_BWDRatedStroke,
        OISX_Resistance,
        OISX_MidTiltTest,
        OISY_CalStroke,
        OISY_NonEPAStroke,
        OISY_Ratedstroke,
        OISY_Forwardstroke,
        OISY_Backwardstroke,
        OISY_Sensitivity,
        OISY_Linearity,
        OISY_Hysteresis,
        OISY_CenterCurrent,
        OISY_MaxCurrent,
        OISY_CodeMid2MechaMid,
        OISY_Crosstalk,
        OISY_SettlingTime,
        OISY_Overshoot,
        OISY_FWDRatedStroke,
        OISY_BWDRatedStroke,
        OISY_Resistance,
        OISY_MidTiltTest,
        OISR_InitDegree,
        OISR_MaxDegree,
        OISR_Sensitivity,
        OISR_Linearity,
        OISR_Hysteresis,
        OISR_MaxCurrent,
        FRAX_PMFreq,
        FRAX_PhaseMargin,
        FRAX_MaxDiffGain,
        FRAX_Gain10Hz,
        FRAX2_PMFreq,
        FRAX2_PhaseMargin,
        FRAX2_MaxDiffGain,
        FRAX2_Gain10Hz,
        FRAY_PMFreq,
        FRAY_PhaseMargin,
        FRAY_MaxDiffGain,
        FRAY_Gain10Hz,
        OIS_HallTest,
        OISX_HALLDEV,
        OISY_HALLDEV,
        OISX_MidTiltDiff,
        OISY_MidTiltDiff,
    };
    public enum ResultItem
    {
        Ratedstroke,
        Forwardstroke,
        Backwardstroke,
        Sensitivity,
        Linearity,
        Hysteresis,
        CenterCurrent,
        MaxCurrent,
        CodeMid2MechaMid,
        Crosstalk,
        InitDegree,
        MaxDegree,
        Count,
    };
    public class Spec : BaseRecipe
    {
        public class ResultItems
        {
            public double Val = 0;
            public bool bPass = true;
            public string msg = "";
        }
        public class PassFail
        {
            public int FirstFailIndex;
            public string FirstFail;
            public string TotalFail;
            public string TotalTime;
            public List<ResultItems> Results = new List<ResultItems>();
        }
        public List<PassFail> PassFails = new List<PassFail>();
        public int LastSampleNum;
        public int TotlaTested;
        public int TotlaPassed;
        public int TotlaFailed;
        public Spec()
        {
            try
            {
                Param.Add(new object[] { "", "Yeild", "0", "0", "0", "0", "0", "0", "0", "0", false });
                Param.Add(new object[] { "X Cal", "Cal stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X NonEPA", "NonEPA stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "Rated stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "Forward stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "Backward stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "Sensitivity", "-1", "1", "0", "0", "0", "0", "0", "um / code", true });
                Param.Add(new object[] { "X", "Linearity", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "Hysteresis", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "Center Current", "0", "120", "0", "0", "0", "0", "0", "mA", true });
                Param.Add(new object[] { "X", "Max Current", "0", "120", "0", "0", "0", "0", "0", "mA", true });
                Param.Add(new object[] { "X", "CodeMid to MechaMid", "0", "120", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "y Crosstalk", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X Step", "SettlingTime", "-1", "1", "0", "0", "0", "0", "0", "msec", true });
                Param.Add(new object[] { "X Step", "Overshoot", "-1", "1", "0", "0", "0", "0", "0", "%", true });
                Param.Add(new object[] { "X", "FWD Rated Stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "BWD Rated Stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "X", "Resistance", "-1", "1", "0", "0", "0", "0", "0", "Ohm", true });
                Param.Add(new object[] { "X", "Hall Mid Tilt", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "Y Cal", "Cal stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y NonEPA", "NonEPA stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "Rated stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "Forward stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "Backward stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "Sensitivity", "-1", "1", "0", "0", "0", "0", "0", "um / code", true });
                Param.Add(new object[] { "Y", "Linearity", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "Hysteresis", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "Center Current", "0", "120", "0", "0", "0", "0", "0", "mA", true });
                Param.Add(new object[] { "Y", "Max Current", "0", "120", "0", "0", "0", "0", "0", "mA", true });
                Param.Add(new object[] { "Y", "CodeMid to MechaMid", "0", "120", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "x Crosstalk", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y Step", "SettlingTime", "-1", "1", "0", "0", "0", "0", "0", "msec", true });
                Param.Add(new object[] { "Y Step", "Overshoot", "-1", "1", "0", "0", "0", "0", "0", "%", true });
                Param.Add(new object[] { "Y", "FWD Rated Stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "BWD Rated Stroke", "-1", "1", "0", "0", "0", "0", "0", "um", true });
                Param.Add(new object[] { "Y", "Resistance", "-1", "1", "0", "0", "0", "0", "0", "Ohm", true });
                Param.Add(new object[] { "Y", "Hall Mid Tilt", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "R", "Init Degree", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "R", "Max Degree", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "R", "Sensitivity", "-1", "1", "0", "0", "0", "0", "0", "deg / code", true });
                Param.Add(new object[] { "R", "Linearity", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "R", "Hysteresis", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "R", "Max Current", "0", "120", "0", "0", "0", "0", "0", "mA", true });
                Param.Add(new object[] { "FRA X", "PM Frequency", "-1", "1", "0", "0", "0", "0", "0", "Hz", true });
                Param.Add(new object[] { "FRA X", "Phase margin", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "FRA X", "Max Diff Gain", "-1", "1", "0", "0", "0", "0", "0", "db", true });
                Param.Add(new object[] { "FRA X", "Gain @ 10Hz", "-1", "1", "0", "0", "0", "0", "0", "db", true });
                Param.Add(new object[] { "FRA X2", "PM Frequency", "-1", "1", "0", "0", "0", "0", "0", "Hz", true });
                Param.Add(new object[] { "FRA X2", "Phase margin", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "FRA X2", "Max Diff Gain", "-1", "1", "0", "0", "0", "0", "0", "db", true });
                Param.Add(new object[] { "FRA X2", "Gain @ 10Hz", "-1", "1", "0", "0", "0", "0", "0", "db", true });
                Param.Add(new object[] { "FRA Y", "PM Frequency", "-1", "1", "0", "0", "0", "0", "0", "Hz", true });
                Param.Add(new object[] { "FRA Y", "Phase margin", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "FRA Y", "Max Diff Gain", "-1", "1", "0", "0", "0", "0", "0", "db", true });
                Param.Add(new object[] { "FRA Y", "Gain @ 10Hz", "-1", "1", "0", "0", "0", "0", "0", "db", true });
                Param.Add(new object[] { "Hall", "OIS Hall Test", "N/A", "N/A", "0", "0", "0", "0", "0", "bool", true });
                Param.Add(new object[] { "Hall Dev", "Dev X", "-1", "1", "0", "0", "0", "0", "0", "code", true });
                Param.Add(new object[] { "Hall Dev", "Dev Y", "-1", "1", "0", "0", "0", "0", "0", "code", true });
                Param.Add(new object[] { "Mid Tilt", "X Mid Tilt Diff", "-1", "1", "0", "0", "0", "0", "0", "deg", true });
                Param.Add(new object[] { "Mid Tilt", "Y Mid Tilt Diff", "-1", "1", "0", "0", "0", "0", "0", "deg", true });

                for (int i = 0; i < 4; i++)
                {
                    PassFails.Add(new PassFail());
                    for (int j = 0; j < Param.Count; j++) PassFails[i].Results.Add(new ResultItems());
                }
            }
            catch(Exception e)
            {
                e.ToString();
            }
        }
        public override void Save(string filePath = "")
        {
            if (filePath != "") FilePath = filePath;
            StreamWriter sw = new StreamWriter(FilePath);

            for (int i = 0; i < Param.Count; i++)
            {
                string data;
                if (i == 0)
                {
                    data = string.Format("{0}\t{1}\t{2}\t{3}\t{4}",
                        Param[i][1],
                        Param[i][2] = LastSampleNum,
                        Param[i][3] = TotlaTested,
                        Param[i][4] = TotlaPassed,
                        Param[i][5] = TotlaFailed);
                }
                else
                {
                    data = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", Param[i][1], Param[i][2], Param[i][3], Param[i][8], Param[i][10]);
                }
                sw.WriteLine(data);
            }
            sw.Close();

            Read();

            bChange = true;
        }
        public override void Read(string filePath = "")
        {
            if (filePath != "")
            {
                FilePath = filePath;
                CurrentName = Path.GetFileName(FilePath);
            }
            StreamReader sr = new StreamReader(FilePath);

            ReadArry = sr.ReadToEnd().Split('\r');

            string[] yeildArry = ReadArry[0].Split('\t');
            LastSampleNum = Convert.ToInt32(yeildArry[1]);
            TotlaTested = Convert.ToInt32(yeildArry[2]);
            TotlaPassed = Convert.ToInt32(yeildArry[3]);
            TotlaFailed = Convert.ToInt32(yeildArry[4]);
            Param[0][10] = false;

            int Paramindex = 1;

            for (int i = 1; i < ReadArry.Length; i++)
            {
                string[] arr = ReadArry[i].Split('\t');
                for (int j = Paramindex; j < Param.Count; j++)
                {
                    arr[0] = arr[0].Trim();
                    if (arr[0] == Param[j][1].ToString())
                    {
                        Param[Paramindex][2] = arr[1];
                        Param[Paramindex][3] = arr[2];
                        if (arr.Length == 4)
                        {
                            Param[Paramindex][10] = arr[3];
                        }
                        else if (arr.Length == 5)
                        {
                            Param[Paramindex][8] = arr[3];
                            Param[Paramindex][10] = arr[4];
                        }
                        Paramindex = j + 1;
                        break;
                    }
                    bChange = true;
                }

                if (Param.Count != ReadArry.Length - 1) bChange = true;
            }

            sr.Close();
        }
        public void InitResult(int ch)
        {
            PassFails[ch].TotalFail = "";
            PassFails[ch].FirstFail = "";
            PassFails[ch].FirstFailIndex = 0;
            for (int i = 0; i < Param.Count; i++)
            {
                PassFails[ch].Results[i].Val = 0;
                PassFails[ch].Results[i].msg = ""; PassFails[ch].Results[i].bPass = true;
            }
        }
        public void SetResult(int ch, string key)
        {
            for (int i = 0; i < Param.Count; i++)
            {
                if (!Convert.ToBoolean(Param[i][10])) continue;
                if (Param[i][0].ToString() != key) continue;

                double lmin, lmax;
                lmin = Convert.ToDouble(Param[i][2]); lmax = Convert.ToDouble(Param[i][3]);
                if (0.0512 < lmin || 0.0512 > lmax)
                {

                }
                if (PassFails[ch].Results[i].Val < lmin || PassFails[ch].Results[i].Val > lmax)
                {
                    PassFails[ch].Results[i].msg = Param[i][0] + "_" + Param[i][1];
                    PassFails[ch].Results[i].bPass = false;
                    PassFails[ch].TotalFail += string.Format("_{0}", i + 1);
                }
                else
                {
                    PassFails[ch].Results[i].msg = ""; PassFails[ch].Results[i].bPass = true;
                }
            }
            if (PassFails[ch].TotalFail != "")
                if (PassFails[ch].TotalFail[0] == '_')
                    PassFails[ch].TotalFail = PassFails[ch].TotalFail.Remove(0, 1);
            for (int i = 0; i < Param.Count; i++)
            {
                if (!PassFails[ch].Results[i].bPass)
                {
                    PassFails[ch].FirstFailIndex = (i + 1);
                    PassFails[ch].FirstFail = PassFails[ch].Results[i].msg;

                    int failCnt = Convert.ToInt32(Param[i][8]); failCnt++;
                    Param[i][8] = failCnt;

                    break;
                }
            }
        }
    }
    public class Option : BaseRecipe
    {
        public bool m_bPasswordOn = false;
        public bool m_bScreenCapture = false;
        public bool m_bSaveRawData = false;
        public bool m_bFixedCenter = false;
        public bool m_bUpdateHallCalData = false;
        public bool m_bSaveResIndexCount = true;
        public bool m_bSkipWriteHallPolarity = false;
        public bool m_bWriteResultToDriverIC = false;
        public bool m_bSafeSensor = false;
        public bool m_bXDirReverse = false;
        public bool m_bYDirReverse = false;
        public bool m_bBarcodeOverwriteDisable = false;
        public bool m_bSkipVisionCheck = false;
        public bool m_bHideYieldGraph = false;
        public bool m_bPhaseInterpolation = true;
        public bool m_bDFTphasemargin = true;
        public bool m_bEPAReset = false;
        public bool m_bCheckChipID = false;
        public bool m_bSaveHallTestData = false;
        public Option()
        {
            Init();
        }
        public override void Init()
        {
            base.Init();
            FilePath = FIO.RootDir + "OptionState.txt";

            Param.Add(new object[] { "Password", false });
            Param.Add(new object[] { "Save Screen", false });
            Param.Add(new object[] { "Save Raw Data", false });
            Param.Add(new object[] { "Fixed Center for F/B Stroke", false });
            Param.Add(new object[] { "Hall Cal Data Update", false });
            Param.Add(new object[] { "Sample Count Enable", false });
            Param.Add(new object[] { "Skip Write Hall Polarity", false });
            Param.Add(new object[] { "Write Result To DriverIC", false });
            Param.Add(new object[] { "Safe Sensor Enable", false });
            Param.Add(new object[] { "X Direction Reversal", false });
            Param.Add(new object[] { "Y Direction Reversal", false });
            Param.Add(new object[] { "Barcode Overwrite Disable", false });
            Param.Add(new object[] { "Skip Vision Check", false });
            Param.Add(new object[] { "Hide All Graph", false });
            Param.Add(new object[] { "Phase Interpolation", false });
            Param.Add(new object[] { "DFT PhaseMargin", false });
            Param.Add(new object[] { "Reset EPA", false });
            Param.Add(new object[] { "Check ChipID", false });
            Param.Add(new object[] { "Save Hall Test RawData", false });

            Read();
        }
        public override void Save(string filePath = "")
        { 
            int index = 0;
            Param[index++][1] = m_bPasswordOn;
            Param[index++][1] = m_bScreenCapture;
            Param[index++][1] = m_bSaveRawData;
            Param[index++][1] = m_bFixedCenter;
            Param[index++][1] = m_bUpdateHallCalData;
            Param[index++][1] = m_bSaveResIndexCount;
            Param[index++][1] = m_bSkipWriteHallPolarity;
            Param[index++][1] = m_bWriteResultToDriverIC;
            Param[index++][1] = m_bSafeSensor;
            Param[index++][1] = m_bXDirReverse;
            Param[index++][1] = m_bYDirReverse;
            Param[index++][1] = m_bBarcodeOverwriteDisable;
            Param[index++][1] = m_bSkipVisionCheck;
            Param[index++][1] = m_bHideYieldGraph;
            Param[index++][1] = m_bPhaseInterpolation;
            Param[index++][1] = m_bDFTphasemargin;
            Param[index++][1] = m_bEPAReset;
            Param[index++][1] = m_bCheckChipID;
            Param[index++][1] = m_bSaveHallTestData;

            List.Clear();
            for (int i = 0; i < Param.Count; i++)
            {
                List.Add(string.Format("{0}\t{1}", Param[i][0], Param[i][1]));
            }
            FIO.SetTextLine(FilePath, List);

            bChange = false;

            Read();
        }
        public override void SetParam()
        {
            base.SetParam();
            int index = 0;
            m_bPasswordOn = Convert.ToBoolean(Param[index++][1]);
            m_bScreenCapture = Convert.ToBoolean(Param[index++][1]);
            m_bSaveRawData = Convert.ToBoolean(Param[index++][1]);
            m_bFixedCenter = Convert.ToBoolean(Param[index++][1]);
            m_bUpdateHallCalData = Convert.ToBoolean(Param[index++][1]);
            m_bSaveResIndexCount = Convert.ToBoolean(Param[index++][1]);
            m_bSkipWriteHallPolarity = Convert.ToBoolean(Param[index++][1]);
            m_bWriteResultToDriverIC = Convert.ToBoolean(Param[index++][1]);
            m_bSafeSensor = Convert.ToBoolean(Param[index++][1]);
            m_bXDirReverse = Convert.ToBoolean(Param[index++][1]);
            m_bYDirReverse = Convert.ToBoolean(Param[index++][1]);
            m_bBarcodeOverwriteDisable = Convert.ToBoolean(Param[index++][1]);
            m_bSkipVisionCheck = Convert.ToBoolean(Param[index++][1]);
            m_bHideYieldGraph = Convert.ToBoolean(Param[index++][1]);
            m_bPhaseInterpolation = Convert.ToBoolean(Param[index++][1]);
            m_bDFTphasemargin = Convert.ToBoolean(Param[index++][1]);
            m_bEPAReset = Convert.ToBoolean(Param[index++][1]);
            m_bCheckChipID = Convert.ToBoolean(Param[index++][1]);
            m_bSaveHallTestData = Convert.ToBoolean(Param[index++][1]);
            if (bChange) Save();
        }
    }
    public class Model : BaseRecipe
    {
        public string Maker;
        public string RevisionNo;
        public string TesterNo;
        public string ProductLine;
        public string Supplier;
        public string MCNo;
        public string DriverIC;
        public string lotID;
        public bool IsVirtual = false;
        public string SafeMode = "0";
        public string LotID
        {
            get { return lotID; }
            set
            {
                if (value != lotID)
                { lotID = value; IsLotChanged = true; }
                else IsLotChanged = false;
            }
        }

        public List<string> MakerList = new List<string>();
        public List<string> ICList = new List<string>();
        public List<string> SupplierList = new List<string>();

        public bool IsLotChanged = false;

        public delegate void Lot_Changed(string Lotname);
        public event Lot_Changed Changed = null;

        public Model()
        {
            Init();  
        }
        public override void Init()
        {
            base.Init();
            FilePath = FIO.RootDir + "Model.txt";
            Param.Add(new object[] { "Maker", "M (SEMCO NPD)" });
            Param.Add(new object[] { "RevisionNo", "0" });
            Param.Add(new object[] { "TesterNo", "0" });
            Param.Add(new object[] { "ProductLine", "Test" });
            Param.Add(new object[] { "Supplier", "Optrontech" });
            Param.Add(new object[] { "MCNo", "0" });
            Param.Add(new object[] { "DriverIC", "SEM1215SA" });
            Param.Add(new object[] { "lotID", "Test" });
            Param.Add(new object[] { "Vitual", false });
            Param.Add(new object[] { "SafeMode", "0" });

            MakerList.Add("M (SEMCO NPD)");
            MakerList.Add("S (SEMV)");

            SupplierList.Add("Optrontech");
            SupplierList.Add("Crystal Optics");

            ICList.Add("SEM1215SA");
            ICList.Add("SEM1215S");

            Read();
        }
        public override void Save(string filePath = "")
        {
            int index = 0;
            Param[index++][1] = Maker;
            Param[index++][1] = RevisionNo;
            Param[index++][1] = TesterNo;
            Param[index++][1] = ProductLine;
            Param[index++][1] = Supplier;
            Param[index++][1] = MCNo;
            Param[index++][1] = DriverIC;
            Param[index++][1] = LotID;
            Param[index++][1] = IsVirtual;
            Param[index++][1] = SafeMode;

            List.Clear();
            for (int i = 0; i < Param.Count; i++)
            {
                List.Add(string.Format("{0}\t{1}", Param[i][0], Param[i][1]));
            }
            FIO.SetTextLine(FilePath, List);

            bChange = false;

            Read();
        }
        public override void SetParam()
        {
            base.SetParam(); 
            int index = 0;
            Maker = Param[index++][1].ToString();
            RevisionNo = Param[index++][1].ToString();
            TesterNo = Param[index++][1].ToString();
            ProductLine = Param[index++][1].ToString();
            Supplier = Param[index++][1].ToString();
            MCNo = Param[index++][1].ToString();
            DriverIC = Param[index++][1].ToString();
            LotID = Param[index++][1].ToString();
            if (Param[index++][1].ToString().Contains("T")) IsVirtual = true;
            else IsVirtual = false;
            SafeMode = Param[index++][1].ToString();

            if (bChange) Save();
        }
        public void LotChanged(string LotName)
        {
            Changed?.Invoke(LotName);
        }
    }
    public class CurrentPath : BaseRecipe
    {
        public string ConditionName;
        public string SpecName;
        public string FWPath;
        public string PIDPath;
        public string MESPath;
        public string ModelPath;

        public CurrentPath()
        {
            Init();
        }
        public override void Init()
        {
            base.Init();
            FilePath = FIO.RootDir + "CurrentPath.txt";
            List.Add(ConditionName = "Default.rcp");
            List.Add(SpecName = "Default.spc");
            List.Add(FWPath = "FWPath.bin");
            List.Add(PIDPath = "PIDPath.txt");
            List.Add(MESPath = "MESPath.txt");
            List.Add(ModelPath = "SS_1.csv");
            Read();
        }
        public override void Read(string filePath = "")
        {
            if (!File.Exists(FilePath))
            {
                FIO.SetTextLine(FilePath, List);
            }
            else
            {
                ReadList = FIO.GetTextAll(FilePath);
                for (int i = 0; i < ReadList.Count; i++)
                    List[i] = ReadList[i];
            }
            SetParam();
        }
        public override void Save(string Path = "")
        {
            List.Clear();
            List.Add(ConditionName);
            List.Add(SpecName);
            List.Add(FWPath);
            List.Add(PIDPath);
            List.Add(MESPath);
            List.Add(ModelPath);
            FIO.SetTextLine(FilePath, List);
        }
        public override void SetParam()
        {
            base.SetParam();

            int index = 0;
            ConditionName = List[index++];
            SpecName = List[index++];
            FWPath = List[index++];
            PIDPath = List[index++];
            MESPath = List[index++];
            ModelPath = List[index++];

            if (ReadList.Count != List.Count)
                Save();
        }
    }
    public class VisionParam : BaseRecipe
    {
        public class ROI
        {
            public int X;
            public int Y;
        }
        public List<ROI> Roi = new List<ROI>();
        public List<double> XYScale = new List<double>();
        public List<double> XMeasureScale = new List<double>();
        public List<double> YMeasureScale = new List<double>();
        public VisionParam()
        {
            Init();
        }
        public override void Init()
        {
            base.Init();
            FilePath = FIO.RootDir + "VisionParam.txt";
            Roi.Add(new ROI());

            XYScale.Add(1);
            XYScale.Add(1);

            XMeasureScale.Add(1);
            XMeasureScale.Add(1);
            YMeasureScale.Add(1);
            YMeasureScale.Add(1);

            List.Add("ROIX1\t0");
            List.Add("ROIY1\t0");

            List.Add("XYScale L\t1");
            List.Add("XYScale R\t1");

            List.Add("XMeasureScale L\t0");
            List.Add("XMeasureScale R\t0");
            List.Add("YMeasureScale L\t0");
            List.Add("YMeasureScale R\t0");

            Read();
        }
        public override void Read(string filePath = "")
        {
            if (!File.Exists(FilePath))
            {
                FIO.SetTextLine(FilePath, List);
            }
            else
            {
                ReadList = FIO.GetTextAll(FilePath);
                for (int i = 0; i < ReadList.Count; i++)
                    List[i] = ReadList[i];
            }
            SetParam();
        }
        public override void Save(string filePath = "")
        {
            List.Clear();
            List.Add(string.Format("ROIX1\t{0}", Roi[0].X));
            List.Add(string.Format("ROIY1\t{0}", Roi[0].Y));
            List.Add(string.Format("XYScale L\t{0}", XYScale[0]));
            List.Add(string.Format("XYScale R\t{0}", XYScale[1]));
            List.Add(string.Format("XMeasureScale L\t{0}", XMeasureScale[0]));
            List.Add(string.Format("XMeasureScale R\t{0}", XMeasureScale[1]));
            List.Add(string.Format("YMeasureScale L\t{0}", YMeasureScale[0]));     
            List.Add(string.Format("YMeasureScale R\t{0}", YMeasureScale[1]));
            FIO.SetTextLine(FilePath, List);
        }

        public override void SetParam()
        {
            base.SetParam();
            int index = 0;
            Roi[0].X = int.Parse(List[index++].Split('\t')[1]);
            Roi[0].Y = int.Parse(List[index++].Split('\t')[1]);
            XYScale[0] = double.Parse(List[index++].Split('\t')[1]);
            XYScale[1] = double.Parse(List[index++].Split('\t')[1]);
            XMeasureScale[0] = double.Parse(List[index++].Split('\t')[1]);
            XMeasureScale[1] = double.Parse(List[index++].Split('\t')[1]);
            YMeasureScale[0] = double.Parse(List[index++].Split('\t')[1]);
            YMeasureScale[1] = double.Parse(List[index++].Split('\t')[1]);


            if (ReadList.Count != List.Count)
                Save();
        }
    }
    public class CurrentLed : BaseRecipe
    {
        public List<double> Val = new List<double>();

        public CurrentLed()
        {
            Init();
        }
        public override void Init()
        {
            base.Init();
            FilePath = FIO.RootDir + "CurrentLed.txt";

            for (int i = 0; i < 4; i++)
                Val.Add(0);
            for (int i = 0; i < 4; i++)
                List.Add("2.7");
            Read();
        }
        public override void Read(string filePath = "")
        {
            if (!File.Exists(FilePath))
            {
                FIO.SetTextLine(FilePath, List);
            }
            else
            {
                ReadList = FIO.GetTextAll(FilePath);
                for (int i = 0; i < ReadList.Count; i++)
                    List[i] = ReadList[i];
            }
            SetParam();
        }
        public override void Save(string filePath = "")
        {
            List.Clear();
            for (int i = 0; i < 4; i++)
                List.Add(Val[i].ToString("F2"));

            FIO.SetTextLine(FilePath, List);
        }
        public override void SetParam()
        {
            base.SetParam();
            for (int i = 0; i < 4; i++)
                Val[i] = double.Parse(List[i]);

            if (ReadList.Count != List.Count)
                Save();
        }
    }

}
