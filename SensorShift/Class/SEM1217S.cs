﻿using Dln;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActroLib
{
    public class SEM1217S : DriverIC
    {
        public Recipe Rcp { get { return PROCESS.Rcp; } }
        public Process Process { get { return PROCESS.Process; } }
        //OIS Mode Set =============================
        public byte STATE_READY = 0x01;
        public short REG_OIS_CTRL = 0x0000;
        public short REG_OIS_STS = 0x0001;
        public short REG_OIS_MODE = 0x0002;
        public short REG_OIS_ERR = 0x0004;
        public short REG_AXIS_X_TARGET = 0x0774;
        public short REG_AXIS_Y_TARGET = 0x0776;
        public short REG_AXIS_R_TARGET = 0x0778;
        public short REG_CH_Y_TARGET = 0x0700;
        public short REG_CH_X1_TARGET = 0x0702;
        public short REG_CH_X2_TARGET = 0x0704;

        public byte REG_CAL_Y_TARGET = 0x005a;
        public byte REG_CAL_X1_TARGET = 0x009a;
        public byte REG_CAL_X2_TARGET = 0x00da;

        public byte OIS_ON = 0x01;
        public byte OIS_OFF = 0x00;
        public byte FIXED_MODE = 0x0B;

        //Status Monitor =============================
        public short REG_AF_CTRL = 0x0200;
        public short REG_STATMON_CTRL = 0x0B00;
        public short REG_GYRO_X = 0x0B04;
        public short REG_GYRO_Y = 0x0B06;
        public short REG_GYRO_R = 0x0B08;
        public short REG_CH_TARGET_Y = 0x0B0A;
        public short REG_CH_TARGET_X1 = 0x0B0C;
        public short REG_CH_TARGET_X2 = 0x0B0E;
        public short REG_VCM_IDAC_Y1 = 0x0B10;
        public short REG_VCM_IDAC_X1 = 0x0B12;
        public short REG_VCM_IDAC_X2 = 0x0B14;
        public short REG_SY_OUT = 0x0B16;
        public short REG_SX1_OUT = 0x0B18;
        public short REG_SX2_OUT = 0x0B1A;
        public short REG_AF_TG_MON = 0x0B1C;
        public short REG_VCM_IDAC_AF = 0x0B1E;
        public short REG_SAF_OUT = 0x0B20;
        public byte AF_ON = 0x01;
        public byte MONITOR_EN = 1;

        //Hall Cal =======================================
        public short REG_OIS_HALL = 0x0401;

        //FW Download======================================
        public int TX_SIZE_32_BYTE = 32;
        public int TX_SIZE_64_BYTE = 64;
        public int TX_SIZE_128_BYTE = 128;
        public int TX_SIZE_256_BYTE = 256;
        public int TX_BUFFER_SIZE = 256;
        public int RX_BUFFER_SIZE = 4;

        public short REG_FWUP_CTRL = 0x1000;
        public short REG_FWUP_ERR = 0x1001;
        public short REG_FWUP_CHKSUM = 0x1002;
        public short REG_APP_VER = 0x1008;
        public short REG_DATA_BUF = 0x1100;
        public byte NO_ERROR = 0x00;
        public byte RESET_REQ = 0x80;
        public byte FWUP_CTRL_32_SET = 0x01;
        public byte FWUP_CTRL_64_SET = 0x03;
        public byte FWUP_CTRL_128_SET = 0x05;
        public byte FWUP_CTRL_256_SET = 0x07;
        public int APP_FW_SIZE = (48 * 1024);

        //Epa Control======================================
        public short OIS_REG_Y_EPA_CUT_BOT = 0x0040;
        public short OIS_REG_Y_EPA_CUT_TOP = 0x0042;
        public short OIS_REG_X1_EPA_CUT_BOT = 0x0080;
        public short OIS_REG_X1_EPA_CUT_TOP = 0x0082;
        public short OIS_REG_X2_EPA_CUT_BOT = 0x00C0;
        public short OIS_REG_X2_EPA_CUT_TOP = 0x00C2;
        public short OIS_REG_EPA_CTRL = 0x0012;
        public short OIS_REG_OIS_DATAWRITE = 0x0306;

        //Measure Loop Gain ===============================
        public short REG_AF_STS = 0x0201;
        public short REG_LG_CTRL = 0x0C00;
        public short REG_LG_AMP = 0x0C01;
        public short REG_LG_FREQ = 0x0C02;
        public short REG_LG_EXEC_CYCLE = 0x0C04;
        public short REG_LG_RESULT1 = 0x0C08;
        public short REG_LG_RESULT2 = 0x0C0C;
        public short REG_LG_RESULT3 = 0x0C10;
        public short REG_LG_RESULT4 = 0x0C14;

        public byte LOOPGAIN_AMP = 15;
        public short LOOPGAIN_FREQ = 350;
        public byte LOOPGAIN_EXEC_CYCLE = 3;
        public byte LOOGAIN_CTRL_OIS_CHY = 0x01;
        public byte LOOGAIN_CTRL_OIS_CHX1 = 0x11;
        public byte LOOGAIN_CTRL_OIS_CHX2 = 0x21;

        public short LG_EN = 0x01;

        //Open Short Test ===============================
        public short OIS_REG_OPENSHORT_COIL_Y_MIN = 0x1814;
        public short OIS_REG_OPENSHORT_COIL_Y_MAX = 0x1816;
        public short OIS_REG_OPENSHORT_COIL_X1_MIN = 0x1818;
        public short OIS_REG_OPENSHORT_COIL_X1_MAX = 0x181A;
        public short OIS_REG_OPENSHORT_COIL_X2_MIN = 0x1820;
        public short OIS_REG_OPENSHORT_COIL_X2_MAX = 0x1822;
        public short OIS_REG_OPENSHORT_HALL_Y_MIN = 0x1824;
        public short OIS_REG_OPENSHORT_HALL_Y_MAX = 0x1826;
        public short OIS_REG_OPENSHORT_HALL_X1_MIN = 0x1828;
        public short OIS_REG_OPENSHORT_HALL_X1_MAX = 0x182A;
        public short OIS_REG_OPENSHORT_HALL_X2_MIN = 0x1830;
        public short OIS_REG_OPENSHORT_HALL_X2_MAX = 0x1832;
        public byte OPEN_SHORT_MODE_COIL = 0x01;
        public byte OPEN_SHORT_MODE_HALL = 0x02;
        public byte OPEN_SHORT_CH_Y = 0x01;
        public byte OPEN_SHORT_CH_X1 = 0x02;
        public byte OPEN_SHORT_CH_X2 = 0x08;
        public short OIS_REG_OPENSHORT_CTRL = 0x1811;
        public short OIS_REG_OPENSHORT_MODE = 0x1812;
        public short OIS_REG_OPENSHORT_CHANNEL = 0x1813;

        //OIS Calibrated Data Store ===============================
        public short REG_INFO_BLK_UP_CTRL = 0x0306;
        public byte OIS_INFO_EN = 0x01;
        public short ERR_ODI = 0x0040;

        public SEM1217S(bool isVirtual) : base(isVirtual)
        {
            SlaveAddr = 0x61;
        }
        public void WaitWork()
        {
            while (m_bOccupied) Thread.Sleep(1);
            m_bOccupied = true;
            if (IsVirtual)
            {
                m_bOccupied = false;
            }
        }
        public void WriteByte(int ch, int memAddr, byte data)
        {
            WaitWork();
            try
            {
                byte[] sData = new byte[1] { data };
                DLNi2c[ch].Write(SlaveAddr, 2, memAddr, sData);
                m_bOccupied = false;
            }
            catch (Exception)
            {
                m_bOccupied = false;
            }
        }
        public void Write2Byte(int ch, int memAddr, short data)
        {
            WaitWork();
            try
            {
                byte[] sData = BitConverter.GetBytes(data);
                DLNi2c[ch].Write(SlaveAddr, 2, memAddr, sData);
                m_bOccupied = false;
            }
            catch (Exception)
            {
                m_bOccupied = false;
            }
        }
        public void Write4Byte(int ch, int memAddr, int data)
        {
            WaitWork();
            try
            {
                byte[] sData = BitConverter.GetBytes(data);
                DLNi2c[ch].Write(SlaveAddr, 2, memAddr, sData);
                m_bOccupied = false;
            }
            catch (Exception)
            {
                m_bOccupied = false;
            }
        }
        public void WriteAnyByte(int ch, int memAddr, byte[] data)
        {
            WaitWork();
            try
            {
                DLNi2c[ch].Write(SlaveAddr, 2, memAddr, data);
                m_bOccupied = false;
            }
            catch (Exception)
            {
                m_bOccupied = false;
            }
        }
        public byte ReadByte(int ch, int memAddr)
        {
            WaitWork();
            try
            {
                byte[] data = new byte[1];
                DLNi2c[ch].Read(SlaveAddr, 2, memAddr, data);
                m_bOccupied = false;
                return data[0];
            }
            catch (Exception)
            {
                m_bOccupied = false;
                return 0;
            }
        }
        public short Read2Byte(int ch, int memAddr)
        {
            WaitWork();
            try
            {
                byte[] data = new byte[2];
                DLNi2c[ch].Read(SlaveAddr, 2, memAddr, data);
                m_bOccupied = false;
                return BitConverter.ToInt16(data, 0);
            }
            catch (Exception)
            {
                m_bOccupied = false;
                return 0;
            }
        }
        public int Read4Byte(int ch, int memAddr)
        {
            WaitWork();
            try
            {
                byte[] data = new byte[4];
                DLNi2c[ch].Read(SlaveAddr, 2, memAddr, data);
                m_bOccupied = false;
                return BitConverter.ToInt32(data, 0);
            }
            catch (Exception)
            {
                m_bOccupied = false;
                return 0;
            }
        }
        public byte[] ReadAnyByte(int ch, int memAddr, int dataSize)
        {
            WaitWork();
            try
            {
                byte[] data = new byte[dataSize];
                DLNi2c[ch].Read(SlaveAddr, 2, memAddr, data);
                m_bOccupied = false;
                return data;
            }
            catch (Exception)
            {
                m_bOccupied = false;
                return null;
            }
        }
        public bool WaitCheck(int ch, int memAddr, int memCnt, int target, long timeOut)
        {
            int rData = 0;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                switch (memCnt)
                {
                    case 1: rData = ReadByte(ch, memAddr); break;
                    case 2: rData = Read2Byte(ch, memAddr); break;
                    case 4: rData = Read4Byte(ch, memAddr); break;
                }
                if (rData == target) return true;

                if (sw.ElapsedMilliseconds > timeOut) return false;
            }
        }
        public void FWVersionRead(int ch)
        {
            int Version = Read4Byte(ch, 0x1008);
            Log.AddLog(ch, string.Format("FW Version = {0}", Version));
        }
        public void OIS_On(int ch, bool isOn = true)
        {
            string strOn = "On";
            if (isOn)
                WriteByte(ch, REG_OIS_CTRL, 0x01);
            else
            {
                WriteByte(ch, REG_OIS_CTRL, 0x00);
                strOn = "Off";
            }
            byte state = ReadByte(ch, 0x0000);
            Log.AddLog(ch, string.Format("OIS {0}, State : {1}", strOn, state));
        }
        public void SetFixMode(int ch)
        {
            byte byRet = ReadByte(ch, REG_OIS_STS);
            if (byRet != STATE_READY)
            {
                OIS_On(ch, false);
            }
            ReadByte(ch, REG_OIS_STS);

            WriteByte(ch, REG_OIS_MODE, FIXED_MODE);

            OIS_On(ch, true);
        }
        public void Move(int ch, int axis, short target)
        {
            switch (axis)
            {
                case 0:
                case 3: // Step
                    Write2Byte(ch, REG_AXIS_X_TARGET, target);
                    break;
                case 1:
                case 5: // Step
                    Write2Byte(ch, REG_AXIS_Y_TARGET, target);
                    break;
                case 2:
                    Write2Byte(ch, REG_AXIS_R_TARGET, target);
                    break;
            }
        }
        public void MoveCal(int ch, int axis, short target)
        {
            switch (axis)
            {
                case 0: //X
                    Write2Byte(ch, REG_CAL_X1_TARGET, target);
                    Write2Byte(ch, REG_CAL_X2_TARGET, target);
                    break;
                case 1: //Y
                    Write2Byte(ch, REG_CAL_Y_TARGET, target);
                    break;
            }
        }
        public short ReadHall(int ch, int axis)
        {
            WriteByte(ch, REG_STATMON_CTRL, 0x01);
            byte state = ReadByte(ch, REG_STATMON_CTRL);

            switch (axis)
            {
                case 0:
                    return Read2Byte(ch, REG_SX1_OUT);
                case 1:
                    return Read2Byte(ch, REG_SX2_OUT);
                case 2:
                    return Read2Byte(ch, REG_SY_OUT);
            }
            return -1;
        }
        public bool HallCalStart(int ch)
        {
            WriteByte(ch, REG_OIS_HALL, 0x01);
            if (!WaitCheck(ch, REG_OIS_HALL, 1, 0x00, 10000)) return false;
            return true;
        }
        public HallParam HallCalRead(int ch, int axis)
        {
            HallParam result = new HallParam();
            switch (axis)
            {
                case (int)AO_TYPE._CLX:
                    result.gain = Read2Byte(ch, 0x0480);
                    result.Offset = Read2Byte(ch, 0x0481);
                    result.Bias = Read2Byte(ch, 0x0482);
                    result.min = Read2Byte(ch, 0x0484);
                    result.max = Read2Byte(ch, 0x0486);
                    result.mid = Read2Byte(ch, 0x0488);
                    break;
                case (int)AO_TYPE._CLY:
                    result.Offset = Read2Byte(ch, 0x0441);
                    result.Bias = Read2Byte(ch, 0x0442);
                    result.min = Read2Byte(ch, 0x0444);
                    result.max = Read2Byte(ch, 0x0446);
                    result.mid = Read2Byte(ch, 0x0448);
                    break;
            }
            return result;
        }
        public byte CheckIfOISSTS_IDLE(int ch, bool bCheck0000 = true)
        {
            byte[] wBuffer = new Byte[1];
            wBuffer[0] = 0;
            if (bCheck0000)
                WriteAnyByte(ch, 0x0, wBuffer);

            int memAddr = 0x0001;
            byte lToBe = 0x01;

            Thread.Sleep(10);
            byte result;
            result = RetryRead(ch, memAddr, lToBe, 10);

            if (result != lToBe)
            {
                if (bCheck0000)
                {
                    WriteAnyByte(ch, 0x0, wBuffer);
                }

                Thread.Sleep(100);
                result = RetryRead(ch, memAddr, lToBe, 10);
            }
            if (result != lToBe)
            {
                return result;
            }

            return 0;
        }
        public byte RetryRead(int ch, int memAddr, byte toBe, int maxRetry = 1)
        {
            byte[] buffer = new byte[1];
            int MaxRetry = maxRetry;
            int iRetry = 0;
            try
            {
                while (iRetry++ < MaxRetry)
                {
                    Thread.Sleep(20);
                    while (m_bOccupied)
                    {
                        Thread.Sleep(1);
                    }
                    m_bOccupied = true;
                    if (IsVirtual)
                    {
                        m_bOccupied = false;
                        return buffer[0];
                    }
                    DLNi2c[ch].Read(m_DrvIC_Addr, 2, memAddr, buffer);
                    m_bOccupied = false;
                    if (buffer[0] == toBe)
                        return buffer[0];

                }
                return buffer[0];
            }
            catch //(Exception ex)
            {
                // MessageBox.Show("Retry Read : " + ch.ToString() + " " + ex.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                m_bOccupied = false;
                return 0xFF;
            }
        }
        public void EPAControl(int ch)
        {
            if (ReadByte(ch, REG_OIS_STS) != STATE_READY)
            {
                WriteByte(ch, REG_OIS_CTRL, OIS_OFF); /* Write 1 Byte to REG_OIS_CTRL */
            }

            Write2Byte(ch, OIS_REG_Y_EPA_CUT_BOT, (short)Rcp.iYEPACutBottom);
            Log.AddLog(ch, string.Format("Y axis EPA Bottom Write {0}, Read {1}", Rcp.iYEPACutBottom, Read2Byte(ch, OIS_REG_Y_EPA_CUT_BOT)));

            Write2Byte(ch, OIS_REG_Y_EPA_CUT_TOP, (short)Rcp.iYEPACutTop);
            Log.AddLog(ch, string.Format("Y axis EPA Top Write {0}, Read {1}", Rcp.iYEPACutBottom, Read2Byte(ch, OIS_REG_Y_EPA_CUT_TOP)));

            Write2Byte(ch, OIS_REG_X1_EPA_CUT_BOT, (short)Rcp.iXEPACutBottom);
            Log.AddLog(ch, string.Format("X1 axis EPA Bottom Write {0}, Read {1}", Rcp.iXEPACutBottom, Read2Byte(ch, OIS_REG_X1_EPA_CUT_BOT)));

            Write2Byte(ch, OIS_REG_X1_EPA_CUT_TOP, (short)Rcp.iXEPACutTop);
            Log.AddLog(ch, string.Format("X1 axis EPA Top Write {0}, Read {1}", Rcp.iXEPACutTop, Read2Byte(ch, OIS_REG_X1_EPA_CUT_TOP)));

            Write2Byte(ch, OIS_REG_X2_EPA_CUT_BOT, (short)Rcp.iXEPACutBottom);
            Log.AddLog(ch, string.Format("X2 axis EPA Bottom Write {0}, Read {1}", Rcp.iXEPACutBottom, Read2Byte(ch, OIS_REG_X2_EPA_CUT_BOT)));

            Write2Byte(ch, OIS_REG_X2_EPA_CUT_TOP, (short)Rcp.iXEPACutTop);
            Log.AddLog(ch, string.Format("X2 axis EPA Top Write {0}, Read {1}", Rcp.iXEPACutTop, Read2Byte(ch, OIS_REG_X2_EPA_CUT_TOP)));

            WriteByte(ch, OIS_REG_EPA_CTRL, 0x01); /* OIS EPA apply */
            WriteByte(ch, OIS_REG_OIS_DATAWRITE, 0x01); /* Save OIS EPA cut into nvm*/
            Log.AddLog(ch, string.Format("OIS EPA apply & Save =="));
        }
        public void EPAReset(int ch)
        {
            if (ReadByte(ch, REG_OIS_STS) != STATE_READY)
            {
                WriteByte(ch, REG_OIS_CTRL, OIS_OFF); /* Write 1 Byte to REG_OIS_CTRL */
            }
            Write2Byte(ch, OIS_REG_Y_EPA_CUT_BOT, 0);
            Log.AddLog(ch, string.Format("Y axis EPA Bottom Write {0}, Read {1}", 0, Read2Byte(ch, OIS_REG_Y_EPA_CUT_BOT)));

            Write2Byte(ch, OIS_REG_Y_EPA_CUT_TOP, 0);
            Log.AddLog(ch, string.Format("Y axis EPA Top Write {0}, Read {1}", 0, Read2Byte(ch, OIS_REG_Y_EPA_CUT_TOP)));

            Write2Byte(ch, OIS_REG_X1_EPA_CUT_BOT, 0);
            Log.AddLog(ch, string.Format("X1 axis EPA Bottom Write {0}, Read {1}", 0, Read2Byte(ch, OIS_REG_X1_EPA_CUT_BOT)));

            Write2Byte(ch, OIS_REG_X1_EPA_CUT_TOP, 0);
            Log.AddLog(ch, string.Format("X1 axis EPA Top Write {0}, Read {1}", 0, Read2Byte(ch, OIS_REG_X1_EPA_CUT_TOP)));

            Write2Byte(ch, OIS_REG_X2_EPA_CUT_BOT, 0);
            Log.AddLog(ch, string.Format("X2 axis EPA Bottom Write {0}, Read {1}", 0, Read2Byte(ch, OIS_REG_X2_EPA_CUT_BOT)));

            Write2Byte(ch, OIS_REG_X2_EPA_CUT_TOP, 0);
            Log.AddLog(ch, string.Format("X2 axis EPA Top Write {0}, Read {1}", 0, Read2Byte(ch, OIS_REG_X2_EPA_CUT_TOP)));

            WriteByte(ch, OIS_REG_EPA_CTRL, 0x01); /* OIS EPA apply */
            WriteByte(ch, OIS_REG_OIS_DATAWRITE, 0x01); /* Save OIS EPA cut into nvm*/
            Log.AddLog(ch, string.Format("OIS EPA apply & Save =="));
        }
        public void MeasureLoopGain(int ch, int axis)
        {
            if (ReadByte(ch, REG_OIS_STS) != STATE_READY)
            {
                WriteByte(ch, REG_OIS_CTRL, OIS_OFF); /* Write 1 Byte to REG_OIS_CTRL */
            }

            int startFrq = 0;

            switch (axis)
            {
                case 0: //X1
                    Log.AddLog(ch, string.Format("Measure Loop Gain X1 Axis Start == "));
                    startFrq = Rcp.iXChirpFrom;
                    break;
                case 1: //X2
                    Log.AddLog(ch, string.Format("Measure Loop Gain X2 Axis Start == "));
                    startFrq = Rcp.iXChirpFrom;
                    break;
                case 2: //Y
                    Log.AddLog(ch, string.Format("Measure Loop Gain Y Axis Start == "));
                    startFrq = Rcp.iYChirpFrom;
                    break;
            }
            bool isFindPm = false;
            bool isFindGain = false;
            while (true)
            {
                switch (axis)
                {
                    case 0: //X1
                        WriteByte(ch, REG_LG_AMP, (byte)Rcp.iXAmplitude);
                        Write2Byte(ch, REG_LG_FREQ, (short)startFrq);
                        WriteByte(ch, REG_LG_EXEC_CYCLE, (byte)Rcp.iFRAloop);
                        WriteByte(ch, REG_LG_CTRL, LOOGAIN_CTRL_OIS_CHX1);
                        break;
                    case 1: //X2
                        WriteByte(ch, REG_LG_AMP, (byte)Rcp.iXAmplitude);
                        Write2Byte(ch, REG_LG_FREQ, (short)startFrq);
                        WriteByte(ch, REG_LG_EXEC_CYCLE, (byte)Rcp.iFRAloop);
                        WriteByte(ch, REG_LG_CTRL, LOOGAIN_CTRL_OIS_CHX2);
                        break;
                    case 2: //Y 
                        WriteByte(ch, REG_LG_AMP, (byte)Rcp.iYAmplitude);
                        Write2Byte(ch, REG_LG_FREQ, (short)startFrq);
                        WriteByte(ch, REG_LG_EXEC_CYCLE, LOOPGAIN_EXEC_CYCLE);
                        WriteByte(ch, REG_LG_CTRL, LOOGAIN_CTRL_OIS_CHY);
                        break;
                }

                Stopwatch sw = new Stopwatch();
                sw.Start();
                while (true)
                {
                    Thread.Sleep(50);
                    byte data = ReadByte(ch, REG_LG_CTRL);
                    byte flag = 0;
                    switch (axis)
                    {
                        case 0: //X1
                            flag = 0x10;
                            break;
                        case 1: //X2
                            flag = 0x20;
                            break;
                        case 2: //Y 
                            flag = 0x00;
                            break;
                    }
                    if (data == flag) break;

                    if (sw.ElapsedMilliseconds > 5000)
                    {
                        Log.AddLog(ch, string.Format("Loop Gain Time Out =="));
                        break;
                    }
                }
                sw.Stop();
                Thread.Sleep(1000);

                short ret = Read2Byte(ch, REG_LG_CTRL);
                double inputSine = Read4Byte(ch, REG_LG_RESULT1);
                double inputCosine = Read4Byte(ch, REG_LG_RESULT2);
                double outputSine = Read4Byte(ch, REG_LG_RESULT3);
                double outputCosine = Read4Byte(ch, REG_LG_RESULT4);

                Log.AddLog(ch, string.Format("REG_LG_RESULT : {0}, {1}, {2}, {3}", inputSine, inputCosine, outputSine, outputCosine));

                double[] ampDft = new double[2];
                double[] phaseDft = new double[2];

                ampDft[0] = Math.Sqrt(Math.Pow(inputSine, 2) + Math.Pow(inputCosine, 2));

                ampDft[1] = Math.Sqrt(Math.Pow(outputSine, 2) + Math.Pow(outputCosine, 2));

                phaseDft[0] = Math.Atan2(inputSine, inputCosine) * 180 / Math.PI;

                phaseDft[1] = Math.Atan2(outputSine, outputCosine) * 180 / Math.PI;

                Log.AddLog(ch, string.Format("ampDft[0] : {0:0.0}, ampDft[1] : {1:0.0}, phaseDft[0] : {2:0.0}, phaseDft[1] : {3:0.0}", ampDft[0], ampDft[1], phaseDft[0], phaseDft[1]));

                double gain = 20 * Math.Log10((double)ampDft[1] / (double)(ampDft[0]));

                double phase = 180 - (phaseDft[1] - phaseDft[0]);

                Log.AddLog(ch, string.Format("Frq : {0}, Gain : {1:0.00}, Phase : {2:0.00}", startFrq, gain, phase));

                Log.AddLog(ch, string.Format("================"));

                startFrq = startFrq - Rcp.iFRAstep;

                if (axis == 0 || axis == 1) 
                {
                    if (startFrq <= Rcp.iXChirpTo) break;
                }
                else 
                {
                    if (startFrq <= Rcp.iYChirpTo) break;
                }

                if(gain > 0 && !isFindPm)
                {
                    Process.LG_Result[ch].pmFreq = startFrq;
                    Process.LG_Result[ch].phaseMargin = phase;
                    isFindPm = true;
                }

                if (phase > 0 && !isFindGain)
                {
                    Process.LG_Result[ch].maxDiffGain = gain;
                    isFindGain = true;
                }
            }

        }
        public void MeasureLoop10HzGain(int ch, int axis)
        {
            if (ReadByte(ch, REG_OIS_STS) != STATE_READY)
            {
                WriteByte(ch, REG_OIS_CTRL, OIS_OFF); /* Write 1 Byte to REG_OIS_CTRL */
            }

            switch (axis)
            {
                case 0: //X1
                    Log.AddLog(ch, string.Format("Measure Loop 10Hz Gain X1 Axis Start == "));
                    break;
                case 1: //X2
                    Log.AddLog(ch, string.Format("Measure Loop 10Hz Gain X2 Axis Start == "));
                    break;
                case 2: //Y
                    Log.AddLog(ch, string.Format("Measure Loop 10Hz Gain Y Axis Start == "));
                    break;
            }

            switch (axis)
            {
                case 0: //X1
                    WriteByte(ch, REG_LG_AMP, (byte)Rcp.i10HzXAmp);
                    Write2Byte(ch, REG_LG_FREQ, 10);
                    WriteByte(ch, REG_LG_EXEC_CYCLE, (byte)Rcp.iFRAloop);
                    WriteByte(ch, REG_LG_CTRL, LOOGAIN_CTRL_OIS_CHX1);
                    break;
                case 1: //X2
                    WriteByte(ch, REG_LG_AMP, (byte)Rcp.i10HzXAmp);
                    Write2Byte(ch, REG_LG_FREQ, 10);
                    WriteByte(ch, REG_LG_EXEC_CYCLE, (byte)Rcp.iFRAloop);
                    WriteByte(ch, REG_LG_CTRL, LOOGAIN_CTRL_OIS_CHX2);
                    break;
                case 2: //Y 
                    WriteByte(ch, REG_LG_AMP, (byte)Rcp.i10HzYAmp);
                    Write2Byte(ch, REG_LG_FREQ, 10);
                    WriteByte(ch, REG_LG_EXEC_CYCLE, LOOPGAIN_EXEC_CYCLE);
                    WriteByte(ch, REG_LG_CTRL, LOOGAIN_CTRL_OIS_CHY);
                    break;
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                Thread.Sleep(50);
                byte data = ReadByte(ch, REG_LG_CTRL);
                byte flag = 0;
                switch (axis)
                {
                    case 0: //X1
                        flag = 0x10;
                        break;
                    case 1: //X2
                        flag = 0x20;
                        break;
                    case 2: //Y 
                        flag = 0x00;
                        break;
                }
                if (data == flag) break;

                if (sw.ElapsedMilliseconds > 5000)
                {
                    Log.AddLog(ch, string.Format("Loop Gain Time Out =="));
                    break;
                }
            }
            sw.Stop();
            Thread.Sleep(1000);

            short ret = Read2Byte(ch, REG_LG_CTRL);
            double inputSine = Read4Byte(ch, REG_LG_RESULT1);
            double inputCosine = Read4Byte(ch, REG_LG_RESULT2);
            double outputSine = Read4Byte(ch, REG_LG_RESULT3);
            double outputCosine = Read4Byte(ch, REG_LG_RESULT4);

            Log.AddLog(ch, string.Format("REG_LG_RESULT : {0}, {1}, {2}, {3}", inputSine, inputCosine, outputSine, outputCosine));

            double[] ampDft = new double[2];
            double[] phaseDft = new double[2];

            ampDft[0] = Math.Sqrt(Math.Pow(inputSine, 2) + Math.Pow(inputCosine, 2));

            ampDft[1] = Math.Sqrt(Math.Pow(outputSine, 2) + Math.Pow(outputCosine, 2));

            phaseDft[0] = Math.Atan2(inputSine, inputCosine) * 180 / Math.PI;

            phaseDft[1] = Math.Atan2(outputSine, outputCosine) * 180 / Math.PI;

            Log.AddLog(ch, string.Format("ampDft[0] : {0:0.0}, ampDft[1] : {1:0.0}, phaseDft[0] : {2:0.0}, phaseDft[1] : {3:0.0}", ampDft[0], ampDft[1], phaseDft[0], phaseDft[1]));

            double gain = 20 * Math.Log10(ampDft[1] / ampDft[0]);

            double phase = 180 - (phaseDft[1] - phaseDft[0]);

            Log.AddLog(ch, string.Format("Frq : {0}, Gain : {1:0.00}, Phase : {2:0.00}", 10, gain, phase));

            Log.AddLog(ch, string.Format("================"));

            Process.LG_Result[ch].maxDiffGain = gain;
        }
        public void Circuit_Open_Short(int ch)
        {
            byte state = ReadByte(ch, REG_OIS_STS); /* Read REG_OIS_STS */
            if (state != STATE_READY)
                OIS_On(ch, false);

            WriteByte(ch, OIS_REG_OPENSHORT_CHANNEL, (byte)(OPEN_SHORT_CH_Y | OPEN_SHORT_CH_X1 | OPEN_SHORT_CH_X2) ); /* Set test channel */
            state = ReadByte(ch, OIS_REG_OPENSHORT_CHANNEL);
            Log.AddLog(ch, "Set OPEN_SHORT_MODE_COIL mode");
            WriteByte(ch, OIS_REG_OPENSHORT_MODE, OPEN_SHORT_MODE_COIL); /* Set test mode*/
            WriteByte(ch, OIS_REG_OPENSHORT_CTRL, 1); /* Start Test and Do not save the result into nvm */

            Thread.Sleep(100);

            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_COIL_Y_MIN : {0}", Read2Byte(ch, OIS_REG_OPENSHORT_COIL_Y_MIN)));
            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_COIL_Y_MAX : {0}", Read2Byte(ch, OIS_REG_OPENSHORT_COIL_Y_MAX)));

            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_COIL_X1_MIN : {0}", Read2Byte(ch, OIS_REG_OPENSHORT_COIL_X1_MIN)));
            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_COIL_X1_MAX : {0}", Read2Byte(ch, OIS_REG_OPENSHORT_COIL_X1_MAX)));

            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_COIL_X2_MIN : {0}", Read2Byte(ch, OIS_REG_OPENSHORT_COIL_X2_MIN)));
            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_COIL_X2_MAX : {0}", Read2Byte(ch, OIS_REG_OPENSHORT_COIL_X2_MAX)));

            Log.AddLog(ch, "Set OPEN_SHORT_MODE_HALL mode");
            WriteByte(ch, OIS_REG_OPENSHORT_MODE, OPEN_SHORT_MODE_HALL); /* Set test mode*/
            WriteByte(ch, OIS_REG_OPENSHORT_CTRL, 1); /* Start Test and Do not save the result into nvm */

            Thread.Sleep(3000);

            ushort ret = 0;
            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_HALL_Y_MIN : {0}", ret = (ushort)Read2Byte(ch, OIS_REG_OPENSHORT_HALL_Y_MIN)));
            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_HALL_Y_MAX : {0}", ret = (ushort)Read2Byte(ch, OIS_REG_OPENSHORT_HALL_Y_MAX)));

            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_HALL_X1_MIN : {0}", ret = (ushort)Read2Byte(ch, OIS_REG_OPENSHORT_HALL_X1_MIN)));
            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_HALL_X1_MAX : {0}", ret = (ushort)Read2Byte(ch, OIS_REG_OPENSHORT_HALL_X1_MAX)));

            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_HALL_X2_MIN : {0}", ret = (ushort)Read2Byte(ch, OIS_REG_OPENSHORT_HALL_X2_MIN)));
            Log.AddLog(ch, string.Format("Read OIS_REG_OPENSHORT_HALL_X2_MAX : {0}", ret = (ushort)Read2Byte(ch, OIS_REG_OPENSHORT_HALL_X2_MAX)));
        }
        public void Hall_Decenter(int ch, int axis)
        {
            SetFixMode(ch);

            Log.AddLog(ch, string.Format("=== Start Single Mid Move ==="));

            double initalMid = 0;

            if (axis == 0)
            {
                Move(ch, axis, 16000);
                Thread.Sleep(2000);
                Log.AddLog(ch, string.Format("Move Mid Pos : {0}, Delay 2 Sec After X Hall : {1}", 16000, initalMid = ReadHall(ch, 0)));
            }
            else
            {
                Move(ch, axis, 16000);
                Thread.Sleep(2000);
                Log.AddLog(ch, string.Format("Move Mid Pos : {0}, Delay 2 Sec After Y Hall : {1}", 16000, initalMid = ReadHall(ch, 2)));
            }

            Log.AddLog(ch, string.Format("=== Start 1000 Step Move ==="));

            double stepHall = 0;
            double stepHallMid = 0;
            if (axis == 0)
            {
                for (int i = 0; i < 24; i++)
                {
                    short pos = (short)(4000 + 1000 * i);
                    Move(ch, axis, pos);
                    Thread.Sleep(Rcp.iDrvStepInterval);
                    Log.AddLog(ch, string.Format("Move Step Pos : {0}, X Hall : {1}", pos, stepHall = ReadHall(ch, 0)));
                    if (16000 == pos) stepHallMid = stepHall;
                }
                Log.AddLog(ch, string.Format("=== Initial Mid : {0}, Step Mid : {1}, X Hall Decenter {2}", initalMid, stepHallMid, Math.Abs(initalMid - stepHallMid)));
            }
            else
            {
                for (int i = 0; i < 24; i++)
                {
                    short pos = (short)(4000 + 1000 * i);
                    Move(ch, axis, pos);
                    Thread.Sleep(Rcp.iDrvStepInterval);
                    Log.AddLog(ch, string.Format("Move Step Pos : {0}, Y Hall : {1}", pos, stepHall = ReadHall(ch, 2)));
                    if (16000 == pos) stepHallMid = stepHall;
                }

                Log.AddLog(ch, string.Format("=== Initial Mid : {0}, Step Mid : {1}, Y Hall Decenter {2}", initalMid, stepHallMid, Math.Abs(initalMid - stepHallMid)));
            }


            Log.AddLog(ch, string.Format("================================================="));
        }
        public void Hall_Stdev(int ch, int axis)
        {
            SetFixMode(ch);

            List<double> datas = new List<double>();

            double min = 99999;
            double max = -99999;
            double ret = 0;

            if (axis == 0)
            {
                Move(ch, axis, 16000);
                Thread.Sleep(2000);
                Log.AddLog(ch, string.Format("Move Mid Pos Initial and Delay 2 Sec After Hall Read : {0}, X Hall : {1}", 16000, ret = ReadHall(ch, 0)));
            }
            else
            {
                Move(ch, axis, 16000);
                Thread.Sleep(2000);
                Log.AddLog(ch, string.Format("Move Mid Pos Initial and Delay 2 Sec After Hall Read: {0}, Y Hall : {1}", 16000, ret = ReadHall(ch, 2)));
            }

            Stopwatch sw = new Stopwatch();
            Log.AddLog(ch, string.Format("=== Start Stdev Mearsure during 1 Sec"));

            sw.Reset(); sw.Start();
            while (true)
            {
                if (axis == 0)
                {
                    Move(ch, axis, 16000);
                    Thread.Sleep(10);
                    Log.AddLog(ch, string.Format("Move Mid Pos : {0}, X Hall : {1}", 16000, ret = ReadHall(ch, 0)));
                }
                else
                {
                    Move(ch, axis, 16000);
                    Thread.Sleep(10);
                    Log.AddLog(ch, string.Format("Move Mid Pos : {0}, Y Hall : {1}", 16000, ret =  ReadHall(ch, 2)));
                }
                min = Math.Min(min, ret);
                max = Math.Max(max, ret);

                datas.Add(ret);

                if (sw.ElapsedMilliseconds > 1000) break;
            }
            double sum = 0;
            double avg = 0;
            double std = 0;
            foreach (var d in datas)
                sum += d;
            avg = sum / datas.Count();

            sum = 0;
            foreach (var d in datas)
                sum += (avg - d)*(avg - d);

            std = sum / (datas.Count() - 1);
            std = Math.Sqrt(std);

            if (axis == 0)
            {
                Log.AddLog(ch, string.Format("=====X Min Hall : {0}, X max Hall : {1}", min, max));
                Log.AddLog(ch, string.Format("=====X Avg : {0:0.00}, X StDev : {1:0.00}", avg, std));
            }
            else
            {
                Log.AddLog(ch, string.Format("=====Y Min Hall : {0}, Y max Hall : {1}", min, max));
                Log.AddLog(ch, string.Format("=====Y Avg : {0:0.00}, Y StDev : {1:0.00}", avg, std));
            }


            Log.AddLog(ch, string.Format("================================================="));
            sw.Stop();
        }
        public bool Store_Hall_Cal(int ch)
        {
            if (ch == 1) return false;
            if (ReadByte(ch, REG_OIS_STS) != STATE_READY)
            {
                WriteByte(ch, REG_OIS_CTRL, OIS_OFF); /* Write 1 Byte to REG_OIS_CTRL */
            }

            Write2Byte(ch, REG_INFO_BLK_UP_CTRL, OIS_INFO_EN);

            Thread.Sleep(1000);

            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //while (true)
            //{
            //    Thread.Sleep(50);
            //    byte data = ReadByte(ch, REG_INFO_BLK_UP_CTRL);

            //    if (data == OIS_INFO_EN) break;

            //    if (sw.ElapsedMilliseconds > 5000)
            //    {
            //        Log.AddLog(ch, string.Format("Store Hall Cal Time Out =="));
            //        break;
            //    }
            //}
            //sw.Stop();

            if (Read2Byte(ch, REG_OIS_ERR) != NO_ERROR)
            {
                Log.AddLog(ch, string.Format("Store Hall Cal Error =="));
                return false;
            }

            return true;
        }
    }
}
