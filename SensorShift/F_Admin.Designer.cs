﻿

namespace SensorShift
{
    partial class F_Admin
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F_Admin));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TPblank = new System.Windows.Forms.TableLayoutPanel();
            this.TesterSetTLP = new System.Windows.Forms.TableLayoutPanel();
            this.MakerLB = new System.Windows.Forms.Label();
            this.MCNumber = new System.Windows.Forms.TextBox();
            this.DriverICType = new System.Windows.Forms.ListBox();
            this.DriverICLB = new System.Windows.Forms.Label();
            this.BarcodeMaker = new System.Windows.Forms.ListBox();
            this.McNoLB = new System.Windows.Forms.Label();
            this.RevisionNumberLB = new System.Windows.Forms.Label();
            this.BarcodeRevNo = new System.Windows.Forms.TextBox();
            this.TesterNoLB = new System.Windows.Forms.Label();
            this.TesterNumber = new System.Windows.Forms.TextBox();
            this.ProductLB = new System.Windows.Forms.Label();
            this.PrismSupplier = new System.Windows.Forms.ListBox();
            this.ProductLine = new System.Windows.Forms.TextBox();
            this.PrismLB = new System.Windows.Forms.Label();
            this.PIDFilePathTB = new System.Windows.Forms.RichTextBox();
            this.MESPathTB = new System.Windows.Forms.RichTextBox();
            this.FWFilePathTB = new System.Windows.Forms.RichTextBox();
            this.HandlerSettingGB = new System.Windows.Forms.GroupBox();
            this.SendTCPBtn = new System.Windows.Forms.Button();
            this.SaveStartBtn = new System.Windows.Forms.Button();
            this.MyIPLB = new System.Windows.Forms.Label();
            this.SentLB = new System.Windows.Forms.Label();
            this.ReceivedLB = new System.Windows.Forms.Label();
            this.TCPSentTB = new System.Windows.Forms.TextBox();
            this.TCPReceivedTB = new System.Windows.Forms.TextBox();
            this.HostPortTB = new System.Windows.Forms.TextBox();
            this.AppLB = new System.Windows.Forms.Label();
            this.AppPortTB = new System.Windows.Forms.TextBox();
            this.HostLB = new System.Windows.Forms.Label();
            this.SpecP = new System.Windows.Forms.Panel();
            this.OpenSpecFileBtn = new System.Windows.Forms.Button();
            this.EditSpecCB = new System.Windows.Forms.CheckBox();
            this.SaveSpecFileBtn = new System.Windows.Forms.Button();
            this.SaveAsSpecFileBtn = new System.Windows.Forms.Button();
            this.TestSpecTB = new System.Windows.Forms.TextBox();
            this.CurrSpecTB = new System.Windows.Forms.TextBox();
            this.Specdgv = new System.Windows.Forms.DataGridView();
            this.CurrRecipeTB = new System.Windows.Forms.TextBox();
            this.Recipedgv = new System.Windows.Forms.DataGridView();
            this.ItemDownBtn = new System.Windows.Forms.Button();
            this.ItemUPBtn = new System.Windows.Forms.Button();
            this.ActionListBox = new System.Windows.Forms.ListBox();
            this.RecipeP = new System.Windows.Forms.Panel();
            this.SaveAsRecipeFileBtn = new System.Windows.Forms.Button();
            this.SaveRecipeFileBtn = new System.Windows.Forms.Button();
            this.OpenRecipeFileBtn = new System.Windows.Forms.Button();
            this.EditRecipeCB = new System.Windows.Forms.CheckBox();
            this.ToDoListBox = new System.Windows.Forms.ListBox();
            this.ActionListTB = new System.Windows.Forms.TextBox();
            this.RemoveItemBtn = new System.Windows.Forms.Button();
            this.AddItemBtn = new System.Windows.Forms.Button();
            this.ToDoListTB = new System.Windows.Forms.TextBox();
            this.TestRecipeTB = new System.Windows.Forms.TextBox();
            this.ActionToDoList = new System.Windows.Forms.Panel();
            this.FWUpdateBtn = new System.Windows.Forms.Button();
            this.PIDUpdateBtn = new System.Windows.Forms.Button();
            this.SetMESBtn = new System.Windows.Forms.Button();
            this.OptionChk19 = new System.Windows.Forms.CheckBox();
            this.OptionChk18 = new System.Windows.Forms.CheckBox();
            this.OptionChk17 = new System.Windows.Forms.CheckBox();
            this.OptionChk4 = new System.Windows.Forms.CheckBox();
            this.OptionChk5 = new System.Windows.Forms.CheckBox();
            this.OptionChk6 = new System.Windows.Forms.CheckBox();
            this.OptionChk7 = new System.Windows.Forms.CheckBox();
            this.OptionChk8 = new System.Windows.Forms.CheckBox();
            this.OptionChk9 = new System.Windows.Forms.CheckBox();
            this.OptionChk10 = new System.Windows.Forms.CheckBox();
            this.OptionChk11 = new System.Windows.Forms.CheckBox();
            this.OptionChk12 = new System.Windows.Forms.CheckBox();
            this.OptionChk13 = new System.Windows.Forms.CheckBox();
            this.OptionChk14 = new System.Windows.Forms.CheckBox();
            this.OptionChk15 = new System.Windows.Forms.CheckBox();
            this.OptionChk16 = new System.Windows.Forms.CheckBox();
            this.OptionChk1 = new System.Windows.Forms.CheckBox();
            this.OptionChk2 = new System.Windows.Forms.CheckBox();
            this.OptionChk3 = new System.Windows.Forms.CheckBox();
            this.ApplyBtn = new System.Windows.Forms.Button();
            this.OptionStateTLP = new System.Windows.Forms.TableLayoutPanel();
            this.btnSetModel = new System.Windows.Forms.Button();
            this.rtbFiducialMark = new System.Windows.Forms.RichTextBox();
            this.TesterSetTLP.SuspendLayout();
            this.HandlerSettingGB.SuspendLayout();
            this.SpecP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Specdgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recipedgv)).BeginInit();
            this.RecipeP.SuspendLayout();
            this.ActionToDoList.SuspendLayout();
            this.OptionStateTLP.SuspendLayout();
            this.SuspendLayout();
            // 
            // TPblank
            // 
            this.TPblank.ColumnCount = 1;
            this.TPblank.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TPblank.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TPblank.Location = new System.Drawing.Point(0, 918);
            this.TPblank.Name = "TPblank";
            this.TPblank.RowCount = 1;
            this.TPblank.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TPblank.Size = new System.Drawing.Size(1904, 60);
            this.TPblank.TabIndex = 145;
            // 
            // TesterSetTLP
            // 
            this.TesterSetTLP.BackColor = System.Drawing.Color.SteelBlue;
            this.TesterSetTLP.ColumnCount = 2;
            this.TesterSetTLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.96183F));
            this.TesterSetTLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.03817F));
            this.TesterSetTLP.Controls.Add(this.MakerLB, 0, 0);
            this.TesterSetTLP.Controls.Add(this.MCNumber, 1, 5);
            this.TesterSetTLP.Controls.Add(this.DriverICType, 1, 6);
            this.TesterSetTLP.Controls.Add(this.DriverICLB, 0, 6);
            this.TesterSetTLP.Controls.Add(this.BarcodeMaker, 1, 0);
            this.TesterSetTLP.Controls.Add(this.McNoLB, 0, 5);
            this.TesterSetTLP.Controls.Add(this.RevisionNumberLB, 0, 1);
            this.TesterSetTLP.Controls.Add(this.BarcodeRevNo, 1, 1);
            this.TesterSetTLP.Controls.Add(this.TesterNoLB, 0, 2);
            this.TesterSetTLP.Controls.Add(this.TesterNumber, 1, 2);
            this.TesterSetTLP.Controls.Add(this.ProductLB, 0, 3);
            this.TesterSetTLP.Controls.Add(this.PrismSupplier, 1, 4);
            this.TesterSetTLP.Controls.Add(this.ProductLine, 1, 3);
            this.TesterSetTLP.Controls.Add(this.PrismLB, 0, 4);
            this.TesterSetTLP.Location = new System.Drawing.Point(1009, 2);
            this.TesterSetTLP.Name = "TesterSetTLP";
            this.TesterSetTLP.RowCount = 7;
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.TesterSetTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TesterSetTLP.Size = new System.Drawing.Size(522, 302);
            this.TesterSetTLP.TabIndex = 202;
            // 
            // MakerLB
            // 
            this.MakerLB.AutoSize = true;
            this.MakerLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MakerLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MakerLB.Location = new System.Drawing.Point(3, 0);
            this.MakerLB.Name = "MakerLB";
            this.MakerLB.Size = new System.Drawing.Size(150, 55);
            this.MakerLB.TabIndex = 150;
            this.MakerLB.Text = "Maker";
            this.MakerLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MCNumber
            // 
            this.MCNumber.BackColor = System.Drawing.Color.White;
            this.MCNumber.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MCNumber.Location = new System.Drawing.Point(159, 206);
            this.MCNumber.Name = "MCNumber";
            this.MCNumber.Size = new System.Drawing.Size(360, 25);
            this.MCNumber.TabIndex = 184;
            // 
            // DriverICType
            // 
            this.DriverICType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DriverICType.BackColor = System.Drawing.Color.LightCyan;
            this.DriverICType.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DriverICType.FormattingEnabled = true;
            this.DriverICType.ItemHeight = 18;
            this.DriverICType.Items.AddRange(new object[] {
            "SEM1215SA",
            "SEM1215S"});
            this.DriverICType.Location = new System.Drawing.Point(159, 239);
            this.DriverICType.Name = "DriverICType";
            this.DriverICType.Size = new System.Drawing.Size(360, 58);
            this.DriverICType.TabIndex = 176;
            // 
            // DriverICLB
            // 
            this.DriverICLB.AutoSize = true;
            this.DriverICLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DriverICLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DriverICLB.Location = new System.Drawing.Point(3, 234);
            this.DriverICLB.Name = "DriverICLB";
            this.DriverICLB.Size = new System.Drawing.Size(150, 68);
            this.DriverICLB.TabIndex = 175;
            this.DriverICLB.Text = "Driver IC";
            this.DriverICLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BarcodeMaker
            // 
            this.BarcodeMaker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarcodeMaker.BackColor = System.Drawing.Color.LightCyan;
            this.BarcodeMaker.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BarcodeMaker.FormattingEnabled = true;
            this.BarcodeMaker.ItemHeight = 18;
            this.BarcodeMaker.Items.AddRange(new object[] {
            "M (SEMCO NPD)",
            "S (SEMV)"});
            this.BarcodeMaker.Location = new System.Drawing.Point(159, 7);
            this.BarcodeMaker.Name = "BarcodeMaker";
            this.BarcodeMaker.Size = new System.Drawing.Size(360, 40);
            this.BarcodeMaker.TabIndex = 153;
            // 
            // McNoLB
            // 
            this.McNoLB.AutoSize = true;
            this.McNoLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.McNoLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.McNoLB.Location = new System.Drawing.Point(3, 203);
            this.McNoLB.Name = "McNoLB";
            this.McNoLB.Size = new System.Drawing.Size(150, 31);
            this.McNoLB.TabIndex = 183;
            this.McNoLB.Text = "MC No.";
            this.McNoLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RevisionNumberLB
            // 
            this.RevisionNumberLB.AutoSize = true;
            this.RevisionNumberLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RevisionNumberLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RevisionNumberLB.Location = new System.Drawing.Point(3, 55);
            this.RevisionNumberLB.Name = "RevisionNumberLB";
            this.RevisionNumberLB.Size = new System.Drawing.Size(150, 31);
            this.RevisionNumberLB.TabIndex = 151;
            this.RevisionNumberLB.Text = "Revision No.";
            this.RevisionNumberLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BarcodeRevNo
            // 
            this.BarcodeRevNo.BackColor = System.Drawing.Color.LightCyan;
            this.BarcodeRevNo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BarcodeRevNo.Location = new System.Drawing.Point(159, 58);
            this.BarcodeRevNo.Name = "BarcodeRevNo";
            this.BarcodeRevNo.Size = new System.Drawing.Size(360, 25);
            this.BarcodeRevNo.TabIndex = 152;
            // 
            // TesterNoLB
            // 
            this.TesterNoLB.AutoSize = true;
            this.TesterNoLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TesterNoLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TesterNoLB.Location = new System.Drawing.Point(3, 86);
            this.TesterNoLB.Name = "TesterNoLB";
            this.TesterNoLB.Size = new System.Drawing.Size(150, 31);
            this.TesterNoLB.TabIndex = 158;
            this.TesterNoLB.Text = "Tester No.";
            this.TesterNoLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TesterNumber
            // 
            this.TesterNumber.BackColor = System.Drawing.Color.White;
            this.TesterNumber.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TesterNumber.Location = new System.Drawing.Point(159, 89);
            this.TesterNumber.Name = "TesterNumber";
            this.TesterNumber.Size = new System.Drawing.Size(360, 25);
            this.TesterNumber.TabIndex = 156;
            // 
            // ProductLB
            // 
            this.ProductLB.AutoSize = true;
            this.ProductLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProductLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductLB.Location = new System.Drawing.Point(3, 117);
            this.ProductLB.Name = "ProductLB";
            this.ProductLB.Size = new System.Drawing.Size(150, 31);
            this.ProductLB.TabIndex = 171;
            this.ProductLB.Text = "Product Line";
            this.ProductLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PrismSupplier
            // 
            this.PrismSupplier.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PrismSupplier.BackColor = System.Drawing.Color.LightCyan;
            this.PrismSupplier.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrismSupplier.FormattingEnabled = true;
            this.PrismSupplier.ItemHeight = 18;
            this.PrismSupplier.Items.AddRange(new object[] {
            "Optrontech",
            "Crystal Optics"});
            this.PrismSupplier.Location = new System.Drawing.Point(159, 155);
            this.PrismSupplier.Name = "PrismSupplier";
            this.PrismSupplier.Size = new System.Drawing.Size(360, 40);
            this.PrismSupplier.TabIndex = 173;
            // 
            // ProductLine
            // 
            this.ProductLine.BackColor = System.Drawing.Color.White;
            this.ProductLine.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductLine.Location = new System.Drawing.Point(159, 120);
            this.ProductLine.Name = "ProductLine";
            this.ProductLine.Size = new System.Drawing.Size(360, 25);
            this.ProductLine.TabIndex = 170;
            // 
            // PrismLB
            // 
            this.PrismLB.AutoSize = true;
            this.PrismLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrismLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrismLB.Location = new System.Drawing.Point(3, 148);
            this.PrismLB.Name = "PrismLB";
            this.PrismLB.Size = new System.Drawing.Size(150, 55);
            this.PrismLB.TabIndex = 172;
            this.PrismLB.Text = "Prism Supplier";
            this.PrismLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PIDFilePathTB
            // 
            this.PIDFilePathTB.BackColor = System.Drawing.Color.LightGray;
            this.PIDFilePathTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PIDFilePathTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PIDFilePathTB.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PIDFilePathTB.Location = new System.Drawing.Point(1009, 522);
            this.PIDFilePathTB.Name = "PIDFilePathTB";
            this.PIDFilePathTB.ReadOnly = true;
            this.PIDFilePathTB.Size = new System.Drawing.Size(521, 98);
            this.PIDFilePathTB.TabIndex = 193;
            this.PIDFilePathTB.Text = "";
            this.PIDFilePathTB.Visible = false;
            // 
            // MESPathTB
            // 
            this.MESPathTB.BackColor = System.Drawing.Color.LightGray;
            this.MESPathTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MESPathTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MESPathTB.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.MESPathTB.Location = new System.Drawing.Point(1009, 682);
            this.MESPathTB.Name = "MESPathTB";
            this.MESPathTB.ReadOnly = true;
            this.MESPathTB.Size = new System.Drawing.Size(521, 98);
            this.MESPathTB.TabIndex = 190;
            this.MESPathTB.Text = "";
            this.MESPathTB.Visible = false;
            // 
            // FWFilePathTB
            // 
            this.FWFilePathTB.BackColor = System.Drawing.Color.LightGray;
            this.FWFilePathTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FWFilePathTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FWFilePathTB.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.FWFilePathTB.Location = new System.Drawing.Point(1009, 363);
            this.FWFilePathTB.Name = "FWFilePathTB";
            this.FWFilePathTB.ReadOnly = true;
            this.FWFilePathTB.Size = new System.Drawing.Size(521, 98);
            this.FWFilePathTB.TabIndex = 191;
            this.FWFilePathTB.Text = "";
            // 
            // HandlerSettingGB
            // 
            this.HandlerSettingGB.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.HandlerSettingGB.Controls.Add(this.SendTCPBtn);
            this.HandlerSettingGB.Controls.Add(this.SaveStartBtn);
            this.HandlerSettingGB.Controls.Add(this.MyIPLB);
            this.HandlerSettingGB.Controls.Add(this.SentLB);
            this.HandlerSettingGB.Controls.Add(this.ReceivedLB);
            this.HandlerSettingGB.Controls.Add(this.TCPSentTB);
            this.HandlerSettingGB.Controls.Add(this.TCPReceivedTB);
            this.HandlerSettingGB.Controls.Add(this.HostPortTB);
            this.HandlerSettingGB.Controls.Add(this.AppLB);
            this.HandlerSettingGB.Controls.Add(this.AppPortTB);
            this.HandlerSettingGB.Controls.Add(this.HostLB);
            this.HandlerSettingGB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HandlerSettingGB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HandlerSettingGB.Location = new System.Drawing.Point(1009, 784);
            this.HandlerSettingGB.Name = "HandlerSettingGB";
            this.HandlerSettingGB.Size = new System.Drawing.Size(521, 128);
            this.HandlerSettingGB.TabIndex = 207;
            this.HandlerSettingGB.TabStop = false;
            this.HandlerSettingGB.Text = "Handler Interface Port Setting";
            this.HandlerSettingGB.Visible = false;
            // 
            // SendTCPBtn
            // 
            this.SendTCPBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SendTCPBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SendTCPBtn.BackgroundImage")));
            this.SendTCPBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SendTCPBtn.FlatAppearance.BorderSize = 0;
            this.SendTCPBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SendTCPBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SendTCPBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.SendTCPBtn.Location = new System.Drawing.Point(314, 73);
            this.SendTCPBtn.Name = "SendTCPBtn";
            this.SendTCPBtn.Size = new System.Drawing.Size(201, 48);
            this.SendTCPBtn.TabIndex = 228;
            this.SendTCPBtn.Text = "Send to Client";
            this.SendTCPBtn.UseVisualStyleBackColor = false;
            // 
            // SaveStartBtn
            // 
            this.SaveStartBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SaveStartBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SaveStartBtn.BackgroundImage")));
            this.SaveStartBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SaveStartBtn.FlatAppearance.BorderSize = 0;
            this.SaveStartBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveStartBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveStartBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.SaveStartBtn.Location = new System.Drawing.Point(235, 45);
            this.SaveStartBtn.Name = "SaveStartBtn";
            this.SaveStartBtn.Size = new System.Drawing.Size(280, 23);
            this.SaveStartBtn.TabIndex = 227;
            this.SaveStartBtn.Text = "Save && Start";
            this.SaveStartBtn.UseVisualStyleBackColor = false;
            // 
            // MyIPLB
            // 
            this.MyIPLB.AutoSize = true;
            this.MyIPLB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyIPLB.Location = new System.Drawing.Point(238, 23);
            this.MyIPLB.Name = "MyIPLB";
            this.MyIPLB.Size = new System.Drawing.Size(27, 15);
            this.MyIPLB.TabIndex = 144;
            this.MyIPLB.Text = "IP : ";
            // 
            // SentLB
            // 
            this.SentLB.AutoSize = true;
            this.SentLB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SentLB.Location = new System.Drawing.Point(17, 101);
            this.SentLB.Name = "SentLB";
            this.SentLB.Size = new System.Drawing.Size(32, 15);
            this.SentLB.TabIndex = 143;
            this.SentLB.Text = "Sent";
            // 
            // ReceivedLB
            // 
            this.ReceivedLB.AutoSize = true;
            this.ReceivedLB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceivedLB.Location = new System.Drawing.Point(17, 75);
            this.ReceivedLB.Name = "ReceivedLB";
            this.ReceivedLB.Size = new System.Drawing.Size(58, 15);
            this.ReceivedLB.TabIndex = 142;
            this.ReceivedLB.Text = "Received";
            // 
            // TCPSentTB
            // 
            this.TCPSentTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TCPSentTB.Location = new System.Drawing.Point(78, 100);
            this.TCPSentTB.Name = "TCPSentTB";
            this.TCPSentTB.Size = new System.Drawing.Size(230, 21);
            this.TCPSentTB.TabIndex = 141;
            this.TCPSentTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TCPReceivedTB
            // 
            this.TCPReceivedTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TCPReceivedTB.Location = new System.Drawing.Point(78, 74);
            this.TCPReceivedTB.Name = "TCPReceivedTB";
            this.TCPReceivedTB.ReadOnly = true;
            this.TCPReceivedTB.Size = new System.Drawing.Size(230, 21);
            this.TCPReceivedTB.TabIndex = 140;
            this.TCPReceivedTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // HostPortTB
            // 
            this.HostPortTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HostPortTB.Location = new System.Drawing.Point(78, 20);
            this.HostPortTB.Name = "HostPortTB";
            this.HostPortTB.Size = new System.Drawing.Size(151, 21);
            this.HostPortTB.TabIndex = 135;
            this.HostPortTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AppLB
            // 
            this.AppLB.AutoSize = true;
            this.AppLB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppLB.Location = new System.Drawing.Point(17, 50);
            this.AppLB.Name = "AppLB";
            this.AppLB.Size = new System.Drawing.Size(53, 15);
            this.AppLB.TabIndex = 138;
            this.AppLB.Text = "App Port";
            // 
            // AppPortTB
            // 
            this.AppPortTB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppPortTB.Location = new System.Drawing.Point(78, 47);
            this.AppPortTB.Name = "AppPortTB";
            this.AppPortTB.Size = new System.Drawing.Size(151, 21);
            this.AppPortTB.TabIndex = 136;
            this.AppPortTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // HostLB
            // 
            this.HostLB.AutoSize = true;
            this.HostLB.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HostLB.Location = new System.Drawing.Point(17, 23);
            this.HostLB.Name = "HostLB";
            this.HostLB.Size = new System.Drawing.Size(58, 15);
            this.HostLB.TabIndex = 137;
            this.HostLB.Text = "Host Port";
            // 
            // SpecP
            // 
            this.SpecP.BackColor = System.Drawing.Color.CadetBlue;
            this.SpecP.Controls.Add(this.OpenSpecFileBtn);
            this.SpecP.Controls.Add(this.EditSpecCB);
            this.SpecP.Controls.Add(this.SaveSpecFileBtn);
            this.SpecP.Controls.Add(this.SaveAsSpecFileBtn);
            this.SpecP.Location = new System.Drawing.Point(504, 29);
            this.SpecP.Name = "SpecP";
            this.SpecP.Size = new System.Drawing.Size(500, 58);
            this.SpecP.TabIndex = 210;
            // 
            // OpenSpecFileBtn
            // 
            this.OpenSpecFileBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenSpecFileBtn.Location = new System.Drawing.Point(36, 15);
            this.OpenSpecFileBtn.Name = "OpenSpecFileBtn";
            this.OpenSpecFileBtn.Size = new System.Drawing.Size(120, 28);
            this.OpenSpecFileBtn.TabIndex = 88;
            this.OpenSpecFileBtn.Text = "Open";
            this.OpenSpecFileBtn.UseVisualStyleBackColor = true;
            this.OpenSpecFileBtn.Click += new System.EventHandler(this.OpenSpecFile_Click);
            // 
            // EditSpecCB
            // 
            this.EditSpecCB.AutoSize = true;
            this.EditSpecCB.BackColor = System.Drawing.Color.CadetBlue;
            this.EditSpecCB.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditSpecCB.ForeColor = System.Drawing.Color.White;
            this.EditSpecCB.Location = new System.Drawing.Point(422, 21);
            this.EditSpecCB.Name = "EditSpecCB";
            this.EditSpecCB.Size = new System.Drawing.Size(47, 19);
            this.EditSpecCB.TabIndex = 91;
            this.EditSpecCB.Text = "Edit";
            this.EditSpecCB.UseVisualStyleBackColor = false;
            this.EditSpecCB.CheckedChanged += new System.EventHandler(this.EditSpec_CheckedChanged);
            // 
            // SaveSpecFileBtn
            // 
            this.SaveSpecFileBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveSpecFileBtn.Location = new System.Drawing.Point(162, 15);
            this.SaveSpecFileBtn.Name = "SaveSpecFileBtn";
            this.SaveSpecFileBtn.Size = new System.Drawing.Size(120, 28);
            this.SaveSpecFileBtn.TabIndex = 87;
            this.SaveSpecFileBtn.Text = "Save";
            this.SaveSpecFileBtn.UseVisualStyleBackColor = true;
            this.SaveSpecFileBtn.Click += new System.EventHandler(this.SaveSpecFile_Click);
            // 
            // SaveAsSpecFileBtn
            // 
            this.SaveAsSpecFileBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveAsSpecFileBtn.Location = new System.Drawing.Point(288, 15);
            this.SaveAsSpecFileBtn.Name = "SaveAsSpecFileBtn";
            this.SaveAsSpecFileBtn.Size = new System.Drawing.Size(120, 28);
            this.SaveAsSpecFileBtn.TabIndex = 89;
            this.SaveAsSpecFileBtn.Text = "Save As";
            this.SaveAsSpecFileBtn.UseVisualStyleBackColor = true;
            this.SaveAsSpecFileBtn.Click += new System.EventHandler(this.SaveAsSpecFile_Click);
            // 
            // TestSpecTB
            // 
            this.TestSpecTB.BackColor = System.Drawing.Color.CadetBlue;
            this.TestSpecTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TestSpecTB.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestSpecTB.ForeColor = System.Drawing.Color.White;
            this.TestSpecTB.Location = new System.Drawing.Point(504, 3);
            this.TestSpecTB.Name = "TestSpecTB";
            this.TestSpecTB.ReadOnly = true;
            this.TestSpecTB.Size = new System.Drawing.Size(500, 26);
            this.TestSpecTB.TabIndex = 90;
            this.TestSpecTB.TabStop = false;
            this.TestSpecTB.Text = "Test Spec";
            this.TestSpecTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CurrSpecTB
            // 
            this.CurrSpecTB.BackColor = System.Drawing.Color.Navy;
            this.CurrSpecTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CurrSpecTB.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrSpecTB.ForeColor = System.Drawing.Color.LightGray;
            this.CurrSpecTB.Location = new System.Drawing.Point(504, 87);
            this.CurrSpecTB.Name = "CurrSpecTB";
            this.CurrSpecTB.ReadOnly = true;
            this.CurrSpecTB.Size = new System.Drawing.Size(500, 26);
            this.CurrSpecTB.TabIndex = 209;
            this.CurrSpecTB.TabStop = false;
            this.CurrSpecTB.Text = "Spec File Name";
            this.CurrSpecTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Specdgv
            // 
            this.Specdgv.AllowUserToAddRows = false;
            this.Specdgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Specdgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Specdgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Specdgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Specdgv.Location = new System.Drawing.Point(504, 115);
            this.Specdgv.Name = "Specdgv";
            this.Specdgv.RowTemplate.Height = 23;
            this.Specdgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Specdgv.Size = new System.Drawing.Size(500, 801);
            this.Specdgv.TabIndex = 208;
            this.Specdgv.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Specdgv_CellMouseDoubleClick);
            // 
            // CurrRecipeTB
            // 
            this.CurrRecipeTB.BackColor = System.Drawing.Color.Navy;
            this.CurrRecipeTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CurrRecipeTB.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrRecipeTB.ForeColor = System.Drawing.Color.LightGray;
            this.CurrRecipeTB.Location = new System.Drawing.Point(2, 87);
            this.CurrRecipeTB.Name = "CurrRecipeTB";
            this.CurrRecipeTB.ReadOnly = true;
            this.CurrRecipeTB.Size = new System.Drawing.Size(500, 26);
            this.CurrRecipeTB.TabIndex = 211;
            this.CurrRecipeTB.TabStop = false;
            this.CurrRecipeTB.Text = "Recipe File Name";
            this.CurrRecipeTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Recipedgv
            // 
            this.Recipedgv.AllowUserToAddRows = false;
            this.Recipedgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Recipedgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Recipedgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Recipedgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Recipedgv.Location = new System.Drawing.Point(2, 251);
            this.Recipedgv.Name = "Recipedgv";
            this.Recipedgv.RowTemplate.Height = 23;
            this.Recipedgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Recipedgv.Size = new System.Drawing.Size(500, 665);
            this.Recipedgv.TabIndex = 220;
            this.Recipedgv.VisibleChanged += new System.EventHandler(this.Recipedgv_VisibleChanged);
            // 
            // ItemDownBtn
            // 
            this.ItemDownBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemDownBtn.Location = new System.Drawing.Point(439, 79);
            this.ItemDownBtn.Name = "ItemDownBtn";
            this.ItemDownBtn.Size = new System.Drawing.Size(61, 56);
            this.ItemDownBtn.TabIndex = 219;
            this.ItemDownBtn.Text = "Dn";
            this.ItemDownBtn.UseVisualStyleBackColor = true;
            this.ItemDownBtn.Click += new System.EventHandler(this.ItemDown_Click);
            // 
            // ItemUPBtn
            // 
            this.ItemUPBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemUPBtn.Location = new System.Drawing.Point(439, 23);
            this.ItemUPBtn.Name = "ItemUPBtn";
            this.ItemUPBtn.Size = new System.Drawing.Size(61, 56);
            this.ItemUPBtn.TabIndex = 218;
            this.ItemUPBtn.Text = "Up";
            this.ItemUPBtn.UseVisualStyleBackColor = true;
            this.ItemUPBtn.Click += new System.EventHandler(this.ItemUP_Click);
            // 
            // ActionListBox
            // 
            this.ActionListBox.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActionListBox.FormattingEnabled = true;
            this.ActionListBox.ItemHeight = 15;
            this.ActionListBox.Location = new System.Drawing.Point(0, 24);
            this.ActionListBox.Name = "ActionListBox";
            this.ActionListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ActionListBox.Size = new System.Drawing.Size(187, 109);
            this.ActionListBox.TabIndex = 213;
            // 
            // RecipeP
            // 
            this.RecipeP.BackColor = System.Drawing.Color.CadetBlue;
            this.RecipeP.Controls.Add(this.SaveAsRecipeFileBtn);
            this.RecipeP.Controls.Add(this.SaveRecipeFileBtn);
            this.RecipeP.Controls.Add(this.OpenRecipeFileBtn);
            this.RecipeP.Controls.Add(this.EditRecipeCB);
            this.RecipeP.Location = new System.Drawing.Point(2, 29);
            this.RecipeP.Name = "RecipeP";
            this.RecipeP.Size = new System.Drawing.Size(500, 58);
            this.RecipeP.TabIndex = 212;
            // 
            // SaveAsRecipeFileBtn
            // 
            this.SaveAsRecipeFileBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveAsRecipeFileBtn.Location = new System.Drawing.Point(286, 14);
            this.SaveAsRecipeFileBtn.Name = "SaveAsRecipeFileBtn";
            this.SaveAsRecipeFileBtn.Size = new System.Drawing.Size(120, 28);
            this.SaveAsRecipeFileBtn.TabIndex = 70;
            this.SaveAsRecipeFileBtn.Text = "Save As";
            this.SaveAsRecipeFileBtn.UseVisualStyleBackColor = true;
            this.SaveAsRecipeFileBtn.Click += new System.EventHandler(this.SaveAsRecipeFile_Click);
            // 
            // SaveRecipeFileBtn
            // 
            this.SaveRecipeFileBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveRecipeFileBtn.Location = new System.Drawing.Point(160, 14);
            this.SaveRecipeFileBtn.Name = "SaveRecipeFileBtn";
            this.SaveRecipeFileBtn.Size = new System.Drawing.Size(120, 28);
            this.SaveRecipeFileBtn.TabIndex = 69;
            this.SaveRecipeFileBtn.Text = "Save";
            this.SaveRecipeFileBtn.UseVisualStyleBackColor = true;
            this.SaveRecipeFileBtn.Click += new System.EventHandler(this.SaveRecipeFile_Click);
            // 
            // OpenRecipeFileBtn
            // 
            this.OpenRecipeFileBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenRecipeFileBtn.Location = new System.Drawing.Point(34, 14);
            this.OpenRecipeFileBtn.Name = "OpenRecipeFileBtn";
            this.OpenRecipeFileBtn.Size = new System.Drawing.Size(120, 28);
            this.OpenRecipeFileBtn.TabIndex = 68;
            this.OpenRecipeFileBtn.Text = "Open";
            this.OpenRecipeFileBtn.UseVisualStyleBackColor = true;
            this.OpenRecipeFileBtn.Click += new System.EventHandler(this.OpenRecipeFile_Click);
            // 
            // EditRecipeCB
            // 
            this.EditRecipeCB.AutoSize = true;
            this.EditRecipeCB.BackColor = System.Drawing.Color.CadetBlue;
            this.EditRecipeCB.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditRecipeCB.ForeColor = System.Drawing.Color.White;
            this.EditRecipeCB.Location = new System.Drawing.Point(423, 19);
            this.EditRecipeCB.Name = "EditRecipeCB";
            this.EditRecipeCB.Size = new System.Drawing.Size(47, 19);
            this.EditRecipeCB.TabIndex = 90;
            this.EditRecipeCB.Text = "Edit";
            this.EditRecipeCB.UseVisualStyleBackColor = false;
            this.EditRecipeCB.CheckedChanged += new System.EventHandler(this.EditRecipe_CheckedChanged);
            // 
            // ToDoListBox
            // 
            this.ToDoListBox.BackColor = System.Drawing.Color.PaleGreen;
            this.ToDoListBox.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToDoListBox.FormattingEnabled = true;
            this.ToDoListBox.ItemHeight = 15;
            this.ToDoListBox.Location = new System.Drawing.Point(251, 24);
            this.ToDoListBox.Name = "ToDoListBox";
            this.ToDoListBox.Size = new System.Drawing.Size(187, 109);
            this.ToDoListBox.TabIndex = 217;
            // 
            // ActionListTB
            // 
            this.ActionListTB.BackColor = System.Drawing.Color.White;
            this.ActionListTB.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActionListTB.ForeColor = System.Drawing.Color.Black;
            this.ActionListTB.Location = new System.Drawing.Point(0, 0);
            this.ActionListTB.Name = "ActionListTB";
            this.ActionListTB.ReadOnly = true;
            this.ActionListTB.Size = new System.Drawing.Size(249, 23);
            this.ActionListTB.TabIndex = 214;
            this.ActionListTB.TabStop = false;
            this.ActionListTB.Text = "Action List";
            this.ActionListTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RemoveItemBtn
            // 
            this.RemoveItemBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemoveItemBtn.Location = new System.Drawing.Point(188, 78);
            this.RemoveItemBtn.Name = "RemoveItemBtn";
            this.RemoveItemBtn.Size = new System.Drawing.Size(61, 56);
            this.RemoveItemBtn.TabIndex = 216;
            this.RemoveItemBtn.Text = "<<";
            this.RemoveItemBtn.UseVisualStyleBackColor = true;
            this.RemoveItemBtn.Click += new System.EventHandler(this.RemoveItem_Click);
            // 
            // AddItemBtn
            // 
            this.AddItemBtn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddItemBtn.Location = new System.Drawing.Point(188, 23);
            this.AddItemBtn.Name = "AddItemBtn";
            this.AddItemBtn.Size = new System.Drawing.Size(61, 56);
            this.AddItemBtn.TabIndex = 215;
            this.AddItemBtn.Text = ">>";
            this.AddItemBtn.UseVisualStyleBackColor = true;
            this.AddItemBtn.Click += new System.EventHandler(this.AddItem_Click);
            // 
            // ToDoListTB
            // 
            this.ToDoListTB.BackColor = System.Drawing.Color.White;
            this.ToDoListTB.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToDoListTB.ForeColor = System.Drawing.Color.Black;
            this.ToDoListTB.Location = new System.Drawing.Point(251, 0);
            this.ToDoListTB.Name = "ToDoListTB";
            this.ToDoListTB.ReadOnly = true;
            this.ToDoListTB.Size = new System.Drawing.Size(249, 23);
            this.ToDoListTB.TabIndex = 221;
            this.ToDoListTB.TabStop = false;
            this.ToDoListTB.Text = "To Do List";
            this.ToDoListTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TestRecipeTB
            // 
            this.TestRecipeTB.BackColor = System.Drawing.Color.CadetBlue;
            this.TestRecipeTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TestRecipeTB.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestRecipeTB.ForeColor = System.Drawing.Color.White;
            this.TestRecipeTB.Location = new System.Drawing.Point(2, 3);
            this.TestRecipeTB.Name = "TestRecipeTB";
            this.TestRecipeTB.ReadOnly = true;
            this.TestRecipeTB.Size = new System.Drawing.Size(500, 26);
            this.TestRecipeTB.TabIndex = 222;
            this.TestRecipeTB.TabStop = false;
            this.TestRecipeTB.Text = "Test Condition";
            this.TestRecipeTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ActionToDoList
            // 
            this.ActionToDoList.Controls.Add(this.ActionListTB);
            this.ActionToDoList.Controls.Add(this.ToDoListTB);
            this.ActionToDoList.Controls.Add(this.ActionListBox);
            this.ActionToDoList.Controls.Add(this.ToDoListBox);
            this.ActionToDoList.Controls.Add(this.ItemDownBtn);
            this.ActionToDoList.Controls.Add(this.AddItemBtn);
            this.ActionToDoList.Controls.Add(this.ItemUPBtn);
            this.ActionToDoList.Controls.Add(this.RemoveItemBtn);
            this.ActionToDoList.Location = new System.Drawing.Point(2, 114);
            this.ActionToDoList.Name = "ActionToDoList";
            this.ActionToDoList.Size = new System.Drawing.Size(500, 135);
            this.ActionToDoList.TabIndex = 223;
            // 
            // FWUpdateBtn
            // 
            this.FWUpdateBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.FWUpdateBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("FWUpdateBtn.BackgroundImage")));
            this.FWUpdateBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FWUpdateBtn.FlatAppearance.BorderSize = 0;
            this.FWUpdateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FWUpdateBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FWUpdateBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.FWUpdateBtn.Location = new System.Drawing.Point(1009, 307);
            this.FWUpdateBtn.Name = "FWUpdateBtn";
            this.FWUpdateBtn.Size = new System.Drawing.Size(521, 55);
            this.FWUpdateBtn.TabIndex = 0;
            this.FWUpdateBtn.Text = "Set FW Update File";
            this.FWUpdateBtn.UseVisualStyleBackColor = false;
            this.FWUpdateBtn.Click += new System.EventHandler(this.ScriptUpdate_Click);
            // 
            // PIDUpdateBtn
            // 
            this.PIDUpdateBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PIDUpdateBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PIDUpdateBtn.BackgroundImage")));
            this.PIDUpdateBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PIDUpdateBtn.FlatAppearance.BorderSize = 0;
            this.PIDUpdateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PIDUpdateBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PIDUpdateBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.PIDUpdateBtn.Location = new System.Drawing.Point(1009, 466);
            this.PIDUpdateBtn.Name = "PIDUpdateBtn";
            this.PIDUpdateBtn.Size = new System.Drawing.Size(521, 55);
            this.PIDUpdateBtn.TabIndex = 1;
            this.PIDUpdateBtn.Text = "Set PID Update File";
            this.PIDUpdateBtn.UseVisualStyleBackColor = false;
            this.PIDUpdateBtn.Visible = false;
            this.PIDUpdateBtn.Click += new System.EventHandler(this.ScriptUpdate_Click);
            // 
            // SetMESBtn
            // 
            this.SetMESBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SetMESBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SetMESBtn.BackgroundImage")));
            this.SetMESBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SetMESBtn.FlatAppearance.BorderSize = 0;
            this.SetMESBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetMESBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetMESBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.SetMESBtn.Location = new System.Drawing.Point(1009, 626);
            this.SetMESBtn.Name = "SetMESBtn";
            this.SetMESBtn.Size = new System.Drawing.Size(521, 55);
            this.SetMESBtn.TabIndex = 2;
            this.SetMESBtn.Text = "Set MES Path";
            this.SetMESBtn.UseVisualStyleBackColor = false;
            this.SetMESBtn.Visible = false;
            this.SetMESBtn.Click += new System.EventHandler(this.ScriptUpdate_Click);
            // 
            // OptionChk19
            // 
            this.OptionChk19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk19.AutoSize = true;
            this.OptionChk19.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk19.Location = new System.Drawing.Point(3, 561);
            this.OptionChk19.Name = "OptionChk19";
            this.OptionChk19.Size = new System.Drawing.Size(76, 25);
            this.OptionChk19.TabIndex = 201;
            this.OptionChk19.Text = "Option";
            this.OptionChk19.UseVisualStyleBackColor = false;
            // 
            // OptionChk18
            // 
            this.OptionChk18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk18.AutoSize = true;
            this.OptionChk18.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk18.Location = new System.Drawing.Point(3, 530);
            this.OptionChk18.Name = "OptionChk18";
            this.OptionChk18.Size = new System.Drawing.Size(76, 25);
            this.OptionChk18.TabIndex = 198;
            this.OptionChk18.Text = "Option";
            this.OptionChk18.UseVisualStyleBackColor = false;
            // 
            // OptionChk17
            // 
            this.OptionChk17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk17.AutoSize = true;
            this.OptionChk17.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk17.Location = new System.Drawing.Point(3, 499);
            this.OptionChk17.Name = "OptionChk17";
            this.OptionChk17.Size = new System.Drawing.Size(76, 25);
            this.OptionChk17.TabIndex = 199;
            this.OptionChk17.Text = "Option";
            this.OptionChk17.UseVisualStyleBackColor = false;
            // 
            // OptionChk4
            // 
            this.OptionChk4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk4.AutoSize = true;
            this.OptionChk4.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk4.Location = new System.Drawing.Point(3, 96);
            this.OptionChk4.Name = "OptionChk4";
            this.OptionChk4.Size = new System.Drawing.Size(76, 25);
            this.OptionChk4.TabIndex = 132;
            this.OptionChk4.Text = "Option";
            this.OptionChk4.UseVisualStyleBackColor = false;
            // 
            // OptionChk5
            // 
            this.OptionChk5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk5.AutoSize = true;
            this.OptionChk5.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk5.Location = new System.Drawing.Point(3, 127);
            this.OptionChk5.Name = "OptionChk5";
            this.OptionChk5.Size = new System.Drawing.Size(76, 25);
            this.OptionChk5.TabIndex = 145;
            this.OptionChk5.Text = "Option";
            this.OptionChk5.UseVisualStyleBackColor = false;
            // 
            // OptionChk6
            // 
            this.OptionChk6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk6.AutoSize = true;
            this.OptionChk6.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk6.Location = new System.Drawing.Point(3, 158);
            this.OptionChk6.Name = "OptionChk6";
            this.OptionChk6.Size = new System.Drawing.Size(76, 25);
            this.OptionChk6.TabIndex = 154;
            this.OptionChk6.Text = "Option";
            this.OptionChk6.UseVisualStyleBackColor = false;
            // 
            // OptionChk7
            // 
            this.OptionChk7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk7.AutoSize = true;
            this.OptionChk7.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk7.Location = new System.Drawing.Point(3, 189);
            this.OptionChk7.Name = "OptionChk7";
            this.OptionChk7.Size = new System.Drawing.Size(76, 25);
            this.OptionChk7.TabIndex = 134;
            this.OptionChk7.Text = "Option";
            this.OptionChk7.UseVisualStyleBackColor = false;
            // 
            // OptionChk8
            // 
            this.OptionChk8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk8.AutoSize = true;
            this.OptionChk8.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk8.Location = new System.Drawing.Point(3, 220);
            this.OptionChk8.Name = "OptionChk8";
            this.OptionChk8.Size = new System.Drawing.Size(76, 25);
            this.OptionChk8.TabIndex = 144;
            this.OptionChk8.Text = "Option";
            this.OptionChk8.UseVisualStyleBackColor = false;
            // 
            // OptionChk9
            // 
            this.OptionChk9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk9.AutoSize = true;
            this.OptionChk9.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk9.Location = new System.Drawing.Point(3, 251);
            this.OptionChk9.Name = "OptionChk9";
            this.OptionChk9.Size = new System.Drawing.Size(76, 25);
            this.OptionChk9.TabIndex = 148;
            this.OptionChk9.Text = "Option";
            this.OptionChk9.UseVisualStyleBackColor = false;
            // 
            // OptionChk10
            // 
            this.OptionChk10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk10.AutoSize = true;
            this.OptionChk10.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk10.Location = new System.Drawing.Point(3, 282);
            this.OptionChk10.Name = "OptionChk10";
            this.OptionChk10.Size = new System.Drawing.Size(76, 25);
            this.OptionChk10.TabIndex = 177;
            this.OptionChk10.Text = "Option";
            this.OptionChk10.UseVisualStyleBackColor = false;
            // 
            // OptionChk11
            // 
            this.OptionChk11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk11.AutoSize = true;
            this.OptionChk11.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk11.Location = new System.Drawing.Point(3, 313);
            this.OptionChk11.Name = "OptionChk11";
            this.OptionChk11.Size = new System.Drawing.Size(76, 25);
            this.OptionChk11.TabIndex = 160;
            this.OptionChk11.Text = "Option";
            this.OptionChk11.UseVisualStyleBackColor = false;
            // 
            // OptionChk12
            // 
            this.OptionChk12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk12.AutoSize = true;
            this.OptionChk12.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk12.Location = new System.Drawing.Point(3, 344);
            this.OptionChk12.Name = "OptionChk12";
            this.OptionChk12.Size = new System.Drawing.Size(76, 25);
            this.OptionChk12.TabIndex = 161;
            this.OptionChk12.Text = "Option";
            this.OptionChk12.UseVisualStyleBackColor = false;
            // 
            // OptionChk13
            // 
            this.OptionChk13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk13.AutoSize = true;
            this.OptionChk13.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk13.Location = new System.Drawing.Point(3, 375);
            this.OptionChk13.Name = "OptionChk13";
            this.OptionChk13.Size = new System.Drawing.Size(76, 25);
            this.OptionChk13.TabIndex = 162;
            this.OptionChk13.Text = "Option";
            this.OptionChk13.UseVisualStyleBackColor = false;
            // 
            // OptionChk14
            // 
            this.OptionChk14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk14.AutoSize = true;
            this.OptionChk14.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk14.Location = new System.Drawing.Point(3, 406);
            this.OptionChk14.Name = "OptionChk14";
            this.OptionChk14.Size = new System.Drawing.Size(76, 25);
            this.OptionChk14.TabIndex = 163;
            this.OptionChk14.Text = "Option";
            this.OptionChk14.UseVisualStyleBackColor = false;
            // 
            // OptionChk15
            // 
            this.OptionChk15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk15.AutoSize = true;
            this.OptionChk15.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk15.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk15.Location = new System.Drawing.Point(3, 437);
            this.OptionChk15.Name = "OptionChk15";
            this.OptionChk15.Size = new System.Drawing.Size(76, 25);
            this.OptionChk15.TabIndex = 164;
            this.OptionChk15.Text = "Option";
            this.OptionChk15.UseVisualStyleBackColor = false;
            // 
            // OptionChk16
            // 
            this.OptionChk16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk16.AutoSize = true;
            this.OptionChk16.BackColor = System.Drawing.Color.Transparent;
            this.OptionChk16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptionChk16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptionChk16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.OptionChk16.Location = new System.Drawing.Point(3, 468);
            this.OptionChk16.Name = "OptionChk16";
            this.OptionChk16.Size = new System.Drawing.Size(76, 25);
            this.OptionChk16.TabIndex = 174;
            this.OptionChk16.Text = "Option";
            this.OptionChk16.UseVisualStyleBackColor = false;
            // 
            // OptionChk1
            // 
            this.OptionChk1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk1.AutoSize = true;
            this.OptionChk1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.OptionChk1.Location = new System.Drawing.Point(3, 1);
            this.OptionChk1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.OptionChk1.Name = "OptionChk1";
            this.OptionChk1.Size = new System.Drawing.Size(79, 29);
            this.OptionChk1.TabIndex = 206;
            this.OptionChk1.Text = "Option";
            this.OptionChk1.UseVisualStyleBackColor = true;
            // 
            // OptionChk2
            // 
            this.OptionChk2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk2.AutoSize = true;
            this.OptionChk2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.OptionChk2.Location = new System.Drawing.Point(3, 32);
            this.OptionChk2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.OptionChk2.Name = "OptionChk2";
            this.OptionChk2.Size = new System.Drawing.Size(79, 29);
            this.OptionChk2.TabIndex = 205;
            this.OptionChk2.Text = "Option";
            this.OptionChk2.UseVisualStyleBackColor = true;
            // 
            // OptionChk3
            // 
            this.OptionChk3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OptionChk3.AutoSize = true;
            this.OptionChk3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.OptionChk3.Location = new System.Drawing.Point(3, 63);
            this.OptionChk3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.OptionChk3.Name = "OptionChk3";
            this.OptionChk3.Size = new System.Drawing.Size(79, 29);
            this.OptionChk3.TabIndex = 204;
            this.OptionChk3.Text = "Option";
            this.OptionChk3.UseVisualStyleBackColor = true;
            // 
            // ApplyBtn
            // 
            this.ApplyBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ApplyBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ApplyBtn.BackgroundImage")));
            this.ApplyBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ApplyBtn.FlatAppearance.BorderSize = 0;
            this.ApplyBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ApplyBtn.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ApplyBtn.ForeColor = System.Drawing.Color.White;
            this.ApplyBtn.Location = new System.Drawing.Point(1536, 723);
            this.ApplyBtn.Name = "ApplyBtn";
            this.ApplyBtn.Size = new System.Drawing.Size(366, 57);
            this.ApplyBtn.TabIndex = 229;
            this.ApplyBtn.Text = "Apply";
            this.ApplyBtn.UseVisualStyleBackColor = false;
            this.ApplyBtn.Click += new System.EventHandler(this.Apply_Click);
            // 
            // OptionStateTLP
            // 
            this.OptionStateTLP.BackColor = System.Drawing.Color.SteelBlue;
            this.OptionStateTLP.ColumnCount = 1;
            this.OptionStateTLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.OptionStateTLP.Controls.Add(this.OptionChk3, 0, 2);
            this.OptionStateTLP.Controls.Add(this.OptionChk2, 0, 1);
            this.OptionStateTLP.Controls.Add(this.OptionChk1, 0, 0);
            this.OptionStateTLP.Controls.Add(this.OptionChk16, 0, 15);
            this.OptionStateTLP.Controls.Add(this.OptionChk15, 0, 14);
            this.OptionStateTLP.Controls.Add(this.OptionChk14, 0, 13);
            this.OptionStateTLP.Controls.Add(this.OptionChk13, 0, 12);
            this.OptionStateTLP.Controls.Add(this.OptionChk12, 0, 11);
            this.OptionStateTLP.Controls.Add(this.OptionChk11, 0, 10);
            this.OptionStateTLP.Controls.Add(this.OptionChk10, 0, 9);
            this.OptionStateTLP.Controls.Add(this.OptionChk9, 0, 8);
            this.OptionStateTLP.Controls.Add(this.OptionChk8, 0, 7);
            this.OptionStateTLP.Controls.Add(this.OptionChk7, 0, 6);
            this.OptionStateTLP.Controls.Add(this.OptionChk6, 0, 5);
            this.OptionStateTLP.Controls.Add(this.OptionChk5, 0, 4);
            this.OptionStateTLP.Controls.Add(this.OptionChk4, 0, 3);
            this.OptionStateTLP.Controls.Add(this.OptionChk17, 0, 16);
            this.OptionStateTLP.Controls.Add(this.OptionChk18, 0, 17);
            this.OptionStateTLP.Controls.Add(this.OptionChk19, 0, 18);
            this.OptionStateTLP.Location = new System.Drawing.Point(1536, 2);
            this.OptionStateTLP.Name = "OptionStateTLP";
            this.OptionStateTLP.RowCount = 26;
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.OptionStateTLP.Size = new System.Drawing.Size(366, 718);
            this.OptionStateTLP.TabIndex = 203;
            // 
            // btnSetModel
            // 
            this.btnSetModel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnSetModel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSetModel.BackgroundImage")));
            this.btnSetModel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSetModel.FlatAppearance.BorderSize = 0;
            this.btnSetModel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetModel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetModel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.btnSetModel.Location = new System.Drawing.Point(1536, 784);
            this.btnSetModel.Name = "btnSetModel";
            this.btnSetModel.Size = new System.Drawing.Size(366, 55);
            this.btnSetModel.TabIndex = 3;
            this.btnSetModel.Text = "Fiducial Mark Model";
            this.btnSetModel.UseVisualStyleBackColor = false;
            this.btnSetModel.Click += new System.EventHandler(this.ScriptUpdate_Click);
            // 
            // rtbFiducialMark
            // 
            this.rtbFiducialMark.BackColor = System.Drawing.Color.LightGray;
            this.rtbFiducialMark.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbFiducialMark.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbFiducialMark.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.rtbFiducialMark.Location = new System.Drawing.Point(1536, 840);
            this.rtbFiducialMark.Name = "rtbFiducialMark";
            this.rtbFiducialMark.ReadOnly = true;
            this.rtbFiducialMark.Size = new System.Drawing.Size(366, 72);
            this.rtbFiducialMark.TabIndex = 233;
            this.rtbFiducialMark.Text = "";
            // 
            // F_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 978);
            this.Controls.Add(this.btnSetModel);
            this.Controls.Add(this.rtbFiducialMark);
            this.Controls.Add(this.ApplyBtn);
            this.Controls.Add(this.SetMESBtn);
            this.Controls.Add(this.PIDUpdateBtn);
            this.Controls.Add(this.FWUpdateBtn);
            this.Controls.Add(this.ActionToDoList);
            this.Controls.Add(this.TestRecipeTB);
            this.Controls.Add(this.CurrRecipeTB);
            this.Controls.Add(this.Recipedgv);
            this.Controls.Add(this.RecipeP);
            this.Controls.Add(this.TestSpecTB);
            this.Controls.Add(this.SpecP);
            this.Controls.Add(this.CurrSpecTB);
            this.Controls.Add(this.Specdgv);
            this.Controls.Add(this.HandlerSettingGB);
            this.Controls.Add(this.OptionStateTLP);
            this.Controls.Add(this.TesterSetTLP);
            this.Controls.Add(this.PIDFilePathTB);
            this.Controls.Add(this.MESPathTB);
            this.Controls.Add(this.FWFilePathTB);
            this.Controls.Add(this.TPblank);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "F_Admin";
            this.Load += new System.EventHandler(this.F_Admin_Load);
            this.TesterSetTLP.ResumeLayout(false);
            this.TesterSetTLP.PerformLayout();
            this.HandlerSettingGB.ResumeLayout(false);
            this.HandlerSettingGB.PerformLayout();
            this.SpecP.ResumeLayout(false);
            this.SpecP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Specdgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recipedgv)).EndInit();
            this.RecipeP.ResumeLayout(false);
            this.RecipeP.PerformLayout();
            this.ActionToDoList.ResumeLayout(false);
            this.ActionToDoList.PerformLayout();
            this.OptionStateTLP.ResumeLayout(false);
            this.OptionStateTLP.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TPblank;
        private System.Windows.Forms.TextBox MCNumber;
        private System.Windows.Forms.Label McNoLB;
        private System.Windows.Forms.Label DriverICLB;
        private System.Windows.Forms.ListBox DriverICType;
        private System.Windows.Forms.ListBox PrismSupplier;
        private System.Windows.Forms.Label PrismLB;
        private System.Windows.Forms.Label ProductLB;
        private System.Windows.Forms.TextBox ProductLine;
        private System.Windows.Forms.Label TesterNoLB;
        private System.Windows.Forms.Label RevisionNumberLB;
        private System.Windows.Forms.TextBox TesterNumber;
        private System.Windows.Forms.Label MakerLB;
        private System.Windows.Forms.TextBox BarcodeRevNo;
        private System.Windows.Forms.RichTextBox PIDFilePathTB;
        private System.Windows.Forms.RichTextBox MESPathTB;
        private System.Windows.Forms.RichTextBox FWFilePathTB;
        private System.Windows.Forms.TableLayoutPanel TesterSetTLP;
        private System.Windows.Forms.GroupBox HandlerSettingGB;
        private System.Windows.Forms.Label MyIPLB;
        private System.Windows.Forms.Label SentLB;
        private System.Windows.Forms.Label ReceivedLB;
        private System.Windows.Forms.TextBox TCPSentTB;
        private System.Windows.Forms.TextBox TCPReceivedTB;
        private System.Windows.Forms.TextBox HostPortTB;
        private System.Windows.Forms.Label AppLB;
        private System.Windows.Forms.TextBox AppPortTB;
        private System.Windows.Forms.Label HostLB;
        private System.Windows.Forms.Panel SpecP;
        private System.Windows.Forms.Button OpenSpecFileBtn;
        private System.Windows.Forms.CheckBox EditSpecCB;
        private System.Windows.Forms.Button SaveSpecFileBtn;
        private System.Windows.Forms.Button SaveAsSpecFileBtn;
        private System.Windows.Forms.TextBox TestSpecTB;
        private System.Windows.Forms.TextBox CurrSpecTB;
        private System.Windows.Forms.DataGridView Specdgv;
        private System.Windows.Forms.TextBox CurrRecipeTB;
        private System.Windows.Forms.DataGridView Recipedgv;
        private System.Windows.Forms.Button ItemDownBtn;
        private System.Windows.Forms.Button ItemUPBtn;
        private System.Windows.Forms.ListBox ActionListBox;
        private System.Windows.Forms.Panel RecipeP;
        private System.Windows.Forms.Button SaveAsRecipeFileBtn;
        private System.Windows.Forms.Button SaveRecipeFileBtn;
        private System.Windows.Forms.Button OpenRecipeFileBtn;
        private System.Windows.Forms.CheckBox EditRecipeCB;
        private System.Windows.Forms.ListBox ToDoListBox;
        private System.Windows.Forms.TextBox ActionListTB;
        private System.Windows.Forms.Button RemoveItemBtn;
        private System.Windows.Forms.Button AddItemBtn;
        private System.Windows.Forms.TextBox ToDoListTB;
        private System.Windows.Forms.TextBox TestRecipeTB;
        private System.Windows.Forms.Panel ActionToDoList;
        private System.Windows.Forms.Button SendTCPBtn;
        private System.Windows.Forms.Button SaveStartBtn;
        private System.Windows.Forms.Button FWUpdateBtn;
        private System.Windows.Forms.Button PIDUpdateBtn;
        private System.Windows.Forms.Button SetMESBtn;
        private System.Windows.Forms.ListBox BarcodeMaker;
        private System.Windows.Forms.CheckBox OptionChk19;
        private System.Windows.Forms.CheckBox OptionChk18;
        private System.Windows.Forms.CheckBox OptionChk17;
        private System.Windows.Forms.CheckBox OptionChk4;
        private System.Windows.Forms.CheckBox OptionChk5;
        private System.Windows.Forms.CheckBox OptionChk6;
        private System.Windows.Forms.CheckBox OptionChk7;
        private System.Windows.Forms.CheckBox OptionChk8;
        private System.Windows.Forms.CheckBox OptionChk9;
        private System.Windows.Forms.CheckBox OptionChk10;
        private System.Windows.Forms.CheckBox OptionChk11;
        private System.Windows.Forms.CheckBox OptionChk12;
        private System.Windows.Forms.CheckBox OptionChk13;
        private System.Windows.Forms.CheckBox OptionChk14;
        private System.Windows.Forms.CheckBox OptionChk15;
        private System.Windows.Forms.CheckBox OptionChk16;
        private System.Windows.Forms.CheckBox OptionChk1;
        private System.Windows.Forms.CheckBox OptionChk2;
        private System.Windows.Forms.CheckBox OptionChk3;
        private System.Windows.Forms.Button ApplyBtn;
        private System.Windows.Forms.TableLayoutPanel OptionStateTLP;
        private System.Windows.Forms.Button btnSetModel;
        private System.Windows.Forms.RichTextBox rtbFiducialMark;
    }
}

