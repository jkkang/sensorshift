﻿using ActroLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorShift
{
    public partial class F_Admin : Form
    {
        public CurrentPath CurrentPath { get { return PROCESS.Current; } }
        public Recipe Rcp { get { return PROCESS.Rcp; } }
        public Spec Spec { get { return PROCESS.Spec; } }
        public Model Model { get { return PROCESS.Model; } }
        public Option Option { get { return PROCESS.Option; } }
        public Process Process { get { return PROCESS.Process; } }
        public CurrentLed Leds { get { return PROCESS.CurrentLed; } }
        public SEM1217S DrvIC { get { return PROCESS.DrvIC; } }

        public F_Admin()
        {
            InitializeComponent();
            InitCondition();
            InitDataSpec();
            InitModel();
            InitOption();
            InitScripPath();
            DrvIC.SwitchOn += DriverIC_SwitchOn;
        }

        private void F_Admin_Load(object sender, EventArgs e)
        {


        }

        #region *Init

        public void InitCondition()
        {
            Rcp.Init(CurrentPath.ConditionName, "\\Recipe\\");
  
            ActionListBox.Items.Clear();

            for (int i = 0; i < 4; i++)
                Rcp.Param[(int)RecipeItem.CurrentLedL1 + i][2] = Leds.Val[i].ToString("F2");

            for (int i = 0; i < Process.ItemList.Count; i++)
            {
                ActionListBox.Items.Add(Process.ItemList[i].Name);
            }

            ToDoListBox.Items.Clear();
            for (int i = 0; i < Rcp.ToDoList.Count; i++)
            {
                ToDoListBox.Items.Add(Rcp.ToDoList[i]);
            }
            CurrRecipeTB.Text = Rcp.CurrentName;

            Recipedgv.ColumnCount = 4;
            Recipedgv.Font = new Font("Arial", 10, FontStyle.Bold);
            for (int i = 0; i < Recipedgv.ColumnCount; i++)
            {
                Recipedgv.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            Recipedgv.RowHeadersVisible = false;
            Recipedgv.BackgroundColor = Color.LightGray;
            Recipedgv.Columns[0].Name = "Class";
            Recipedgv.Columns[1].Name = "Condition Item";
            Recipedgv.Columns[2].Name = "Value";
            Recipedgv.Columns[3].Name = "Unit";

            Recipedgv.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Recipedgv.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            Recipedgv.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            Recipedgv.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            Recipedgv.Columns[0].Width = 80;
            Recipedgv.Columns[1].Width = 230;
            Recipedgv.Columns[2].Width = 100;
            Recipedgv.Columns[3].Width = 70;

            Recipedgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            Recipedgv.ColumnHeadersHeight = 22;

            int effRowNum = 0;
            string colTitle;
            bool bColorChange = false;
            Recipedgv.Rows.Clear();

            for (int i = 0; i < Rcp.Param.Count; i++)
            {
                if (i == 0) colTitle = Rcp.Param[i][0].ToString();
                else
                {
                    if (Rcp.Param[i - 1][0].ToString() == Rcp.Param[i][0].ToString()) colTitle = "";
                    else
                    {
                        colTitle = Rcp.Param[i][0].ToString();
                        bColorChange = !bColorChange;
                    }
                }
                Recipedgv.Rows.Add(colTitle, Rcp.Param[i][1], Rcp.Param[i][2], Rcp.Param[i][3]);

                if (bColorChange)
                {
                    Recipedgv[0, effRowNum].Style.BackColor = Color.Lavender;
                    Recipedgv[1, effRowNum].Style.BackColor = Color.Lavender;
                    Recipedgv[3, effRowNum].Style.BackColor = Color.Lavender;
                }
                else
                {
                    Recipedgv[0, effRowNum].Style.BackColor = Color.White;
                    Recipedgv[1, effRowNum].Style.BackColor = Color.White;
                    Recipedgv[3, effRowNum].Style.BackColor = Color.White;
                }
                Recipedgv.Rows[effRowNum].Visible = Convert.ToBoolean(Rcp.Param[i][4]);
                effRowNum++;
            }
            Recipedgv.Rows.Add("", "", "", "", "");

            for (int i = 0; i < effRowNum; i++)
            {
                Recipedgv.Rows[i].Height = 15;
                Recipedgv.Rows[i].Resizable = DataGridViewTriState.False;
                Recipedgv.Rows[i].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
                Recipedgv[1, i].Style.Font = new Font("Arial", 9, FontStyle.Bold);
                Recipedgv[2, i].Style.Font = new Font("Arial", 9, FontStyle.Bold);
                Recipedgv[3, i].Style.Font = new Font("Arial", 9, FontStyle.Italic);
            }

            for (int colum = 2; colum < 3; colum++)
            {
                for (int row = 0; row < effRowNum; row++)
                {
                    Recipedgv[colum, row].Style.BackColor = Color.LightGray;
                    Recipedgv.ReadOnly = true;
                }
            }

            IsEdit();
        }
        private void InitDataSpec()
        {
            Spec.Init(CurrentPath.SpecName, "\\Spec\\");

            Specdgv.ColumnCount = 5;
            Specdgv.Font = new Font("Calibri", 10, FontStyle.Bold);
            for (int i = 0; i < Specdgv.ColumnCount; i++)
            {
                Specdgv.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            Specdgv.RowHeadersVisible = false;
            Specdgv.BackgroundColor = Color.LightGray;

            CurrSpecTB.Text = Spec.CurrentName;
            // Column
            Specdgv.Columns[0].Name = "Axis";
            Specdgv.Columns[1].Name = "Test Item";
            Specdgv.Columns[2].Name = "Min";
            Specdgv.Columns[3].Name = "Max";
            Specdgv.Columns[4].Name = "unit";
            for (int i = 0; i < 5; i++)
                Specdgv.Columns[i].DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            Specdgv.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Specdgv.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            Specdgv.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            Specdgv.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            Specdgv.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            Specdgv.Columns[0].Width = 80;
            Specdgv.Columns[1].Width = 180;
            Specdgv.Columns[2].Width = 70;
            Specdgv.Columns[3].Width = 70;
            Specdgv.Columns[4].Width = 80;

            // Row
            int effRowNum = 0;
            bool bColorChange = false;
            Specdgv.Rows.Clear();

            for (int i = 0; i < Spec.Param.Count; i++)
            {
                if (i > 0)
                    if (Spec.Param[i - 1][0].ToString() != Spec.Param[i][0].ToString())
                        bColorChange = !bColorChange;

                Specdgv.Rows.Add(Spec.Param[i][0], Spec.Param[i][1], Spec.Param[i][2], Spec.Param[i][3], Spec.Param[i][9]);
                if (bColorChange)
                {
                    Specdgv[0, effRowNum].Style.BackColor = Color.Lavender;
                    Specdgv[1, effRowNum].Style.BackColor = Color.Lavender;
                    Specdgv[4, effRowNum].Style.BackColor = Color.Lavender;
                }
                else
                {
                    Specdgv[0, effRowNum].Style.BackColor = Color.White;
                    Specdgv[1, effRowNum].Style.BackColor = Color.White;
                    Specdgv[4, effRowNum].Style.BackColor = Color.White;
                }
                Specdgv.Rows[effRowNum].Visible = Convert.ToBoolean(Spec.Param[i][10]);
                effRowNum++;
            }
            string oldkey = "";
            for (int i = 0; i < Spec.Param.Count - 1; i++)
            {
                if (Specdgv.Rows[i].Visible)
                {
                    string newKey = Specdgv.Rows[i].Cells[0].Value.ToString();
                    if (oldkey == newKey) Specdgv.Rows[i].Cells[0].Value = "";
                    oldkey = newKey;
                }
            }

            Specdgv.Rows.Add("", "", "", "", "");

            Specdgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            Specdgv.ColumnHeadersHeight = 22;

            for (int i = 0; i < effRowNum; i++)
            {
                Specdgv.Rows[i].Height = 15;     // spec 높이조절A
                Specdgv.Rows[i].Resizable = DataGridViewTriState.False;
                Specdgv.Rows[i].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
                Specdgv[1, i].Style.Font = new Font("Arial", 9, FontStyle.Bold);
                Specdgv[2, i].Style.Font = new Font("Arial", 9, FontStyle.Bold);
                Specdgv[4, i].Style.Font = new Font("Arial", 9, FontStyle.Italic);
            }

            for (int colum = 2; colum < 4; colum++)
            {
                for (int row = 0; row < Specdgv.Rows.Count; row++)
                {
                    Specdgv[colum, row].Style.BackColor = Color.LightGray;
                    Specdgv.ReadOnly = true;
                }
            }
            Specdgv.ReadOnly = true;
            IsEdit();
        }
        private void InitTodoList()
        {
            ToDoListBox.Items.Clear();
            for (int i = 0; i < Rcp.ToDoList.Count; i++)
                ToDoListBox.Items.Add(Rcp.ToDoList[i]);
        }
        private void InitOption()
        {
            List<CheckBox> listChk = FindCheckBox();
            for (int i = 0; i < listChk.Count; i++)
            {
                if (i >= Option.Param.Count)
                {
                    listChk[i].Text = "Option";
                    listChk[i].Enabled = false;
                }
                else
                {
                    listChk[i].Text = Option.Param[i][0].ToString();
                    listChk[i].Checked = Convert.ToBoolean(Option.Param[i][1]);
                    if (listChk[i].Text == "Safe Sensor Enable")
                    {
                        //if (Model.Maker != "Ansan") listChk[i].Enabled = false;
                    }
                }
            }
        }
        private void InitModel()
        {
            //Barcode Maker
            BarcodeMaker.Items.Clear();
            for (int i = 0; i < Model.MakerList.Count; i++)
                BarcodeMaker.Items.Add(Model.MakerList[i]);
            BarcodeMaker.SelectedItem = Model.Maker;

            //Prism Supplier
            PrismSupplier.Items.Clear();
            for (int i = 0; i < Model.SupplierList.Count; i++)
                PrismSupplier.Items.Add(Model.SupplierList[i]);
            PrismSupplier.SelectedItem = Model.Supplier;

            //Driver IC Type
            DriverICType.Items.Clear();
            for (int i = 0; i < Model.ICList.Count; i++)
                DriverICType.Items.Add(Model.ICList[i]);
            DriverICType.SelectedItem = Model.DriverIC;

            BarcodeRevNo.Text = Model.RevisionNo;
            TesterNumber.Text = Model.TesterNo;
            ProductLine.Text = Model.ProductLine;
            MCNumber.Text = Model.MCNo;

        }
        private void InitScripPath()
        {
            string fwExt = Path.GetExtension(CurrentPath.FWPath);
            FWFilePathTB.Text = FIO.UserScriptDir + CurrentPath.FWPath;
            rtbFiducialMark.Text = FIO.RootDir + CurrentPath.ModelPath;
            Process.LoadBaseModel(rtbFiducialMark.Text);

            if (fwExt == ".bin")
            {
                Process.FWCode = BinFileRead(FWFilePathTB.Text);
            }
            else if (fwExt == ".hex")
            {
                List<string> List = FIO.GetTextAll(FWFilePathTB.Text);

                byte[] result = new byte[40960];
                int i = 0;
                for (i = 0; i < List.Count; i++)
                {
                    byte[] convert = new byte[List[i].Length / 2];

                    for (int j = 0; j < convert.Length; j++)
                    {
                        string div = List[i].Substring(j * 2, 2);
                        convert[j] = Convert.ToByte(div, 16);
                        result[i * convert.Length + j] = convert[j];
                    }
                }
            }
            else if (fwExt == ".txt")
            {

            }

            Process.PIDLine = UserFileRead(PIDFilePathTB.Text = FIO.UserScriptDir + CurrentPath.PIDPath);
            Process.MESLine = UserFileRead(MESPathTB.Text = FIO.UserScriptDir + CurrentPath.MESPath);
        }

        #endregion

        #region *Button Event

        //Recipe
        private void OpenRecipeFile_Click(object sender, EventArgs e)
        {
            string result = FIO.OpenFile(Rcp.InitDir, Rcp.Ext);
            if (result == null) return;
            Rcp.Read(result);
            CurrentPath.ConditionName = Rcp.CurrentName;
            CurrentPath.Save();
            InitCondition();
        }
        private void SaveRecipeFile_Click(object sender, EventArgs e)
        {
            UpdateUI();
            Rcp.Save();
        }
        private void SaveAsRecipeFile_Click(object sender, EventArgs e)
        {
            string result = FIO.OpenFile(Rcp.InitDir, Rcp.Ext, true);
            UpdateUI();
            Rcp.Read(result);
            Rcp.Save(result);
            CurrentPath.ConditionName = Rcp.CurrentName;
            CurrentPath.Save();
            InitCondition();
        }
        private void EditRecipe_CheckedChanged(object sender, EventArgs e)
        {
            IsEdit();
        }

        //Spec
        private void OpenSpecFile_Click(object sender, EventArgs e)
        {
            string result = FIO.OpenFile(Spec.InitDir, Spec.Ext);
            if (result == null) return;
            Spec.Read(result);
            CurrentPath.SpecName = Spec.CurrentName;
            CurrentPath.Save();
            InitDataSpec();
        }
        private void SaveSpecFile_Click(object sender, EventArgs e)
        {
            UpdateUI();
            Spec.Save();
  
        }
        private void SaveAsSpecFile_Click(object sender, EventArgs e)
        {
            string result = FIO.OpenFile(Spec.InitDir, Spec.Ext, true);
            UpdateUI();
            Spec.Save(result);
            Spec.Read(result);
            CurrentPath.SpecName = Spec.CurrentName;
            CurrentPath.Save();
            InitDataSpec();
        }
        private void EditSpec_CheckedChanged(object sender, EventArgs e)
        {
            IsEdit();
        }


        //Test Items
        private void AddItem_Click(object sender, EventArgs e)
        {
            if (ActionListBox.SelectedItems == null) return;
            for (int i = 0; i < ActionListBox.SelectedItems.Count; i++)
            {
                string target = ActionListBox.SelectedItems[i].ToString();
                foreach (var l in Process.ItemList)
                    if (l.Name == target)
                    {
                        bool isExist = false;
                        foreach (var t in Rcp.ToDoList) if (t == target) isExist = true;
                        if (!isExist) Rcp.ToDoList.Add(l.Name);
                    }
            }

            InitTodoList();
        }
        private void RemoveItem_Click(object sender, EventArgs e)
        {
            if (ToDoListBox.SelectedItems == null) return;
            for (int i = 0; i < ToDoListBox.SelectedItems.Count; i++)
            {
                string sName = ToDoListBox.SelectedItems[i].ToString();
                foreach (var l in Process.ItemList)
                    if (sName.Contains(l.Name))
                    {
                        Rcp.ToDoList.Remove(sName);
                    }
            }

            InitTodoList();
        }
        private void ItemUP_Click(object sender, EventArgs e)
        {
            MoveTodo(true);
        }
        private void ItemDown_Click(object sender, EventArgs e)
        {
            MoveTodo(false);
        }
        private void ScriptUpdate_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;

            string ext = "";
            switch (bt.TabIndex)
            {
                case 0:
                    ext = Path.GetExtension(CurrentPath.FWPath); break;
                case 1:
                    ext = Path.GetExtension(CurrentPath.PIDPath); break;
                case 2:
                    ext = Path.GetExtension(CurrentPath.MESPath); break;
                case 3:
                    ext = Path.GetExtension(CurrentPath.ModelPath); break;

            }
            string newPath = "";
            if(bt.TabIndex == 3) newPath = FIO.OpenFile(FIO.RootDir, ext);        
            else newPath = FIO.OpenFile(FIO.UserScriptDir, ext);

            if (newPath == null) return;
            newPath = Path.GetFileName(newPath);
            switch (bt.TabIndex)
            {
                case 0:
                    CurrentPath.FWPath = newPath; break;
                case 1:
                    CurrentPath.PIDPath = newPath; break;
                case 2:
                    CurrentPath.MESPath = newPath; break;
                case 3:
                    CurrentPath.ModelPath = newPath; break;
            }

            InitScripPath();
        }
        private void Apply_Click(object sender, EventArgs e)
        {
            try
            {
                //Current Path
                CurrentPath.Save();

                //Option
                List<CheckBox> listChk = FindCheckBox();
                for (int i = 0; i < Option.Param.Count; i++)
                    Option.Param[i][1] = listChk[i].Checked;
                Option.SetParam();
                Option.Save();

                //Model
                if (BarcodeMaker.SelectedItem != null) Model.Maker = BarcodeMaker.SelectedItem.ToString();
                if (PrismSupplier.SelectedItem != null) Model.Supplier = PrismSupplier.SelectedItem.ToString();
                if (DriverICType.SelectedItem != null) Model.DriverIC = DriverICType.SelectedItem.ToString();
                Model.RevisionNo = BarcodeRevNo.Text;
                Model.TesterNo = TesterNumber.Text;
                Model.ProductLine = ProductLine.Text;
                Model.MCNo = MCNumber.Text;
                Model.Save();
                InitModel();

                MessageBox.Show("Save Complete.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                throw;
            }
           
        }
        private void Specdgv_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                new TestItemOnOff().ShowDialog();
                InitDataSpec();
            }
        }

        #endregion

        #region *Etc
        private List<CheckBox> FindCheckBox()
        {
            List<CheckBox> listChk = new List<CheckBox>();
            for (int i = 1; i < 21; i++)
            {
                foreach (Control c in OptionStateTLP.Controls)
                {
                    string name = string.Format("OptionChk{0}", i);
                    if (c.Name == name) listChk.Add((CheckBox)c);
                }
            }
            return listChk;
        }
        private void DriverIC_SwitchOn(object sender, EventArgs e)
        {
            Process.SwitchRun();
        }
        public string[] UserFileRead(string fileName)
        {
            string[] reselt;
            if (fileName != "")
            {
                if (!File.Exists(fileName))
                {
                    return null;
                }
                StreamReader sr = new StreamReader(fileName);
                string AllLines = sr.ReadToEnd();
                reselt = AllLines.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                sr.Close();
            }
            else
            {
                return null;
            }
            return reselt;
        }
        public byte[] BinFileRead(string fileName)
        {
            byte[] reselt;
            if (fileName != "")
            {
                if (!File.Exists(fileName))
                {
                    return null;
                }
                BinaryReader binReader = new BinaryReader(File.Open(fileName, FileMode.Open));
                reselt = binReader.ReadBytes(49152);
                binReader.Close();
            }
            else
            {
                return null;
            }
            return reselt;
        }
        private void IsEdit()
        {
            if (EditRecipeCB.Checked)
            {
                Recipedgv.ReadOnly = false;
                for (int row = 0; row < Recipedgv.Rows.Count; row++)
                {
                    {
                        Recipedgv[2, row].Style.BackColor = Color.White;
                        Recipedgv[0, row].ReadOnly = true;
                        Recipedgv[1, row].ReadOnly = true;
                        Recipedgv[3, row].ReadOnly = true;
                    }
                }
            }
            else
            {
                Recipedgv.ReadOnly = true;
                for (int row = 0; row < Recipedgv.Rows.Count; row++)
                {
                    Recipedgv[2, row].Style.BackColor = Color.LightGray;
                }
            }
            if (EditSpecCB.Checked == true)
            {
                Specdgv.ReadOnly = false;
                for (int row = 0; row < Specdgv.Rows.Count; row++)
                {
                    {
                        Specdgv[2, row].Style.BackColor = Color.White;
                        Specdgv[3, row].Style.BackColor = Color.White;
                        Specdgv[0, row].ReadOnly = true;
                        Specdgv[1, row].ReadOnly = true;
                        Specdgv[4, row].ReadOnly = true;
                    }
                }
            }
            else
            {
                Specdgv.ReadOnly = true;
                for (int row = 0; row < Specdgv.Rows.Count; row++)
                {
                    Specdgv[2, row].Style.BackColor = Color.LightGray;
                    Specdgv[3, row].Style.BackColor = Color.LightGray;
                }
            }
        }
        public void UpdateUI()
        {
            Rcp.ToDoList.Clear();
            for (int i = 0; i < ToDoListBox.Items.Count; i++)
            {
                Rcp.ToDoList.Add(ToDoListBox.Items[i].ToString());
            }
            for (int i = 0; i < Rcp.Param.Count; i++)
            {
                Rcp.Param[i][2] = Recipedgv[2, i].Value.ToString();
            }
            for (int i = 0; i < Spec.Param.Count; i++)
            {
                Spec.Param[i][2] = Specdgv[2, i].Value.ToString();
                Spec.Param[i][3] = Specdgv[3, i].Value.ToString();
            }
        }
        public void MoveTodo(bool dir) // true : up, false : down
        {
            int cIndex = ToDoListBox.SelectedIndex;
            if (cIndex < 0) return;
            if (cIndex <= 0 && dir) return;
            if ((cIndex + 1 >= ToDoListBox.Items.Count) && !dir) return;

            int target = 0;
            for (int i = 0; i < ToDoListBox.SelectedItems.Count; i++)
            {
                if (dir)
                    Rcp.ToDoList.Move(cIndex + i, target = (cIndex + i - 1));
                else
                    Rcp.ToDoList.Move(cIndex + i, target = (cIndex + i + 1));
            }

            ToDoListBox.Items.Clear();
            for (int i = 0; i < Rcp.ToDoList.Count; i++)
            {
                ToDoListBox.Items.Add(Rcp.ToDoList[i]);
            }
            ToDoListBox.SelectedIndex = target;
        }
        private void Recipedgv_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                InitCondition();
            }
        }


        #endregion
    }
}
