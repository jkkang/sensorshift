﻿

namespace SensorShift
{
    partial class F_Oprator
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F_Oprator));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series27 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series28 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series29 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series30 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series31 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series32 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series33 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series34 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series35 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series36 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series37 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series38 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series39 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series40 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series41 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series42 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title6 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series43 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series44 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series45 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series46 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series47 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series48 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series49 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series50 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series51 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title7 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series52 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series53 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series54 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series55 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series56 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series57 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series58 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series59 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title8 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series60 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series61 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series62 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series63 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title9 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series64 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series65 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series66 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series67 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title10 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend11 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series68 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series69 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series70 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series71 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title11 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.ResultDataGrid = new System.Windows.Forms.DataGridView();
            this.LogCH2 = new System.Windows.Forms.TextBox();
            this.LogCH1 = new System.Windows.Forms.TextBox();
            this.TPblank = new System.Windows.Forms.TableLayoutPanel();
            this.PcontrolBtn = new System.Windows.Forms.Panel();
            this.LastSampleNum = new System.Windows.Forms.TextBox();
            this.SaveLogsBtn = new System.Windows.Forms.Button();
            this.LastTestedSPLNoLbl = new System.Windows.Forms.Label();
            this.SetSampleNumberBtn = new System.Windows.Forms.Button();
            this.ClearLogsBtn = new System.Windows.Forms.Button();
            this.RepeatStartTest = new System.Windows.Forms.Button();
            this.SuddenStop = new System.Windows.Forms.Button();
            this.NewSampleNumber = new System.Windows.Forms.TextBox();
            this.Progressbar = new System.Windows.Forms.PictureBox();
            this.RepeatLoadingUnloadingLbl = new System.Windows.Forms.Label();
            this.CurrentRunCnt = new System.Windows.Forms.TextBox();
            this.RepeatRunCnt = new System.Windows.Forms.TextBox();
            this.LotNameLbl = new System.Windows.Forms.Label();
            this.LotID = new System.Windows.Forms.TextBox();
            this.CheckPinContactBtn = new System.Windows.Forms.Button();
            this.YieldChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblCH1 = new System.Windows.Forms.Label();
            this.StrokeChart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ThetaChart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RotationChart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.TrajectoryChart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.StrokeChart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ThetaChart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RotationChart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.TrajectoryChart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblCH2 = new System.Windows.Forms.Label();
            this.InfoView1 = new System.Windows.Forms.Button();
            this.InfoView0 = new System.Windows.Forms.Button();
            this.SafeSensor = new System.Windows.Forms.Label();
            this.CoverLoadUnload = new System.Windows.Forms.Label();
            this.SocketLoadUnload = new System.Windows.Forms.Label();
            this.State = new System.Windows.Forms.GroupBox();
            this.StepChart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.StepChart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.ResultDataGrid)).BeginInit();
            this.PcontrolBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Progressbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YieldChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrokeChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThetaChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RotationChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrajectoryChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrokeChart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThetaChart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RotationChart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrajectoryChart2)).BeginInit();
            this.State.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StepChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StepChart2)).BeginInit();
            this.SuspendLayout();
            // 
            // ResultDataGrid
            // 
            this.ResultDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.ResultDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ResultDataGrid.Location = new System.Drawing.Point(477, 614);
            this.ResultDataGrid.Name = "ResultDataGrid";
            this.ResultDataGrid.ReadOnly = true;
            this.ResultDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ResultDataGrid.RowTemplate.Height = 23;
            this.ResultDataGrid.Size = new System.Drawing.Size(940, 302);
            this.ResultDataGrid.TabIndex = 4;
            this.ResultDataGrid.Tag = "S";
            this.ResultDataGrid.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Result_MouseDoubleClick);
            // 
            // LogCH2
            // 
            this.LogCH2.BackColor = System.Drawing.Color.Black;
            this.LogCH2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LogCH2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogCH2.ForeColor = System.Drawing.Color.LemonChiffon;
            this.LogCH2.Location = new System.Drawing.Point(1427, 2);
            this.LogCH2.Multiline = true;
            this.LogCH2.Name = "LogCH2";
            this.LogCH2.ReadOnly = true;
            this.LogCH2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LogCH2.Size = new System.Drawing.Size(476, 80);
            this.LogCH2.TabIndex = 1;
            this.LogCH2.Tag = "S";
            this.LogCH2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Log_MouseDoubleClick);
            // 
            // LogCH1
            // 
            this.LogCH1.BackColor = System.Drawing.Color.Black;
            this.LogCH1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LogCH1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogCH1.ForeColor = System.Drawing.Color.LemonChiffon;
            this.LogCH1.Location = new System.Drawing.Point(477, 2);
            this.LogCH1.Multiline = true;
            this.LogCH1.Name = "LogCH1";
            this.LogCH1.ReadOnly = true;
            this.LogCH1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LogCH1.Size = new System.Drawing.Size(476, 80);
            this.LogCH1.TabIndex = 0;
            this.LogCH1.Tag = "S";
            this.LogCH1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Log_MouseDoubleClick);
            // 
            // TPblank
            // 
            this.TPblank.ColumnCount = 1;
            this.TPblank.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TPblank.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TPblank.Location = new System.Drawing.Point(0, 918);
            this.TPblank.Name = "TPblank";
            this.TPblank.RowCount = 1;
            this.TPblank.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TPblank.Size = new System.Drawing.Size(1904, 60);
            this.TPblank.TabIndex = 144;
            // 
            // PcontrolBtn
            // 
            this.PcontrolBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.PcontrolBtn.Controls.Add(this.LastSampleNum);
            this.PcontrolBtn.Controls.Add(this.SaveLogsBtn);
            this.PcontrolBtn.Controls.Add(this.LastTestedSPLNoLbl);
            this.PcontrolBtn.Controls.Add(this.SetSampleNumberBtn);
            this.PcontrolBtn.Controls.Add(this.ClearLogsBtn);
            this.PcontrolBtn.Controls.Add(this.RepeatStartTest);
            this.PcontrolBtn.Controls.Add(this.SuddenStop);
            this.PcontrolBtn.Controls.Add(this.NewSampleNumber);
            this.PcontrolBtn.Controls.Add(this.Progressbar);
            this.PcontrolBtn.Controls.Add(this.RepeatLoadingUnloadingLbl);
            this.PcontrolBtn.Controls.Add(this.CurrentRunCnt);
            this.PcontrolBtn.Controls.Add(this.RepeatRunCnt);
            this.PcontrolBtn.Location = new System.Drawing.Point(0, 665);
            this.PcontrolBtn.Name = "PcontrolBtn";
            this.PcontrolBtn.Size = new System.Drawing.Size(477, 251);
            this.PcontrolBtn.TabIndex = 145;
            // 
            // LastSampleNum
            // 
            this.LastSampleNum.BackColor = System.Drawing.Color.White;
            this.LastSampleNum.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastSampleNum.ForeColor = System.Drawing.Color.Black;
            this.LastSampleNum.Location = new System.Drawing.Point(168, 123);
            this.LastSampleNum.Name = "LastSampleNum";
            this.LastSampleNum.Size = new System.Drawing.Size(60, 25);
            this.LastSampleNum.TabIndex = 204;
            this.LastSampleNum.Text = "9999";
            this.LastSampleNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // SaveLogsBtn
            // 
            this.SaveLogsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SaveLogsBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg4;
            this.SaveLogsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SaveLogsBtn.FlatAppearance.BorderSize = 0;
            this.SaveLogsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveLogsBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveLogsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.SaveLogsBtn.Location = new System.Drawing.Point(239, 63);
            this.SaveLogsBtn.Name = "SaveLogsBtn";
            this.SaveLogsBtn.Size = new System.Drawing.Size(236, 53);
            this.SaveLogsBtn.TabIndex = 217;
            this.SaveLogsBtn.Text = "Save Log";
            this.SaveLogsBtn.UseVisualStyleBackColor = false;
            this.SaveLogsBtn.Click += new System.EventHandler(this.SaveLogsBtn_Click);
            this.SaveLogsBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.SaveLogsBtn.MouseHover += new System.EventHandler(this.Button_MouseMove);
            // 
            // LastTestedSPLNoLbl
            // 
            this.LastTestedSPLNoLbl.AutoSize = true;
            this.LastTestedSPLNoLbl.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastTestedSPLNoLbl.Location = new System.Drawing.Point(10, 126);
            this.LastTestedSPLNoLbl.Name = "LastTestedSPLNoLbl";
            this.LastTestedSPLNoLbl.Size = new System.Drawing.Size(152, 18);
            this.LastTestedSPLNoLbl.TabIndex = 201;
            this.LastTestedSPLNoLbl.Text = "Last Tested SPL No.";
            // 
            // SetSampleNumberBtn
            // 
            this.SetSampleNumberBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SetSampleNumberBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg4;
            this.SetSampleNumberBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SetSampleNumberBtn.FlatAppearance.BorderSize = 0;
            this.SetSampleNumberBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetSampleNumberBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetSampleNumberBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.SetSampleNumberBtn.Location = new System.Drawing.Point(239, 123);
            this.SetSampleNumberBtn.Name = "SetSampleNumberBtn";
            this.SetSampleNumberBtn.Size = new System.Drawing.Size(170, 29);
            this.SetSampleNumberBtn.TabIndex = 213;
            this.SetSampleNumberBtn.Text = "Set Sample No.";
            this.SetSampleNumberBtn.UseVisualStyleBackColor = false;
            this.SetSampleNumberBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.SetSampleNumberBtn.MouseHover += new System.EventHandler(this.Button_MouseMove);
            // 
            // ClearLogsBtn
            // 
            this.ClearLogsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ClearLogsBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg4;
            this.ClearLogsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClearLogsBtn.FlatAppearance.BorderSize = 0;
            this.ClearLogsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearLogsBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearLogsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.ClearLogsBtn.Location = new System.Drawing.Point(2, 63);
            this.ClearLogsBtn.Name = "ClearLogsBtn";
            this.ClearLogsBtn.Size = new System.Drawing.Size(236, 53);
            this.ClearLogsBtn.TabIndex = 215;
            this.ClearLogsBtn.Text = "Clear Log";
            this.ClearLogsBtn.UseVisualStyleBackColor = false;
            this.ClearLogsBtn.Click += new System.EventHandler(this.btnClearLogs_Click);
            this.ClearLogsBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.ClearLogsBtn.MouseHover += new System.EventHandler(this.Button_MouseMove);
            // 
            // RepeatStartTest
            // 
            this.RepeatStartTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.RepeatStartTest.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg0;
            this.RepeatStartTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RepeatStartTest.FlatAppearance.BorderSize = 0;
            this.RepeatStartTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RepeatStartTest.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepeatStartTest.ForeColor = System.Drawing.Color.White;
            this.RepeatStartTest.Location = new System.Drawing.Point(2, 188);
            this.RepeatStartTest.Name = "RepeatStartTest";
            this.RepeatStartTest.Size = new System.Drawing.Size(236, 63);
            this.RepeatStartTest.TabIndex = 211;
            this.RepeatStartTest.Text = "Start Test";
            this.RepeatStartTest.UseVisualStyleBackColor = false;
            this.RepeatStartTest.Click += new System.EventHandler(this.StartTestBtn_Click);
            this.RepeatStartTest.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.RepeatStartTest.MouseHover += new System.EventHandler(this.Button_MouseMove);
            // 
            // SuddenStop
            // 
            this.SuddenStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.SuddenStop.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.SuddenStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SuddenStop.FlatAppearance.BorderSize = 0;
            this.SuddenStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SuddenStop.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SuddenStop.ForeColor = System.Drawing.Color.White;
            this.SuddenStop.Location = new System.Drawing.Point(239, 188);
            this.SuddenStop.Name = "SuddenStop";
            this.SuddenStop.Size = new System.Drawing.Size(236, 63);
            this.SuddenStop.TabIndex = 212;
            this.SuddenStop.Text = "Stop";
            this.SuddenStop.UseVisualStyleBackColor = false;
            this.SuddenStop.Click += new System.EventHandler(this.StopTestBtn_Click);
            this.SuddenStop.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.SuddenStop.MouseHover += new System.EventHandler(this.Button_MouseMove);
            // 
            // NewSampleNumber
            // 
            this.NewSampleNumber.BackColor = System.Drawing.Color.White;
            this.NewSampleNumber.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewSampleNumber.ForeColor = System.Drawing.Color.Black;
            this.NewSampleNumber.Location = new System.Drawing.Point(415, 125);
            this.NewSampleNumber.Name = "NewSampleNumber";
            this.NewSampleNumber.Size = new System.Drawing.Size(60, 25);
            this.NewSampleNumber.TabIndex = 200;
            this.NewSampleNumber.Text = "9999";
            this.NewSampleNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Progressbar
            // 
            this.Progressbar.BackColor = System.Drawing.Color.Gray;
            this.Progressbar.ErrorImage = null;
            this.Progressbar.Image = ((System.Drawing.Image)(resources.GetObject("Progressbar.Image")));
            this.Progressbar.Location = new System.Drawing.Point(0, 1);
            this.Progressbar.Name = "Progressbar";
            this.Progressbar.Size = new System.Drawing.Size(477, 56);
            this.Progressbar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Progressbar.TabIndex = 216;
            this.Progressbar.TabStop = false;
            // 
            // RepeatLoadingUnloadingLbl
            // 
            this.RepeatLoadingUnloadingLbl.AutoSize = true;
            this.RepeatLoadingUnloadingLbl.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepeatLoadingUnloadingLbl.Location = new System.Drawing.Point(9, 161);
            this.RepeatLoadingUnloadingLbl.Name = "RepeatLoadingUnloadingLbl";
            this.RepeatLoadingUnloadingLbl.Size = new System.Drawing.Size(216, 18);
            this.RepeatLoadingUnloadingLbl.TabIndex = 195;
            this.RepeatLoadingUnloadingLbl.Text = "Repeat Loading / Unloading #";
            // 
            // CurrentRunCnt
            // 
            this.CurrentRunCnt.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentRunCnt.Location = new System.Drawing.Point(361, 157);
            this.CurrentRunCnt.Name = "CurrentRunCnt";
            this.CurrentRunCnt.ReadOnly = true;
            this.CurrentRunCnt.Size = new System.Drawing.Size(112, 25);
            this.CurrentRunCnt.TabIndex = 196;
            this.CurrentRunCnt.Text = "1";
            this.CurrentRunCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RepeatRunCnt
            // 
            this.RepeatRunCnt.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepeatRunCnt.Location = new System.Drawing.Point(239, 157);
            this.RepeatRunCnt.Name = "RepeatRunCnt";
            this.RepeatRunCnt.Size = new System.Drawing.Size(112, 25);
            this.RepeatRunCnt.TabIndex = 194;
            this.RepeatRunCnt.Text = "1";
            this.RepeatRunCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LotNameLbl
            // 
            this.LotNameLbl.AutoSize = true;
            this.LotNameLbl.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LotNameLbl.Location = new System.Drawing.Point(10, 627);
            this.LotNameLbl.Name = "LotNameLbl";
            this.LotNameLbl.Size = new System.Drawing.Size(75, 18);
            this.LotNameLbl.TabIndex = 219;
            this.LotNameLbl.Text = "Lot Name";
            // 
            // LotID
            // 
            this.LotID.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.LotID.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LotID.ForeColor = System.Drawing.Color.Red;
            this.LotID.Location = new System.Drawing.Point(90, 623);
            this.LotID.Name = "LotID";
            this.LotID.Size = new System.Drawing.Size(137, 25);
            this.LotID.TabIndex = 218;
            this.LotID.TextChanged += new System.EventHandler(this.LotID_TextChanged);
            // 
            // CheckPinContactBtn
            // 
            this.CheckPinContactBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.CheckPinContactBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg4;
            this.CheckPinContactBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CheckPinContactBtn.FlatAppearance.BorderSize = 0;
            this.CheckPinContactBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CheckPinContactBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckPinContactBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(16)))), ((int)(((byte)(0)))));
            this.CheckPinContactBtn.Location = new System.Drawing.Point(238, 614);
            this.CheckPinContactBtn.Name = "CheckPinContactBtn";
            this.CheckPinContactBtn.Size = new System.Drawing.Size(236, 45);
            this.CheckPinContactBtn.TabIndex = 214;
            this.CheckPinContactBtn.Text = "Check Pin Contact\r\n";
            this.CheckPinContactBtn.UseVisualStyleBackColor = false;
            this.CheckPinContactBtn.Click += new System.EventHandler(this.btnCheckPinContact_Click);
            this.CheckPinContactBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.CheckPinContactBtn.MouseHover += new System.EventHandler(this.Button_MouseMove);
            // 
            // YieldChart
            // 
            this.YieldChart.AllowDrop = true;
            this.YieldChart.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.YieldChart.BorderlineColor = System.Drawing.Color.Black;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            chartArea1.ShadowColor = System.Drawing.Color.White;
            this.YieldChart.ChartAreas.Add(chartArea1);
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.BackColor = System.Drawing.Color.Transparent;
            legend1.BackSecondaryColor = System.Drawing.Color.Transparent;
            legend1.BorderColor = System.Drawing.Color.Transparent;
            legend1.BorderWidth = 0;
            legend1.DockedToChartArea = "ChartArea1";
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            legend1.Position.Auto = false;
            legend1.Position.Height = 25F;
            legend1.Position.Width = 98F;
            legend1.Position.Y = 75F;
            legend1.ShadowColor = System.Drawing.Color.White;
            legend1.TitleBackColor = System.Drawing.Color.Transparent;
            legend1.TitleFont = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YieldChart.Legends.Add(legend1);
            this.YieldChart.Location = new System.Drawing.Point(1422, 614);
            this.YieldChart.Name = "YieldChart";
            this.YieldChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series5";
            this.YieldChart.Series.Add(series1);
            this.YieldChart.Size = new System.Drawing.Size(470, 302);
            this.YieldChart.TabIndex = 163;
            this.YieldChart.Tag = "S";
            title1.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title1.BackColor = System.Drawing.Color.Transparent;
            title1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title1.Name = "Title1";
            title1.Position.Auto = false;
            title1.Position.Height = 8F;
            title1.Position.Width = 55F;
            title1.Text = "Yield";
            title1.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.YieldChart.Titles.Add(title1);
            this.YieldChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.YieldChart_MouseDoubleClick);
            // 
            // lblCH1
            // 
            this.lblCH1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCH1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCH1.Location = new System.Drawing.Point(1, 52);
            this.lblCH1.Name = "lblCH1";
            this.lblCH1.Size = new System.Drawing.Size(473, 30);
            this.lblCH1.TabIndex = 149;
            this.lblCH1.Text = "CH1";
            this.lblCH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StrokeChart1
            // 
            this.StrokeChart1.AllowDrop = true;
            this.StrokeChart1.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.StrokeChart1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.StrokeChart1.BorderlineColor = System.Drawing.Color.Black;
            chartArea2.Name = "ChartArea1";
            this.StrokeChart1.ChartAreas.Add(chartArea2);
            legend2.Alignment = System.Drawing.StringAlignment.Far;
            legend2.BackColor = System.Drawing.Color.Transparent;
            legend2.BorderColor = System.Drawing.Color.Transparent;
            legend2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend2.IsTextAutoFit = false;
            legend2.Name = "Legend1";
            legend2.Position.Auto = false;
            legend2.Position.Height = 20F;
            legend2.Position.Width = 40F;
            this.StrokeChart1.Legends.Add(legend2);
            this.StrokeChart1.Location = new System.Drawing.Point(3, 88);
            this.StrokeChart1.Name = "StrokeChart1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Stroke FWD";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Stroke BWD";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "Hall Y FWD";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "Hall Y BWD";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Legend = "Legend1";
            series6.Name = "Hall X FWD";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Legend = "Legend1";
            series7.Name = "Hall X BWD";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Legend = "Legend1";
            series8.Name = "Current";
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Legend = "Legend1";
            series9.Name = "StepResponse";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Legend = "Legend1";
            series10.Name = "Cross Stroke FWD";
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Legend = "Legend1";
            series11.Name = "Cross Stroke BWD";
            this.StrokeChart1.Series.Add(series2);
            this.StrokeChart1.Series.Add(series3);
            this.StrokeChart1.Series.Add(series4);
            this.StrokeChart1.Series.Add(series5);
            this.StrokeChart1.Series.Add(series6);
            this.StrokeChart1.Series.Add(series7);
            this.StrokeChart1.Series.Add(series8);
            this.StrokeChart1.Series.Add(series9);
            this.StrokeChart1.Series.Add(series10);
            this.StrokeChart1.Series.Add(series11);
            this.StrokeChart1.Size = new System.Drawing.Size(470, 257);
            this.StrokeChart1.TabIndex = 0;
            this.StrokeChart1.Text = "chart1";
            title2.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title2.BackColor = System.Drawing.Color.Transparent;
            title2.DockedToChartArea = "ChartArea1";
            title2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title2.Name = "Title1";
            title2.Position.Auto = false;
            title2.Text = "Stroke";
            title2.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.StrokeChart1.Titles.Add(title2);
            this.StrokeChart1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.StrokeChart1_MouseDoubleClick);
            // 
            // ThetaChart1
            // 
            this.ThetaChart1.AllowDrop = true;
            this.ThetaChart1.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.ThetaChart1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.ThetaChart1.BorderlineColor = System.Drawing.Color.Black;
            chartArea3.Name = "ChartArea1";
            this.ThetaChart1.ChartAreas.Add(chartArea3);
            legend3.Alignment = System.Drawing.StringAlignment.Far;
            legend3.BackColor = System.Drawing.Color.Transparent;
            legend3.BorderColor = System.Drawing.Color.Transparent;
            legend3.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend3.IsTextAutoFit = false;
            legend3.Name = "Legend1";
            legend3.Position.Auto = false;
            legend3.Position.Height = 20F;
            legend3.Position.Width = 40F;
            this.ThetaChart1.Legends.Add(legend3);
            this.ThetaChart1.Location = new System.Drawing.Point(479, 88);
            this.ThetaChart1.Name = "ThetaChart1";
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Legend = "Legend1";
            series12.Name = "Tilt ~30um";
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Legend = "Legend1";
            series13.Name = "Stroke FWD";
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Legend = "Legend1";
            series14.Name = "Stroke BWD";
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series15.Legend = "Legend1";
            series15.Name = "X Hall";
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series16.Legend = "Legend1";
            series16.Name = "15minCircle";
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series17.Legend = "Legend1";
            series17.Name = "20minCircle";
            series18.ChartArea = "ChartArea1";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series18.Legend = "Legend1";
            series18.Name = "Series7";
            series19.ChartArea = "ChartArea1";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series19.Legend = "Legend1";
            series19.Name = "Cross FWD";
            series20.ChartArea = "ChartArea1";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series20.Legend = "Legend1";
            series20.Name = "Cross BWD";
            this.ThetaChart1.Series.Add(series12);
            this.ThetaChart1.Series.Add(series13);
            this.ThetaChart1.Series.Add(series14);
            this.ThetaChart1.Series.Add(series15);
            this.ThetaChart1.Series.Add(series16);
            this.ThetaChart1.Series.Add(series17);
            this.ThetaChart1.Series.Add(series18);
            this.ThetaChart1.Series.Add(series19);
            this.ThetaChart1.Series.Add(series20);
            this.ThetaChart1.Size = new System.Drawing.Size(470, 257);
            this.ThetaChart1.TabIndex = 1;
            this.ThetaChart1.Text = "chart1";
            title3.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title3.BackColor = System.Drawing.Color.Transparent;
            title3.DockedToChartArea = "ChartArea1";
            title3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title3.Name = "Title1";
            title3.Position.Auto = false;
            title3.Text = "Thete Plot";
            title3.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.ThetaChart1.Titles.Add(title3);
            this.ThetaChart1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ThetaChart1_MouseDoubleClick);
            // 
            // RotationChart1
            // 
            this.RotationChart1.AllowDrop = true;
            this.RotationChart1.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.RotationChart1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.RotationChart1.BorderlineColor = System.Drawing.Color.Black;
            chartArea4.Name = "ChartArea1";
            this.RotationChart1.ChartAreas.Add(chartArea4);
            legend4.Alignment = System.Drawing.StringAlignment.Far;
            legend4.BackColor = System.Drawing.Color.Transparent;
            legend4.BorderColor = System.Drawing.Color.Transparent;
            legend4.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend4.IsTextAutoFit = false;
            legend4.Name = "Legend1";
            legend4.Position.Auto = false;
            legend4.Position.Height = 20F;
            legend4.Position.Width = 40F;
            this.RotationChart1.Legends.Add(legend4);
            this.RotationChart1.Location = new System.Drawing.Point(3, 351);
            this.RotationChart1.Name = "RotationChart1";
            series21.ChartArea = "ChartArea1";
            series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series21.Legend = "Legend1";
            series21.Name = "MainStroke";
            series22.ChartArea = "ChartArea1";
            series22.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series22.Legend = "Legend1";
            series22.Name = "SubStroke";
            series23.ChartArea = "ChartArea1";
            series23.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series23.Legend = "Legend1";
            series23.Name = "BackStroke";
            series24.ChartArea = "ChartArea1";
            series24.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series24.Legend = "Legend1";
            series24.Name = "MainHall";
            series25.ChartArea = "ChartArea1";
            series25.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series25.Legend = "Legend1";
            series25.Name = "SubHall";
            series26.ChartArea = "ChartArea1";
            series26.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series26.Legend = "Legend1";
            series26.Name = "Series6";
            series27.ChartArea = "ChartArea1";
            series27.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series27.Legend = "Legend1";
            series27.Name = "Series7";
            series28.ChartArea = "ChartArea1";
            series28.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series28.Legend = "Legend1";
            series28.Name = "Series8";
            this.RotationChart1.Series.Add(series21);
            this.RotationChart1.Series.Add(series22);
            this.RotationChart1.Series.Add(series23);
            this.RotationChart1.Series.Add(series24);
            this.RotationChart1.Series.Add(series25);
            this.RotationChart1.Series.Add(series26);
            this.RotationChart1.Series.Add(series27);
            this.RotationChart1.Series.Add(series28);
            this.RotationChart1.Size = new System.Drawing.Size(470, 257);
            this.RotationChart1.TabIndex = 0;
            this.RotationChart1.Text = "chart1";
            title4.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title4.BackColor = System.Drawing.Color.Transparent;
            title4.DockedToChartArea = "ChartArea1";
            title4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title4.Name = "Title1";
            title4.Position.Auto = false;
            title4.Text = "Rotation";
            title4.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.RotationChart1.Titles.Add(title4);
            this.RotationChart1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.RotationChart1_MouseDoubleClick);
            // 
            // TrajectoryChart1
            // 
            this.TrajectoryChart1.AllowDrop = true;
            this.TrajectoryChart1.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.TrajectoryChart1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.TrajectoryChart1.BorderlineColor = System.Drawing.Color.Black;
            chartArea5.Name = "ChartArea1";
            this.TrajectoryChart1.ChartAreas.Add(chartArea5);
            legend5.Alignment = System.Drawing.StringAlignment.Far;
            legend5.BackColor = System.Drawing.Color.Transparent;
            legend5.BorderColor = System.Drawing.Color.Transparent;
            legend5.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend5.IsTextAutoFit = false;
            legend5.Name = "Legend1";
            legend5.Position.Auto = false;
            legend5.Position.Height = 20F;
            legend5.Position.Width = 40F;
            this.TrajectoryChart1.Legends.Add(legend5);
            this.TrajectoryChart1.Location = new System.Drawing.Point(479, 351);
            this.TrajectoryChart1.Name = "TrajectoryChart1";
            series29.ChartArea = "ChartArea1";
            series29.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series29.Legend = "Legend1";
            series29.Name = "X Amplitude";
            series30.ChartArea = "ChartArea1";
            series30.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series30.Legend = "Legend1";
            series30.Name = "X Phase";
            series31.ChartArea = "ChartArea1";
            series31.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series31.Legend = "Legend1";
            series31.Name = "Series3";
            series32.ChartArea = "ChartArea1";
            series32.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series32.Legend = "Legend1";
            series32.Name = "Series4";
            this.TrajectoryChart1.Series.Add(series29);
            this.TrajectoryChart1.Series.Add(series30);
            this.TrajectoryChart1.Series.Add(series31);
            this.TrajectoryChart1.Series.Add(series32);
            this.TrajectoryChart1.Size = new System.Drawing.Size(470, 257);
            this.TrajectoryChart1.TabIndex = 1;
            this.TrajectoryChart1.Text = "chart1";
            title5.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title5.BackColor = System.Drawing.Color.Transparent;
            title5.DockedToChartArea = "ChartArea1";
            title5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title5.Name = "Title1";
            title5.Position.Auto = false;
            title5.Text = "Trajectory";
            title5.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.TrajectoryChart1.Titles.Add(title5);
            this.TrajectoryChart1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrajectoryChart1_MouseDoubleClick);
            // 
            // StrokeChart2
            // 
            this.StrokeChart2.AllowDrop = true;
            this.StrokeChart2.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.StrokeChart2.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.StrokeChart2.BorderlineColor = System.Drawing.Color.Black;
            chartArea6.Name = "ChartArea1";
            this.StrokeChart2.ChartAreas.Add(chartArea6);
            legend6.Alignment = System.Drawing.StringAlignment.Far;
            legend6.BackColor = System.Drawing.Color.Transparent;
            legend6.BorderColor = System.Drawing.Color.Transparent;
            legend6.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend6.IsTextAutoFit = false;
            legend6.Name = "Legend1";
            legend6.Position.Auto = false;
            legend6.Position.Height = 20F;
            legend6.Position.Width = 40F;
            this.StrokeChart2.Legends.Add(legend6);
            this.StrokeChart2.Location = new System.Drawing.Point(955, 88);
            this.StrokeChart2.Name = "StrokeChart2";
            series33.ChartArea = "ChartArea1";
            series33.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series33.Legend = "Legend1";
            series33.Name = "Stroke FWD";
            series34.ChartArea = "ChartArea1";
            series34.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series34.Legend = "Legend1";
            series34.Name = "Stroke BWD";
            series35.ChartArea = "ChartArea1";
            series35.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series35.Legend = "Legend1";
            series35.Name = "Hall Y FWD";
            series36.ChartArea = "ChartArea1";
            series36.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series36.Legend = "Legend1";
            series36.Name = "Hall Y BWD";
            series37.ChartArea = "ChartArea1";
            series37.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series37.Legend = "Legend1";
            series37.Name = "Hall X FWD";
            series38.ChartArea = "ChartArea1";
            series38.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series38.Legend = "Legend1";
            series38.Name = "Hall X BWD";
            series39.ChartArea = "ChartArea1";
            series39.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series39.Legend = "Legend1";
            series39.Name = "Current";
            series40.ChartArea = "ChartArea1";
            series40.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series40.Legend = "Legend1";
            series40.Name = "StepResponse";
            series41.ChartArea = "ChartArea1";
            series41.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series41.Legend = "Legend1";
            series41.Name = "Cross Stroke FWD";
            series42.ChartArea = "ChartArea1";
            series42.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series42.Legend = "Legend1";
            series42.Name = "Cross Stroke BWD";
            this.StrokeChart2.Series.Add(series33);
            this.StrokeChart2.Series.Add(series34);
            this.StrokeChart2.Series.Add(series35);
            this.StrokeChart2.Series.Add(series36);
            this.StrokeChart2.Series.Add(series37);
            this.StrokeChart2.Series.Add(series38);
            this.StrokeChart2.Series.Add(series39);
            this.StrokeChart2.Series.Add(series40);
            this.StrokeChart2.Series.Add(series41);
            this.StrokeChart2.Series.Add(series42);
            this.StrokeChart2.Size = new System.Drawing.Size(470, 257);
            this.StrokeChart2.TabIndex = 2;
            this.StrokeChart2.Text = "chart1";
            title6.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title6.BackColor = System.Drawing.Color.Transparent;
            title6.DockedToChartArea = "ChartArea1";
            title6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title6.Name = "Title1";
            title6.Position.Auto = false;
            title6.Text = "Stroke";
            title6.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.StrokeChart2.Titles.Add(title6);
            this.StrokeChart2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.StrokeChart2_MouseDoubleClick);
            // 
            // ThetaChart2
            // 
            this.ThetaChart2.AllowDrop = true;
            this.ThetaChart2.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.ThetaChart2.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.ThetaChart2.BorderlineColor = System.Drawing.Color.Black;
            chartArea7.Name = "ChartArea1";
            this.ThetaChart2.ChartAreas.Add(chartArea7);
            legend7.Alignment = System.Drawing.StringAlignment.Far;
            legend7.BackColor = System.Drawing.Color.Transparent;
            legend7.BorderColor = System.Drawing.Color.Transparent;
            legend7.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend7.IsTextAutoFit = false;
            legend7.Name = "Legend1";
            legend7.Position.Auto = false;
            legend7.Position.Height = 20F;
            legend7.Position.Width = 40F;
            this.ThetaChart2.Legends.Add(legend7);
            this.ThetaChart2.Location = new System.Drawing.Point(1431, 88);
            this.ThetaChart2.Name = "ThetaChart2";
            series43.ChartArea = "ChartArea1";
            series43.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series43.Legend = "Legend1";
            series43.Name = "Tilt ~30um";
            series44.ChartArea = "ChartArea1";
            series44.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series44.Legend = "Legend1";
            series44.Name = "Stroke FWD";
            series45.ChartArea = "ChartArea1";
            series45.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series45.Legend = "Legend1";
            series45.Name = "Stroke BWD";
            series46.ChartArea = "ChartArea1";
            series46.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series46.Legend = "Legend1";
            series46.Name = "X Hall";
            series47.ChartArea = "ChartArea1";
            series47.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series47.Legend = "Legend1";
            series47.Name = "15minCircle";
            series48.ChartArea = "ChartArea1";
            series48.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series48.Legend = "Legend1";
            series48.Name = "20minCircle";
            series49.ChartArea = "ChartArea1";
            series49.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series49.Legend = "Legend1";
            series49.Name = "Series7";
            series50.ChartArea = "ChartArea1";
            series50.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series50.Legend = "Legend1";
            series50.Name = "Cross FWD";
            series51.ChartArea = "ChartArea1";
            series51.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series51.Legend = "Legend1";
            series51.Name = "Cross BWD";
            this.ThetaChart2.Series.Add(series43);
            this.ThetaChart2.Series.Add(series44);
            this.ThetaChart2.Series.Add(series45);
            this.ThetaChart2.Series.Add(series46);
            this.ThetaChart2.Series.Add(series47);
            this.ThetaChart2.Series.Add(series48);
            this.ThetaChart2.Series.Add(series49);
            this.ThetaChart2.Series.Add(series50);
            this.ThetaChart2.Series.Add(series51);
            this.ThetaChart2.Size = new System.Drawing.Size(470, 257);
            this.ThetaChart2.TabIndex = 3;
            this.ThetaChart2.Text = "chart1";
            title7.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title7.BackColor = System.Drawing.Color.Transparent;
            title7.DockedToChartArea = "ChartArea1";
            title7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title7.Name = "Title1";
            title7.Position.Auto = false;
            title7.Text = "Thete Plot";
            title7.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.ThetaChart2.Titles.Add(title7);
            this.ThetaChart2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ThetaChart2_MouseDoubleClick);
            // 
            // RotationChart2
            // 
            this.RotationChart2.AllowDrop = true;
            this.RotationChart2.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.RotationChart2.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.RotationChart2.BorderlineColor = System.Drawing.Color.Black;
            chartArea8.Name = "ChartArea1";
            this.RotationChart2.ChartAreas.Add(chartArea8);
            legend8.Alignment = System.Drawing.StringAlignment.Far;
            legend8.BackColor = System.Drawing.Color.Transparent;
            legend8.BorderColor = System.Drawing.Color.Transparent;
            legend8.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend8.IsTextAutoFit = false;
            legend8.Name = "Legend1";
            legend8.Position.Auto = false;
            legend8.Position.Height = 20F;
            legend8.Position.Width = 40F;
            this.RotationChart2.Legends.Add(legend8);
            this.RotationChart2.Location = new System.Drawing.Point(955, 351);
            this.RotationChart2.Name = "RotationChart2";
            series52.ChartArea = "ChartArea1";
            series52.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series52.Legend = "Legend1";
            series52.Name = "MainStroke";
            series53.ChartArea = "ChartArea1";
            series53.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series53.Legend = "Legend1";
            series53.Name = "SubStroke";
            series54.ChartArea = "ChartArea1";
            series54.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series54.Legend = "Legend1";
            series54.Name = "BackStroke";
            series55.ChartArea = "ChartArea1";
            series55.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series55.Legend = "Legend1";
            series55.Name = "MainHall";
            series56.ChartArea = "ChartArea1";
            series56.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series56.Legend = "Legend1";
            series56.Name = "SubHall";
            series57.ChartArea = "ChartArea1";
            series57.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series57.Legend = "Legend1";
            series57.Name = "Series6";
            series58.ChartArea = "ChartArea1";
            series58.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series58.Legend = "Legend1";
            series58.Name = "Series7";
            series59.ChartArea = "ChartArea1";
            series59.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series59.Legend = "Legend1";
            series59.Name = "Series8";
            this.RotationChart2.Series.Add(series52);
            this.RotationChart2.Series.Add(series53);
            this.RotationChart2.Series.Add(series54);
            this.RotationChart2.Series.Add(series55);
            this.RotationChart2.Series.Add(series56);
            this.RotationChart2.Series.Add(series57);
            this.RotationChart2.Series.Add(series58);
            this.RotationChart2.Series.Add(series59);
            this.RotationChart2.Size = new System.Drawing.Size(470, 257);
            this.RotationChart2.TabIndex = 2;
            this.RotationChart2.Text = "chart1";
            title8.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title8.BackColor = System.Drawing.Color.Transparent;
            title8.DockedToChartArea = "ChartArea1";
            title8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title8.Name = "Title1";
            title8.Position.Auto = false;
            title8.Text = "Rotation";
            title8.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.RotationChart2.Titles.Add(title8);
            this.RotationChart2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.RotationChart2_MouseDoubleClick);
            // 
            // TrajectoryChart2
            // 
            this.TrajectoryChart2.AllowDrop = true;
            this.TrajectoryChart2.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.TrajectoryChart2.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.TrajectoryChart2.BorderlineColor = System.Drawing.Color.Black;
            chartArea9.Name = "ChartArea1";
            this.TrajectoryChart2.ChartAreas.Add(chartArea9);
            legend9.Alignment = System.Drawing.StringAlignment.Far;
            legend9.BackColor = System.Drawing.Color.Transparent;
            legend9.BorderColor = System.Drawing.Color.Transparent;
            legend9.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend9.IsTextAutoFit = false;
            legend9.Name = "Legend1";
            legend9.Position.Auto = false;
            legend9.Position.Height = 20F;
            legend9.Position.Width = 40F;
            this.TrajectoryChart2.Legends.Add(legend9);
            this.TrajectoryChart2.Location = new System.Drawing.Point(1431, 351);
            this.TrajectoryChart2.Name = "TrajectoryChart2";
            series60.ChartArea = "ChartArea1";
            series60.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series60.Legend = "Legend1";
            series60.Name = "X Amplitude";
            series61.ChartArea = "ChartArea1";
            series61.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series61.Legend = "Legend1";
            series61.Name = "X Phase";
            series62.ChartArea = "ChartArea1";
            series62.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series62.Legend = "Legend1";
            series62.Name = "Series3";
            series63.ChartArea = "ChartArea1";
            series63.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series63.Legend = "Legend1";
            series63.Name = "Series4";
            this.TrajectoryChart2.Series.Add(series60);
            this.TrajectoryChart2.Series.Add(series61);
            this.TrajectoryChart2.Series.Add(series62);
            this.TrajectoryChart2.Series.Add(series63);
            this.TrajectoryChart2.Size = new System.Drawing.Size(470, 257);
            this.TrajectoryChart2.TabIndex = 3;
            this.TrajectoryChart2.Text = "chart1";
            title9.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title9.BackColor = System.Drawing.Color.Transparent;
            title9.DockedToChartArea = "ChartArea1";
            title9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title9.Name = "Title1";
            title9.Position.Auto = false;
            title9.Text = "Trajectory";
            title9.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.TrajectoryChart2.Titles.Add(title9);
            this.TrajectoryChart2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrajectoryChart2_MouseDoubleClick);
            // 
            // lblCH2
            // 
            this.lblCH2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCH2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCH2.Location = new System.Drawing.Point(952, 52);
            this.lblCH2.Name = "lblCH2";
            this.lblCH2.Size = new System.Drawing.Size(473, 30);
            this.lblCH2.TabIndex = 221;
            this.lblCH2.Text = "CH2";
            this.lblCH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoView1
            // 
            this.InfoView1.BackColor = System.Drawing.Color.Black;
            this.InfoView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold);
            this.InfoView1.ForeColor = System.Drawing.Color.Transparent;
            this.InfoView1.Location = new System.Drawing.Point(1210, 230);
            this.InfoView1.Name = "InfoView1";
            this.InfoView1.Size = new System.Drawing.Size(475, 200);
            this.InfoView1.TabIndex = 1;
            this.InfoView1.Text = "Start Test";
            this.InfoView1.UseVisualStyleBackColor = false;
            this.InfoView1.Click += new System.EventHandler(this.InfoView_Click);
            // 
            // InfoView0
            // 
            this.InfoView0.BackColor = System.Drawing.Color.Black;
            this.InfoView0.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold);
            this.InfoView0.ForeColor = System.Drawing.Color.Transparent;
            this.InfoView0.Location = new System.Drawing.Point(257, 230);
            this.InfoView0.Name = "InfoView0";
            this.InfoView0.Size = new System.Drawing.Size(475, 200);
            this.InfoView0.TabIndex = 0;
            this.InfoView0.Text = "Start Test";
            this.InfoView0.UseVisualStyleBackColor = false;
            this.InfoView0.Click += new System.EventHandler(this.InfoView_Click);
            // 
            // SafeSensor
            // 
            this.SafeSensor.AutoSize = true;
            this.SafeSensor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SafeSensor.Location = new System.Drawing.Point(12, 21);
            this.SafeSensor.Name = "SafeSensor";
            this.SafeSensor.Size = new System.Drawing.Size(85, 16);
            this.SafeSensor.TabIndex = 225;
            this.SafeSensor.Text = "Safe Sensor";
            // 
            // CoverLoadUnload
            // 
            this.CoverLoadUnload.AutoSize = true;
            this.CoverLoadUnload.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CoverLoadUnload.Location = new System.Drawing.Point(109, 21);
            this.CoverLoadUnload.Name = "CoverLoadUnload";
            this.CoverLoadUnload.Size = new System.Drawing.Size(45, 16);
            this.CoverLoadUnload.TabIndex = 226;
            this.CoverLoadUnload.Text = "Cover";
            // 
            // SocketLoadUnload
            // 
            this.SocketLoadUnload.AutoSize = true;
            this.SocketLoadUnload.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SocketLoadUnload.Location = new System.Drawing.Point(164, 21);
            this.SocketLoadUnload.Name = "SocketLoadUnload";
            this.SocketLoadUnload.Size = new System.Drawing.Size(51, 16);
            this.SocketLoadUnload.TabIndex = 227;
            this.SocketLoadUnload.Text = "Socket";
            // 
            // State
            // 
            this.State.Controls.Add(this.SocketLoadUnload);
            this.State.Controls.Add(this.SafeSensor);
            this.State.Controls.Add(this.CoverLoadUnload);
            this.State.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.State.Location = new System.Drawing.Point(4, 1);
            this.State.Name = "State";
            this.State.Size = new System.Drawing.Size(462, 46);
            this.State.TabIndex = 228;
            this.State.TabStop = false;
            this.State.Text = "State";
            // 
            // StepChart1
            // 
            this.StepChart1.AllowDrop = true;
            this.StepChart1.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.StepChart1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.StepChart1.BorderlineColor = System.Drawing.Color.Black;
            chartArea10.Name = "ChartArea1";
            this.StepChart1.ChartAreas.Add(chartArea10);
            legend10.Alignment = System.Drawing.StringAlignment.Far;
            legend10.BackColor = System.Drawing.Color.Transparent;
            legend10.BorderColor = System.Drawing.Color.Transparent;
            legend10.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend10.IsTextAutoFit = false;
            legend10.Name = "Legend1";
            legend10.Position.Auto = false;
            legend10.Position.Height = 20F;
            legend10.Position.Width = 40F;
            this.StepChart1.Legends.Add(legend10);
            this.StepChart1.Location = new System.Drawing.Point(480, 351);
            this.StepChart1.Name = "StepChart1";
            series64.ChartArea = "ChartArea1";
            series64.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series64.Legend = "Legend1";
            series64.Name = "X Amplitude";
            series65.ChartArea = "ChartArea1";
            series65.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series65.Legend = "Legend1";
            series65.Name = "X Phase";
            series66.ChartArea = "ChartArea1";
            series66.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series66.Legend = "Legend1";
            series66.Name = "Series3";
            series67.ChartArea = "ChartArea1";
            series67.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series67.Legend = "Legend1";
            series67.Name = "Series4";
            this.StepChart1.Series.Add(series64);
            this.StepChart1.Series.Add(series65);
            this.StepChart1.Series.Add(series66);
            this.StepChart1.Series.Add(series67);
            this.StepChart1.Size = new System.Drawing.Size(470, 257);
            this.StepChart1.TabIndex = 229;
            this.StepChart1.Text = "chart1";
            title10.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title10.BackColor = System.Drawing.Color.Transparent;
            title10.DockedToChartArea = "ChartArea1";
            title10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title10.Name = "Title1";
            title10.Position.Auto = false;
            title10.Text = "Step";
            title10.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.StepChart1.Titles.Add(title10);
            this.StepChart1.Visible = false;
            this.StepChart1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.StepChart1_MouseDoubleClick);
            // 
            // StepChart2
            // 
            this.StepChart2.AllowDrop = true;
            this.StepChart2.AntiAliasing = System.Windows.Forms.DataVisualization.Charting.AntiAliasingStyles.Text;
            this.StepChart2.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.StepChart2.BorderlineColor = System.Drawing.Color.Black;
            chartArea11.Name = "ChartArea1";
            this.StepChart2.ChartAreas.Add(chartArea11);
            legend11.Alignment = System.Drawing.StringAlignment.Far;
            legend11.BackColor = System.Drawing.Color.Transparent;
            legend11.BorderColor = System.Drawing.Color.Transparent;
            legend11.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend11.IsTextAutoFit = false;
            legend11.Name = "Legend1";
            legend11.Position.Auto = false;
            legend11.Position.Height = 20F;
            legend11.Position.Width = 40F;
            this.StepChart2.Legends.Add(legend11);
            this.StepChart2.Location = new System.Drawing.Point(1431, 351);
            this.StepChart2.Name = "StepChart2";
            series68.ChartArea = "ChartArea1";
            series68.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series68.Legend = "Legend1";
            series68.Name = "X Amplitude";
            series69.ChartArea = "ChartArea1";
            series69.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series69.Legend = "Legend1";
            series69.Name = "X Phase";
            series70.ChartArea = "ChartArea1";
            series70.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series70.Legend = "Legend1";
            series70.Name = "Series3";
            series71.ChartArea = "ChartArea1";
            series71.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series71.Legend = "Legend1";
            series71.Name = "Series4";
            this.StepChart2.Series.Add(series68);
            this.StepChart2.Series.Add(series69);
            this.StepChart2.Series.Add(series70);
            this.StepChart2.Series.Add(series71);
            this.StepChart2.Size = new System.Drawing.Size(470, 257);
            this.StepChart2.TabIndex = 230;
            this.StepChart2.Text = "chart1";
            title11.Alignment = System.Drawing.ContentAlignment.TopLeft;
            title11.BackColor = System.Drawing.Color.Transparent;
            title11.DockedToChartArea = "ChartArea1";
            title11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title11.Name = "Title1";
            title11.Position.Auto = false;
            title11.Text = "Step";
            title11.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Shadow;
            this.StepChart2.Titles.Add(title11);
            this.StepChart2.Visible = false;
            this.StepChart2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.StepChart2_MouseDoubleClick);
            // 
            // F_Oprator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 978);
            this.Controls.Add(this.YieldChart);
            this.Controls.Add(this.ResultDataGrid);
            this.Controls.Add(this.LotNameLbl);
            this.Controls.Add(this.State);
            this.Controls.Add(this.InfoView1);
            this.Controls.Add(this.InfoView0);
            this.Controls.Add(this.lblCH2);
            this.Controls.Add(this.LogCH2);
            this.Controls.Add(this.LogCH1);
            this.Controls.Add(this.RotationChart2);
            this.Controls.Add(this.ThetaChart2);
            this.Controls.Add(this.CheckPinContactBtn);
            this.Controls.Add(this.LotID);
            this.Controls.Add(this.StrokeChart2);
            this.Controls.Add(this.StrokeChart1);
            this.Controls.Add(this.ThetaChart1);
            this.Controls.Add(this.RotationChart1);
            this.Controls.Add(this.lblCH1);
            this.Controls.Add(this.PcontrolBtn);
            this.Controls.Add(this.TPblank);
            this.Controls.Add(this.StepChart2);
            this.Controls.Add(this.StepChart1);
            this.Controls.Add(this.TrajectoryChart2);
            this.Controls.Add(this.TrajectoryChart1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "F_Oprator";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.F_Oprator_Load);
            this.VisibleChanged += new System.EventHandler(this.F_Oprator_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.ResultDataGrid)).EndInit();
            this.PcontrolBtn.ResumeLayout(false);
            this.PcontrolBtn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Progressbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YieldChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrokeChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThetaChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RotationChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrajectoryChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrokeChart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThetaChart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RotationChart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrajectoryChart2)).EndInit();
            this.State.ResumeLayout(false);
            this.State.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StepChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StepChart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox LogCH2;
        private System.Windows.Forms.TextBox LogCH1;
        private System.Windows.Forms.TableLayoutPanel TPblank;
        private System.Windows.Forms.Panel PcontrolBtn;
        private System.Windows.Forms.Label RepeatLoadingUnloadingLbl;
        public System.Windows.Forms.TextBox RepeatRunCnt;
        private System.Windows.Forms.TextBox CurrentRunCnt;
        private System.Windows.Forms.Label LastTestedSPLNoLbl;
        private System.Windows.Forms.TextBox NewSampleNumber;
        private System.Windows.Forms.TextBox LastSampleNum;
        private System.Windows.Forms.DataVisualization.Charting.Chart YieldChart;
        private System.Windows.Forms.Label lblCH1;
        private System.Windows.Forms.DataVisualization.Charting.Chart StrokeChart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart ThetaChart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart RotationChart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart TrajectoryChart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart StrokeChart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart ThetaChart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart RotationChart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart TrajectoryChart2;
        private System.Windows.Forms.Label lblCH2;
        private System.Windows.Forms.Button ClearLogsBtn;
        private System.Windows.Forms.Button CheckPinContactBtn;
        private System.Windows.Forms.Button SetSampleNumberBtn;
        private System.Windows.Forms.Button SuddenStop;
        private System.Windows.Forms.Button RepeatStartTest;
        private System.Windows.Forms.Button InfoView1;
        private System.Windows.Forms.Button InfoView0;
        private System.Windows.Forms.Button SaveLogsBtn;
        private System.Windows.Forms.TextBox LotID;
        private System.Windows.Forms.Label LotNameLbl;
        private System.Windows.Forms.PictureBox Progressbar;
        private System.Windows.Forms.Label SafeSensor;
        private System.Windows.Forms.Label CoverLoadUnload;
        private System.Windows.Forms.Label SocketLoadUnload;
        private System.Windows.Forms.GroupBox State;
        private System.Windows.Forms.DataVisualization.Charting.Chart StepChart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart StepChart2;
        public System.Windows.Forms.DataGridView ResultDataGrid;
    }
}

