﻿using ActroLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace SensorShift
{
    public partial class F_Oprator : Form
    {
        //Class Reference ====================================================================
        public Process Process { get { return PROCESS.Process; } }
        public Spec Spec { get { return PROCESS.Spec; } }
        public Option Option { get { return PROCESS.Option; } }
        public ChartList Chart { get { return PROCESS.Chart; } }
        public Model Model { get { return PROCESS.Model; } }
        public SEM1217S DrvIC
        {
            get { return PROCESS.DrvIC; }
        }
        //====================================================================================
        private TextBox[] mViewLog = null;
        private Button[] mInfoBtn = null;
        public F_Oprator()
        {
            InitializeComponent();
            InitializeChart();
        }
        private void F_Oprator_Load(object sender, EventArgs e)
        {
            try     
            {
                PROCESS.LogEvent.Evented += LogEvent_Evented;
                PROCESS.ShowEvent.Evented += ShowEvent_Evented;

                if (mInfoBtn == null)
                {
                    mInfoBtn = new Button[2];
                    mInfoBtn[0] = InfoView0;
                    mInfoBtn[1] = InfoView1;
                }

                if (mViewLog == null)
                {
                    mViewLog = new TextBox[2];
                    mViewLog[0] = LogCH1;
                    mViewLog[1] = LogCH2;
                }

                InitializeDataGridView();

                for (int i = 0; i < 2; i++)
                    mInfoBtn[i].Hide();

                Progressbar.SizeMode = PictureBoxSizeMode.StretchImage;
                SetStyle(ControlStyles.SupportsTransparentBackColor, true);
                Progressbar.BackColor = Color.Transparent;
                Progressbar.Hide();

                InitYield();

                if (Process != null)
                {
                    Process.RunStart += Process_RunStart;
                    Process.RunEnd += Process_RunEnd;
                    Process.RepeatEnd += Process_RepeatEnd;
                }

                DrvIC.SafetyOn += DriverIC_SafetyOn;
                //Model.Changed += Model_Changed;
                //UpdateLotInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        //Event ==============================================================================
        private void DriverIC_SafetyOn(object sender, EventArgs e)
        {
            if (Option.m_bSafeSensor)
            {
                if (DrvIC.IsSafeOn)
                {
                    DrvIC.IsSafeCheck = false;
                    SafeSensor.BackColor = Color.Red;
                    MessageBox.Show("Safe Sensor detected\r\nBe careful with socket movement!!", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    DrvIC.IsSafeCheck = true;
                                
                }
                else SafeSensor.BackColor = Color.Transparent;     
            }
        }
        private void LogEvent_Evented(object sender, LogEvent.Params e)
        {
            try
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        mViewLog[e.Ch].AppendText(e.Msg + "\r\n");
                    });
                }
                else
                {
                    mViewLog[e.Ch].AppendText(e.Msg + "\r\n");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void ShowEvent_Evented(object sender, ShowEvent.Params e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    ShowDataResults(e.Ch, e.Key);
                });
            }
            else
            {
                ShowDataResults(e.Ch, e.Key);
            }
        }
        private void Process_RunEnd(object sender, int port)
        {
            SafeControlView(Progressbar, false);
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    SetInforView();
                });
            }
            else
            {
                SetInforView();
            }
            SafeControlView(InfoView0, true);
            SafeControlView(InfoView1, true);

            if (Option.m_bScreenCapture)
            {
                DateTime dtNow = DateTime.Now;   // 현재 날짜, 시간 얻기
                string pngname = "Screen" + "_" + dtNow.ToString("dd_hh_mm_ss") + ".png";
                string sScreenCapturePath = FIO.BaseDir + "\\ScreenCapture\\" + pngname;
                string sDir = FIO.BaseDir + "\\ScreenCapture\\";
                Bitmap memoryImage;
                memoryImage = new Bitmap(1906, 1080);
                Size s = new Size(memoryImage.Width, memoryImage.Height);
                Graphics memoryGraphics = Graphics.FromImage(memoryImage);


                if (!Directory.Exists(sDir))
                    Directory.CreateDirectory(sDir);

                Thread.Sleep(300);
                memoryGraphics.CopyFromScreen(7, 31, 0, 0, s);
                memoryImage.Save(sScreenCapturePath);
            }
        }
        private void Process_RunStart(object sender, int port)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    if (RepeatRunCnt.Text == "") RepeatRunCnt.Text = "1";
                    CurrentRunCnt.Text = Process.CurrentRun.ToString();
                    RepeatRunCnt.Text = Process.RepeatRun.ToString();

                    ClearChart();
                    ClearLog();
                });
            }
            else
            {
                if (RepeatRunCnt.Text == "") RepeatRunCnt.Text = "1";
                CurrentRunCnt.Text = Process.CurrentRun.ToString();
                RepeatRunCnt.Text = Process.RepeatRun.ToString();

                ClearChart();
                ClearLog();
            }

            SafeControlView(InfoView0, false);
            SafeControlView(InfoView1, false);
            ShowDataResultsInit(0);
            ShowDataResultsInit(1);
            SafeControlView(Progressbar, true);
        }
        //====================================================================================
        public void ShowDataResultsInit(int ch)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    Spec.InitResult(ch);
                    for (int i = 0; i < Spec.Param.Count; i++)
                    {
                        ResultDataGrid[ch + 4, i].Value = Spec.PassFails[ch].Results[i].Val.ToString("F0");
                        ResultDataGrid[ch + 4, i].Style.BackColor = Color.White;
                    }
                });
            }
            else
            {
                Spec.InitResult(ch);
                for (int i = 0; i < Spec.Param.Count; i++)
                {
                    ResultDataGrid[ch + 4, i].Value = Spec.PassFails[ch].Results[i].Val.ToString("F0");
                    ResultDataGrid[ch + 4, i].Style.BackColor = Color.White;
                }
            }
        }
        public void ShowDataResults(int ch, string key)
        {
            Spec.SetResult(ch, key);
            for (int i = 0; i < Spec.Param.Count; i++)
            {
                if (Spec.Param[i][0].ToString() != key) continue;

                if (Spec.PassFails[ch].Results[i].Val != 0)
                {
                    //if (Spec.Param[i][1].ToString().Contains("Sensitivity") || Spec.Param[i][1].ToString().Contains("Linearity")
                    //|| Spec.Param[i][1].ToString().Contains("Hysteresis"))
                    //    ResultDataGrid[ch + 4, i].Value = Spec.PassFails[ch].Results[i].Val.ToString("F3");
                    //else ResultDataGrid[ch + 4, i].Value = Spec.PassFails[ch].Results[i].Val.ToString("F2");
                    if (key.Contains("FRA") || key.Contains("Gyro"))
                    {
                        ResultDataGrid[ch + 4, i].Value = Spec.PassFails[ch].Results[i].Val.ToString("F1");
                    }
                    else
                    {
                        ResultDataGrid[ch + 4, i].Value = Spec.PassFails[ch].Results[i].Val.ToString("F3");
                    }
                }
                if (Spec.PassFails[ch].Results[i].bPass) ResultDataGrid[ch + 4, i].Style.BackColor = Color.White;
                else ResultDataGrid[ch + 4, i].Style.BackColor = Color.Orange;
            }
        }
        private void SafeControlView(Control con, bool bShow)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    if (bShow) con.Show(); else con.Hide();
                });
            }
            else
            {
                if (bShow) con.Show(); else con.Hide();
            }
        }
        private void SetInforView()
        {
            for (int i = 0; i < 2; i++)
            {
                if (Process.ErrMsg[i] == "")
                {
                    mInfoBtn[i].Text = "PASS";
                    mInfoBtn[i].Font = new Font("Malgun Gothic", 60, FontStyle.Bold);
                    mInfoBtn[i].ForeColor = Color.Cyan;
                }
                else
                {
                    mInfoBtn[i].Text = Process.ErrMsg[i];
                    mInfoBtn[i].Font = new Font("Malgun Gothic", 24, FontStyle.Bold);
                    mInfoBtn[i].ForeColor = Color.OrangeRed;
                }
            }

        }
        private void Process_RepeatEnd(object sender, int port)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    CurrentRunCnt.Text = Process.CurrentRun.ToString();
                    LastSampleNum.Text = Spec.LastSampleNum.ToString();
                    NewSampleNumber.Text = (Spec.LastSampleNum + 1).ToString();
                    SafeInitYield();
                });
            }
            else
            {

                CurrentRunCnt.Text = Process.CurrentRun.ToString();
                LastSampleNum.Text = Spec.LastSampleNum.ToString();
                NewSampleNumber.Text = (Spec.LastSampleNum + 1).ToString();
                SafeInitYield();
            }
        }
        public void SafeInitYield()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    InitYield();
                });
            }
            else
                InitYield();
        }
        private void InitYield()
        {
            LastSampleNum.Text = Spec.LastSampleNum.ToString();
            NewSampleNumber.Text = (Spec.LastSampleNum + 1).ToString();
            List<string> litem = new List<string>();
            List<double> lratio = new List<double>();

            if (Spec.TotlaTested == 0)
            {
                for (int i = 0; i < Spec.Param.Count; i++)
                {
                    Spec.Param[i][8] = 0;
                }
            }

            for (int i = 0; i < Spec.Param.Count; i++)
            {
                int Failed = Convert.ToInt32(Spec.Param[i][8]);
                if (Failed > 0)
                {
                    litem.Add(string.Format("{0} {1}", Spec.Param[i][0], Spec.Param[i][1]));
                    lratio.Add(Failed / (double)Spec.TotlaTested);
                }
            }
            double lyield = 100;
            if (Spec.TotlaTested > 0)
                lyield = (1 - Spec.TotlaFailed / (double)Spec.TotlaTested) * 100;

            YieldChart.Titles[0].Text = "Yield " + lyield.ToString("F2") + "% \t" + (Spec.TotlaTested - Spec.TotlaFailed).ToString() + " / " + Spec.TotlaTested.ToString();
            if (litem.Count > 0) YieldChart.Series[0].Points.DataBindXY(litem, lratio);
            YieldChart.DataManipulator.Sort(PointSortOrder.Descending, YieldChart.Series[0]);
        }
        //public void UpdateLotInfo()
        //{
        //    OperatorName.Text = Rcp.Model.OperatorName;
        //    LotID.Text = Rcp.Model.LotID;
        //}
        public void InitializeChart()
        {
            //Top
            Chart.Stroke[0].Init(StrokeChart1);
            Chart.Theta[0].Init(ThetaChart1);
            Chart.Stroke[1].Init(StrokeChart2);
            Chart.Theta[1].Init(ThetaChart2);
            //Bottom
            Chart.Rotation[0].Init(RotationChart1);
            Chart.Trajectory[0].Init(TrajectoryChart1);
            Chart.Rotation[1].Init(RotationChart2);
            Chart.Trajectory[1].Init(TrajectoryChart2);
            Chart.Step[0].Init(StepChart1);
            Chart.Step[1].Init(StepChart2);

            YieldChart.ChartAreas[0].Position.X = 0;
            YieldChart.ChartAreas[0].Position.Y = 8;
            YieldChart.ChartAreas[0].Position.Height = 67;
            YieldChart.ChartAreas[0].Position.Width = 100;
            YieldChart.Series[0].LegendText = "#VALX (#PERCENT)";
            YieldChart.Series[0]["PieLabelStyle"] = "Outside";
            YieldChart.Series[0].BorderWidth = 1;
            YieldChart.Series[0].BorderColor = Color.FromArgb(64, 64, 64);
            YieldChart.Legends[0].Enabled = true;
            YieldChart.Legends[0].Docking = Docking.Bottom;
            YieldChart.Legends[0].Alignment = StringAlignment.Center;
        }
        public void ClearChart()
        {
            //Top
            Chart.Stroke[0].Clear();
            Chart.Theta[0].Clear();
            Chart.Stroke[1].Clear();
            Chart.Theta[1].Clear();
            //Bottom
            Chart.Rotation[0].Clear();
            Chart.Trajectory[0].Clear();
            Chart.Rotation[1].Clear();
            Chart.Trajectory[1].Clear();
            Chart.Step[0].Clear();
            Chart.Step[1].Clear();
        }
        public void InitializeDataGridView()
        {
            ResultDataGrid.ColumnCount = 9; //  Group, Item, min, max, r0, r1, r2, r3, unit, Fratio
            ResultDataGrid.Font = new Font("Calibri", 14, FontStyle.Bold);
            for (int i = 0; i < ResultDataGrid.ColumnCount; i++)
            {
                ResultDataGrid.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            ResultDataGrid.RowHeadersVisible = false;
            ResultDataGrid.BackgroundColor = Color.LightGray;

            //// Column
            ResultDataGrid.Columns[0].Name = "Axis";
            ResultDataGrid.Columns[1].Name = "Items";
            ResultDataGrid.Columns[2].Name = "Min";
            ResultDataGrid.Columns[3].Name = "Max";
            ResultDataGrid.Columns[4].Name = "#1 Result";
            ResultDataGrid.Columns[5].Name = "#2 Result";
            ResultDataGrid.Columns[6].Name = "#3 Result";
            ResultDataGrid.Columns[7].Name = "#4 Result";
            ResultDataGrid.Columns[8].Name = "unit";

            ResultDataGrid.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter;
            ResultDataGrid.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;
            ResultDataGrid.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            ResultDataGrid.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            ResultDataGrid.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            ResultDataGrid.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            ResultDataGrid.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            ResultDataGrid.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            ResultDataGrid.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;

            ResultDataGrid.Columns[0].Width = 100;
            ResultDataGrid.Columns[1].Width = 215;
            ResultDataGrid.Columns[2].Width = 80;
            ResultDataGrid.Columns[3].Width = 80;
            ResultDataGrid.Columns[4].Width = 90;
            ResultDataGrid.Columns[5].Width = 90;
            ResultDataGrid.Columns[6].Width = 90;
            ResultDataGrid.Columns[7].Width = 90;
            ResultDataGrid.Columns[8].Width = 90;

            ResultDataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            ResultDataGrid.ColumnHeadersHeight = 28;

            if (Process.ChannelCount == 2)
            {
                ResultDataGrid.Columns[6].Visible = false;
                ResultDataGrid.Columns[7].Visible = false;

                ResultDataGrid.Columns[0].Width = 130;
                ResultDataGrid.Columns[1].Width = 250;
                ResultDataGrid.Columns[2].Width = 100;
                ResultDataGrid.Columns[3].Width = 100;
                ResultDataGrid.Columns[4].Width = 120;
                ResultDataGrid.Columns[5].Width = 120;
                ResultDataGrid.Columns[8].Width = 100;
            }

            bool bColorChange = true;
            ResultDataGrid.Rows.Clear();
            for (int i = 0; i < Spec.Param.Count; i++)
            {
                if (i > 0)
                    if (Spec.Param[i - 1][0].ToString() != Spec.Param[i][0].ToString())
                        bColorChange = !bColorChange;

                ResultDataGrid.Rows.Add(Spec.Param[i][0], Spec.Param[i][1], Spec.Param[i][2], Spec.Param[i][3], Spec.Param[i][4], Spec.Param[i][5], Spec.Param[i][6], Spec.Param[i][7], Spec.Param[i][9]);

                if (bColorChange) for (int k = 0; k < ResultDataGrid.ColumnCount; k++) ResultDataGrid[k, i].Style.BackColor = Color.Lavender;
                else for (int k = 0; k < ResultDataGrid.ColumnCount; k++) ResultDataGrid[k, i].Style.BackColor = Color.White;

                ResultDataGrid.Rows[i].Visible = Convert.ToBoolean(Spec.Param[i][10]);

                ResultDataGrid.Rows[i].Height = 22;
                ResultDataGrid.Rows[i].Resizable = DataGridViewTriState.False;
                ResultDataGrid.Rows[i].DefaultCellStyle.Font = new Font("Calibri", 12, FontStyle.Bold);
                ResultDataGrid[1, i].Style.Font = new Font("Calibri", 12, FontStyle.Bold);
                ResultDataGrid[2, i].Style.Font = new Font("Calibri", 12, FontStyle.Bold);
                ResultDataGrid[8, i].Style.Font = new Font("Calibri", 12, FontStyle.Italic);

                ResultDataGrid.ReadOnly = true;
            }

            string oldkey = "";
            for (int i = 0; i < Spec.Param.Count - 1; i++)
            {
                if (ResultDataGrid.Rows[i].Visible)
                {
                    string newKey = ResultDataGrid.Rows[i].Cells[0].Value.ToString();
                    if (oldkey == newKey) ResultDataGrid.Rows[i].Cells[0].Value = "";
                    oldkey = newKey;
                }
            }
        }
        private void Log_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Control c = (Control)sender;
            int id = c.TabIndex;
            if (mViewLog[id].Tag.ToString() == "S")
            {
                mViewLog[id].Size = new Size(476, 80 + 526);
                mViewLog[id].Tag = "L";
            }
            else
            {
                mViewLog[id].Size = new Size(476, 80);
                mViewLog[id].Tag = "S";
            }
        }
        private void Result_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (ResultDataGrid.Tag.ToString() == "S")
            {
                ResultDataGrid.Location = new Point(477, 2);
                ResultDataGrid.Size = new Size(948, 914);
                ResultDataGrid.BringToFront();
                ResultDataGrid.Tag = "L";
            }
            else
            {

                ResultDataGrid.Location = new Point(477, 612);
                ResultDataGrid.Size = new Size(948, 304);
                ResultDataGrid.Tag = "S";
            }
        }
        private void YieldChart_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (YieldChart.Tag == null) YieldChart.Tag = "S";
            if (YieldChart.Tag.ToString() == "S")
            {
                YieldChart.Location = new Point(1166, 344);
                YieldChart.Size = new Size(738, 572);
                YieldChart.BringToFront();
                YieldChart.Tag = "L";
            }
            else
            {
                YieldChart.Location = new Point(1431, 612);
                YieldChart.Size = new Size(470, 304);
                YieldChart.Tag = "S";
            }
        }
        private async void StartTestBtn_Click(object sender, EventArgs e)
        {
            Process.RepeatRun = int.Parse(RepeatRunCnt.Text);
            if (Process.RepeatRun == 1)
            {
                SuddenStop.Enabled = true;
                Process.CurrentRun = 1;
                EnableUi(true);
                await Task.Factory.StartNew(() => Process.RunTest());
                EnableUi(false);
            }
            else
            {
                EnableUi(true);
                await Task.Factory.StartNew(() => Process.RunTest());
                EnableUi(false);
            }
        }
        public void ClearLog()
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    mViewLog[0].Text = "";
                    mViewLog[1].Text = "";
                });
            }
            else
            {
                mViewLog[0].Text = "";
                mViewLog[1].Text = "";
            }
        }
        private void StopTestBtn_Click(object sender, EventArgs e)
        {
            Process.SuddenStop = true;

            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    RepeatStartTest.Enabled = false;
                    SuddenStop.Enabled = false;
                });
            }
            else
            {
                RepeatStartTest.Enabled = false;
                SuddenStop.Enabled = false;
            }
        }
        public void EnableUi(bool bStart)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    RepeatStartTest.Enabled = !bStart;
                    SuddenStop.Enabled = bStart;
                });
            }
            else
            {
                RepeatStartTest.Enabled = !bStart;
                SuddenStop.Enabled = bStart;
            }
        }
        private void InfoView_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 2; i++)
                mInfoBtn[i].Hide();
        }
        private void StrokeChart1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Chart.Stroke[0].ToggleZoom(0);
        }
        private void ThetaChart1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Chart.Theta[0].ToggleZoom(1);
        }
        private void StrokeChart2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Chart.Stroke[1].ToggleZoom(2);
        }
        private void ThetaChart2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Chart.Theta[1].ToggleZoom(3);
        }
        private void RotationChart1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Chart.Rotation[0].ToggleZoom(0);
        }
        private void TrajectoryChart1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Location.X >= 190 && e.Location.X <= 280 && e.Location.Y >= 0 && e.Location.Y <= 30)
            {
                Chart.Trajectory[0].C.Hide();
                Chart.Step[0].C.Show();
            }
            else
            {
                Chart.Trajectory[0].ToggleZoom(1);
            }
        }
        private void StepChart1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Location.X >= 190 && e.Location.X <= 280 && e.Location.Y >= 0 && e.Location.Y <= 30)
            {
                Chart.Step[0].C.Hide();
                Chart.Trajectory[0].C.Show();
            }
            else
            {
                Chart.Step[0].ToggleZoom(1);
            }
        }
        private void RotationChart2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Chart.Rotation[1].ToggleZoom(2);
        }
        private void TrajectoryChart2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Location.X >= 190 && e.Location.X <= 280 && e.Location.Y >= 0 && e.Location.Y <= 30)
            {
                Chart.Trajectory[1].C.Hide();
                Chart.Step[1].C.Show();
            }
            else
            {
                Chart.Trajectory[1].ToggleZoom(3);
            }
        }
        private void StepChart2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Location.X >= 190 && e.Location.X <= 280 && e.Location.Y >= 0 && e.Location.Y <= 30)
            {
                Chart.Step[1].C.Hide();
                Chart.Trajectory[1].C.Show();
            }
            else
            {
                Chart.Step[1].ToggleZoom(3);
            }
        }
        private void btnCheckPinContact_Click(object sender, EventArgs e)
        {
            if (Process.IsRun[0]) return;
    
            DrvIC.LoadSocket(0, true);

            Process.ErrMsg[0] = "";
            Process.ErrMsg[1] = "";

            DrvIC.LoadCover(0, true);

            Process.CheckDriverIC(0);
            Process.CheckDriverIC(1);
            
            DrvIC.UnloadCover(0, true);

            DrvIC.UnloadSocket(0, true);

        }
        private void btnClearLogs_Click(object sender, EventArgs e)
        {
            ClearLog(0);
            //ClearLog(1);
        }
        public void ClearLog(int port)
        {

            if (port == 0)
            {
                mViewLog[0].Text = "";
                mViewLog[1].Text = "";
            }
            //else
            //{
            //    mViewLog[2].Text = "";
            //    mViewLog[3].Text = "";
            //}

        }
        private void SaveLogsBtn_Click(object sender, EventArgs e)
        {
            string strPath = FIO.BaseDir + "\\Result\\";
            for (int i = 0; i < 2; i++)
            {
                string fn = strPath + "LogLast_" + i.ToString() + ".txt";
                StreamWriter wr = new StreamWriter(fn);
                wr.Write(mViewLog[i].Text);
                wr.Close();
            }

        }
        private void LotID_TextChanged(object sender, EventArgs e)
        {
            Model.LotID = LotID.Text;
            Model.LotChanged(LotID.Text);
        }

        private void Button_MouseMove(object sender, EventArgs e)
        {
            //Button btn = (Button)sender;
            //btn.FlatStyle = FlatStyle.System;
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            //Button btn = (Button)sender;
            //btn.FlatStyle = FlatStyle.Flat;
        }

        private void SetSampleNumberBtn_Click(object sender, EventArgs e)
        {

        }

        private void F_Oprator_VisibleChanged(object sender, EventArgs e)
        {
         
        }
    }
}
