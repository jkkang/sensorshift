﻿
namespace SensorShift
{
    partial class F_Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F_Start));
            this.SysLog = new System.Windows.Forms.TextBox();
            this.LogoPB = new System.Windows.Forms.PictureBox();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.LogoP = new System.Windows.Forms.Panel();
            this.LogP = new System.Windows.Forms.Panel();
            this.BarP = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPB)).BeginInit();
            this.LogoP.SuspendLayout();
            this.LogP.SuspendLayout();
            this.BarP.SuspendLayout();
            this.SuspendLayout();
            // 
            // SysLog
            // 
            this.SysLog.BackColor = System.Drawing.Color.White;
            this.SysLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SysLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SysLog.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SysLog.ForeColor = System.Drawing.Color.Black;
            this.SysLog.Location = new System.Drawing.Point(0, 0);
            this.SysLog.Multiline = true;
            this.SysLog.Name = "SysLog";
            this.SysLog.ReadOnly = true;
            this.SysLog.Size = new System.Drawing.Size(459, 121);
            this.SysLog.TabIndex = 21;
            this.SysLog.TabStop = false;
            // 
            // LogoPB
            // 
            this.LogoPB.BackColor = System.Drawing.Color.Transparent;
            this.LogoPB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogoPB.Image = ((System.Drawing.Image)(resources.GetObject("LogoPB.Image")));
            this.LogoPB.Location = new System.Drawing.Point(0, 0);
            this.LogoPB.Name = "LogoPB";
            this.LogoPB.Size = new System.Drawing.Size(459, 203);
            this.LogoPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LogoPB.TabIndex = 20;
            this.LogoPB.TabStop = false;
            // 
            // ProgressBar
            // 
            this.ProgressBar.BackColor = System.Drawing.Color.Black;
            this.ProgressBar.ForeColor = System.Drawing.Color.Black;
            this.ProgressBar.Location = new System.Drawing.Point(1, 0);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(458, 30);
            this.ProgressBar.TabIndex = 22;
            // 
            // LogoP
            // 
            this.LogoP.Controls.Add(this.LogoPB);
            this.LogoP.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogoP.Location = new System.Drawing.Point(0, 0);
            this.LogoP.Name = "LogoP";
            this.LogoP.Size = new System.Drawing.Size(459, 203);
            this.LogoP.TabIndex = 23;
            // 
            // LogP
            // 
            this.LogP.Controls.Add(this.SysLog);
            this.LogP.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogP.Location = new System.Drawing.Point(0, 203);
            this.LogP.Name = "LogP";
            this.LogP.Size = new System.Drawing.Size(459, 121);
            this.LogP.TabIndex = 24;
            // 
            // BarP
            // 
            this.BarP.Controls.Add(this.ProgressBar);
            this.BarP.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarP.Location = new System.Drawing.Point(0, 324);
            this.BarP.Name = "BarP";
            this.BarP.Size = new System.Drawing.Size(459, 30);
            this.BarP.TabIndex = 25;
            // 
            // F_Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 356);
            this.ControlBox = false;
            this.Controls.Add(this.BarP);
            this.Controls.Add(this.LogP);
            this.Controls.Add(this.LogoP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "F_Start";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "F_Start";
            this.Shown += new System.EventHandler(this.F_Start_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.LogoPB)).EndInit();
            this.LogoP.ResumeLayout(false);
            this.LogP.ResumeLayout(false);
            this.LogP.PerformLayout();
            this.BarP.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoPB;
        private System.Windows.Forms.TextBox SysLog;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.Panel LogoP;
        private System.Windows.Forms.Panel LogP;
        private System.Windows.Forms.Panel BarP;
    }
}