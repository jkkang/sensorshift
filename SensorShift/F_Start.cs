﻿using ActroLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SensorShift
{
    public partial class F_Start : Form
    {
        public CurrentPath CurrentPath { get { return PROCESS.Current; } }
        public SEM1217S DrvIC { get { return PROCESS.DrvIC; } }
        public Recipe Rcp { get { return PROCESS.Rcp; } }
        public Spec Spec { get { return PROCESS.Spec; } }
        public Process Process { get { return PROCESS.Process; } }
        public VisionParam VParam { get { return PROCESS.VParam; } }
        public F_Start()
        {
            InitializeComponent();
            SysLog.AppendText("Loading System...\r\n");
            ProgressBar.Value = 0;
            Application.DoEvents();
        }

        private void F_Start_Shown(object sender, EventArgs e)
        {
            try
            {
                SysLog.AppendText("Loading Setting Data...\r\n");
                if (!Directory.Exists(FIO.RootDir)) Directory.CreateDirectory(FIO.BaseDir);
                if (!Directory.Exists(FIO.RootDir)) Directory.CreateDirectory(FIO.RootDir);
                if (!Directory.Exists(FIO.RowDataDir)) Directory.CreateDirectory(FIO.RowDataDir);
                if (!Directory.Exists(FIO.UserScriptDir)) Directory.CreateDirectory(FIO.UserScriptDir);

                Spec.Init(CurrentPath.SpecName, "\\Spec\\");
                Rcp.Init(CurrentPath.ConditionName, "\\Recipe\\");

                SysLog.AppendText("Loading I2C Data...\r\n");
                if (DrvIC.PortCnt == 0 && !Process.IsVirtual)
                {
                    MessageBox.Show("Reconnect the I2C cable", null, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    this.Close();
                    return;
                }

                if (DrvIC.DLNdevice[0] == null && !Process.IsVirtual)
                {
                    MessageBox.Show("Reconnect the Power cable", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }

                SysLog.AppendText("Loading Vision Data...\r\n");
                Process.Initial_Vision();
                if (!Process.IsVirtual) Process.SetRoi(VParam.Roi[0].X, VParam.Roi[0].Y);
                if (Process.CamCount == 0)
                {
                    MessageBox.Show("Reconnect the Camera cable", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }

                SysLog.AppendText("Complete!!\r\n");

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                this.DialogResult = DialogResult.Cancel;
                return;
            }
        }

        public static void Wait(uint Millisec)
        {
            double rDblStart;
            rDblStart = DateTime.Now.Ticks;
            while (((DateTime.Now.Ticks - rDblStart) / 10000000f) <= (Millisec / 1000f))
            {
                Application.DoEvents();
            }
        }

    }
}
