﻿
namespace SensorShift
{
    partial class F_Vision
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.NthMeasureBtn = new System.Windows.Forms.Button();
            this.NthMeasureValue = new System.Windows.Forms.TextBox();
            this.VsnLog = new System.Windows.Forms.TextBox();
            this.TPblank = new System.Windows.Forms.TableLayoutPanel();
            this.panelCam0 = new System.Windows.Forms.Panel();
            this.LiveState = new System.Windows.Forms.Label();
            this.ReadDriverICBtn = new System.Windows.Forms.Button();
            this.WriteDriverICBtn = new System.Windows.Forms.Button();
            this.AddrLB = new System.Windows.Forms.Label();
            this.WriteDataValue = new System.Windows.Forms.TextBox();
            this.DataLB = new System.Windows.Forms.Label();
            this.WriteAddrValue = new System.Windows.Forms.TextBox();
            this.ReadDataValue = new System.Windows.Forms.TextBox();
            this.ReadAddrValue = new System.Windows.Forms.TextBox();
            this.LEDDownBtn = new System.Windows.Forms.Button();
            this.LEDUpBtn = new System.Windows.Forms.Button();
            this.AllLEDOnOffBtn = new System.Windows.Forms.Button();
            this.CheckDriverICBtn = new System.Windows.Forms.Button();
            this.LoadUnloadBtn = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.GrabBtn = new System.Windows.Forms.Button();
            this.HaltBtn = new System.Windows.Forms.Button();
            this.LiveBtn = new System.Windows.Forms.Button();
            this.FOVRightBtn = new System.Windows.Forms.Button();
            this.FOVLeftBtn = new System.Windows.Forms.Button();
            this.FOVDownBtn = new System.Windows.Forms.Button();
            this.FOVUpBtn = new System.Windows.Forms.Button();
            this.OISXReplayBtn = new System.Windows.Forms.Button();
            this.OISYReplayBtn = new System.Windows.Forms.Button();
            this.RotationReplayBtn = new System.Windows.Forms.Button();
            this.AddrLB2 = new System.Windows.Forms.Label();
            this.DataLB2 = new System.Windows.Forms.Label();
            this.UptoNthMeasureBtn = new System.Windows.Forms.Button();
            this.ScaleCalibrationBtn = new System.Windows.Forms.Button();
            this.GaussianBlurBtn = new System.Windows.Forms.Button();
            this.SetLEDBtn = new System.Windows.Forms.Button();
            this.SetExposureBtn = new System.Windows.Forms.Button();
            this.SelectLED_CH1L = new System.Windows.Forms.RadioButton();
            this.SelectLED_CH1R = new System.Windows.Forms.RadioButton();
            this.SetExposureValue = new System.Windows.Forms.TextBox();
            this.SetThreshBtn = new System.Windows.Forms.Button();
            this.SetThreshValue = new System.Windows.Forms.TextBox();
            this.CalcCOisBtn = new System.Windows.Forms.Button();
            this.XDiffBtn = new System.Windows.Forms.Button();
            this.YDiffBtn = new System.Windows.Forms.Button();
            this.XYDiffBtn = new System.Windows.Forms.Button();
            this.ScaleCalibrationValue = new System.Windows.Forms.TextBox();
            this.CurrentLEDPower = new System.Windows.Forms.Label();
            this.LoadUnloadCoverBtn = new System.Windows.Forms.Button();
            this.SelectLED_CH2L = new System.Windows.Forms.RadioButton();
            this.SelectLED_CH2R = new System.Windows.Forms.RadioButton();
            this.LeftLEDOnOffBtn = new System.Windows.Forms.Button();
            this.RightLEDOnOffBtn = new System.Windows.Forms.Button();
            this.SavebmpBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbYTargetCode = new System.Windows.Forms.TextBox();
            this.tbXTargetCode = new System.Windows.Forms.TextBox();
            this.LoadUnloadAllBtn = new System.Windows.Forms.Button();
            this.SrcImageBox = new System.Windows.Forms.PictureBox();
            this.CH2_L = new System.Windows.Forms.Label();
            this.CH2_R = new System.Windows.Forms.Label();
            this.CH1_L = new System.Windows.Forms.Label();
            this.CH1_R = new System.Windows.Forms.Label();
            this.ClearLogBtn = new System.Windows.Forms.Button();
            this.ReadHall = new System.Windows.Forms.Button();
            this.MoveCode = new System.Windows.Forms.Button();
            this.MeasureBtn = new System.Windows.Forms.Button();
            this.AxisRRdo = new System.Windows.Forms.RadioButton();
            this.AxisXRdo = new System.Windows.Forms.RadioButton();
            this.AxisYRdo = new System.Windows.Forms.RadioButton();
            this.MeasureAllBtn = new System.Windows.Forms.Button();
            this.LoadBmpBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SrcImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // NthMeasureBtn
            // 
            this.NthMeasureBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.NthMeasureBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.NthMeasureBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NthMeasureBtn.FlatAppearance.BorderSize = 0;
            this.NthMeasureBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NthMeasureBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NthMeasureBtn.ForeColor = System.Drawing.Color.Black;
            this.NthMeasureBtn.Location = new System.Drawing.Point(571, 781);
            this.NthMeasureBtn.Name = "NthMeasureBtn";
            this.NthMeasureBtn.Size = new System.Drawing.Size(131, 60);
            this.NthMeasureBtn.TabIndex = 332;
            this.NthMeasureBtn.Text = "N\'th Measure";
            this.NthMeasureBtn.UseVisualStyleBackColor = false;
            this.NthMeasureBtn.Click += new System.EventHandler(this.NthMeasureBtn_Click);
            this.NthMeasureBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.NthMeasureBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // NthMeasureValue
            // 
            this.NthMeasureValue.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NthMeasureValue.Location = new System.Drawing.Point(850, 798);
            this.NthMeasureValue.Name = "NthMeasureValue";
            this.NthMeasureValue.Size = new System.Drawing.Size(67, 26);
            this.NthMeasureValue.TabIndex = 289;
            this.NthMeasureValue.Text = "0";
            this.NthMeasureValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // VsnLog
            // 
            this.VsnLog.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.VsnLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VsnLog.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VsnLog.ForeColor = System.Drawing.Color.LemonChiffon;
            this.VsnLog.Location = new System.Drawing.Point(1288, 395);
            this.VsnLog.Multiline = true;
            this.VsnLog.Name = "VsnLog";
            this.VsnLog.ReadOnly = true;
            this.VsnLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.VsnLog.Size = new System.Drawing.Size(613, 518);
            this.VsnLog.TabIndex = 283;
            // 
            // TPblank
            // 
            this.TPblank.ColumnCount = 1;
            this.TPblank.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TPblank.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TPblank.Location = new System.Drawing.Point(0, 918);
            this.TPblank.Name = "TPblank";
            this.TPblank.RowCount = 1;
            this.TPblank.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TPblank.Size = new System.Drawing.Size(1904, 60);
            this.TPblank.TabIndex = 284;
            // 
            // panelCam0
            // 
            this.panelCam0.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelCam0.Location = new System.Drawing.Point(1748, 354);
            this.panelCam0.Name = "panelCam0";
            this.panelCam0.Size = new System.Drawing.Size(141, 35);
            this.panelCam0.TabIndex = 303;
            // 
            // LiveState
            // 
            this.LiveState.AutoSize = true;
            this.LiveState.BackColor = System.Drawing.Color.Yellow;
            this.LiveState.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LiveState.ForeColor = System.Drawing.Color.Red;
            this.LiveState.Location = new System.Drawing.Point(251, 327);
            this.LiveState.Name = "LiveState";
            this.LiveState.Size = new System.Drawing.Size(41, 19);
            this.LiveState.TabIndex = 377;
            this.LiveState.Text = "Live";
            this.LiveState.Visible = false;
            // 
            // ReadDriverICBtn
            // 
            this.ReadDriverICBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ReadDriverICBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.ReadDriverICBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ReadDriverICBtn.FlatAppearance.BorderSize = 0;
            this.ReadDriverICBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReadDriverICBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReadDriverICBtn.ForeColor = System.Drawing.Color.Black;
            this.ReadDriverICBtn.Location = new System.Drawing.Point(249, 787);
            this.ReadDriverICBtn.Name = "ReadDriverICBtn";
            this.ReadDriverICBtn.Size = new System.Drawing.Size(111, 60);
            this.ReadDriverICBtn.TabIndex = 333;
            this.ReadDriverICBtn.Text = "Read Driver IC";
            this.ReadDriverICBtn.UseVisualStyleBackColor = false;
            this.ReadDriverICBtn.Click += new System.EventHandler(this.ReadDriverICBtn_Click);
            this.ReadDriverICBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.ReadDriverICBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // WriteDriverICBtn
            // 
            this.WriteDriverICBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.WriteDriverICBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.WriteDriverICBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.WriteDriverICBtn.FlatAppearance.BorderSize = 0;
            this.WriteDriverICBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WriteDriverICBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WriteDriverICBtn.ForeColor = System.Drawing.Color.Black;
            this.WriteDriverICBtn.Location = new System.Drawing.Point(249, 853);
            this.WriteDriverICBtn.Name = "WriteDriverICBtn";
            this.WriteDriverICBtn.Size = new System.Drawing.Size(111, 60);
            this.WriteDriverICBtn.TabIndex = 334;
            this.WriteDriverICBtn.Text = "Write Driver IC";
            this.WriteDriverICBtn.UseVisualStyleBackColor = false;
            this.WriteDriverICBtn.Click += new System.EventHandler(this.WriteDriverICBtn_Click);
            this.WriteDriverICBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.WriteDriverICBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // AddrLB
            // 
            this.AddrLB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AddrLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddrLB.Location = new System.Drawing.Point(366, 794);
            this.AddrLB.Name = "AddrLB";
            this.AddrLB.Size = new System.Drawing.Size(65, 23);
            this.AddrLB.TabIndex = 282;
            this.AddrLB.Text = "Addr";
            this.AddrLB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WriteDataValue
            // 
            this.WriteDataValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WriteDataValue.Location = new System.Drawing.Point(437, 886);
            this.WriteDataValue.MaxLength = 4;
            this.WriteDataValue.Name = "WriteDataValue";
            this.WriteDataValue.Size = new System.Drawing.Size(65, 21);
            this.WriteDataValue.TabIndex = 310;
            this.WriteDataValue.Text = "0";
            this.WriteDataValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WriteDataValue_KeyPress);
            // 
            // DataLB
            // 
            this.DataLB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DataLB.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataLB.Location = new System.Drawing.Point(437, 794);
            this.DataLB.Name = "DataLB";
            this.DataLB.Size = new System.Drawing.Size(65, 23);
            this.DataLB.TabIndex = 311;
            this.DataLB.Text = "Data";
            this.DataLB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WriteAddrValue
            // 
            this.WriteAddrValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WriteAddrValue.Location = new System.Drawing.Point(366, 886);
            this.WriteAddrValue.MaxLength = 4;
            this.WriteAddrValue.Name = "WriteAddrValue";
            this.WriteAddrValue.Size = new System.Drawing.Size(65, 21);
            this.WriteAddrValue.TabIndex = 309;
            this.WriteAddrValue.Text = "0";
            this.WriteAddrValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WriteAddrValue_KeyPress);
            // 
            // ReadDataValue
            // 
            this.ReadDataValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReadDataValue.Location = new System.Drawing.Point(437, 820);
            this.ReadDataValue.MaxLength = 4;
            this.ReadDataValue.Name = "ReadDataValue";
            this.ReadDataValue.ReadOnly = true;
            this.ReadDataValue.Size = new System.Drawing.Size(65, 21);
            this.ReadDataValue.TabIndex = 308;
            this.ReadDataValue.Text = "0";
            this.ReadDataValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ReadDataValue_KeyPress);
            // 
            // ReadAddrValue
            // 
            this.ReadAddrValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReadAddrValue.Location = new System.Drawing.Point(366, 820);
            this.ReadAddrValue.MaxLength = 4;
            this.ReadAddrValue.Name = "ReadAddrValue";
            this.ReadAddrValue.Size = new System.Drawing.Size(65, 21);
            this.ReadAddrValue.TabIndex = 307;
            this.ReadAddrValue.Text = "0";
            this.ReadAddrValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ReadAddrValue_KeyPress);
            // 
            // LEDDownBtn
            // 
            this.LEDDownBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LEDDownBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.LEDDownBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LEDDownBtn.FlatAppearance.BorderSize = 0;
            this.LEDDownBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LEDDownBtn.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LEDDownBtn.ForeColor = System.Drawing.Color.Black;
            this.LEDDownBtn.Location = new System.Drawing.Point(722, 394);
            this.LEDDownBtn.Name = "LEDDownBtn";
            this.LEDDownBtn.Size = new System.Drawing.Size(134, 59);
            this.LEDDownBtn.TabIndex = 331;
            this.LEDDownBtn.Text = "LED Darker";
            this.LEDDownBtn.UseVisualStyleBackColor = false;
            this.LEDDownBtn.Click += new System.EventHandler(this.LEDDownBtn_Click);
            this.LEDDownBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.LEDDownBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // LEDUpBtn
            // 
            this.LEDUpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LEDUpBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.LEDUpBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LEDUpBtn.FlatAppearance.BorderSize = 0;
            this.LEDUpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LEDUpBtn.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LEDUpBtn.ForeColor = System.Drawing.Color.Black;
            this.LEDUpBtn.Location = new System.Drawing.Point(583, 394);
            this.LEDUpBtn.Name = "LEDUpBtn";
            this.LEDUpBtn.Size = new System.Drawing.Size(133, 59);
            this.LEDUpBtn.TabIndex = 330;
            this.LEDUpBtn.Text = "LED Brighter";
            this.LEDUpBtn.UseVisualStyleBackColor = false;
            this.LEDUpBtn.Click += new System.EventHandler(this.LEDUpBtn_Click);
            this.LEDUpBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.LEDUpBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // AllLEDOnOffBtn
            // 
            this.AllLEDOnOffBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.AllLEDOnOffBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.AllLEDOnOffBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AllLEDOnOffBtn.FlatAppearance.BorderSize = 0;
            this.AllLEDOnOffBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AllLEDOnOffBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllLEDOnOffBtn.ForeColor = System.Drawing.Color.Black;
            this.AllLEDOnOffBtn.Location = new System.Drawing.Point(583, 459);
            this.AllLEDOnOffBtn.Name = "AllLEDOnOffBtn";
            this.AllLEDOnOffBtn.Size = new System.Drawing.Size(273, 58);
            this.AllLEDOnOffBtn.TabIndex = 336;
            this.AllLEDOnOffBtn.Text = "ALL LED \r\nON / OFF";
            this.AllLEDOnOffBtn.UseVisualStyleBackColor = false;
            this.AllLEDOnOffBtn.Click += new System.EventHandler(this.AllLEDOnOffBtn_Click);
            this.AllLEDOnOffBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.AllLEDOnOffBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // CheckDriverICBtn
            // 
            this.CheckDriverICBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.CheckDriverICBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.CheckDriverICBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CheckDriverICBtn.FlatAppearance.BorderSize = 0;
            this.CheckDriverICBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CheckDriverICBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckDriverICBtn.ForeColor = System.Drawing.Color.White;
            this.CheckDriverICBtn.Location = new System.Drawing.Point(249, 721);
            this.CheckDriverICBtn.Name = "CheckDriverICBtn";
            this.CheckDriverICBtn.Size = new System.Drawing.Size(253, 62);
            this.CheckDriverICBtn.TabIndex = 329;
            this.CheckDriverICBtn.Text = "Check Driver IC";
            this.CheckDriverICBtn.UseVisualStyleBackColor = false;
            this.CheckDriverICBtn.Click += new System.EventHandler(this.CheckDriverICBtn_Click);
            this.CheckDriverICBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.CheckDriverICBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // LoadUnloadBtn
            // 
            this.LoadUnloadBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LoadUnloadBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.LoadUnloadBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LoadUnloadBtn.FlatAppearance.BorderSize = 0;
            this.LoadUnloadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadUnloadBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadUnloadBtn.ForeColor = System.Drawing.Color.White;
            this.LoadUnloadBtn.Location = new System.Drawing.Point(249, 356);
            this.LoadUnloadBtn.Name = "LoadUnloadBtn";
            this.LoadUnloadBtn.Size = new System.Drawing.Size(273, 58);
            this.LoadUnloadBtn.TabIndex = 328;
            this.LoadUnloadBtn.Text = "<Socket> Load  / Unload ";
            this.LoadUnloadBtn.UseVisualStyleBackColor = false;
            this.LoadUnloadBtn.Click += new System.EventHandler(this.LoadUnloadSocketBtn_Click);
            this.LoadUnloadBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.LoadUnloadBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // ClearBtn
            // 
            this.ClearBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ClearBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.ClearBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClearBtn.FlatAppearance.BorderSize = 0;
            this.ClearBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearBtn.ForeColor = System.Drawing.Color.White;
            this.ClearBtn.Location = new System.Drawing.Point(11, 488);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(204, 60);
            this.ClearBtn.TabIndex = 326;
            this.ClearBtn.Text = "Draw";
            this.ClearBtn.UseVisualStyleBackColor = false;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            this.ClearBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.ClearBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // GrabBtn
            // 
            this.GrabBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.GrabBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.GrabBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GrabBtn.FlatAppearance.BorderSize = 0;
            this.GrabBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GrabBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrabBtn.ForeColor = System.Drawing.Color.White;
            this.GrabBtn.Location = new System.Drawing.Point(11, 422);
            this.GrabBtn.Name = "GrabBtn";
            this.GrabBtn.Size = new System.Drawing.Size(204, 60);
            this.GrabBtn.TabIndex = 325;
            this.GrabBtn.Text = "Grab";
            this.GrabBtn.UseVisualStyleBackColor = false;
            this.GrabBtn.Click += new System.EventHandler(this.GrabBtn_Click);
            this.GrabBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.GrabBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // HaltBtn
            // 
            this.HaltBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.HaltBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.HaltBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HaltBtn.FlatAppearance.BorderSize = 0;
            this.HaltBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HaltBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HaltBtn.ForeColor = System.Drawing.Color.White;
            this.HaltBtn.Location = new System.Drawing.Point(11, 356);
            this.HaltBtn.Name = "HaltBtn";
            this.HaltBtn.Size = new System.Drawing.Size(204, 60);
            this.HaltBtn.TabIndex = 324;
            this.HaltBtn.Text = "Halt";
            this.HaltBtn.UseVisualStyleBackColor = false;
            this.HaltBtn.Click += new System.EventHandler(this.HaltBtn_Click);
            this.HaltBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.HaltBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // LiveBtn
            // 
            this.LiveBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LiveBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.LiveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LiveBtn.FlatAppearance.BorderSize = 0;
            this.LiveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LiveBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LiveBtn.ForeColor = System.Drawing.Color.White;
            this.LiveBtn.Location = new System.Drawing.Point(11, 290);
            this.LiveBtn.Name = "LiveBtn";
            this.LiveBtn.Size = new System.Drawing.Size(204, 60);
            this.LiveBtn.TabIndex = 323;
            this.LiveBtn.Text = "Live";
            this.LiveBtn.UseVisualStyleBackColor = false;
            this.LiveBtn.Click += new System.EventHandler(this.LiveBtn_Click);
            this.LiveBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.LiveBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // FOVRightBtn
            // 
            this.FOVRightBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.FOVRightBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.FOVRightBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FOVRightBtn.FlatAppearance.BorderSize = 0;
            this.FOVRightBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FOVRightBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FOVRightBtn.ForeColor = System.Drawing.Color.White;
            this.FOVRightBtn.Location = new System.Drawing.Point(117, 125);
            this.FOVRightBtn.Name = "FOVRightBtn";
            this.FOVRightBtn.Size = new System.Drawing.Size(95, 50);
            this.FOVRightBtn.TabIndex = 3;
            this.FOVRightBtn.Text = "FOV Right";
            this.FOVRightBtn.UseVisualStyleBackColor = false;
            this.FOVRightBtn.Click += new System.EventHandler(this.FOVMoveBtn_Click);
            this.FOVRightBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.FOVRightBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // FOVLeftBtn
            // 
            this.FOVLeftBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.FOVLeftBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.FOVLeftBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FOVLeftBtn.FlatAppearance.BorderSize = 0;
            this.FOVLeftBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FOVLeftBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FOVLeftBtn.ForeColor = System.Drawing.Color.White;
            this.FOVLeftBtn.Location = new System.Drawing.Point(8, 124);
            this.FOVLeftBtn.Name = "FOVLeftBtn";
            this.FOVLeftBtn.Size = new System.Drawing.Size(95, 50);
            this.FOVLeftBtn.TabIndex = 2;
            this.FOVLeftBtn.Text = "FOV Left";
            this.FOVLeftBtn.UseVisualStyleBackColor = false;
            this.FOVLeftBtn.Click += new System.EventHandler(this.FOVMoveBtn_Click);
            this.FOVLeftBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.FOVLeftBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // FOVDownBtn
            // 
            this.FOVDownBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.FOVDownBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.FOVDownBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FOVDownBtn.FlatAppearance.BorderSize = 0;
            this.FOVDownBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FOVDownBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FOVDownBtn.ForeColor = System.Drawing.Color.White;
            this.FOVDownBtn.Location = new System.Drawing.Point(66, 180);
            this.FOVDownBtn.Name = "FOVDownBtn";
            this.FOVDownBtn.Size = new System.Drawing.Size(95, 50);
            this.FOVDownBtn.TabIndex = 1;
            this.FOVDownBtn.Text = "FOV Down";
            this.FOVDownBtn.UseVisualStyleBackColor = false;
            this.FOVDownBtn.Click += new System.EventHandler(this.FOVMoveBtn_Click);
            this.FOVDownBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.FOVDownBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // FOVUpBtn
            // 
            this.FOVUpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.FOVUpBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.FOVUpBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FOVUpBtn.FlatAppearance.BorderSize = 0;
            this.FOVUpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FOVUpBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FOVUpBtn.ForeColor = System.Drawing.Color.White;
            this.FOVUpBtn.Location = new System.Drawing.Point(66, 69);
            this.FOVUpBtn.Name = "FOVUpBtn";
            this.FOVUpBtn.Size = new System.Drawing.Size(95, 50);
            this.FOVUpBtn.TabIndex = 0;
            this.FOVUpBtn.Text = "FOV Up";
            this.FOVUpBtn.UseVisualStyleBackColor = false;
            this.FOVUpBtn.Click += new System.EventHandler(this.FOVMoveBtn_Click);
            this.FOVUpBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.FOVUpBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // OISXReplayBtn
            // 
            this.OISXReplayBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.OISXReplayBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.OISXReplayBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OISXReplayBtn.FlatAppearance.BorderSize = 0;
            this.OISXReplayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OISXReplayBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OISXReplayBtn.ForeColor = System.Drawing.Color.White;
            this.OISXReplayBtn.Location = new System.Drawing.Point(11, 721);
            this.OISXReplayBtn.Name = "OISXReplayBtn";
            this.OISXReplayBtn.Size = new System.Drawing.Size(204, 60);
            this.OISXReplayBtn.TabIndex = 0;
            this.OISXReplayBtn.Text = "X Replay";
            this.OISXReplayBtn.UseVisualStyleBackColor = false;
            this.OISXReplayBtn.Click += new System.EventHandler(this.OISReplayBtn_Click);
            this.OISXReplayBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.OISXReplayBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // OISYReplayBtn
            // 
            this.OISYReplayBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.OISYReplayBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.OISYReplayBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OISYReplayBtn.FlatAppearance.BorderSize = 0;
            this.OISYReplayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OISYReplayBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OISYReplayBtn.ForeColor = System.Drawing.Color.White;
            this.OISYReplayBtn.Location = new System.Drawing.Point(10, 787);
            this.OISYReplayBtn.Name = "OISYReplayBtn";
            this.OISYReplayBtn.Size = new System.Drawing.Size(204, 60);
            this.OISYReplayBtn.TabIndex = 1;
            this.OISYReplayBtn.Text = "Y Replay";
            this.OISYReplayBtn.UseVisualStyleBackColor = false;
            this.OISYReplayBtn.Click += new System.EventHandler(this.OISReplayBtn_Click);
            this.OISYReplayBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.OISYReplayBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // RotationReplayBtn
            // 
            this.RotationReplayBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.RotationReplayBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.RotationReplayBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RotationReplayBtn.FlatAppearance.BorderSize = 0;
            this.RotationReplayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RotationReplayBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RotationReplayBtn.ForeColor = System.Drawing.Color.White;
            this.RotationReplayBtn.Location = new System.Drawing.Point(10, 853);
            this.RotationReplayBtn.Name = "RotationReplayBtn";
            this.RotationReplayBtn.Size = new System.Drawing.Size(204, 60);
            this.RotationReplayBtn.TabIndex = 2;
            this.RotationReplayBtn.Text = "Rotation Replay";
            this.RotationReplayBtn.UseVisualStyleBackColor = false;
            this.RotationReplayBtn.Click += new System.EventHandler(this.RotationReplayBtn_Click);
            this.RotationReplayBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.RotationReplayBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // AddrLB2
            // 
            this.AddrLB2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AddrLB2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddrLB2.Location = new System.Drawing.Point(366, 860);
            this.AddrLB2.Name = "AddrLB2";
            this.AddrLB2.Size = new System.Drawing.Size(65, 23);
            this.AddrLB2.TabIndex = 348;
            this.AddrLB2.Text = "Addr";
            this.AddrLB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DataLB2
            // 
            this.DataLB2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DataLB2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataLB2.Location = new System.Drawing.Point(437, 860);
            this.DataLB2.Name = "DataLB2";
            this.DataLB2.Size = new System.Drawing.Size(65, 23);
            this.DataLB2.TabIndex = 349;
            this.DataLB2.Text = "Data";
            this.DataLB2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UptoNthMeasureBtn
            // 
            this.UptoNthMeasureBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.UptoNthMeasureBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.UptoNthMeasureBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UptoNthMeasureBtn.FlatAppearance.BorderSize = 0;
            this.UptoNthMeasureBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UptoNthMeasureBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UptoNthMeasureBtn.ForeColor = System.Drawing.Color.Black;
            this.UptoNthMeasureBtn.Location = new System.Drawing.Point(708, 781);
            this.UptoNthMeasureBtn.Name = "UptoNthMeasureBtn";
            this.UptoNthMeasureBtn.Size = new System.Drawing.Size(136, 60);
            this.UptoNthMeasureBtn.TabIndex = 350;
            this.UptoNthMeasureBtn.Text = "Upto N’th Measure";
            this.UptoNthMeasureBtn.UseVisualStyleBackColor = false;
            this.UptoNthMeasureBtn.Click += new System.EventHandler(this.UptoNthMeasureBtn_Click);
            this.UptoNthMeasureBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.UptoNthMeasureBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // ScaleCalibrationBtn
            // 
            this.ScaleCalibrationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ScaleCalibrationBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.ScaleCalibrationBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ScaleCalibrationBtn.FlatAppearance.BorderSize = 0;
            this.ScaleCalibrationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ScaleCalibrationBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScaleCalibrationBtn.ForeColor = System.Drawing.Color.Black;
            this.ScaleCalibrationBtn.Location = new System.Drawing.Point(571, 847);
            this.ScaleCalibrationBtn.Name = "ScaleCalibrationBtn";
            this.ScaleCalibrationBtn.Size = new System.Drawing.Size(273, 64);
            this.ScaleCalibrationBtn.TabIndex = 351;
            this.ScaleCalibrationBtn.Text = "Scale Calibration by Mark Distance";
            this.ScaleCalibrationBtn.UseVisualStyleBackColor = false;
            this.ScaleCalibrationBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.ScaleCalibrationBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // GaussianBlurBtn
            // 
            this.GaussianBlurBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.GaussianBlurBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.GaussianBlurBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GaussianBlurBtn.FlatAppearance.BorderSize = 0;
            this.GaussianBlurBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GaussianBlurBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GaussianBlurBtn.ForeColor = System.Drawing.Color.Black;
            this.GaussianBlurBtn.Location = new System.Drawing.Point(1007, 695);
            this.GaussianBlurBtn.Name = "GaussianBlurBtn";
            this.GaussianBlurBtn.Size = new System.Drawing.Size(180, 49);
            this.GaussianBlurBtn.TabIndex = 352;
            this.GaussianBlurBtn.Text = "Gaussian Blur";
            this.GaussianBlurBtn.UseVisualStyleBackColor = false;
            this.GaussianBlurBtn.Visible = false;
            this.GaussianBlurBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.GaussianBlurBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // SetLEDBtn
            // 
            this.SetLEDBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SetLEDBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg20;
            this.SetLEDBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SetLEDBtn.FlatAppearance.BorderSize = 0;
            this.SetLEDBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetLEDBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetLEDBtn.ForeColor = System.Drawing.Color.White;
            this.SetLEDBtn.Location = new System.Drawing.Point(1032, 356);
            this.SetLEDBtn.Name = "SetLEDBtn";
            this.SetLEDBtn.Size = new System.Drawing.Size(155, 33);
            this.SetLEDBtn.TabIndex = 353;
            this.SetLEDBtn.Text = "Set LED Power";
            this.SetLEDBtn.UseVisualStyleBackColor = false;
            this.SetLEDBtn.Click += new System.EventHandler(this.SetLEDBtn_Click);
            this.SetLEDBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.SetLEDBtn.MouseHover += new System.EventHandler(this.Button_MouseMove);
            // 
            // SetExposureBtn
            // 
            this.SetExposureBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SetExposureBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.SetExposureBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SetExposureBtn.FlatAppearance.BorderSize = 0;
            this.SetExposureBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetExposureBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetExposureBtn.ForeColor = System.Drawing.Color.Black;
            this.SetExposureBtn.Location = new System.Drawing.Point(914, 395);
            this.SetExposureBtn.Name = "SetExposureBtn";
            this.SetExposureBtn.Size = new System.Drawing.Size(273, 58);
            this.SetExposureBtn.TabIndex = 356;
            this.SetExposureBtn.Text = "Set Exposure";
            this.SetExposureBtn.UseVisualStyleBackColor = false;
            this.SetExposureBtn.Click += new System.EventHandler(this.SetExposureBtn_Click);
            this.SetExposureBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.SetExposureBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // SelectLED_CH1L
            // 
            this.SelectLED_CH1L.AutoSize = true;
            this.SelectLED_CH1L.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectLED_CH1L.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectLED_CH1L.Location = new System.Drawing.Point(562, 360);
            this.SelectLED_CH1L.Name = "SelectLED_CH1L";
            this.SelectLED_CH1L.Size = new System.Drawing.Size(73, 23);
            this.SelectLED_CH1L.TabIndex = 0;
            this.SelectLED_CH1L.TabStop = true;
            this.SelectLED_CH1L.Text = "CH1 L";
            this.SelectLED_CH1L.UseVisualStyleBackColor = true;
            this.SelectLED_CH1L.CheckedChanged += new System.EventHandler(this.SelectLED_CheckedChanged);
            // 
            // SelectLED_CH1R
            // 
            this.SelectLED_CH1R.AutoSize = true;
            this.SelectLED_CH1R.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectLED_CH1R.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectLED_CH1R.Location = new System.Drawing.Point(641, 360);
            this.SelectLED_CH1R.Name = "SelectLED_CH1R";
            this.SelectLED_CH1R.Size = new System.Drawing.Size(75, 23);
            this.SelectLED_CH1R.TabIndex = 1;
            this.SelectLED_CH1R.TabStop = true;
            this.SelectLED_CH1R.Text = "CH1 R";
            this.SelectLED_CH1R.UseVisualStyleBackColor = true;
            this.SelectLED_CH1R.CheckedChanged += new System.EventHandler(this.SelectLED_CheckedChanged);
            // 
            // SetExposureValue
            // 
            this.SetExposureValue.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetExposureValue.Location = new System.Drawing.Point(1193, 411);
            this.SetExposureValue.Name = "SetExposureValue";
            this.SetExposureValue.Size = new System.Drawing.Size(67, 26);
            this.SetExposureValue.TabIndex = 362;
            this.SetExposureValue.Text = "0";
            // 
            // SetThreshBtn
            // 
            this.SetThreshBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SetThreshBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.SetThreshBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SetThreshBtn.FlatAppearance.BorderSize = 0;
            this.SetThreshBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetThreshBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetThreshBtn.ForeColor = System.Drawing.Color.Black;
            this.SetThreshBtn.Location = new System.Drawing.Point(914, 459);
            this.SetThreshBtn.Name = "SetThreshBtn";
            this.SetThreshBtn.Size = new System.Drawing.Size(273, 58);
            this.SetThreshBtn.TabIndex = 363;
            this.SetThreshBtn.Text = "Set Thresh";
            this.SetThreshBtn.UseVisualStyleBackColor = false;
            this.SetThreshBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.SetThreshBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // SetThreshValue
            // 
            this.SetThreshValue.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetThreshValue.Location = new System.Drawing.Point(1193, 477);
            this.SetThreshValue.Name = "SetThreshValue";
            this.SetThreshValue.Size = new System.Drawing.Size(67, 26);
            this.SetThreshValue.TabIndex = 364;
            this.SetThreshValue.Text = "0";
            // 
            // CalcCOisBtn
            // 
            this.CalcCOisBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.CalcCOisBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.CalcCOisBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CalcCOisBtn.FlatAppearance.BorderSize = 0;
            this.CalcCOisBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CalcCOisBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalcCOisBtn.ForeColor = System.Drawing.Color.Black;
            this.CalcCOisBtn.Location = new System.Drawing.Point(914, 523);
            this.CalcCOisBtn.Name = "CalcCOisBtn";
            this.CalcCOisBtn.Size = new System.Drawing.Size(273, 58);
            this.CalcCOisBtn.TabIndex = 365;
            this.CalcCOisBtn.Text = "Calc COis";
            this.CalcCOisBtn.UseVisualStyleBackColor = false;
            this.CalcCOisBtn.Visible = false;
            this.CalcCOisBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.CalcCOisBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // XDiffBtn
            // 
            this.XDiffBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.XDiffBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.XDiffBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.XDiffBtn.FlatAppearance.BorderSize = 0;
            this.XDiffBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.XDiffBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XDiffBtn.ForeColor = System.Drawing.Color.Black;
            this.XDiffBtn.Location = new System.Drawing.Point(1007, 750);
            this.XDiffBtn.Name = "XDiffBtn";
            this.XDiffBtn.Size = new System.Drawing.Size(180, 49);
            this.XDiffBtn.TabIndex = 366;
            this.XDiffBtn.Text = "X Diff";
            this.XDiffBtn.UseVisualStyleBackColor = false;
            this.XDiffBtn.Visible = false;
            this.XDiffBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.XDiffBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // YDiffBtn
            // 
            this.YDiffBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.YDiffBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.YDiffBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.YDiffBtn.FlatAppearance.BorderSize = 0;
            this.YDiffBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.YDiffBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YDiffBtn.ForeColor = System.Drawing.Color.Black;
            this.YDiffBtn.Location = new System.Drawing.Point(1007, 805);
            this.YDiffBtn.Name = "YDiffBtn";
            this.YDiffBtn.Size = new System.Drawing.Size(180, 49);
            this.YDiffBtn.TabIndex = 367;
            this.YDiffBtn.Text = "Y Diff";
            this.YDiffBtn.UseVisualStyleBackColor = false;
            this.YDiffBtn.Visible = false;
            this.YDiffBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.YDiffBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // XYDiffBtn
            // 
            this.XYDiffBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.XYDiffBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.XYDiffBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.XYDiffBtn.FlatAppearance.BorderSize = 0;
            this.XYDiffBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.XYDiffBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XYDiffBtn.ForeColor = System.Drawing.Color.Black;
            this.XYDiffBtn.Location = new System.Drawing.Point(1007, 860);
            this.XYDiffBtn.Name = "XYDiffBtn";
            this.XYDiffBtn.Size = new System.Drawing.Size(180, 49);
            this.XYDiffBtn.TabIndex = 368;
            this.XYDiffBtn.Text = "XY Diff";
            this.XYDiffBtn.UseVisualStyleBackColor = false;
            this.XYDiffBtn.Visible = false;
            this.XYDiffBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.XYDiffBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // ScaleCalibrationValue
            // 
            this.ScaleCalibrationValue.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScaleCalibrationValue.Location = new System.Drawing.Point(850, 869);
            this.ScaleCalibrationValue.Name = "ScaleCalibrationValue";
            this.ScaleCalibrationValue.Size = new System.Drawing.Size(67, 26);
            this.ScaleCalibrationValue.TabIndex = 369;
            this.ScaleCalibrationValue.Text = "0";
            this.ScaleCalibrationValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CurrentLEDPower
            // 
            this.CurrentLEDPower.BackColor = System.Drawing.Color.White;
            this.CurrentLEDPower.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLEDPower.ForeColor = System.Drawing.Color.Blue;
            this.CurrentLEDPower.Location = new System.Drawing.Point(914, 358);
            this.CurrentLEDPower.Name = "CurrentLEDPower";
            this.CurrentLEDPower.Size = new System.Drawing.Size(112, 27);
            this.CurrentLEDPower.TabIndex = 371;
            this.CurrentLEDPower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoadUnloadCoverBtn
            // 
            this.LoadUnloadCoverBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LoadUnloadCoverBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.LoadUnloadCoverBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LoadUnloadCoverBtn.FlatAppearance.BorderSize = 0;
            this.LoadUnloadCoverBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadUnloadCoverBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadUnloadCoverBtn.ForeColor = System.Drawing.Color.White;
            this.LoadUnloadCoverBtn.Location = new System.Drawing.Point(249, 419);
            this.LoadUnloadCoverBtn.Name = "LoadUnloadCoverBtn";
            this.LoadUnloadCoverBtn.Size = new System.Drawing.Size(273, 58);
            this.LoadUnloadCoverBtn.TabIndex = 374;
            this.LoadUnloadCoverBtn.Text = "<Cover> UP / Down";
            this.LoadUnloadCoverBtn.UseVisualStyleBackColor = false;
            this.LoadUnloadCoverBtn.Click += new System.EventHandler(this.LoadUnloadCoverBtn_Click);
            this.LoadUnloadCoverBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.LoadUnloadCoverBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // SelectLED_CH2L
            // 
            this.SelectLED_CH2L.AutoSize = true;
            this.SelectLED_CH2L.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectLED_CH2L.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectLED_CH2L.Location = new System.Drawing.Point(722, 360);
            this.SelectLED_CH2L.Name = "SelectLED_CH2L";
            this.SelectLED_CH2L.Size = new System.Drawing.Size(73, 23);
            this.SelectLED_CH2L.TabIndex = 2;
            this.SelectLED_CH2L.TabStop = true;
            this.SelectLED_CH2L.Text = "CH2 L";
            this.SelectLED_CH2L.UseVisualStyleBackColor = true;
            this.SelectLED_CH2L.CheckedChanged += new System.EventHandler(this.SelectLED_CheckedChanged);
            // 
            // SelectLED_CH2R
            // 
            this.SelectLED_CH2R.AutoSize = true;
            this.SelectLED_CH2R.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectLED_CH2R.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectLED_CH2R.Location = new System.Drawing.Point(801, 360);
            this.SelectLED_CH2R.Name = "SelectLED_CH2R";
            this.SelectLED_CH2R.Size = new System.Drawing.Size(75, 23);
            this.SelectLED_CH2R.TabIndex = 3;
            this.SelectLED_CH2R.TabStop = true;
            this.SelectLED_CH2R.Text = "CH2 R";
            this.SelectLED_CH2R.UseVisualStyleBackColor = true;
            this.SelectLED_CH2R.CheckedChanged += new System.EventHandler(this.SelectLED_CheckedChanged);
            // 
            // LeftLEDOnOffBtn
            // 
            this.LeftLEDOnOffBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LeftLEDOnOffBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.LeftLEDOnOffBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LeftLEDOnOffBtn.FlatAppearance.BorderSize = 0;
            this.LeftLEDOnOffBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftLEDOnOffBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftLEDOnOffBtn.ForeColor = System.Drawing.Color.Black;
            this.LeftLEDOnOffBtn.Location = new System.Drawing.Point(583, 523);
            this.LeftLEDOnOffBtn.Name = "LeftLEDOnOffBtn";
            this.LeftLEDOnOffBtn.Size = new System.Drawing.Size(133, 58);
            this.LeftLEDOnOffBtn.TabIndex = 375;
            this.LeftLEDOnOffBtn.Text = "CH1 \r\nLED ON / OFF";
            this.LeftLEDOnOffBtn.UseVisualStyleBackColor = false;
            this.LeftLEDOnOffBtn.Click += new System.EventHandler(this.LeftLEDOnOffBtn_Click);
            this.LeftLEDOnOffBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.LeftLEDOnOffBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // RightLEDOnOffBtn
            // 
            this.RightLEDOnOffBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.RightLEDOnOffBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.RightLEDOnOffBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RightLEDOnOffBtn.FlatAppearance.BorderSize = 0;
            this.RightLEDOnOffBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RightLEDOnOffBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightLEDOnOffBtn.ForeColor = System.Drawing.Color.Black;
            this.RightLEDOnOffBtn.Location = new System.Drawing.Point(722, 523);
            this.RightLEDOnOffBtn.Name = "RightLEDOnOffBtn";
            this.RightLEDOnOffBtn.Size = new System.Drawing.Size(133, 58);
            this.RightLEDOnOffBtn.TabIndex = 376;
            this.RightLEDOnOffBtn.Text = "CH2  \r\nLED ON / OFF";
            this.RightLEDOnOffBtn.UseVisualStyleBackColor = false;
            this.RightLEDOnOffBtn.Click += new System.EventHandler(this.RightLEDOnOffBtn_Click);
            this.RightLEDOnOffBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.RightLEDOnOffBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // SavebmpBtn
            // 
            this.SavebmpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.SavebmpBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.SavebmpBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SavebmpBtn.FlatAppearance.BorderSize = 0;
            this.SavebmpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SavebmpBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SavebmpBtn.ForeColor = System.Drawing.Color.White;
            this.SavebmpBtn.Location = new System.Drawing.Point(11, 554);
            this.SavebmpBtn.Name = "SavebmpBtn";
            this.SavebmpBtn.Size = new System.Drawing.Size(204, 60);
            this.SavebmpBtn.TabIndex = 377;
            this.SavebmpBtn.Text = "Save Bmp";
            this.SavebmpBtn.UseVisualStyleBackColor = false;
            this.SavebmpBtn.Click += new System.EventHandler(this.SavebmpBtn_Click);
            this.SavebmpBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.SavebmpBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(249, 624);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 23);
            this.label10.TabIndex = 384;
            this.label10.Text = "Y Target Code";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(249, 570);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 23);
            this.label8.TabIndex = 385;
            this.label8.Text = "X Target Code";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbYTargetCode
            // 
            this.tbYTargetCode.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbYTargetCode.Location = new System.Drawing.Point(249, 650);
            this.tbYTargetCode.Name = "tbYTargetCode";
            this.tbYTargetCode.Size = new System.Drawing.Size(120, 25);
            this.tbYTargetCode.TabIndex = 382;
            this.tbYTargetCode.Text = "0";
            // 
            // tbXTargetCode
            // 
            this.tbXTargetCode.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbXTargetCode.Location = new System.Drawing.Point(249, 596);
            this.tbXTargetCode.Name = "tbXTargetCode";
            this.tbXTargetCode.Size = new System.Drawing.Size(120, 25);
            this.tbXTargetCode.TabIndex = 383;
            this.tbXTargetCode.Text = "0";
            // 
            // LoadUnloadAllBtn
            // 
            this.LoadUnloadAllBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LoadUnloadAllBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn5;
            this.LoadUnloadAllBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LoadUnloadAllBtn.FlatAppearance.BorderSize = 0;
            this.LoadUnloadAllBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadUnloadAllBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadUnloadAllBtn.ForeColor = System.Drawing.Color.White;
            this.LoadUnloadAllBtn.Location = new System.Drawing.Point(249, 483);
            this.LoadUnloadAllBtn.Name = "LoadUnloadAllBtn";
            this.LoadUnloadAllBtn.Size = new System.Drawing.Size(273, 58);
            this.LoadUnloadAllBtn.TabIndex = 388;
            this.LoadUnloadAllBtn.Text = "All Load  / Unload ";
            this.LoadUnloadAllBtn.UseVisualStyleBackColor = false;
            this.LoadUnloadAllBtn.Click += new System.EventHandler(this.LoadUnloadAllBtn_Click);
            this.LoadUnloadAllBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.LoadUnloadAllBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            // 
            // SrcImageBox
            // 
            this.SrcImageBox.BackColor = System.Drawing.Color.Black;
            this.SrcImageBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SrcImageBox.Location = new System.Drawing.Point(249, 8);
            this.SrcImageBox.Name = "SrcImageBox";
            this.SrcImageBox.Size = new System.Drawing.Size(1640, 340);
            this.SrcImageBox.TabIndex = 389;
            this.SrcImageBox.TabStop = false;
            // 
            // CH2_L
            // 
            this.CH2_L.AutoSize = true;
            this.CH2_L.BackColor = System.Drawing.Color.Black;
            this.CH2_L.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.CH2_L.ForeColor = System.Drawing.Color.DarkViolet;
            this.CH2_L.Location = new System.Drawing.Point(1093, 10);
            this.CH2_L.Name = "CH2_L";
            this.CH2_L.Size = new System.Drawing.Size(46, 19);
            this.CH2_L.TabIndex = 392;
            this.CH2_L.Text = "CH2 L";
            // 
            // CH2_R
            // 
            this.CH2_R.AutoSize = true;
            this.CH2_R.BackColor = System.Drawing.Color.Black;
            this.CH2_R.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.CH2_R.ForeColor = System.Drawing.Color.DarkViolet;
            this.CH2_R.Location = new System.Drawing.Point(1482, 11);
            this.CH2_R.Name = "CH2_R";
            this.CH2_R.Size = new System.Drawing.Size(48, 19);
            this.CH2_R.TabIndex = 393;
            this.CH2_R.Text = "CH2 R";
            // 
            // CH1_L
            // 
            this.CH1_L.AutoSize = true;
            this.CH1_L.BackColor = System.Drawing.Color.Black;
            this.CH1_L.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.CH1_L.ForeColor = System.Drawing.Color.DarkViolet;
            this.CH1_L.Location = new System.Drawing.Point(253, 11);
            this.CH1_L.Name = "CH1_L";
            this.CH1_L.Size = new System.Drawing.Size(46, 19);
            this.CH1_L.TabIndex = 390;
            this.CH1_L.Text = "CH1 L";
            // 
            // CH1_R
            // 
            this.CH1_R.AutoSize = true;
            this.CH1_R.BackColor = System.Drawing.Color.Black;
            this.CH1_R.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.CH1_R.ForeColor = System.Drawing.Color.DarkViolet;
            this.CH1_R.Location = new System.Drawing.Point(667, 11);
            this.CH1_R.Name = "CH1_R";
            this.CH1_R.Size = new System.Drawing.Size(48, 19);
            this.CH1_R.TabIndex = 391;
            this.CH1_R.Text = "CH1 R";
            // 
            // ClearLogBtn
            // 
            this.ClearLogBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ClearLogBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.ClearLogBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClearLogBtn.FlatAppearance.BorderSize = 0;
            this.ClearLogBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearLogBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearLogBtn.ForeColor = System.Drawing.Color.White;
            this.ClearLogBtn.Location = new System.Drawing.Point(1288, 354);
            this.ClearLogBtn.Name = "ClearLogBtn";
            this.ClearLogBtn.Size = new System.Drawing.Size(225, 35);
            this.ClearLogBtn.TabIndex = 394;
            this.ClearLogBtn.Text = "Clear Log";
            this.ClearLogBtn.UseVisualStyleBackColor = false;
            this.ClearLogBtn.Click += new System.EventHandler(this.ClearLogBtn_Click);
            this.ClearLogBtn.MouseEnter += new System.EventHandler(this.Button_MouseMove);
            this.ClearLogBtn.MouseLeave += new System.EventHandler(this.Button_MouseLeave);
            this.ClearLogBtn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Button_MouseMove);
            // 
            // ReadHall
            // 
            this.ReadHall.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ReadHall.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.ReadHall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ReadHall.FlatAppearance.BorderSize = 0;
            this.ReadHall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReadHall.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReadHall.ForeColor = System.Drawing.Color.Black;
            this.ReadHall.Location = new System.Drawing.Point(375, 624);
            this.ReadHall.Name = "ReadHall";
            this.ReadHall.Size = new System.Drawing.Size(147, 51);
            this.ReadHall.TabIndex = 396;
            this.ReadHall.Text = "ReadHall";
            this.ReadHall.UseVisualStyleBackColor = false;
            this.ReadHall.Click += new System.EventHandler(this.ReadHall_Click);
            // 
            // MoveCode
            // 
            this.MoveCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.MoveCode.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.MoveCode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MoveCode.FlatAppearance.BorderSize = 0;
            this.MoveCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MoveCode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoveCode.ForeColor = System.Drawing.Color.Black;
            this.MoveCode.Location = new System.Drawing.Point(375, 570);
            this.MoveCode.Name = "MoveCode";
            this.MoveCode.Size = new System.Drawing.Size(147, 51);
            this.MoveCode.TabIndex = 395;
            this.MoveCode.Text = "MoveCode";
            this.MoveCode.UseVisualStyleBackColor = false;
            this.MoveCode.Click += new System.EventHandler(this.MoveCode_Click);
            // 
            // MeasureBtn
            // 
            this.MeasureBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.MeasureBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.MeasureBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MeasureBtn.FlatAppearance.BorderSize = 0;
            this.MeasureBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MeasureBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeasureBtn.ForeColor = System.Drawing.Color.Black;
            this.MeasureBtn.Location = new System.Drawing.Point(570, 715);
            this.MeasureBtn.Name = "MeasureBtn";
            this.MeasureBtn.Size = new System.Drawing.Size(133, 61);
            this.MeasureBtn.TabIndex = 397;
            this.MeasureBtn.Text = "Measure";
            this.MeasureBtn.UseVisualStyleBackColor = false;
            this.MeasureBtn.Click += new System.EventHandler(this.MeasureBtn_Click);
            // 
            // AxisRRdo
            // 
            this.AxisRRdo.AutoSize = true;
            this.AxisRRdo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AxisRRdo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AxisRRdo.Location = new System.Drawing.Point(728, 686);
            this.AxisRRdo.Name = "AxisRRdo";
            this.AxisRRdo.Size = new System.Drawing.Size(91, 23);
            this.AxisRRdo.TabIndex = 2;
            this.AxisRRdo.TabStop = true;
            this.AxisRRdo.Text = "Rotation";
            this.AxisRRdo.UseVisualStyleBackColor = true;
            this.AxisRRdo.CheckedChanged += new System.EventHandler(this.AxisRdo_CheckedChanged);
            // 
            // AxisXRdo
            // 
            this.AxisXRdo.AutoSize = true;
            this.AxisXRdo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AxisXRdo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AxisXRdo.Location = new System.Drawing.Point(568, 686);
            this.AxisXRdo.Name = "AxisXRdo";
            this.AxisXRdo.Size = new System.Drawing.Size(37, 23);
            this.AxisXRdo.TabIndex = 0;
            this.AxisXRdo.TabStop = true;
            this.AxisXRdo.Text = "X";
            this.AxisXRdo.UseVisualStyleBackColor = true;
            this.AxisXRdo.CheckedChanged += new System.EventHandler(this.AxisRdo_CheckedChanged);
            // 
            // AxisYRdo
            // 
            this.AxisYRdo.AutoSize = true;
            this.AxisYRdo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AxisYRdo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AxisYRdo.Location = new System.Drawing.Point(647, 686);
            this.AxisYRdo.Name = "AxisYRdo";
            this.AxisYRdo.Size = new System.Drawing.Size(36, 23);
            this.AxisYRdo.TabIndex = 1;
            this.AxisYRdo.TabStop = true;
            this.AxisYRdo.Text = "Y";
            this.AxisYRdo.UseVisualStyleBackColor = true;
            this.AxisYRdo.CheckedChanged += new System.EventHandler(this.AxisRdo_CheckedChanged);
            // 
            // MeasureAllBtn
            // 
            this.MeasureAllBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.MeasureAllBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.MeasureAllBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MeasureAllBtn.FlatAppearance.BorderSize = 0;
            this.MeasureAllBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MeasureAllBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeasureAllBtn.ForeColor = System.Drawing.Color.Black;
            this.MeasureAllBtn.Location = new System.Drawing.Point(708, 715);
            this.MeasureAllBtn.Name = "MeasureAllBtn";
            this.MeasureAllBtn.Size = new System.Drawing.Size(136, 60);
            this.MeasureAllBtn.TabIndex = 398;
            this.MeasureAllBtn.Text = "MeasureAllSave";
            this.MeasureAllBtn.UseVisualStyleBackColor = false;
            this.MeasureAllBtn.Click += new System.EventHandler(this.MeasureAllBtn_Click);
            // 
            // LoadBmpBtn
            // 
            this.LoadBmpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LoadBmpBtn.BackgroundImage = global::SensorShift.Properties.Resources.Btn2;
            this.LoadBmpBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LoadBmpBtn.FlatAppearance.BorderSize = 0;
            this.LoadBmpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadBmpBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadBmpBtn.ForeColor = System.Drawing.Color.White;
            this.LoadBmpBtn.Location = new System.Drawing.Point(11, 620);
            this.LoadBmpBtn.Name = "LoadBmpBtn";
            this.LoadBmpBtn.Size = new System.Drawing.Size(204, 60);
            this.LoadBmpBtn.TabIndex = 399;
            this.LoadBmpBtn.Text = "Load Bmp";
            this.LoadBmpBtn.UseVisualStyleBackColor = false;
            this.LoadBmpBtn.Click += new System.EventHandler(this.LoadBmpBtn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.BackgroundImage = global::SensorShift.Properties.Resources.Btn1;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(1007, 636);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 53);
            this.button1.TabIndex = 400;
            this.button1.Text = "TestRoll";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // F_Vision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 978);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.LoadBmpBtn);
            this.Controls.Add(this.MeasureAllBtn);
            this.Controls.Add(this.AxisRRdo);
            this.Controls.Add(this.AxisXRdo);
            this.Controls.Add(this.AxisYRdo);
            this.Controls.Add(this.MeasureBtn);
            this.Controls.Add(this.ReadHall);
            this.Controls.Add(this.MoveCode);
            this.Controls.Add(this.ClearLogBtn);
            this.Controls.Add(this.CH2_L);
            this.Controls.Add(this.CH2_R);
            this.Controls.Add(this.CH1_L);
            this.Controls.Add(this.CH1_R);
            this.Controls.Add(this.LiveState);
            this.Controls.Add(this.LoadUnloadAllBtn);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbYTargetCode);
            this.Controls.Add(this.tbXTargetCode);
            this.Controls.Add(this.SavebmpBtn);
            this.Controls.Add(this.RightLEDOnOffBtn);
            this.Controls.Add(this.LeftLEDOnOffBtn);
            this.Controls.Add(this.SelectLED_CH2L);
            this.Controls.Add(this.SelectLED_CH2R);
            this.Controls.Add(this.LoadUnloadCoverBtn);
            this.Controls.Add(this.CurrentLEDPower);
            this.Controls.Add(this.ScaleCalibrationValue);
            this.Controls.Add(this.XYDiffBtn);
            this.Controls.Add(this.YDiffBtn);
            this.Controls.Add(this.XDiffBtn);
            this.Controls.Add(this.CalcCOisBtn);
            this.Controls.Add(this.SetThreshValue);
            this.Controls.Add(this.SetThreshBtn);
            this.Controls.Add(this.SetExposureValue);
            this.Controls.Add(this.SelectLED_CH1L);
            this.Controls.Add(this.SelectLED_CH1R);
            this.Controls.Add(this.SetExposureBtn);
            this.Controls.Add(this.SetLEDBtn);
            this.Controls.Add(this.GaussianBlurBtn);
            this.Controls.Add(this.ScaleCalibrationBtn);
            this.Controls.Add(this.UptoNthMeasureBtn);
            this.Controls.Add(this.WriteDataValue);
            this.Controls.Add(this.WriteAddrValue);
            this.Controls.Add(this.AddrLB2);
            this.Controls.Add(this.DataLB2);
            this.Controls.Add(this.WriteDriverICBtn);
            this.Controls.Add(this.ReadDriverICBtn);
            this.Controls.Add(this.ReadDataValue);
            this.Controls.Add(this.AddrLB);
            this.Controls.Add(this.ReadAddrValue);
            this.Controls.Add(this.DataLB);
            this.Controls.Add(this.LEDDownBtn);
            this.Controls.Add(this.NthMeasureBtn);
            this.Controls.Add(this.LEDUpBtn);
            this.Controls.Add(this.RotationReplayBtn);
            this.Controls.Add(this.AllLEDOnOffBtn);
            this.Controls.Add(this.NthMeasureValue);
            this.Controls.Add(this.OISYReplayBtn);
            this.Controls.Add(this.OISXReplayBtn);
            this.Controls.Add(this.CheckDriverICBtn);
            this.Controls.Add(this.LoadUnloadBtn);
            this.Controls.Add(this.ClearBtn);
            this.Controls.Add(this.GrabBtn);
            this.Controls.Add(this.HaltBtn);
            this.Controls.Add(this.LiveBtn);
            this.Controls.Add(this.FOVRightBtn);
            this.Controls.Add(this.FOVLeftBtn);
            this.Controls.Add(this.FOVDownBtn);
            this.Controls.Add(this.FOVUpBtn);
            this.Controls.Add(this.panelCam0);
            this.Controls.Add(this.TPblank);
            this.Controls.Add(this.VsnLog);
            this.Controls.Add(this.SrcImageBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "F_Vision";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.F_Vision_FormClosing);
            this.Load += new System.EventHandler(this.F_Vision_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SrcImageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox NthMeasureValue;
        private System.Windows.Forms.TextBox VsnLog;
        private System.Windows.Forms.TableLayoutPanel TPblank;
        public System.Windows.Forms.Panel panelCam0;
        private System.Windows.Forms.Label AddrLB;
        private System.Windows.Forms.TextBox WriteDataValue;
        private System.Windows.Forms.Label DataLB;
        private System.Windows.Forms.TextBox WriteAddrValue;
        private System.Windows.Forms.TextBox ReadDataValue;
        private System.Windows.Forms.TextBox ReadAddrValue;
        private System.Windows.Forms.Button NthMeasureBtn;
        private System.Windows.Forms.Button ReadDriverICBtn;
        private System.Windows.Forms.Button WriteDriverICBtn;
        private System.Windows.Forms.Button LEDDownBtn;
        private System.Windows.Forms.Button LEDUpBtn;
        private System.Windows.Forms.Button FOVUpBtn;
        private System.Windows.Forms.Button FOVDownBtn;
        private System.Windows.Forms.Button FOVLeftBtn;
        private System.Windows.Forms.Button FOVRightBtn;
        private System.Windows.Forms.Button LiveBtn;
        private System.Windows.Forms.Button HaltBtn;
        private System.Windows.Forms.Button GrabBtn;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button LoadUnloadBtn;
        private System.Windows.Forms.Button CheckDriverICBtn;
        private System.Windows.Forms.Button AllLEDOnOffBtn;
        private System.Windows.Forms.Button OISXReplayBtn;
        private System.Windows.Forms.Button OISYReplayBtn;
        private System.Windows.Forms.Button RotationReplayBtn;
        private System.Windows.Forms.Label AddrLB2;
        private System.Windows.Forms.Label DataLB2;
        private System.Windows.Forms.Button UptoNthMeasureBtn;
        private System.Windows.Forms.Button ScaleCalibrationBtn;
        private System.Windows.Forms.Button GaussianBlurBtn;
        private System.Windows.Forms.Button SetLEDBtn;
        private System.Windows.Forms.Button SetExposureBtn;
        private System.Windows.Forms.RadioButton SelectLED_CH1L;
        private System.Windows.Forms.RadioButton SelectLED_CH1R;
        private System.Windows.Forms.TextBox SetExposureValue;
        private System.Windows.Forms.Button SetThreshBtn;
        private System.Windows.Forms.TextBox SetThreshValue;
        private System.Windows.Forms.Button CalcCOisBtn;
        private System.Windows.Forms.Button XDiffBtn;
        private System.Windows.Forms.Button YDiffBtn;
        private System.Windows.Forms.Button XYDiffBtn;
        private System.Windows.Forms.TextBox ScaleCalibrationValue;
        private System.Windows.Forms.Label CurrentLEDPower;
        private System.Windows.Forms.Button LoadUnloadCoverBtn;
        private System.Windows.Forms.RadioButton SelectLED_CH2L;
        private System.Windows.Forms.RadioButton SelectLED_CH2R;
        private System.Windows.Forms.Button LeftLEDOnOffBtn;
        private System.Windows.Forms.Button RightLEDOnOffBtn;
        private System.Windows.Forms.Label LiveState;
        private System.Windows.Forms.Button SavebmpBtn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbYTargetCode;
        private System.Windows.Forms.TextBox tbXTargetCode;
        private System.Windows.Forms.Button LoadUnloadAllBtn;
        private System.Windows.Forms.PictureBox SrcImageBox;
        private System.Windows.Forms.Label CH2_L;
        private System.Windows.Forms.Label CH2_R;
        private System.Windows.Forms.Label CH1_L;
        private System.Windows.Forms.Label CH1_R;
        private System.Windows.Forms.Button ClearLogBtn;
        private System.Windows.Forms.Button ReadHall;
        private System.Windows.Forms.Button MoveCode;
        private System.Windows.Forms.Button MeasureBtn;
        private System.Windows.Forms.RadioButton AxisRRdo;
        private System.Windows.Forms.RadioButton AxisXRdo;
        private System.Windows.Forms.RadioButton AxisYRdo;
        private System.Windows.Forms.Button MeasureAllBtn;
        private System.Windows.Forms.Button LoadBmpBtn;
        private System.Windows.Forms.Button button1;
    }
}

