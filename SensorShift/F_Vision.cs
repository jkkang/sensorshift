﻿using ActroLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorShift
{
    public partial class F_Vision : Form
    {
        public Process Process { get { return PROCESS.Process; } }
        public VisionParam VParam { get { return PROCESS.VParam; } }
        public Recipe Rcp { get { return PROCESS.Rcp; } }
        public CurrentLed Leds { get { return PROCESS.CurrentLed; } }
        public CurrentPath CurrentPath { get { return PROCESS.Current; } }
        public SEM1217S DrvIC { get { return PROCESS.DrvIC; } }
        public int m_FocusedLED = 0;
        public int AxisIndex = 0;

        public F_Vision()
        {
            InitializeComponent();
            AxisXRdo.Checked = true;
            //PROCESS.LogEvent.Evented += LogEvent_Evented;
        }

        private void LogEvent_Evented(object sender, LogEvent.Params e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    VsnLog.AppendText(e.Msg + "\r\n");
                });
            }
            else
            {
                VsnLog.AppendText(e.Msg + "\r\n");
            }
        }

        private void F_Vision_Load(object sender, EventArgs e)
        {
            try
            {
                if (Process.IsVirtual) return;
                Process.oCam[0].SelectWindow(panelCam0.Handle);
                DrvIC.UnloadCover(0);
                DrvIC.IsSafeOn = false;
            }
            catch (Exception)
            {
                throw;
            }
         
        }

        #region *Button Event

        private void FOVMoveBtn_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;


            for (int i = 0; i < Process.PortCnt; i++)
            {
                if (Process.oCam[i] != null)
                {
                    Thread.Sleep(150);
                    Process.LEDOn(i);
                    Process.oCam[i].LiveA();
                }
            }
            LiveState.Visible = true;

            switch (bt.TabIndex)
            {
                case 0://Up
                    for (int i = 0; i < Process.PortCnt; i++)
                        if (!Process.SetRoi(VParam.Roi[i].X, VParam.Roi[i].Y += 4)) VParam.Roi[i].Y -= 4;  
                    break;
                case 1://Down
                    for (int i = 0; i < Process.PortCnt; i++)
                        if (!Process.SetRoi(VParam.Roi[i].X, VParam.Roi[i].Y -= 4)) VParam.Roi[i].Y += 4;
                    break;
                case 2://Left
                    for (int i = 0; i < Process.PortCnt; i++)
                        if (!Process.SetRoi(VParam.Roi[i].X += 4, VParam.Roi[i].Y)) VParam.Roi[i].X -= 4;
                    break;
                case 3://Right
                    for (int i = 0; i < Process.PortCnt; i++)
                        if (!Process.SetRoi(VParam.Roi[i].X -= 4, VParam.Roi[i].Y)) VParam.Roi[i].X += 4;
                    break;
            }

            VParam.Save();
            ViewRoiLog();
        }

        public void ViewRoiLog()
        {
            VsnLog.Text = "X " + VParam.Roi[0].X + "-" + (VParam.Roi[0].X + Process.mHROI).ToString() + " Y " + VParam.Roi[0].Y + "-" + (VParam.Roi[0].Y + Process.mVROI).ToString() + "\r\n";
        }


        private void LiveBtn_Click(object sender, EventArgs e)
        {
            for( int i = 0; i < Process.PortCnt; i ++)
            {
                if (Process.oCam[i] != null)
                {
                    Process.LEDOn(i);
                    Process.oCam[i].ClearDisp();
                }
            }
            Task Func1 = new Task(() => LiveReconfigureImage(0));
            Func1.Start();

            LiveState.Visible = true;
        }
        public void LiveReconfigureImage(int ch = 0)
        {
            while (true)
            {
                Process.oCam[0].GrabA();
                SrcImageBox.Image = Process.ShowReconfigImage();
                Thread.Sleep(30);
                if (!LiveState.Visible)
                    break;
            }
        }
        private void HaltBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Process.PortCnt; i++)
            {
                if (Process.oCam[i] != null)
                {
                    Process.LEDOn(i, false);
                    Process.oCam[i].HaltA();
                    Process.oCam[i].ClearDisp();
                    Process.oCam[i].DrawClear();
                }
            }
            SrcImageBox.Image = Process.ShowReconfigImage();
            LiveState.Visible = false;
        }

        private void GrabBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Process.PortCnt; i++)
            {
                Process.LEDOn(i);
            }

            Thread.Sleep(150);

            for (int i = 0; i < Process.PortCnt; i++)
            {
                if (Process.oCam[i] != null) Process.oCam[i].GrabA();
                
            }
            SrcImageBox.Image = Process.ShowReconfigImage();

            LiveState.Visible = false;
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            if(bt.Text.Contains("Clear"))
            {
                for (int i = 0; i < Process.PortCnt; i++)
                    if (Process.oCam[i] != null) Process.oCam[i].DrawClear();

                VsnLog.Clear();
                bt.Text = "Draw";
            }
            else
            {
                for (int i = 0; i < Process.PortCnt; i++)
                    if (Process.oCam[i] != null) Process.oCam[i].DrawDCCross(Brushes.Red);

                bt.Text = "Clear";
            }  
        }

        private void LoadUnloadSocketBtn_Click(object sender, EventArgs e)
        {
            DrvIC.ToggleSocket();
        }

        private void AllLEDOnOffBtn_Click(object sender, EventArgs e)
        {
            if (!Process.m_bAllLEDOn)
            {
                Process.LEDOn(0);
            }
            else
            {
                Process.LEDOn(0, false);
            }
        }
 
        private void SelectLED_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rdo = (RadioButton)sender;

            if (rdo.Checked)
            {
                m_FocusedLED = rdo.TabIndex;
                LEDUpBtn.Text = string.Format("{0} LED BRIGHTER", rdo.Text);
                LEDDownBtn.Text = string.Format("{0} LED DARKER", rdo.Text);

                if (rdo.TabIndex == 2 || rdo.TabIndex == 3)
                {
                    Process.m_bRightLEDOn = true;
                }
                else
                {
                    Process.m_bLeftLEDOn = true;
                }

                CurrentLEDPower.Text = Leds.Val[m_FocusedLED].ToString("F2") + " V";
             
            }
        }
    #endregion

    private void LEDUpBtn_Click(object sender, EventArgs e)
        {
            int ch = m_FocusedLED;
            if (Leds.Val[ch] < 4)
                Leds.Val[ch] += 0.01;
            DrvIC.SetLEDpower(m_FocusedLED, (int)(Leds.Val[ch] * 1240));
            CurrentLEDPower.Text = Leds.Val[ch].ToString("F2") + " V";
        }

        private void LEDDownBtn_Click(object sender, EventArgs e)
        {
            int ch = m_FocusedLED;
            if (Leds.Val[ch] > 0)
                Leds.Val[ch] -= 0.01;

            DrvIC.SetLEDpower(m_FocusedLED, (int)(Leds.Val[ch] * 1240));
            CurrentLEDPower.Text = Leds.Val[ch].ToString("F2") + " V";
        }

        private void SetLEDBtn_Click(object sender, EventArgs e)
        {
            Leds.Save();
            MessageBox.Show("Set LED Voltage Complete.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void CheckDriverICBtn_Click(object sender, EventArgs e)
        {

            for(int i = 0;i<Process.ChannelCount;i++)
            {
                byte[] rFWBuffer = new byte[4];
                byte[] rICBuffer = new byte[4];

                rFWBuffer = DrvIC.ReadAnyByte(i, 0x1008, 4);

                //int value = rFWBuffer[0] + (rFWBuffer[1] << 8) + (rFWBuffer[2] << 16) + (rFWBuffer[3] << 24);  
                VsnLog.Text += "FW Ver " + " : " + rFWBuffer[3].ToString("X") + " " + rFWBuffer[2].ToString("X") + " " + rFWBuffer[1].ToString("X") + " " + rFWBuffer[0].ToString("X") + "\r\n";

                rICBuffer = DrvIC.ReadAnyByte(i, 0x6014, 4);
                VsnLog.Text += "IC Ver " + " : " + rICBuffer[0].ToString("X") + " " + rICBuffer[1].ToString("X") + " " + rICBuffer[2].ToString("X") + " " + rICBuffer[3].ToString("X") + "\r\n";

            }

        }

        private void ReadDriverICBtn_Click(object sender, EventArgs e)
        {

            VsnLog.Clear();

            int rAddbuffer = int.Parse(ReadAddrValue.Text, System.Globalization.NumberStyles.HexNumber);
            byte[] rDatabuffer = new byte[1];

            if (Process.ChannelOn[0])
            {
                rDatabuffer = DrvIC.ReadAnyByte(0, rAddbuffer, 1);

                VsnLog.Text += "CH1 : Read Driver IC" + "\r\n" + " Addr: " + rAddbuffer.ToString("X4") + " Read Data: " + rDatabuffer[0].ToString("X4") + "\r\n";

            }
            if (Process.ChannelOn[1])
            {
                rDatabuffer = DrvIC.ReadAnyByte(1, rAddbuffer, 1);

                VsnLog.Text += "CH2 : Read Driver IC" + "\r\n" + " Addr: " + rAddbuffer.ToString("X4") + " Read Data: " + rDatabuffer[0].ToString("X4") + "\r\n";
            }
        }

        private void WriteDriverICBtn_Click(object sender, EventArgs e)
        {
   
            VsnLog.Clear();

            int wAddbuffer = int.Parse(WriteAddrValue.Text, System.Globalization.NumberStyles.HexNumber);
            byte[] wDatabuffer = new byte[1];

            wDatabuffer[0] = (byte)int.Parse(WriteDataValue.Text, System.Globalization.NumberStyles.HexNumber);

            if (Process.ChannelOn[0])
            {
                DrvIC.WriteAnyByte(0, wAddbuffer, wDatabuffer);

                VsnLog.Text += "CH1 : Write Driver IC" + "\r\n" + " Addr: " + wAddbuffer.ToString("X4") + " Write Data: " + wDatabuffer[0].ToString("X4") + "\r\n";

            }
            if (Process.ChannelOn[1])
            {
                DrvIC.WriteAnyByte(1, wAddbuffer, wDatabuffer);

                VsnLog.Text += "CH2 : Write Driver IC" + "\r\n" + " Addr: " + wAddbuffer.ToString("X4") + " Write Data: " + wDatabuffer[0].ToString("X4") + "\r\n";
            }

        }

        private void ReadAddrValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) ||
            "ABCDEF0123456789abcdef".IndexOf(e.KeyChar) != -1))
            {
                e.Handled = true;
            }
        }

        private void ReadDataValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) ||
            "ABCDEF0123456789abcdef".IndexOf(e.KeyChar) != -1))
            {
                e.Handled = true;
            }
        }

        private void WriteAddrValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) ||
            "ABCDEF0123456789abcdef".IndexOf(e.KeyChar) != -1))
            {
                e.Handled = true;
            }
        }

        private void WriteDataValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) ||
            "ABCDEF0123456789abcdef".IndexOf(e.KeyChar) != -1))
            {
                e.Handled = true;
            }
        }

        private void SetExposureBtn_Click(object sender, EventArgs e)
        {
            int value = int.Parse(SetExposureValue.Text);

            Process.SetExposure(value);
        }

        private void LoadUnloadCoverBtn_Click(object sender, EventArgs e)
        {
            DrvIC.ToggleCover();
        }

        private void F_Vision_FormClosing(object sender, FormClosingEventArgs e)
        {
            Process.LEDOn(0, false);
            DrvIC.UnloadCover(0);
        }

        private void LeftLEDOnOffBtn_Click(object sender, EventArgs e)
        {
            if (!Process.m_bLeftLEDOn)
            {
                DrvIC.SetLEDpower(0, (int)(Leds.Val[0] * 1240));
                DrvIC.SetLEDpower(1, (int)(Leds.Val[1] * 1240));
                Process.m_bLeftLEDOn = true;
            }
            else
            {
                DrvIC.SetLEDpower(0, 0);
                DrvIC.SetLEDpower(1, 0);
                Process.m_bLeftLEDOn = false;
            }
        }

        private void RightLEDOnOffBtn_Click(object sender, EventArgs e)
        {
            if (!Process.m_bRightLEDOn)
            {
                DrvIC.SetLEDpower(2, (int)(Leds.Val[2] * 1240));
                DrvIC.SetLEDpower(3, (int)(Leds.Val[3] * 1240));
                Process.m_bRightLEDOn = true;
            }
            else
            {
                DrvIC.SetLEDpower(2, 0);
                DrvIC.SetLEDpower(3, 0);
                Process.m_bRightLEDOn = false;
            }

        }

        private void Button_MouseMove(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.FlatStyle = FlatStyle.System;
        }

        private void Button_MouseLeave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.FlatStyle = FlatStyle.Flat;
        }
        public int Imageindex = 0;
        private void SavebmpBtn_Click(object sender, EventArgs e)
        {
            string newDir = FIO.BaseDir + "\\Image\\";
            if (!Directory.Exists(newDir))
            {
                Directory.CreateDirectory(newDir);
            }
            string bmpName = newDir + "TestImage" + Imageindex++.ToString() + ".bmp";
            if (Imageindex == 20) Imageindex = 0;
            if (Process.oCam[0] != null)
            {
                Process.oCam[0].SaveImage(bmpName, -1);
            }
            LiveState.Visible = false;
        }
        private void ReadHall_Click(object sender, EventArgs e)
        {
            byte[] tmpbyte = new byte[1] { 0x01 };
            DrvIC.WriteByte(0, 0x0B00, tmpbyte[0]);

            int RdataX = DrvIC.Read4Byte(0, 0x0B18);
            int RdataY = DrvIC.Read4Byte(0, 0x0B16);

            VsnLog.Text = "X Hall : " + (short)RdataX + " YHall : " + (short)RdataY + "\r\n";

        }

        private void MoveCode_Click(object sender, EventArgs e)
        {

            int valueX = int.Parse(tbXTargetCode.Text);
            DrvIC.Move(0, 0, (short)valueX);

            int valueY = int.Parse(tbYTargetCode.Text);
            DrvIC.Move(0, 1, (short)valueY);

        }

        private void LoadUnloadAllBtn_Click(object sender, EventArgs e)
        {
            DrvIC.ToggleAll(DrvIC.IsAllLoad = !DrvIC.IsAllLoad);
        }

        private void ClearLogBtn_Click(object sender, EventArgs e)
        {
            VsnLog.Text = "";
        }

        private async void OISReplayBtn_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;

            for(int i = 0; i< Process.PortCnt;i++)
            {
                Process.oCam[i].ClearDisp();
                Process.oCam[i].DrawClear();
            }

            bt.Enabled = false;

            for (int i = 0; i < Process.PortCnt; i++)
            {
                await Task.Factory.StartNew(() => Process_OISReplay(i, bt.TabIndex));
            }

            bt.Enabled = true;

        }
        private void Process_OISReplay(int port, int axis)
        {
            for (int n = 2; n < Process.CL_DataCount[axis] - 1; n += 2)
            {
                Process.oCam[port].BufCopy2Disp_OIS(n, axis);
                SrcImageBox.Image = Process.ShowReconfigImage(n, axis);
                Thread.Sleep(50);
            }
        }

        private void MeasureBtn_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < Process.PortCnt; i++)
            {
                Process.LEDOn(i);
            }

            Thread.Sleep(150);

            for (int i = 0; i < Process.PortCnt; i++)
            {
                if (Process.oCam[i] != null) Process.oCam[i].GrabA();
            }

            LiveState.Visible = false;

            MeasureLog(0, -1);

            for (int i = 0; i < Process.PortCnt; i++)
            {
                Process.LEDOn(i, false);
            }

        }
        private void MeasureLog(int index, int axis, bool isSave = false)
        {
            double[] resTheta = new double[2];
            double[] cx = new double[4];
            double[] cy = new double[4];
            double[] rescx = new double[2];
            double[] rescy = new double[2];
            Process.SetSearchYRegion();
            Process.MeasureXYT(index, axis, ref cx, ref cy, ref rescx, ref rescy, ref resTheta, isSave);
            SrcImageBox.Image = Process.ShowReconfigImage(index, axis);
            SrcImageBox.Image = Process.DrawBoxesOnTarget(cx, cy);

            double absXL = (cx[3] - cx[1]) * 11 / 1000;
            double absXR = (cx[2] - cx[0]) * 11 / 1000;
            double absYL = (cy[3] - cy[1]) * 11 / 1000;
            double absYR = (cy[2] - cy[0]) * 11 / 1000;
            //Scale 

            absXL = absXL + VParam.XMeasureScale[0];
            absYL = absYL + VParam.YMeasureScale[0];
            absXR = absXR + VParam.XMeasureScale[1];
            absYR = absYR + VParam.YMeasureScale[1];

            double strokeXL = rescy[0];
            double strokeXR = rescy[1];
            double strokeYL = rescx[0];
            double strokeYR = rescx[1];
            double strokeTL = resTheta[0];
            double strokeTR = resTheta[1];

            AddLog(string.Format("Image Index : {0}==========================================", index));
            AddLog(string.Format("x,y[{0:0.00},{1:0.00}], x,y[{2:0.00},{3:0.00}], x,y[{4:0.00},{5:0.00}], x,y[{6:0.00},{7:0.00}],", cx[0], cy[0], cx[1], cy[1], cx[2], cy[2], cx[3], cy[3]));
            AddLog(string.Format("Left avg : X = {0:0.000} um, Y = {1:0.000} um, Th = {2:0.000} um", strokeXL, strokeYL, strokeTL));
            AddLog(string.Format("Right avg : X = {0:0.000} um, Y = {1:0.000} um, Th = {2:0.000} um", strokeXR, strokeYR, strokeTR));
            AddLog(string.Format("Left abs : X = {0:0.000} mm, Y = {1:0.000} mm", absXL, absYL));
            AddLog(string.Format("Right abs : X = {0:0.000} mm, Y = {1:0.000} mm", absXR, absYR));
            AddLog(string.Format("==========================================================="));
        }
        public void AddLog(string log)
        {
            if (InvokeRequired)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    VsnLog.AppendText(log + "\r\n");
                });
            }
            else
            {
                VsnLog.AppendText(log + "\r\n");
            }
        }
        private void NthMeasureBtn_Click(object sender, EventArgs e)
        {
            int index = int.Parse(NthMeasureValue.Text);

            MeasureLog(index, AxisIndex);
        }

        private void AxisRdo_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rdo = (RadioButton)sender;

            if (rdo.Checked)
                AxisIndex = rdo.TabIndex;
        }

        private int MeasureIndex = 0;
        private void UptoNthMeasureBtn_Click(object sender, EventArgs e)
        {
            MeasureIndex = int.Parse(NthMeasureValue.Text);

            MeasureLog(MeasureIndex, AxisIndex);

            MeasureIndex += 1;
            NthMeasureValue.Text = MeasureIndex.ToString();
        }

        private async void MeasureAllBtn_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            bt.Enabled = false;
            await Task.Factory.StartNew(() => MeasureAllSave());
            bt.Enabled = true;

        }
        private void MeasureAllSave()
        {
            for (int i = 0; i < Process.oCam[0].CLFrameCount[AxisIndex]; i++)
            {
                MeasureLog(i, AxisIndex, true);

                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        NthMeasureValue.Text = i.ToString();
                    });
                }
                else
                {
                    NthMeasureValue.Text = i.ToString();
                }

            }
        }

        private void LoadBmpBtn_Click(object sender, EventArgs e)
        {
            string sFilePath = FIO.BaseDir;
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = "bmp";
            openFile.InitialDirectory = sFilePath;

            openFile.Filter = "BMP(*.bmp)|*.bmp";
            if (openFile.ShowDialog() != DialogResult.OK)
                return;
            Process.oCam[0].LoadBMPtoBuf0(openFile.FileName);

            MeasureLog(0, -1);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            bt.Enabled = false;
            await Task.Factory.StartNew(() => TestRoll());
            bt.Enabled = true;
        }
        public void TestRoll()
        {
            short target = 2000;
            short mtarget = -2000;

            for (int j = 0; j < 2; j++)
            {
                DrvIC.HallCalStart(j);
                DrvIC.OIS_On(j, true);
                DrvIC.WriteByte(j, DrvIC.REG_OIS_MODE, DrvIC.FIXED_MODE);
                DrvIC.WriteByte(j, DrvIC.REG_OIS_MODE, DrvIC.FIXED_MODE);
            }


            //DrvIC.Move(0, 0, 4000);
            ////DrvIC.Write2Byte(1, DrvIC.REG_AXIS_X_TARGET, 10000);
            ////DrvIC.Write2Byte(0, DrvIC.REG_AXIS_R_TARGET, target);
            //Thread.Sleep(500);

            for (int i = 0; i < 10; i++)
            {
                for( int j = 0; j < 2; j ++)
                {
                    DrvIC.Move(j, 2, target);
                    //DrvIC.Write2Byte(1, DrvIC.REG_AXIS_X_TARGET, 10000);
                    //DrvIC.Write2Byte(0, DrvIC.REG_AXIS_R_TARGET, target);
                    Thread.Sleep(500);
                    if (InvokeRequired)
                    {
                        BeginInvoke((MethodInvoker)delegate
                        {
                            VsnLog.Text += (DrvIC.ReadHall(j, 0)).ToString();
                            VsnLog.Text += " , ";
                            VsnLog.Text += (DrvIC.ReadHall(j, 1)).ToString();
                            VsnLog.Text += "\r\n";
                        });
                    }
                    else
                    {
                        VsnLog.Text += (DrvIC.ReadHall(j, 0)).ToString();
                        VsnLog.Text += " , ";
                        VsnLog.Text += (DrvIC.ReadHall(j, 1)).ToString();
                        VsnLog.Text += "\r\n";
                    }

                    DrvIC.Move(j, 2, mtarget);
                    //DrvIC.Write2Byte(1, DrvIC.REG_AXIS_X_TARGET, 20000);
                    //DrvIC.Write2Byte(0, DrvIC.REG_AXIS_R_TARGET, mtarget);
                    Thread.Sleep(500);
                    if (InvokeRequired)
                    {
                        BeginInvoke((MethodInvoker)delegate
                        {
                            VsnLog.Text += (DrvIC.ReadHall(j, 0)).ToString();
                            VsnLog.Text += " , ";
                            VsnLog.Text += (DrvIC.ReadHall(j, 1)).ToString();
                            VsnLog.Text += "\r\n";
                        });
                    }
                    else
                    {
                        VsnLog.Text += (DrvIC.ReadHall(j, 0)).ToString();
                        VsnLog.Text += " , ";
                        VsnLog.Text += (DrvIC.ReadHall(j, 1)).ToString();
                        VsnLog.Text += "\r\n";
                    }
                }

            }
        }

        private async void RotationReplayBtn_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;

            for (int i = 0; i < Process.PortCnt; i++)
            {
                Process.oCam[i].ClearDisp();
                Process.oCam[i].DrawClear();
            }

            bt.Enabled = false;

            for (int i = 0; i < Process.PortCnt; i++)
            {
                await Task.Factory.StartNew(() => Process_OISReplay(i, bt.TabIndex));
            }

            bt.Enabled = true;
        }
    }
    
}
