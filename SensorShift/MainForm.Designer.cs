﻿
using ActroLib;

namespace SensorShift
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainTab = new System.Windows.Forms.TabControl();
            this.TLPtitle = new System.Windows.Forms.TableLayoutPanel();
            this.OpratorBtn = new System.Windows.Forms.Button();
            this.AutoLearnBtn = new System.Windows.Forms.Button();
            this.VisionBtn = new System.Windows.Forms.Button();
            this.btnTitle = new System.Windows.Forms.Button();
            this.SaveScreenBtn = new System.Windows.Forms.Button();
            this.AdminBtn = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.TLPtitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTab
            // 
            this.MainTab.AccessibleDescription = "";
            this.MainTab.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.MainTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTab.Location = new System.Drawing.Point(0, 63);
            this.MainTab.Name = "MainTab";
            this.MainTab.SelectedIndex = 0;
            this.MainTab.Size = new System.Drawing.Size(1904, 978);
            this.MainTab.TabIndex = 137;
            // 
            // TLPtitle
            // 
            this.TLPtitle.ColumnCount = 7;
            this.TLPtitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.91892F));
            this.TLPtitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.51351F));
            this.TLPtitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.51351F));
            this.TLPtitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.51351F));
            this.TLPtitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.51351F));
            this.TLPtitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.51351F));
            this.TLPtitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.51351F));
            this.TLPtitle.Controls.Add(this.OpratorBtn, 0, 0);
            this.TLPtitle.Controls.Add(this.AutoLearnBtn, 3, 0);
            this.TLPtitle.Controls.Add(this.VisionBtn, 2, 0);
            this.TLPtitle.Controls.Add(this.btnTitle, 0, 0);
            this.TLPtitle.Controls.Add(this.SaveScreenBtn, 4, 0);
            this.TLPtitle.Controls.Add(this.AdminBtn, 1, 0);
            this.TLPtitle.Controls.Add(this.btnExit, 5, 0);
            this.TLPtitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.TLPtitle.Location = new System.Drawing.Point(0, 0);
            this.TLPtitle.Name = "TLPtitle";
            this.TLPtitle.RowCount = 1;
            this.TLPtitle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TLPtitle.Size = new System.Drawing.Size(1904, 63);
            this.TLPtitle.TabIndex = 138;
            // 
            // OpratorBtn
            // 
            this.OpratorBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(48)))));
            this.OpratorBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg2;
            this.OpratorBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OpratorBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OpratorBtn.FlatAppearance.BorderSize = 0;
            this.OpratorBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpratorBtn.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpratorBtn.ForeColor = System.Drawing.Color.White;
            this.OpratorBtn.Location = new System.Drawing.Point(363, 3);
            this.OpratorBtn.Name = "OpratorBtn";
            this.OpratorBtn.Size = new System.Drawing.Size(251, 57);
            this.OpratorBtn.TabIndex = 0;
            this.OpratorBtn.Text = "Operator Mode";
            this.OpratorBtn.UseVisualStyleBackColor = false;
            this.OpratorBtn.Click += new System.EventHandler(this.FormBtn_Click);
            // 
            // AutoLearnBtn
            // 
            this.AutoLearnBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(48)))));
            this.AutoLearnBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg2;
            this.AutoLearnBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AutoLearnBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AutoLearnBtn.FlatAppearance.BorderSize = 0;
            this.AutoLearnBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AutoLearnBtn.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoLearnBtn.ForeColor = System.Drawing.Color.White;
            this.AutoLearnBtn.Location = new System.Drawing.Point(1134, 3);
            this.AutoLearnBtn.Name = "AutoLearnBtn";
            this.AutoLearnBtn.Size = new System.Drawing.Size(251, 57);
            this.AutoLearnBtn.TabIndex = 3;
            this.AutoLearnBtn.Text = "AutoLean";
            this.AutoLearnBtn.UseVisualStyleBackColor = false;
            this.AutoLearnBtn.Click += new System.EventHandler(this.FormBtn_Click);
            // 
            // VisionBtn
            // 
            this.VisionBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(48)))));
            this.VisionBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg2;
            this.VisionBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.VisionBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VisionBtn.FlatAppearance.BorderSize = 0;
            this.VisionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VisionBtn.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VisionBtn.ForeColor = System.Drawing.Color.White;
            this.VisionBtn.Location = new System.Drawing.Point(877, 3);
            this.VisionBtn.Name = "VisionBtn";
            this.VisionBtn.Size = new System.Drawing.Size(251, 57);
            this.VisionBtn.TabIndex = 2;
            this.VisionBtn.Text = "Vision Mode";
            this.VisionBtn.UseVisualStyleBackColor = false;
            this.VisionBtn.Click += new System.EventHandler(this.FormBtn_Click);
            // 
            // btnTitle
            // 
            this.btnTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(232)))));
            this.btnTitle.BackgroundImage = global::SensorShift.Properties.Resources.Btn4;
            this.btnTitle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTitle.FlatAppearance.BorderSize = 0;
            this.btnTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTitle.Font = new System.Drawing.Font("Arial", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTitle.ForeColor = System.Drawing.Color.Blue;
            this.btnTitle.Location = new System.Drawing.Point(3, 3);
            this.btnTitle.Name = "btnTitle";
            this.btnTitle.Size = new System.Drawing.Size(354, 57);
            this.btnTitle.TabIndex = 139;
            this.btnTitle.Text = "Sensor Shift Tester";
            this.btnTitle.UseVisualStyleBackColor = false;
            this.btnTitle.Click += new System.EventHandler(this.btnTitle_Click);
            // 
            // SaveScreenBtn
            // 
            this.SaveScreenBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(232)))));
            this.SaveScreenBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg0;
            this.SaveScreenBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SaveScreenBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveScreenBtn.FlatAppearance.BorderSize = 0;
            this.SaveScreenBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveScreenBtn.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveScreenBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(48)))));
            this.SaveScreenBtn.Location = new System.Drawing.Point(1391, 3);
            this.SaveScreenBtn.Name = "SaveScreenBtn";
            this.SaveScreenBtn.Size = new System.Drawing.Size(251, 57);
            this.SaveScreenBtn.TabIndex = 143;
            this.SaveScreenBtn.Text = "Save Screen";
            this.SaveScreenBtn.UseVisualStyleBackColor = false;
            this.SaveScreenBtn.Click += new System.EventHandler(this.SaveScreenBtn_Click);
            // 
            // AdminBtn
            // 
            this.AdminBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(48)))));
            this.AdminBtn.BackgroundImage = global::SensorShift.Properties.Resources.BtnBg2;
            this.AdminBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AdminBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdminBtn.FlatAppearance.BorderSize = 0;
            this.AdminBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AdminBtn.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdminBtn.ForeColor = System.Drawing.Color.White;
            this.AdminBtn.Location = new System.Drawing.Point(620, 3);
            this.AdminBtn.Name = "AdminBtn";
            this.AdminBtn.Size = new System.Drawing.Size(251, 57);
            this.AdminBtn.TabIndex = 1;
            this.AdminBtn.Text = "Admin Mode";
            this.AdminBtn.UseVisualStyleBackColor = false;
            this.AdminBtn.Click += new System.EventHandler(this.FormBtn_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(232)))));
            this.btnExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExit.BackgroundImage")));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(48)))));
            this.btnExit.Location = new System.Drawing.Point(1648, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(253, 57);
            this.btnExit.TabIndex = 144;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.MainTab);
            this.Controls.Add(this.TLPtitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sensor Shift Tester";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.TLPtitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel TLPtitle;
        private System.Windows.Forms.Button btnTitle;
        private System.Windows.Forms.Button SaveScreenBtn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button AutoLearnBtn;
        private System.Windows.Forms.Button VisionBtn;
        private System.Windows.Forms.Button AdminBtn;
        private System.Windows.Forms.TabControl MainTab;
        private System.Windows.Forms.Button OpratorBtn;
    }
}

