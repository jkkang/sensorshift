﻿using ActroLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorShift
{
    public partial class MainForm : Form
    {
        public List<Form> FormList = new List<Form>();
        public SEM1217S DrvIC { get { return PROCESS.DrvIC; } }
        public Spec Spec { get { return PROCESS.Spec; } }
        public FAutoLearn.FAutoLearn mFAL = new FAutoLearn.FAutoLearn();

        public MainForm()
        {
            InitializeComponent();
            FormList.Add(new F_Oprator());
            FormList.Add(new F_Admin());
            FormList.Add(new F_Vision());
            //FormList.Add(new FAutoLearn.FAutoLearn());

            for (int i = 0; i < FormList.Count; i++)
            {
                FormList[i].TopLevel = false;
                MainTab.TabPages.Add(new TabPage(FormList[i].Name));
                MainTab.TabPages[i].Controls.Add(FormList[i]);
                FormList[i].Show();
            }

            btnTitle.Text += " " + PROCESS.SWVersion;
        }

        #region *Event
        //*Form 전환 속도 개선
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void FormBtn_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            if(bt.TabIndex == 3)
            {
                mFAL.Show();
                mFAL.WindowState = FormWindowState.Maximized;
                //mFAL.Size = new Size(1920, 1045);
                mFAL.Location = new Point(0, 0);
            }
            else
                MainTab.SelectedIndex = bt.TabIndex;
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnTitle_Click(object sender, EventArgs e)
        {
            MessageBox.Show("SW Version : " + "Ver : " + PROCESS.SWVersion + "\r\n" +
                            "Bulid Data : " + "Ver : " + PROCESS.SWVersion + "\r\n" +
                            "Copyright ⓒ 2022 [ActRo] All rights reserved." + "\r\n", "Sensorshift Tester",
                             MessageBoxButtons.OK, MessageBoxIcon.Information
                );


        }

        private void SaveScreenBtn_Click(object sender, EventArgs e)
        {
            DateTime dtNow = DateTime.Now;

            string pngname = dtNow.ToString("yy-MM-dd-hh-mm-ss") + ".png";
            string sScreenCapturePath = FIO.BaseDir + "\\User_ScreenShot\\" + pngname;
            string sDir = FIO.BaseDir + "\\User_ScreenShot";

            Bitmap memoryImage;
            memoryImage = new Bitmap(1920, 1080);
            Size s = new Size(memoryImage.Width, memoryImage.Height);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);

            if (!Directory.Exists(sDir))
                Directory.CreateDirectory(sDir);

            memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);
            memoryImage.Save(sScreenCapturePath);

            MessageBox.Show("Save Complete.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Spec.Save();
        }


        #endregion
    }
}
