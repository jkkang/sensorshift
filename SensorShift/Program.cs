﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorShift
{
    static class Program
    {
        /// <summary>
        /// 해당 애플리케이션의 주 진입점입니다.
        /// </summary>
        [STAThread]

        static void Main()
        {

            bool isNew;

            Mutex mutex = new Mutex(true, "SensorShift", out isNew);

            if (isNew)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                using (F_Start startForm = new F_Start())
                {
                    DialogResult dr = startForm.ShowDialog();
                    if (dr == DialogResult.No || dr == DialogResult.Cancel)
                    {

                        Application.Exit();
                    }
                    else
                        Application.Run(new MainForm());
                }

                mutex.ReleaseMutex();
                System.Diagnostics.Process.GetCurrentProcess().Kill();

            }
            else
            {
                MessageBox.Show("Still Running Process .....");
                Application.Exit();
            }

        }
    }
}
